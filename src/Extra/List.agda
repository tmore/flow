module Extra.List where

open import Data.List

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Data.Unit using ( ⊤ ; tt)
open import Data.Empty
open import Relation.Nullary using ( ¬_ )

concatMap-id : {A B : Set}
              → {f : A → B}
              → {g : B → List A}
              → (((a : A) → ((g (f a)) ≡ [ a ])))
              → (xs : List A)
              → concat (map g (map f xs)) ≡ xs
concatMap-id _ [] = refl
concatMap-id {_} {_} {f} {g} id-ish (x ∷ xs) =
  begin
    concat (map g (map f (x ∷ xs)))
  ≡⟨⟩
    concat (g (f x) ∷ map g (map f xs))
  ≡⟨ cong (λ z →  concat (z ∷ map g (map f xs))) (id-ish x) ⟩
    x ∷ concat (map g (map f xs))
  ≡⟨ cong (x ∷_) (concatMap-id id-ish xs) ⟩
    (x ∷ xs)
  ∎

Null : {A : Set} → List A → Set
Null [] = ⊤
Null (_ ∷ _) = ⊥


safe-head : {A : Set} → (l : List A) → ¬ (Null l) → A
safe-head [] ¬null = ⊥-elim (¬null tt)
safe-head (x ∷ _) _ = x



open import Data.List.Relation.Unary.All


++-All :
  {A : Set}
  → {P : A → Set}
  → {xs₁ : List A}
  → {xs₂ : List A}
  → All P xs₁
  → All P xs₂
  → All P (xs₁ ++ xs₂)
++-All [] all-rest = all-rest
++-All (p ∷ rest) all-rest =
  p ∷ (++-All rest all-rest)
