module Extra.Sum where

open import Function.Base
open import Data.Sum

open import Agda.Builtin.Equality using (_≡_ ; refl )

map-composes : {A B C D E F : Set}
             → {g₁ : A → B}
             → {f₁ : B → C}
             → {g₂ : D → E}
             → {f₂ : E → F}
             → (e : (A ⊎ D))
             → (Data.Sum.map f₁ f₂ ∘ Data.Sum.map g₁ g₂) e
             ≡ Data.Sum.map (f₁ ∘ g₁) (f₂ ∘ g₂) e
map-composes (inj₁ _) = refl
map-composes (inj₂ _) = refl
