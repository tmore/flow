module Extra.NonEmpty where

open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_ )
import Data.List.NonEmpty as NE
import Data.List
import Data.List.Properties
open import Data.List using ( [] ; _∷_ ; _++_ )

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_ ; refl ; cong)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)



∷-injectiveˡ : {A : Set}
             → {a b : A}
             → {as bs : Data.List.List A}
             → (a NE.∷ as) ≡ (b NE.∷ bs)
             → a ≡ b
∷-injectiveˡ refl = refl



⁺++⁺-assoc :
  {A : Set }
  → (l1 : List⁺ A)
  → (l2 : List⁺ A)
  → (l3 : List⁺ A)
  → ((l1 ⁺++⁺ l2) ⁺++⁺ l3) ≡ (l1 ⁺++⁺ (l2 ⁺++⁺ l3))
⁺++⁺-assoc
  l1@( x NE.∷ xs)
  l2@( y NE.∷ ys)
  l3@( z NE.∷ zs)
  =
  cong (x NE.∷_)  (Data.List.Properties.++-assoc xs (y ∷ ys) (z ∷ zs))



open import Data.List.Relation.Unary.All

⁺++⁺-All :
  {A : Set}
  → {P : A → Set}
  → {xs₁ : List⁺ A}
  → {xs₂ : List⁺ A}
  → All P (NE.toList xs₁)
  → All P (NE.toList xs₂)
  → All P (NE.toList (xs₁ ⁺++⁺ xs₂))
⁺++⁺-All (p ∷ []) all-rest =
   p ∷ all-rest
⁺++⁺-All (p ∷ rest@( p₂ ∷ _)) all-rest =
  p ∷ (⁺++⁺-All rest all-rest)
