{-# OPTIONS --rewriting #-}
open import Agda.Builtin.Equality.Rewrite

import Flow.Core as Flow
import Flow.Semantics
import Flow.Contraction as Flow
import Flow.Contraction.BlockifyProps as Flow
import Flow.Contraction.ContractionProps as Flow

open import Flow.Relation.Unary.Any using (Any ; AnySeq ; here ; inBranch ; inLoop)
import Flow.Relation.Unary.Any as Any

open import Flow.Relation.Unary.NIL-Any using (NILAny ; NILAnySeq ; here ; inBranch)
import Flow.Relation.Unary.NIL-Any as NILAny

open import Flow.Relation.Unary.All using (All ; AllSeq ; leaf ; branch ; loop)
import Flow.Relation.Unary.All as All

open import Data.Empty

import While
import Maps
import Ctx
import LoopStack

open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)

open import Util using ( excluded-middle
                       ; if-guard-subst
                       ; maybe-map-compose
                       ; lift
                       )

open import Function.Base

open import Data.Unit using (⊤) renaming ( tt to unit)

open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary

open import Data.List using (List ; [] ; [_] ; _∷_ ; _++_)
open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)
import Data.List.Properties
open import Data.List.Properties using ( map-++-commute)
import Data.List.NonEmpty as NE

import Data.List.Relation.Unary.All as List using ( All)
import Data.List.Relation.Unary.All as ListAll
import Data.List.Relation.Unary.Any as List using ( Any )
import Data.List.Relation.Unary.Linked as List using ( Linked )

open import Agda.Builtin.Equality using (_≡_ ; refl )
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; _≢_ ; refl; trans; sym; cong; cong₂ ; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)
import Data.Maybe.Properties


open import Data.Integer using (ℤ)
                         renaming (+_ to ℤ+_)

import Extra.Sum

import Expr

module WhileToFlow.CoreProperties
       (MapType : Set → Set)
       (MapInstance : Maps.IntMap MapType)
       where

import WhileToFlow
open WhileToFlow MapType MapInstance



toFlowSeq≡map[toFlowStm] :
  ∀ {A}
  → (xs : List (𝕎.Stm A))
  → toFlowSeq xs ≡ Data.List.map toFlowStm xs
toFlowSeq≡map[toFlowStm] [] = refl
toFlowSeq≡map[toFlowStm] (x ∷ xs) =
  cong (_ ∷_) (toFlowSeq≡map[toFlowStm] xs)

{-# REWRITE toFlowSeq≡map[toFlowStm] #-}

toFlowSeq-++-distr :
  ∀ {A}
  → (seq₁ : List (𝕎.Stm A))
  → (seq₂ : List (𝕎.Stm A))
  → toFlowSeq (seq₁ ++ seq₂)
  ≡ toFlowSeq seq₁ ++ toFlowSeq seq₂
toFlowSeq-++-distr [] _ = refl
toFlowSeq-++-distr seq₁@(x ∷ xs) seq₂ =
  cong (_ ∷_) (toFlowSeq-++-distr xs seq₂)




toFlowStm⇒¬IsBlock :
  ∀ {A}
  → (stm : 𝕎.Stm A)
  → ¬ (Flow.IsBlock (toFlowStm stm))
toFlowStm⇒¬IsBlock (𝕎.assignExpr _ _ _)  = λ ()
toFlowStm⇒¬IsBlock (𝕎.assignCall _ _ _ _)  = λ ()
toFlowStm⇒¬IsBlock (𝕎.call _ _ _)  = λ  ()
toFlowStm⇒¬IsBlock (𝕎.continue _)  = λ  ()
toFlowStm⇒¬IsBlock (𝕎.break _)  = λ ()
toFlowStm⇒¬IsBlock (𝕎.return _ _) = λ  ()
toFlowStm⇒¬IsBlock (𝕎.ifte _ _ _ _) = λ ()
toFlowStm⇒¬IsBlock (𝕎.while _ _ _) = λ ()



toWhileStm∘toFlowStm≡id :
  ∀ {A}
  → (stmt : 𝕎.Stm A)
  → toWhileStm (toFlowStm stmt) ≡ [ stmt ]
toWhileStm∘toFlowStm≡id
  (𝕎.assignExpr _ _ _) = refl
toWhileStm∘toFlowStm≡id
  (𝕎.assignCall _ _ _ _) = refl
toWhileStm∘toFlowStm≡id
  (𝕎.call _ _ _) = refl
toWhileStm∘toFlowStm≡id
  (𝕎.ifte _ _ _ _) = refl
toWhileStm∘toFlowStm≡id
  (𝕎.while _ _ _) = refl
toWhileStm∘toFlowStm≡id
  (𝕎.continue _) = refl
toWhileStm∘toFlowStm≡id
  (𝕎.break _) = refl
toWhileStm∘toFlowStm≡id
  (𝕎.return _ _) = refl


toWhileSeq∘toFlowSeq≡id :
  ∀ {A}
  → (stmts : List (𝕎.Stm A))
  → (toWhileSeq ∘ toFlowSeq) stmts ≡ stmts
toWhileSeq∘toFlowSeq≡id [] =
  refl
toWhileSeq∘toFlowSeq≡id (x ∷ xs) =
  begin
    toWhileSeq (toFlowStm x ∷ toFlowSeq xs)
  ≡⟨ cong₂ (_++_)
        (toWhileStm∘toFlowStm≡id x)
        (toWhileSeq∘toFlowSeq≡id xs) ⟩
    (x ∷ xs)
  ∎

toFlowStm-NewlyTranslated :
  ∀ {A}
  → (stm : 𝕎.Stm A)
  → Flow.NewlyTranslated
      toFlowStm
      (toFlowStm stm)
toFlowStm-NewlyTranslated
  (𝕎.assignExpr _ _ _)  = refl
toFlowStm-NewlyTranslated
  (𝕎.assignCall _ _ _ _) = refl
toFlowStm-NewlyTranslated
  (𝕎.call _ _ _) = refl
toFlowStm-NewlyTranslated
  (𝕎.ifte _ _ _ _) = refl
toFlowStm-NewlyTranslated
  (𝕎.while  _ _ _) = refl
toFlowStm-NewlyTranslated
  (𝕎.continue  _) = refl
toFlowStm-NewlyTranslated
  (𝕎.break _) = refl
toFlowStm-NewlyTranslated
  (𝕎.return _ _) = refl

open import Data.List.Relation.Unary.All using ( _∷_ ; [])

toFlowSeq-NewlyTranslated :
  ∀ {A}
  → (seq : List (𝕎.Stm A))
  → AllSeq (Flow.NewlyTranslated toFlowStm)
           (toFlowSeq seq)
toFlowSeq-NewlyTranslated
  [] = []
toFlowSeq-NewlyTranslated
  (stm@(𝕎.assignExpr _ _ _) ∷ rest) =
  (leaf _ (toFlowStm-NewlyTranslated stm))
    ∷ toFlowSeq-NewlyTranslated rest
toFlowSeq-NewlyTranslated
  (stm@(𝕎.assignCall _ _ _ _) ∷ rest) =
  (leaf _ (toFlowStm-NewlyTranslated stm))
    ∷ toFlowSeq-NewlyTranslated rest
toFlowSeq-NewlyTranslated
  (stm@(𝕎.call _ _ _) ∷ rest) =
  (leaf _ (toFlowStm-NewlyTranslated stm))
    ∷ toFlowSeq-NewlyTranslated rest
toFlowSeq-NewlyTranslated
  (stm@(𝕎.ifte _ _ tc fc) ∷ rest) =
  (branch
    _
    (toFlowStm-NewlyTranslated stm)
    (toFlowSeq-NewlyTranslated tc)
    (toFlowSeq-NewlyTranslated fc)
    )
    ∷ toFlowSeq-NewlyTranslated rest
toFlowSeq-NewlyTranslated
  (stm@(𝕎.while _ _ body) ∷ rest) =
  (loop
    _
    (toFlowStm-NewlyTranslated stm)
    (toFlowSeq-NewlyTranslated body)
    )
    ∷ toFlowSeq-NewlyTranslated rest
toFlowSeq-NewlyTranslated
  (stm@(𝕎.continue _) ∷ rest) =
  (leaf _ (toFlowStm-NewlyTranslated stm))
    ∷ toFlowSeq-NewlyTranslated rest
toFlowSeq-NewlyTranslated
  (stm@(𝕎.break  _) ∷ rest) =
  (leaf _ (toFlowStm-NewlyTranslated stm))
    ∷ toFlowSeq-NewlyTranslated rest
toFlowSeq-NewlyTranslated
  (stm@(𝕎.return _  _) ∷ rest) =
  (leaf _ (toFlowStm-NewlyTranslated stm))
    ∷ toFlowSeq-NewlyTranslated rest



toFlowStm-holds-BlockConstraint :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (stm : (𝕎.Stm A))
  → Flow.BlockConstraint cc toFlowStm (toFlowStm stm)
toFlowStm-holds-BlockConstraint (𝕎.assignExpr _ _ _) = unit
toFlowStm-holds-BlockConstraint (𝕎.assignCall _ _ _ _) = unit
toFlowStm-holds-BlockConstraint (𝕎.call _ _ _) = unit
toFlowStm-holds-BlockConstraint (𝕎.ifte _ _ _ _) = unit
toFlowStm-holds-BlockConstraint (𝕎.while  _ _ _) = unit
toFlowStm-holds-BlockConstraint (𝕎.continue  _) = unit
toFlowStm-holds-BlockConstraint (𝕎.break _) = unit
toFlowStm-holds-BlockConstraint (𝕎.return _ _) = unit

toFlowSeq-holds-BlockConstraint :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (seq : List (𝕎.Stm A))
  → AllSeq (Flow.BlockConstraint cc toFlowStm)
           (toFlowSeq seq)
toFlowSeq-holds-BlockConstraint
  [] = []
toFlowSeq-holds-BlockConstraint
  (stm@(𝕎.assignExpr _ _ _) ∷ rest) =
  (leaf _ unit)
    ∷ toFlowSeq-holds-BlockConstraint rest
toFlowSeq-holds-BlockConstraint
  (stm@(𝕎.assignCall _ _ _ _) ∷ rest) =
  (leaf _ unit)
    ∷ toFlowSeq-holds-BlockConstraint rest
toFlowSeq-holds-BlockConstraint
  (stm@(𝕎.call _ _ _) ∷ rest) =
  (leaf _ unit)
    ∷ toFlowSeq-holds-BlockConstraint rest
toFlowSeq-holds-BlockConstraint
  (stm@(𝕎.ifte _ _ tc fc) ∷ rest) =
  (branch _ unit
    (toFlowSeq-holds-BlockConstraint tc)
    (toFlowSeq-holds-BlockConstraint fc))
    ∷ toFlowSeq-holds-BlockConstraint rest
toFlowSeq-holds-BlockConstraint
  (stm@(𝕎.while _ _ body) ∷ rest) =
  (loop _ unit
    (toFlowSeq-holds-BlockConstraint body))
    ∷ toFlowSeq-holds-BlockConstraint rest
toFlowSeq-holds-BlockConstraint
  (stm@(𝕎.continue _) ∷ rest) =
  (leaf _ unit)
    ∷ toFlowSeq-holds-BlockConstraint rest
toFlowSeq-holds-BlockConstraint
  (stm@(𝕎.break  _) ∷ rest) =
  (leaf _ unit)
    ∷ toFlowSeq-holds-BlockConstraint rest
toFlowSeq-holds-BlockConstraint
  (stm@(𝕎.return _  _) ∷ rest) =
  (leaf _ unit)
    ∷ toFlowSeq-holds-BlockConstraint rest

contract∘blockify∘toFlowSeq-holds-BlockConstraint :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (seq : List (𝕎.Stm A))
  → AllSeq (Flow.BlockConstraint cc toFlowStm)
           (Flow.contract (Flow.blockifySeq cc (toFlowSeq seq)))
contract∘blockify∘toFlowSeq-holds-BlockConstraint
  {cc = cc} seq =
  Flow.contract-keeps-BlockConstraint
    (Flow.blockifySeq cc (toFlowSeq seq))
    (Flow.blockify-keeps-BlockConstraint
      (toFlowSeq seq)
      (ListAll.map (All.map inj₁) (toFlowSeq-NewlyTranslated seq))
      (toFlowSeq-holds-BlockConstraint seq))



translated-annots≡1 :
  ∀ {A}
  → (stm : 𝕎.Stm A)
  → Flow.numAnnots (toFlowStm stm) ≡ 1
translated-annots≡1 (𝕎.assignExpr _ _ _) = refl
translated-annots≡1 (𝕎.assignCall _ _ _ _) = refl
translated-annots≡1 (𝕎.call _ _ _) = refl
translated-annots≡1 (𝕎.ifte _ _ _ _) = refl
translated-annots≡1 (𝕎.while  _ _ _) = refl
translated-annots≡1 (𝕎.continue  _) = refl
translated-annots≡1 (𝕎.break _) = refl
translated-annots≡1 (𝕎.return _ _) = refl

toFlowSeq-Unblockified :
  ∀ {A}
  → (seq : List (𝕎.Stm A))
  → AllSeq (Flow.UnblockifiedStm toFlowStm)
           (toFlowSeq seq)
toFlowSeq-Unblockified [] = List.All.[]
toFlowSeq-Unblockified (stm@(𝕎.assignExpr _ _ _) ∷ rest)  =
  (leaf unit ( translated-annots≡1 stm
                       , toFlowStm-NewlyTranslated stm))
  List.All.∷ toFlowSeq-Unblockified rest
toFlowSeq-Unblockified (stm@(𝕎.assignCall _ _ _ _) ∷ rest) =
  (leaf unit ( translated-annots≡1 stm
                       , toFlowStm-NewlyTranslated stm))
  List.All.∷ toFlowSeq-Unblockified rest
toFlowSeq-Unblockified (stm@(𝕎.call _ _ _) ∷ rest) =
  (leaf unit ( translated-annots≡1 stm
                       , toFlowStm-NewlyTranslated stm ))
  List.All.∷ toFlowSeq-Unblockified rest
toFlowSeq-Unblockified (stm@(𝕎.ifte _ _ tc fc) ∷ rest) =
  branch unit ( translated-annots≡1 stm
                        , toFlowStm-NewlyTranslated stm)
                        (toFlowSeq-Unblockified tc)
                        (toFlowSeq-Unblockified fc)
  List.All.∷ toFlowSeq-Unblockified rest
toFlowSeq-Unblockified (stm@(𝕎.while _ _ body) ∷ rest) =
  loop unit ( translated-annots≡1 stm
                      , toFlowStm-NewlyTranslated stm )
                      (toFlowSeq-Unblockified body)
  List.All.∷ toFlowSeq-Unblockified rest
toFlowSeq-Unblockified (stm@(𝕎.continue  _) ∷ rest) =
  (leaf unit ( translated-annots≡1 stm
                       , toFlowStm-NewlyTranslated stm ))
  List.All.∷ toFlowSeq-Unblockified rest
toFlowSeq-Unblockified (stm@(𝕎.break _) ∷ rest) =
  (leaf unit ( translated-annots≡1 stm
                       , toFlowStm-NewlyTranslated stm ))
  List.All.∷ toFlowSeq-Unblockified rest
toFlowSeq-Unblockified (stm@(𝕎.return _ _) ∷ rest) =
  (leaf unit ( translated-annots≡1 stm
                       , toFlowStm-NewlyTranslated stm ))
  List.All.∷ toFlowSeq-Unblockified rest

-- We need to make sure that the identity propert of unblockify-blockify holds
-- for our translation
unblockifySeq∘blockifySeq≡id-holds :
  ∀ {A}
  → (cc : Flow.ContractionCtx (𝕎.Stm A))
  → (seq : List (𝕎.Stm A))
  → (Flow.unblockifySeq toFlowStm
    ∘ Flow.blockifySeq cc)
    (toFlowSeq seq)
    ≡ (toFlowSeq seq)
unblockifySeq∘blockifySeq≡id-holds cc seq =
  Flow.unblockify∘blockify≡id
    toFlowStm cc (toFlowSeq seq) (toFlowSeq-Unblockified seq)

-- We need to make sure that the identity propert of blockify-unblockify holds
-- for our translation
blockify∘unblockify≡id-holds :
  ∀ {A}
  → (cc : Flow.ContractionCtx (𝕎.Stm A))
  → (seq : List (𝕎.Stm A))
  → (Flow.blockifySeq cc
     ∘ Flow.unblockifySeq toFlowStm)
     (Flow.blockifySeq cc (toFlowSeq seq))
   ≡ (Flow.blockifySeq cc ∘ toFlowSeq) seq
blockify∘unblockify≡id-holds {A} cc seq =
  Flow.blockify∘unblockify≡id
    toFlowStm
    cc
    (Flow.blockifySeq cc (toFlowSeq seq))
    blockify-props
  where
    blockify-props =
      Flow.blockify-holds-NewlyBlockified
        cc toFlowStm
        (toFlowSeq seq)
        (toFlowSeq-Unblockified seq)

-- rewrite version of above
-- blockify∘unblockify≡id-holds-rewrite :
--   ∀ {A}
--   → (cc : Flow.ContractionCtx (𝕎.Stm A))
--   → (seq : List (𝕎.Stm A))
--   → Data.List.map
--      (Flow.blockify-stm cc)
--      (Data.List.map (Flow.unblockify-stm toFlowStm)
--       (Data.List.map (Flow.blockify-stm cc)
--        (Data.List.map toFlowStm seq)))
--     ≡ Data.List.map (Flow.blockify-stm cc) (Data.List.map toFlowStm seq)
-- blockify∘unblockify≡id-holds-rewrite =
--   blockify∘unblockify≡id-holds
-- {-# REWRITE blockify∘unblockify≡id-holds-rewrite #-}

uncontract∘contract≡id-holds :
  ∀ {A}
  → (cc : Flow.ContractionCtx (𝕎.Stm A))
  → (seq : List (𝕎.Stm A))
  → (Flow.uncontract
    ∘ Flow.contract)
    (Flow.blockifySeq cc (toFlowSeq seq))
  ≡ Flow.blockifySeq cc (toFlowSeq seq)
uncontract∘contract≡id-holds {A} cc seq =
  Flow.uncontract∘contract≡id
    (Flow.blockifySeq cc (toFlowSeq seq))
    uncontracted
  where
    translated-holds-props : AllSeq (λ stm →
                                       Flow.numAnnots stm ≡ 1
                                      × Flow.NewlyTranslated
                                          toFlowStm stm
                                    )
                                    (toFlowSeq seq)
    translated-holds-props = toFlowSeq-Unblockified seq
    map' : {stm : Flow.Stm (𝕎.Stm A)}
        → ( Flow.NewlyBlockifiedStm cc toFlowStm stm
          × Flow.numAnnots stm ≡ 1
          × (Flow.blockifyStm
                      cc (toFlowStm
                           (Flow.firstAnnot stm)) ≡ stm ))
        → Flow.numAnnots stm ≡ 1
    map' (_ , p , _) = p
    uncontracted : AllSeq (λ stm → Flow.numAnnots stm ≡ 1)
                          (Flow.blockifySeq cc (toFlowSeq seq))
    uncontracted = All.mapSeq map'
                     (Flow.blockify-holds-NewlyBlockified
                       cc toFlowStm
                       (toFlowSeq seq)
                       translated-holds-props)

contract∘uncontract≡id-holds :
  ∀ {A}
  → (cc : Flow.ContractionCtx (𝕎.Stm A))
  → (seq : List (𝕎.Stm A))
  → (Flow.contract
     ∘ Flow.uncontract)
    (Flow.contract (Flow.blockifySeq cc (toFlowSeq seq)))
  ≡ (Flow.contract (Flow.blockifySeq cc (toFlowSeq seq)))
contract∘uncontract≡id-holds cc seq =
  Flow.contract∘uncontract≡id
    (Flow.contract (Flow.blockifySeq cc (toFlowSeq seq)))
    contracted
  where
    contracted =
      Flow.contract-holds-Contracted
        (Flow.blockifySeq cc (toFlowSeq seq))

toWhile∘toContractedFlow≡id :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (seq : List (𝕎.Stm A))
  → ( toWhile ∘ toContractedFlow cc) seq
  ≡ seq
toWhile∘toContractedFlow≡id {_} {cc} [] = refl
toWhile∘toContractedFlow≡id {_} {cc} seq@(x ∷ xs) =
  begin
    ( toWhileSeq
    ∘ Flow.unblockifySeq toFlowStm
    ∘ Flow.uncontract
    ∘ Flow.contract
    ∘ Flow.blockifySeq cc
    ∘ toFlowSeq)
    seq
  ≡⟨ cong ((Data.List.concat
           ∘ Data.List.map toWhileStm
           ∘ Flow.unblockifySeq toFlowStm))
           (uncontract∘contract≡id-holds cc seq)
     ⟩
    (toWhileSeq
    ∘ Flow.unblockifySeq toFlowStm
    ∘ Flow.blockifySeq cc
    ∘ toFlowSeq)
    seq
  ≡⟨ cong ((Data.List.concat
          ∘ Data.List.map toWhileStm))
          (unblockifySeq∘blockifySeq≡id-holds cc seq)
    ⟩
    ( toWhileSeq
    ∘ toFlowSeq)
    seq
  ≡⟨ toWhileSeq∘toFlowSeq≡id seq ⟩
    seq
  ∎
  where
    flow-seq = Data.List.map toFlowStm seq


++-perserves-toFlowSeq :
  ∀ {A}
  → (init : List (𝕎.Stm A))
  → (tail : List (𝕎.Stm A))
  → toFlowSeq init ++ toFlowSeq tail
    ≡ toFlowSeq (init ++ tail)
++-perserves-toFlowSeq init tail =
  sym (toFlowSeq-++-distr init tail)


Ctx-concatMap_toWhileStm∘toFlowCtx≡id
  : ∀ {A}
  → (ctx : Ctx (𝕎.Stm A))
  → Ctx'.concatMap toWhileStm (toFlowCtx ctx) ≡ ctx
Ctx-concatMap_toWhileStm∘toFlowCtx≡id ctx =
  Ctx'.Properties.concatMap-id toWhileStm∘toFlowStm≡id ctx

toWhile-not-empty :
  ∀ {A}
  → (x : Flow.Stm (𝕎.Stm A))
  → (xs : List (Flow.Stm (𝕎.Stm A)))
  → toWhile (x ∷ xs) ≢ []
toWhile-not-empty (Flow.call _) _ = λ ()
toWhile-not-empty (Flow.assign _) _  = λ ()
toWhile-not-empty (Flow.block _) _ = λ ()
toWhile-not-empty (Flow.return _ _) _ = λ ()
toWhile-not-empty (Flow.continue _) _ = λ ()
toWhile-not-empty (Flow.break _)  _ = λ ()
toWhile-not-empty (Flow.branch _ _ _ _) _ = λ ()
toWhile-not-empty (Flow.loop _ _ _) _ = λ ()

toFlowSeq-not-empty :
  ∀ {A}
  → (x : 𝕎.Stm A)
  → (xs : List (𝕎.Stm A))
  → toFlowSeq (x ∷ xs) ≢ []
toFlowSeq-not-empty (𝕎.assignExpr _ _ _) _ = λ ()
toFlowSeq-not-empty (𝕎.assignCall _ _ _ _) _ = λ ()
toFlowSeq-not-empty (𝕎.call _ _ _) _ = λ ()
toFlowSeq-not-empty (𝕎.continue _) _ = λ ()
toFlowSeq-not-empty (𝕎.break _) _ = λ ()
toFlowSeq-not-empty (𝕎.return _ _) _ = λ ()
toFlowSeq-not-empty (𝕎.while _ _ _) _ = λ ()
toFlowSeq-not-empty (𝕎.ifte _ _ _ _) _ = λ ()

toContractedFlow-not-empty :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (x : 𝕎.Stm A)
  → (xs : List (𝕎.Stm A))
  → toContractedFlow cc (x ∷ xs) ≢ []
toContractedFlow-not-empty {A = A} {cc = cc} w ws
  with toFlowSeq (w ∷ ws) | Eq.inspect toFlowSeq (w ∷ ws)
...| [] | Eq.[ toFlowSeq≡[] ] =
  ⊥-elim (toFlowSeq-not-empty w ws toFlowSeq≡[])
...| (f ∷ fs)  | Eq.[ toFlowSeq≡f∷fs ] with Flow.blockifySeq cc (f ∷ fs) | Eq.inspect (Flow.blockifySeq cc) (f ∷ fs)
...| [] | Eq.[ blockifySeq≡[] ] =
  ⊥-elim (Flow.blockifySeq-not-empty f fs blockifySeq≡[])
...| (f' ∷ fs') | Eq.[ blockifySeq≡f'∷fs' ] =
  let (c , cs , contract≡c∷cs) = Flow.contract-not-null (f' ∷ fs') λ ()
  in λ contract≡[] →
                   absurd c cs (trans (sym contract≡[])
                                      (trans foo contract≡c∷cs))
  where
    foo : toContractedFlow cc (w ∷ ws) ≡ Flow.contract (f' ∷ fs')
    foo =
      begin
        (Flow.contract ∘ Flow.blockifySeq cc ∘ toFlowSeq) (w ∷ ws)
      ≡⟨ cong (Flow.contract ∘ Flow.blockifySeq cc) toFlowSeq≡f∷fs ⟩
        (Flow.contract ∘ Flow.blockifySeq cc) (f ∷ fs)
      ≡⟨ cong Flow.contract blockifySeq≡f'∷fs' ⟩
        Flow.contract (f' ∷ fs')
      ∎
    absurd :
      (x : Flow.Stm (𝕎.Stm A))
      → (xs : List (Flow.Stm (𝕎.Stm A)))
      → [] ≡ (x ∷ xs) → ⊥
    absurd x xs ()
