{-# OPTIONS --rewriting #-}
open import Agda.Builtin.Equality.Rewrite

import Flow.Core as Flow
import Flow.Semantics
import Flow.Contraction as Flow
import Flow.Contraction.BlockifyProps as Flow
import Flow.Contraction.ContractionProps as Flow

open import Flow.Relation.Unary.Any using (Any ; AnySeq ; here ; inBranch ; inLoop)
import Flow.Relation.Unary.Any as Any

open import Flow.Relation.Unary.NIL-Any using (NILAny ; NILAnySeq ; here ; inBranch)
import Flow.Relation.Unary.NIL-Any as NILAny

open import Flow.Relation.Unary.All using (All ; AllSeq ; leaf ; branch ; loop)
import Flow.Relation.Unary.All as All

import While
import Maps
import Ctx
import LoopStack

open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)

open import Util using ( excluded-middle
                       ; if-guard-subst
                       ; maybe-map-compose
                       ; lift
                       ; productMapDistr
                       )

open import Function.Base

open import Data.Unit using (⊤) renaming ( tt to unit)

open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary

open import Data.Empty

open import Data.List using (List ; [] ; [_] ; _∷_ ; _++_)
open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)
import Data.List.Properties
open import Data.List.Properties using ( map-++-commute)
import Data.List.NonEmpty as NE

import Data.List.Relation.Unary.All as List using ( All)
import Data.List.Relation.Unary.Any as List using ( Any )
import Data.List.Relation.Unary.Linked as List using ( Linked )

open import Agda.Builtin.Equality using (_≡_ ; refl )
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong₂ ; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
import Data.Sum.Properties

open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)
import Data.Maybe.Properties


open import Data.Integer using (ℤ)
                         renaming (+_ to ℤ+_)

import Extra.Sum

import Expr

module WhileToFlow.ContractedProperties
       (MapType : Set → Set)
       (MapInstance : Maps.IntMap MapType)
       where

import WhileToFlow
open WhileToFlow MapType MapInstance
import WhileToFlow.CoreProperties
open WhileToFlow.CoreProperties MapType MapInstance

-- Rewriting rules to simplify some proofs
{- REWRITE toFlowSeq≡map[toFlowStm] #-}
{-# REWRITE Flow.blockifySeq≡map[blockifyStm] #-}
--

toWhileResult¹∘toContractedFlowCtx≡id :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (ctx : Ctx (𝕎.Stm A))
  → Ctx'.mapSeq (toWhile ∘ toContractedFlow cc) ctx
  ≡ ctx
toWhileResult¹∘toContractedFlowCtx≡id  ctx =
  Ctx'.Properties.mapSeq[id]≡id toWhile∘toContractedFlow≡id ctx


toWhileResult²∘toContractedFlow≡id :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (res₂ : (List (𝕎.Stm A) ⊎ ℤ))
  → Data.Sum.map (toWhile ∘ toContractedFlow cc) id res₂
  ≡ res₂
toWhileResult²∘toContractedFlow≡id
  (inj₂ _) = refl
toWhileResult²∘toContractedFlow≡id
  (inj₁ code) =
  cong inj₁ (toWhile∘toContractedFlow≡id code)


toWhileResult∘toContractedFlowResult≡id :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (res : Result (𝕎.Stm A))
  → (toWhileResult ∘ toContractedFlowResult cc) res
  ≡ res
toWhileResult∘toContractedFlowResult≡id {_} {cc} ( ctx , res) =
  begin
    (toWhileResult ∘ toContractedFlowResult cc) ( ctx , res)
  ≡⟨ cong₂ (_,_)
            (Ctx'.Properties.mapSeq-composes ctx)
            (Extra.Sum.map-composes res)
    ⟩
    ( Ctx'.mapSeq (toWhile ∘ (toContractedFlow cc)) ctx
    , (Data.Sum.map
        (toWhile ∘ (toContractedFlow cc))
        (id ∘ id)
        res
      )
    )
  ≡⟨ cong₂ (_,_)
            (toWhileResult¹∘toContractedFlowCtx≡id ctx)
            (toWhileResult²∘toContractedFlow≡id res)
    ⟩
    ( ctx , res)
  ∎
ctx-toWhile∘toContractedFlowCtx≡id :
  ∀ {A}
  → {cc : Flow.ContractionCtx (𝕎.Stm A)}
  → (ctx : Ctx (𝕎.Stm A))
  → Ctx'.mapSeq toWhile (toContractedFlowCtx cc ctx)
  ≡ ctx
ctx-toWhile∘toContractedFlowCtx≡id
  {cc = cc} ctx =
  begin
    (Ctx'.mapSeq toWhile ∘ toContractedFlowCtx cc) ctx
  ≡⟨ Ctx'.Properties.mapSeq-composes ctx ⟩
    Ctx'.mapSeq (toWhile ∘ (toContractedFlow cc)) ctx
  ≡⟨ toWhileResult¹∘toContractedFlowCtx≡id ctx ⟩
    ctx
  ∎

simplify-ifte-blockified-case :
  ∀ {A}
  → (cc : Flow.ContractionCtx (𝕎.Stm A))
  → (ctx : Ctx (𝕎.Stm A))
  → (body : List (𝕎.Stm A))
  → (rest : List (𝕎.Stm A))
  → (Flow.contract ∘ Flow.blockifySeq cc)
      ((toFlowSeq body) ++ toContractedFlow cc rest)
   ≡ toContractedFlow cc (body ++ rest)
simplify-ifte-blockified-case cc ctx body rest =
  begin
    (Flow.contract ∘ Flow.blockifySeq cc)
      ((toFlowSeq body) ++ toContractedFlow cc rest)
  ≡⟨ cong Flow.contract
          (Flow.blockifySeq-++-distr
            (toFlowSeq body)
            (toContractedFlow cc rest))
    ⟩
    Flow.contract
      ((Flow.blockifySeq cc ∘ toFlowSeq) body
        ++ Flow.blockifySeq cc (toContractedFlow cc rest))
  ≡⟨ cong (λ z →
             Flow.contract
               ((Flow.blockifySeq cc ∘ toFlowSeq) body
               ++ z))
          (Flow.blockifySeq∘contract∘blockifySeq≡contract∘blockifySeq
             (toFlowSeq rest))
    ⟩
    Flow.contract
      ((Flow.blockifySeq cc ∘ toFlowSeq) body
        ++ (toContractedFlow cc rest))
  ≡⟨ (sym (Flow.contract-++-cancel
            ((Flow.blockifySeq cc ∘ toFlowSeq) body)
            ((toContractedFlow cc rest))
           ))
    ⟩
    Flow.contract
      ((Flow.contract ∘ Flow.blockifySeq cc ∘ toFlowSeq) body
        ++ Flow.contract (toContractedFlow cc rest))
  ≡⟨ cong (λ z →
            Flow.contract
              ((Flow.contract ∘ Flow.blockifySeq cc ∘ toFlowSeq) body
                ++ z))
            (Flow.contract∘contract≡contract
              (Flow.blockifySeq cc (toFlowSeq rest)))
     ⟩
    Flow.contract
      (toContractedFlow cc body
        ++ (toContractedFlow cc rest))
  ≡⟨ Flow.contract-++-cancel
       ((Flow.blockifySeq cc ∘ toFlowSeq) body)
       ((Flow.blockifySeq cc ∘ toFlowSeq) rest)

     ⟩
    (Flow.contract
      ((Flow.blockifySeq cc ∘ toFlowSeq) body
        ++ (Flow.blockifySeq cc ∘ toFlowSeq) rest))
  ≡⟨ cong Flow.contract
           (sym (Flow.blockifySeq-++-distr
                  (toFlowSeq body)
                  (toFlowSeq rest)))
    ⟩
    ((Flow.contract ∘ Flow.blockifySeq cc)
               (toFlowSeq body
                ++ toFlowSeq rest))
  ≡⟨ cong (Flow.contract ∘ Flow.blockifySeq cc)
           (sym (toFlowSeq-++-distr body rest))
    ⟩
   (toContractedFlow cc) (body ++ rest)
  ∎




eval𝔹[ctx]≡eval𝔹[flowCtx] :
  ∀ {A}
  → (cc : Flow.ContractionCtx (𝕎.Stm A))
  → (b : Expr.𝔹.𝔹)
  → (ctx : Ctx (𝕎.Stm A))
  → Ctx'.eval𝔹 b ctx
  ≡ Ctx'.eval𝔹 b (toContractedFlowCtx cc ctx)
eval𝔹[ctx]≡eval𝔹[flowCtx] _ _ _ = refl

eval𝔹[ctx]⇒eval𝔹[flowCtx] :
  ∀ {A}
  → (cc : Flow.ContractionCtx (𝕎.Stm A))
  → (b : Expr.𝔹.𝔹)
  → (ctx : Ctx (𝕎.Stm A))
  → Ctx'.eval𝔹 b ctx ≡ true
  → Ctx'.eval𝔹 b (toContractedFlowCtx cc ctx) ≡ true
eval𝔹[ctx]⇒eval𝔹[flowCtx] cc b ctx p =
  trans (sym (eval𝔹[ctx]≡eval𝔹[flowCtx] cc b ctx)) p

-- Special equality for the if then else cases
simplify-ifte-case :
  ∀ {A}
  → (ctx : Ctx (𝕎.Stm A))
  → (cc : Flow.ContractionCtx (𝕎.Stm A))
  → (body : List (𝕎.Stm A))
  → (rest : List (𝕎.Stm A))
  → (Flow.contract ∘ Flow.blockifySeq cc)
      (toContractedFlow cc body ++ toContractedFlow cc rest)
    ≡ (toContractedFlow cc) (body ++ rest)
simplify-ifte-case ctx cc body rest =
  begin
    (Flow.contract ∘ Flow.blockifySeq cc)
      (toContractedFlow cc body ++ toContractedFlow cc rest)
  ≡⟨ cong Flow.contract
          (Flow.blockifySeq-++-distr
            (toContractedFlow cc body)
            (toContractedFlow cc rest))
    ⟩
    Flow.contract
      ((Flow.blockifySeq cc ∘ toContractedFlow cc) body
        ++ (Flow.blockifySeq cc ∘ toContractedFlow cc) rest)
  ≡⟨ cong₂ (λ z₁ z₂ → (Flow.contract (z₁ ++ z₂)))
           (Flow.blockifySeq∘contract∘blockifySeq≡contract∘blockifySeq
             (toFlowSeq body))
           (Flow.blockifySeq∘contract∘blockifySeq≡contract∘blockifySeq
             (toFlowSeq rest))
    ⟩
    (Flow.contract
      ((toContractedFlow cc) body
        ++ (toContractedFlow cc) rest))
  ≡⟨ Flow.contract-++-cancel
       ((Flow.blockifySeq cc ∘ toFlowSeq) body)
       ((Flow.blockifySeq cc ∘ toFlowSeq) rest)

     ⟩
    (Flow.contract
      ((Flow.blockifySeq cc ∘ toFlowSeq) body
        ++ (Flow.blockifySeq cc ∘ toFlowSeq) rest))
  ≡⟨ cong Flow.contract
           (sym (Flow.blockifySeq-++-distr
                  (toFlowSeq body)
                  (toFlowSeq rest)))
    ⟩
    ((Flow.contract ∘ Flow.blockifySeq cc)
               (toFlowSeq body
                ++ toFlowSeq rest))
  ≡⟨ cong (Flow.contract ∘ Flow.blockifySeq cc)
           (sym (toFlowSeq-++-distr body rest))
    ⟩
   (toContractedFlow cc) (body ++ rest)
  ∎

contracted-semanticStep-equals :
  {A : Set}
  → (ctx : Ctx (𝕎.Stm A))
  → (cc  : Flow.ContractionCtx (𝕎.Stm A))
  → (seq : List (𝕎.Stm A))
  → toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (toContractedFlow cc seq))
  ≡ 𝕎.semanticStep ctx seq
contracted-semanticStep-equals ctx cc [] =
  toWhileResult∘toContractedFlowResult≡id _
contracted-semanticStep-equals ctx cc seq@(e@(𝕎.return annot expr) ∷ rest) =
  toWhileResult∘toContractedFlowResult≡id _
contracted-semanticStep-equals
  ctx cc seq@(stm@(𝕎.continue _) ∷ rest)
  with Ctx'.Properties.popElim ctx
... | inj₁ (p , pop≡just) =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (toContractedFlow cc seq))
  ≡⟨⟩
    (after-pop-abrev ∘ popContinue-abrev)
      (Ctx'.pop (toContractedFlowCtx cc ctx))
  ≡⟨ cong (after-pop-abrev ∘ popContinue-abrev)
           (Ctx'.Properties.pop-mapSeq-commutes-ish
             ctx
             (toContractedFlow cc))
    ⟩
    (after-pop-abrev ∘ popContinue-abrev)
      (Data.Maybe.map
        (Data.Product.map (Ctx'.mapSeq (toContractedFlow cc))
                          (LoopStack.Node.mapSeq (toContractedFlow cc)))
        (Ctx'.pop ctx))
  ≡⟨ cong after-pop-abrev
          (maybe-map-compose (Ctx'.pop ctx) _ _)
    ⟩
    (after-pop-abrev)
      (Data.Maybe.map
        (Data.Product.map _
                          _)
        (Ctx'.pop ctx))
  ≡⟨ cong toWhileResult
          (Data.Maybe.Properties.maybe′-map
            _ _ _ (Ctx'.pop ctx))
    ⟩
     toWhileResult
      (maybe
        ((Data.Product.map _
                           _))
        nothing-case
        (Ctx'.pop ctx))
  ≡⟨ cong (λ z → (toWhileResult (maybe just-case-abriv nothing-case z)))
          pop≡just
    ⟩
    (toWhileResult ∘ toContractedFlowResult cc)
        ( (Ctx'.dropFrame ctx)
        , (inj₁ (LoopStack.Node.Node.continue p))
        )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    ( (Ctx'.dropFrame ctx)
    , (inj₁ (LoopStack.Node.Node.continue p))
    )
  ≡⟨⟩
    Data.Product.map₂ inj₁
                      ( (Ctx'.dropFrame ctx)
                      , (LoopStack.Node.Node.continue p)
                      )
  ≡⟨ cong ((maybe (Data.Product.map₂ inj₁)
                 (𝕎.semanticStep ctx rest))
           ∘ (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)))
           (sym pop≡just)
   ⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
   contract-func = (Flow.contract ∘ Flow.blockifySeq cc)
   just-case-abriv =
     ((Data.Product.map (Ctx'.mapSeq (toContractedFlow cc))
                        (inj₁ ∘ LoopStack.Node.Node.continue
                        ∘ LoopStack.Node.mapSeq (toContractedFlow cc))))
   nothing-case =
     (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (Flow.contract
         (Data.List.map (Flow.blockifyStm cc)
          (Data.List.map toFlowStm rest))))

   popContinue-abrev =
     Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)

   after-pop-abrev =
     toWhileResult ∘
      (maybe (Data.Product.map₂ inj₁)
       nothing-case)
... | inj₂ pop≡nothing =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (toContractedFlow cc seq))
  ≡⟨⟩
    (after-pop-abrev ∘ popContinue-abrev)
      (Ctx'.pop (toContractedFlowCtx cc ctx))
  ≡⟨ cong (after-pop-abrev ∘ popContinue-abrev)
           (Ctx'.Properties.pop-mapSeq-commutes-ish
             ctx
             (toContractedFlow cc))
    ⟩
    (after-pop-abrev ∘ popContinue-abrev)
      (Data.Maybe.map
        (Data.Product.map (Ctx'.mapSeq (toContractedFlow cc))
                          (LoopStack.Node.mapSeq (toContractedFlow cc)))
        (Ctx'.pop ctx))
  ≡⟨ cong after-pop-abrev
          (maybe-map-compose (Ctx'.pop ctx) _ _)
    ⟩
    (after-pop-abrev)
      (Data.Maybe.map
        (Data.Product.map _
                          _)
        (Ctx'.pop ctx))
  ≡⟨ cong toWhileResult
          (Data.Maybe.Properties.maybe′-map
            _ _ _ (Ctx'.pop ctx))
    ⟩
     toWhileResult
      (maybe
        ((Data.Product.map _
                           _))
        nothing-case
        (Ctx'.pop ctx))
  ≡⟨ cong (λ z → (toWhileResult (maybe just-case-abriv nothing-case z)))
          pop≡nothing
    ⟩
    toWhileResult
     (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (Flow.contract
         (Data.List.map (Flow.blockifyStm cc)
          (Data.List.map toFlowStm rest))))
  ≡⟨ contracted-semanticStep-equals ctx cc rest ⟩
    (𝕎.semanticStep ctx rest)
  ≡⟨ cong (λ z →
          maybe (Data.Product.map₂ inj₁)
                (𝕎.semanticStep ctx  rest)
                (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue) z)
          )
          (sym pop≡nothing)
     ⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
   contract-func = (Flow.contract ∘ Flow.blockifySeq cc)
   just-case-abriv =
     ((Data.Product.map (Ctx'.mapSeq (toContractedFlow cc))
                        (inj₁ ∘ LoopStack.Node.Node.continue
                        ∘ LoopStack.Node.mapSeq (toContractedFlow cc))))
   nothing-case =
     (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (Flow.contract
         (Data.List.map (Flow.blockifyStm cc)
          (Data.List.map toFlowStm rest))))

   popContinue-abrev =
     Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)

   after-pop-abrev =
     toWhileResult ∘
      (maybe (Data.Product.map₂ inj₁)
       nothing-case)
contracted-semanticStep-equals
  ctx cc seq@(stm@(𝕎.break _) ∷ rest)
  with Ctx'.Properties.popElim ctx
... | inj₁ (p , pop≡just) =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (toContractedFlow cc seq))
  ≡⟨⟩
    (after-pop-abrev ∘ popBreak-abrev)
      (Ctx'.pop (toContractedFlowCtx cc ctx))
  ≡⟨ cong (after-pop-abrev ∘ popBreak-abrev)
           (Ctx'.Properties.pop-mapSeq-commutes-ish
             ctx
             (toContractedFlow cc))
    ⟩
    (after-pop-abrev ∘ popBreak-abrev)
      (Data.Maybe.map
        (Data.Product.map (Ctx'.mapSeq (toContractedFlow cc))
                          (LoopStack.Node.mapSeq (toContractedFlow cc)))
        (Ctx'.pop ctx))
  ≡⟨ cong after-pop-abrev
          (maybe-map-compose (Ctx'.pop ctx) _ _)
    ⟩
    (after-pop-abrev)
      (Data.Maybe.map
        (Data.Product.map _
                          _)
        (Ctx'.pop ctx))
  ≡⟨ cong toWhileResult
          (Data.Maybe.Properties.maybe′-map
            _ _ _ (Ctx'.pop ctx))
    ⟩
     toWhileResult
      (maybe
        ((Data.Product.map _
                           _))
        nothing-case
        (Ctx'.pop ctx))
  ≡⟨ cong (λ z → (toWhileResult (maybe just-case-abriv nothing-case z)))
          pop≡just
    ⟩
    (toWhileResult ∘ toContractedFlowResult cc)
        ( (Ctx'.dropFrame ctx)
        , (inj₁ (LoopStack.Node.Node.break p))
        )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    ( (Ctx'.dropFrame ctx)
    , (inj₁ (LoopStack.Node.Node.break p))
    )
  ≡⟨⟩
    Data.Product.map₂ inj₁
                      ( (Ctx'.dropFrame ctx)
                      , (LoopStack.Node.Node.break p)
                      )
  ≡⟨ cong ((maybe (Data.Product.map₂ inj₁)
                 (𝕎.semanticStep ctx rest))
           ∘ (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)))
           (sym pop≡just)
   ⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
   contract-func = (Flow.contract ∘ Flow.blockifySeq cc)
   just-case-abriv =
     ((Data.Product.map (Ctx'.mapSeq (toContractedFlow cc))
                        (inj₁ ∘ LoopStack.Node.Node.break
                        ∘ LoopStack.Node.mapSeq (toContractedFlow cc))))
   nothing-case =
     (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (Flow.contract
         (Data.List.map (Flow.blockifyStm cc)
          (Data.List.map toFlowStm rest))))

   popBreak-abrev =
     Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)

   after-pop-abrev =
     toWhileResult ∘
      (maybe (Data.Product.map₂ inj₁)
       nothing-case)
... | inj₂ pop≡nothing =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (toContractedFlow cc seq))
  ≡⟨⟩
    (after-pop-abrev ∘ popBreak-abrev)
      (Ctx'.pop (toContractedFlowCtx cc ctx))
  ≡⟨ cong (after-pop-abrev ∘ popBreak-abrev)
           (Ctx'.Properties.pop-mapSeq-commutes-ish
             ctx
             (toContractedFlow cc))
    ⟩
    (after-pop-abrev ∘ popBreak-abrev)
      (Data.Maybe.map
        (Data.Product.map (Ctx'.mapSeq (toContractedFlow cc))
                          (LoopStack.Node.mapSeq (toContractedFlow cc)))
        (Ctx'.pop ctx))
  ≡⟨ cong after-pop-abrev
          (maybe-map-compose (Ctx'.pop ctx) _ _)
    ⟩
    (after-pop-abrev)
      (Data.Maybe.map
        (Data.Product.map _
                          _)
        (Ctx'.pop ctx))
  ≡⟨ cong toWhileResult
          (Data.Maybe.Properties.maybe′-map
            _ _ _ (Ctx'.pop ctx))
    ⟩
     toWhileResult
      (maybe
        ((Data.Product.map _
                           _))
        nothing-case
        (Ctx'.pop ctx))
  ≡⟨ cong (λ z → (toWhileResult (maybe just-case-abriv nothing-case z)))
          pop≡nothing
    ⟩
    toWhileResult
     (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (Flow.contract
         (Data.List.map (Flow.blockifyStm cc)
          (Data.List.map toFlowStm rest))))
  ≡⟨ contracted-semanticStep-equals ctx cc rest ⟩
    (𝕎.semanticStep ctx rest)
  ≡⟨ cong (λ z →
          maybe (Data.Product.map₂ inj₁)
                (𝕎.semanticStep ctx  rest)
                (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break) z)
          )
          (sym pop≡nothing)
     ⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
   contract-func = (Flow.contract ∘ Flow.blockifySeq cc)
   just-case-abriv =
     ((Data.Product.map (Ctx'.mapSeq (toContractedFlow cc))
                        (inj₁ ∘ LoopStack.Node.Node.break
                        ∘ LoopStack.Node.mapSeq (toContractedFlow cc))))
   nothing-case =
     (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (Flow.contract
         (Data.List.map (Flow.blockifyStm cc)
          (Data.List.map toFlowStm rest))))

   popBreak-abrev =
     Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)

   after-pop-abrev =
     toWhileResult ∘
      (maybe (Data.Product.map₂ inj₁)
       nothing-case)
contracted-semanticStep-equals
  ctx cc seq@(stm@(𝕎.call annot procIdx args) ∷ rest)
  with (Flow.interestingNode cc (toFlowStm stm))
... | no since =
  begin
    toWhileResult
    (Flow'.semanticStep
      (contractedSubsemantic cc)
      (toContractedFlowCtx cc ctx)
      flow-seq)
  ≡⟨ cong toWhileResult
           (Flow'.Properties.sematicStep-on-contractHead
             {cc  = cc}
             (toFlowStm⇒¬IsBlock stm)
             blockified-seq
             refl
           )
    ⟩
    toWhileResult
    (Flow'.Properties.semanicStep-block-case
      (contractedSubsemantic cc)
      (toContractedFlowCtx cc ctx)
      (Flow.call stm)
      (toContractedFlow cc rest))
  ≡⟨⟩
    toWhileResult
    ( Ctx'.mapLoopStackSeqs
           (Flow.contract ∘ Flow.blockifySeq cc)
           (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep
                 (Ctx'.clearLoopStack
                   (Ctx'.mapSeq
                     toWhile
                     (toContractedFlowCtx cc ctx)))
                  stm))
             (toContractedFlowCtx cc ctx))
       , ( inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
                   (toContractedFlow cc rest)))
     )
  ≡⟨ cong₂ (λ z₁ z₂  →
              toWhileResult
                ( Ctx'.mapLoopStackSeqs
                  (Flow.contract ∘ Flow.blockifySeq cc)
                  (Ctx'.setVarCtx
                    (Ctx'.variables
                      (whileStep
                        (Ctx'.clearLoopStack
                          z₁)
                         stm))
                    (toContractedFlowCtx cc ctx))
                , ( inj₁ z₂)
                ))
             (ctx-toWhile∘toContractedFlowCtx≡id ctx)
             (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq (toFlowSeq rest))
    ⟩
    ( (Ctx'.mapSeq toWhile
           (Ctx'.mapLoopStackSeqs
           (Flow.contract ∘ Flow.blockifySeq cc)
           (toContractedFlowCtx cc
             (Ctx'.setVarCtx
               (Ctx'.variables
                 (whileStep
                   (Ctx'.clearLoopStack ctx)
                   stm))
               ctx)
             )))
       , inj₁ ((toWhile ∘ (toContractedFlow cc)) rest)
     )
  ≡⟨ cong₂ (λ z₁ z₂  →
              (Ctx'.mapSeq toWhile
                z₁
              , inj₁ z₂
              ))
            (Ctx'.mapLoopStackSeqs∘mapSeq-cancel
              _
              (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq ∘ toFlowSeq))
            (toWhile∘toContractedFlow≡id rest)
    ⟩
    ((Ctx'.mapSeq toWhile
           (toContractedFlowCtx cc
             (Ctx'.setVarCtx
               (Ctx'.variables
                 (whileStep
                   (Ctx'.clearLoopStack ctx)
                   stm))
               ctx)
             ))
     , inj₁ rest
     )
  ≡⟨ cong (λ z →
             ( z
             , inj₁ rest))
             (ctx-toWhile∘toContractedFlowCtx≡id _)
     ⟩
       ((Ctx'.setVarCtx
         (Ctx'.variables
           (whileStep
             (Ctx'.clearLoopStack ctx)
           stm))
         ctx)
     , inj₁ rest
     )
  ≡⟨⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
    blockified-seq =
      (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    flow-seq = (Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest))
... | yes since =
  begin
    toWhileResult
      ( Flow'.runSubSem
          (contractedSubsemantic cc)
          (toContractedFlowCtx cc ctx)
          stm
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨⟩
    toWhileResult
      ( (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep
                 (Ctx'.clearLoopStack
                   (Ctx'.mapSeq
                     toWhile
                     (toContractedFlowCtx cc ctx)))
                  stm))
             (toContractedFlowCtx cc ctx))
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨ cong (λ z →
             toWhileResult
               ((Ctx'.setVarCtx
                 (Ctx'.variables
                   (whileStep
                     (Ctx'.clearLoopStack
                       z)
                   stm))
                 (toContractedFlowCtx cc ctx))
               , inj₁ (toContractedFlow cc rest)
               ))
              (ctx-toWhile∘toContractedFlowCtx≡id ctx)
    ⟩
    toWhileResult
      ((toContractedFlowCtx cc)
        (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep (Ctx'.clearLoopStack ctx) stm)) ctx)
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
    blockified-seq =
      (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    flow-seq =
      Flow.contractHead
        (Flow.block NE.[ stm ])
        (toContractedFlow cc rest)
contracted-semanticStep-equals
  ctx cc seq@(stm@(𝕎.assignCall annot var procIdx args) ∷ rest)
  with (Flow.interestingNode cc (toFlowStm stm))
... | no since =
  begin
    toWhileResult
     (Flow'.semanticStep
       (contractedSubsemantic cc)
       (toContractedFlowCtx cc ctx)
       flow-seq)
  ≡⟨ cong toWhileResult
           (Flow'.Properties.sematicStep-on-contractHead
             {cc  = cc}
             (toFlowStm⇒¬IsBlock stm)
             blockified-seq
             refl
           )
    ⟩
    toWhileResult
    (Flow'.Properties.semanicStep-block-case
      (contractedSubsemantic cc)
      (toContractedFlowCtx cc ctx)
      (Flow.call stm)
      (toContractedFlow cc rest))
  ≡⟨⟩
    toWhileResult
    ( Ctx'.mapLoopStackSeqs
           (Flow.contract ∘ Flow.blockifySeq cc)
           (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep
                 (Ctx'.clearLoopStack
                   (Ctx'.mapSeq
                     toWhile
                     (toContractedFlowCtx cc ctx)))
                  stm))
             (toContractedFlowCtx cc ctx))
       , ( inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
                   (toContractedFlow cc rest)))
     )
  ≡⟨ cong₂ (λ z₁ z₂  →
              toWhileResult
                ( Ctx'.mapLoopStackSeqs
                  (Flow.contract ∘ Flow.blockifySeq cc)
                  (Ctx'.setVarCtx
                    (Ctx'.variables
                      (whileStep
                        (Ctx'.clearLoopStack
                          z₁)
                         stm))
                    (toContractedFlowCtx cc ctx))
                , ( inj₁ z₂)
                ))
             (ctx-toWhile∘toContractedFlowCtx≡id ctx)
             (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq (toFlowSeq rest))
    ⟩
    ( (Ctx'.mapSeq toWhile
           (Ctx'.mapLoopStackSeqs
           (Flow.contract ∘ Flow.blockifySeq cc)
           (toContractedFlowCtx cc
             (Ctx'.setVarCtx
               (Ctx'.variables
                 (whileStep
                   (Ctx'.clearLoopStack ctx)
                   stm))
               ctx)
             )))
       , inj₁ ((toWhile ∘ (toContractedFlow cc)) rest)
     )
  ≡⟨ cong₂ (λ z₁ z₂  →
              (Ctx'.mapSeq toWhile
                z₁
              , inj₁ z₂
              ))
            (Ctx'.mapLoopStackSeqs∘mapSeq-cancel
              _
              (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq ∘ toFlowSeq))
            (toWhile∘toContractedFlow≡id rest)
    ⟩
    ((Ctx'.mapSeq toWhile
           (toContractedFlowCtx cc
             (Ctx'.setVarCtx
               (Ctx'.variables
                 (whileStep
                   (Ctx'.clearLoopStack ctx)
                   stm))
               ctx)
             ))
     , inj₁ rest
     )
  ≡⟨ cong (λ z →
             ( z
             , inj₁ rest))
             (ctx-toWhile∘toContractedFlowCtx≡id _)
     ⟩
       ((Ctx'.setVarCtx
         (Ctx'.variables
           (whileStep
             (Ctx'.clearLoopStack ctx)
           stm))
         ctx)
     , inj₁ rest
     )
  ≡⟨⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
    blockified-seq =
      (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    flow-seq = (Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest))
... | yes since =
  begin
    toWhileResult
      ( Flow'.runSubSem
          (contractedSubsemantic cc)
          (toContractedFlowCtx cc ctx)
          stm
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨⟩
    toWhileResult
      ( (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep
                 (Ctx'.clearLoopStack
                   (Ctx'.mapSeq
                     toWhile
                     (toContractedFlowCtx cc ctx)))
                  stm))
             (toContractedFlowCtx cc ctx))
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨ cong (λ z →
             toWhileResult
               ((Ctx'.setVarCtx
                 (Ctx'.variables
                   (whileStep
                     (Ctx'.clearLoopStack
                       z)
                   stm))
                 (toContractedFlowCtx cc ctx))
               , inj₁ (toContractedFlow cc rest)
               ))
              (ctx-toWhile∘toContractedFlowCtx≡id ctx)
    ⟩
    toWhileResult
      ((toContractedFlowCtx cc)
        (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep (Ctx'.clearLoopStack ctx) stm)) ctx)
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
    blockified-seq =
      (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    flow-seq = (Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest))
contracted-semanticStep-equals
  ctx cc seq@(stm@(𝕎.assignExpr annot var expr) ∷ rest)
  with (Flow.interestingNode cc (toFlowStm stm))
... | no since =
  begin
    toWhileResult
    (Flow'.semanticStep
      (contractedSubsemantic cc)
      (toContractedFlowCtx cc ctx)
      flow-seq)
  ≡⟨ cong toWhileResult
           (Flow'.Properties.sematicStep-on-contractHead
             {cc  = cc}
             (toFlowStm⇒¬IsBlock stm)
             blockified-seq
             refl
           )
    ⟩
    toWhileResult
    (Flow'.Properties.semanicStep-block-case
      (contractedSubsemantic cc)
      (toContractedFlowCtx cc ctx)
      (Flow.assign stm)
      (toContractedFlow cc rest))
  ≡⟨⟩
    toWhileResult
    ( Ctx'.mapLoopStackSeqs
           (Flow.contract ∘ Flow.blockifySeq cc)
           (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep
                 (Ctx'.clearLoopStack
                   (Ctx'.mapSeq
                     toWhile
                     (toContractedFlowCtx cc ctx)))
                  stm))
             (toContractedFlowCtx cc ctx))
       , ( inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
                   (toContractedFlow cc rest)))
     )
  ≡⟨ cong₂ (λ z₁ z₂  →
              toWhileResult
                ( Ctx'.mapLoopStackSeqs
                  (Flow.contract ∘ Flow.blockifySeq cc)
                  (Ctx'.setVarCtx
                    (Ctx'.variables
                      (whileStep
                        (Ctx'.clearLoopStack
                          z₁)
                         stm))
                    (toContractedFlowCtx cc ctx))
                , ( inj₁ z₂)
                ))
             (ctx-toWhile∘toContractedFlowCtx≡id {cc = cc} ctx)
             (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq (toFlowSeq rest))
    ⟩
    ( (Ctx'.mapSeq toWhile
           (Ctx'.mapLoopStackSeqs
           (Flow.contract ∘ Flow.blockifySeq cc)
           (toContractedFlowCtx cc
             (Ctx'.setVarCtx
               (Ctx'.variables
                 (whileStep
                   (Ctx'.clearLoopStack ctx)
                   stm))
               ctx)
             )))
       , inj₁ ((toWhile ∘ (toContractedFlow cc)) rest)
     )
  ≡⟨ cong₂ (λ z₁ z₂  →
              (Ctx'.mapSeq toWhile
                z₁
              , inj₁ z₂
              ))
            (Ctx'.mapLoopStackSeqs∘mapSeq-cancel
              _
              (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq ∘ toFlowSeq))
            (toWhile∘toContractedFlow≡id rest)
    ⟩
    ((Ctx'.mapSeq toWhile
           (toContractedFlowCtx cc
             (Ctx'.setVarCtx
               (Ctx'.variables
                 (whileStep
                   (Ctx'.clearLoopStack ctx)
                   stm))
               ctx)
             ))
     , inj₁ rest
     )
  ≡⟨ cong (λ z →
             ( z
             , inj₁ rest))
             (ctx-toWhile∘toContractedFlowCtx≡id _)
     ⟩
       ((Ctx'.setVarCtx
         (Ctx'.variables
           (whileStep
             (Ctx'.clearLoopStack ctx)
           stm))
         ctx)
     , inj₁ rest
     )
  ≡⟨⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
    blockified-seq =
      (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    flow-seq = (Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest))
... | yes since =
  begin
    toWhileResult
      ( Flow'.runSubSem
          (contractedSubsemantic cc)
          (toContractedFlowCtx cc ctx)
          stm
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨⟩
    toWhileResult
      ( (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep
                 (Ctx'.clearLoopStack
                   (Ctx'.mapSeq
                     toWhile
                     (toContractedFlowCtx cc ctx)))
                  stm))
             (toContractedFlowCtx cc ctx))
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨ cong (λ z →
             toWhileResult
               ((Ctx'.setVarCtx
                 (Ctx'.variables
                   (whileStep
                     (Ctx'.clearLoopStack
                       z)
                   stm))
                 (toContractedFlowCtx cc ctx))
               , inj₁ (toContractedFlow cc rest)
               ))
              (ctx-toWhile∘toContractedFlowCtx≡id {cc = cc} ctx)
    ⟩
    toWhileResult
      ((toContractedFlowCtx cc)
        (Ctx'.setVarCtx
             (Ctx'.variables
               (whileStep (Ctx'.clearLoopStack ctx) stm)) ctx)
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    (𝕎.semanticStep ctx seq)
  ∎
  where
    blockified-seq =
      (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    flow-seq = (Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest))
contracted-semanticStep-equals
  ctx cc seq@(stm@(𝕎.while annot guard body) ∷ rest)
  with Flow.interesting cc (toFlowStm stm) | Eq.inspect (Flow.interesting cc) (toFlowStm stm) | Ctx'.eval𝔹 guard ctx | Eq.inspect (Ctx'.eval𝔹 guard) ctx
...| yes _ | _ | true | Eq.[ eval𝔹≡true ] =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        flow-seq)
  ≡⟨ cong toWhileResult
          (if-guard-subst eval𝔹-contractd≡true refl)
    ⟩
    toWhileResult
      ( Ctx'.pushStack
        flow-seq
        (toContractedFlow cc rest)
        (toContractedFlowCtx cc ctx)
      , inj₁ (toContractedFlow cc body)
      )
  ≡⟨⟩
      ( Ctx'.mapSeq toWhile
        (Ctx'.pushStack
        flow-seq
        (toContractedFlow cc rest)
        (toContractedFlowCtx cc ctx))
      , inj₁ (toWhile (toContractedFlow cc body))
      )
  ≡⟨⟩
      ( (Ctx'.pushStack
        (toWhile flow-seq)
        (toWhile (toContractedFlow cc rest))
        (Ctx'.mapSeq toWhile (toContractedFlowCtx cc ctx)))
      , inj₁ (toWhile (toContractedFlow cc body))
      )
  ≡⟨ cong₂ (λ z₁ z₂ →
              ((Ctx'.pushStack
                (toWhile flow-seq)
                (toWhile (toContractedFlow cc rest))
                z₁)
              , inj₁ z₂))
           (ctx-toWhile∘toContractedFlowCtx≡id ctx)
           (toWhile∘toContractedFlow≡id body)
    ⟩
    ((Ctx'.pushStack
        (stm ∷ (toWhile (toContractedFlow cc rest)))
        (toWhile (toContractedFlow cc rest))
        ctx)
     , inj₁ body
     )
  ≡⟨ cong₂ (λ z₁ z₂ →
              ((Ctx'.pushStack
                (stm ∷ z₁)
                z₂
                ctx)
              , inj₁ body
              ))
              (toWhile∘toContractedFlow≡id rest)
              (toWhile∘toContractedFlow≡id rest)
    ⟩
    (Ctx'.pushStack (𝕎.while annot guard body ∷ rest) rest ctx
    , inj₁ body)
  ∎
  where
    eval𝔹-contractd≡true : Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx) ≡ true
    eval𝔹-contractd≡true = eval𝔹[ctx]⇒eval𝔹[flowCtx] cc guard ctx eval𝔹≡true
    contracted-loop = Flow.loop stm guard (toContractedFlow cc body)
    flow-seq = contracted-loop ∷ (toContractedFlow cc rest)

...| yes _  | _ | false | Eq.[ eval𝔹≡false ] =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        flow-seq)
  ≡⟨ cong toWhileResult
          (if-guard-subst eval𝔹-contractd≡false refl)
    ⟩
    toWhileResult
      ( (toContractedFlowCtx cc ctx)
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨⟩
    (toWhileResult ∘ toContractedFlowResult cc)
      ( ctx
      , inj₁ rest
      )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    ( ctx
    , inj₁ rest
    )
  ∎
  where
    eval𝔹-contractd≡false : Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx) ≡ false
    eval𝔹-contractd≡false = sym (trans (sym eval𝔹≡false) (eval𝔹[ctx]≡eval𝔹[flowCtx] cc guard ctx))
    contracted-loop = Flow.loop stm guard (toContractedFlow cc body)
    flow-seq = contracted-loop ∷ (toContractedFlow cc rest)
...| no _ | Eq.[ interesting≡no ] |  true | Eq.[ eval𝔹≡true ] =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        flow-seq)
  ≡⟨ cong toWhileResult
           (Flow'.Properties.sematicStep-on-contractHead
             {cc = cc}
             (toFlowStm⇒¬IsBlock stm)
             blockified-seq
             refl)
    ⟩
    toWhileResult
      (let (ctx' , e-res) =
                 (if Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx)
                 then ( Ctx'.pushStack
                        ((toFlowStm stm) ∷ (toContractedFlow cc rest))
                        (toContractedFlow cc rest)
                        (toContractedFlowCtx cc ctx)
                      , inj₁ (toFlowSeq body))
                 else ((toContractedFlowCtx cc ctx)
                      , inj₁ (toContractedFlow cc rest)))
       in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
          , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
          ))
  ≡⟨ cong (λ z →
             toWhileResult
               (let (ctx' , e-res) =
                          (if z
                          then ( Ctx'.pushStack
                                 ((toFlowStm stm) ∷ (toContractedFlow cc rest))
                                 (toContractedFlow cc rest)
                                 (toContractedFlowCtx cc ctx)
                               , inj₁ (toFlowSeq body))
                          else ((toContractedFlowCtx cc ctx)
                               , inj₁ (toContractedFlow cc rest)))
                in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
                   , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
                   ))
                   )
             eval𝔹-contractd≡true
    ⟩
    toWhileResult
      (Ctx'.mapLoopStackSeqs
         (Flow.contract ∘ Flow.blockifySeq cc)
           (Ctx'.pushStack
             ((toFlowStm stm) ∷ (toContractedFlow cc rest))
             (toContractedFlow cc rest)
             (toContractedFlowCtx cc ctx))
      , inj₁ (toContractedFlow cc body)
      )
  ≡⟨⟩
    toWhileResult
      (Ctx'.pushStack

        (Flow.contract ((Flow.blockifyStm cc ∘ toFlowStm) stm ∷ (Flow.blockifySeq cc ∘ toContractedFlow cc) rest))
        ((Flow.contract ∘ Flow.blockifySeq cc ∘ toContractedFlow cc) rest)
        (Ctx'.mapLoopStackSeqs
          (Flow.contract ∘ Flow.blockifySeq cc)
          (toContractedFlowCtx cc ctx))
      , inj₁ (toContractedFlow cc body)
      )
  ≡⟨ cong₂ (λ z₁ z₂ →
              toWhileResult
                (Ctx'.pushStack

                  (Flow.contract ((Flow.blockifyStm cc ∘ toFlowStm) stm
                    ∷ (Flow.blockifySeq cc ∘ toContractedFlow cc) rest))
                  z₁
                  z₂
                , inj₁ (toContractedFlow cc body)
                )
              )
              (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq (toFlowSeq rest))
              (Ctx'.mapLoopStackSeqs∘mapSeq-cancel
                _
                (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq ∘ toFlowSeq))
    ⟩
    toWhileResult
      (Ctx'.pushStack
        (Flow.contract ((Flow.blockifyStm cc ∘ toFlowStm) stm
          ∷ (Flow.blockifySeq cc ∘ toContractedFlow cc) rest))
        (toContractedFlow cc rest)
        (toContractedFlowCtx cc ctx)
      , inj₁ (toContractedFlow cc body)
      )
  ≡⟨ cong (λ z →
            toWhileResult
              (Ctx'.pushStack
                (Flow.contract ((Flow.blockifyStm cc ∘ toFlowStm) stm
                  ∷ z))
                (toContractedFlow cc rest)
                (toContractedFlowCtx cc ctx)
              , inj₁ (toContractedFlow cc body)
              )
           )
           (Flow.blockifySeq∘contract∘blockifySeq≡contract∘blockifySeq
             (toFlowSeq rest)
           )
    ⟩
    toWhileResult
      (Ctx'.pushStack
        (Flow.contract ((Flow.blockifyStm cc ∘ toFlowStm) stm
          ∷ toContractedFlow cc rest))
        (toContractedFlow cc rest)
        (toContractedFlowCtx cc ctx)
      , inj₁ (toContractedFlow cc body)
      )
  ≡⟨⟩
      (Ctx'.pushStack
        (toWhile (Flow.contract ((Flow.blockifyStm cc ∘ toFlowStm) stm
          ∷ toContractedFlow cc rest)))
        ((toWhile ∘ toContractedFlow cc) rest)
        (Ctx'.mapSeq toWhile (toContractedFlowCtx cc ctx))
      , inj₁ ((toWhile ∘ toContractedFlow cc) body)
      )
  ≡⟨ cong₂ (λ z₁ z₂ →
               (Ctx'.pushStack
                 (toWhile (Flow.contract ((Flow.blockifyStm cc ∘ toFlowStm) stm
                   ∷ toContractedFlow cc rest)))
                 z₁
                 z₂
               , inj₁ ((toWhile ∘ toContractedFlow cc) body)
               ))
            (toWhile∘toContractedFlow≡id rest)
            (trans (Ctx'.Properties.mapSeq-composes ctx)
                   (toWhileResult¹∘toContractedFlowCtx≡id ctx))

    ⟩
      (Ctx'.pushStack
        (toWhile (Flow.contract ((Flow.blockifyStm cc ∘ toFlowStm) stm
          ∷ toContractedFlow cc rest)))
        rest
        ctx
      , inj₁ ((toWhile ∘ toContractedFlow cc) body)
      )
  ≡⟨ cong₂ (λ z₁ z₂ →
               (Ctx'.pushStack
                 (toWhile (Flow.contract (z₁
                   ∷ toContractedFlow cc rest)))
                 rest
                 ctx
               , inj₁ z₂
               ))
            blockify≡block
            (toWhile∘toContractedFlow≡id body)

    ⟩
      (Ctx'.pushStack
        (toWhile (Flow.contractHead (Flow.block NE.[ stm ]) (Flow.contract (toContractedFlow cc rest))))
        rest
        ctx
      , inj₁ body
      )
  ≡⟨ cong (λ z →
             (Ctx'.pushStack
                 (toWhile (Flow.contractHead (Flow.block NE.[ stm ]) z))
                 rest
                 ctx
               , inj₁ body
               ))
            (Flow.contract∘contract≡contract (Flow.blockifySeq cc (toFlowSeq rest)))
    ⟩
      (Ctx'.pushStack
        (toWhile (Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest)))
        rest
        ctx
      , inj₁ body
      )
  ≡⟨⟩
      (Ctx'.pushStack
        (toWhile (Flow.contract ((Flow.block NE.[ stm ]) ∷  (Flow.blockifySeq cc ∘ toFlowSeq) rest)))
        rest
        ctx
      , inj₁ body
      )
  ≡⟨ cong (λ z →
             (Ctx'.pushStack
                 (toWhile (Flow.contract (z ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest)))
                 rest
                 ctx
               , inj₁ body
               ))
            (sym blockify≡block)
    ⟩
      (Ctx'.pushStack
        ((toWhile ∘ toContractedFlow cc) (stm ∷ rest))
        rest
        ctx
      , inj₁ body
      )
  ≡⟨ cong (λ z →
             (Ctx'.pushStack
               z
               rest
               ctx
             , inj₁ body
             ))
           (toWhile∘toContractedFlow≡id seq)
    ⟩
    (Ctx'.pushStack (𝕎.while annot guard body ∷ rest) rest ctx
    , inj₁ body)
  ∎
  where
    blockified-seq =
        (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    eval𝔹-contractd≡true : Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx) ≡ true
    eval𝔹-contractd≡true = eval𝔹[ctx]⇒eval𝔹[flowCtx] cc guard ctx eval𝔹≡true
    contracted-loop = Flow.loop stm guard (toContractedFlow cc body)
    flow-seq = Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest)
    blockify≡block : Flow.blockifyStm cc (toFlowStm stm) ≡ Flow.block NE.[ stm ]
    blockify≡block = if-guard-subst (cong isYes interesting≡no) refl

...| no _ | Eq.[ interesting≡no ] | false | Eq.[ eval𝔹≡false ] =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        flow-seq)
  ≡⟨ cong toWhileResult
           (Flow'.Properties.sematicStep-on-contractHead
             {cc = cc}
             (toFlowStm⇒¬IsBlock stm)
             blockified-seq
             refl)
    ⟩
    toWhileResult
      (let (ctx' , e-res) =
                 (if Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx)
                 then ( Ctx'.pushStack
                        ((toFlowStm stm) ∷ (toContractedFlow cc rest))
                        (toContractedFlow cc rest)
                        (toContractedFlowCtx cc ctx)
                      , inj₁ (toFlowSeq body))
                 else ((toContractedFlowCtx cc ctx)
                      , inj₁ (toContractedFlow cc rest)))
       in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
          , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
          ))
  ≡⟨ cong (λ z →
             toWhileResult
               (let (ctx' , e-res) =
                          (if z
                          then ( Ctx'.pushStack
                                 ((toFlowStm stm) ∷ (toContractedFlow cc rest))
                                 (toContractedFlow cc rest)
                                 (toContractedFlowCtx cc ctx)
                               , inj₁ (toFlowSeq body))
                          else ((toContractedFlowCtx cc ctx)
                               , inj₁ (toContractedFlow cc rest)))
                in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
                   , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
                   ))
                   )
             (sym (trans (sym eval𝔹≡false)
                         (eval𝔹[ctx]≡eval𝔹[flowCtx] cc guard ctx)))
    ⟩
    toWhileResult
      ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) (toContractedFlowCtx cc ctx)
      , inj₁ ((Flow.contract ∘ Flow.blockifySeq cc ∘ toContractedFlow cc) rest)
      )
  ≡⟨ cong₂ (λ z₁ z₂ →
              toWhileResult
                ( z₁
                , inj₁ z₂
                )
            )
            (Ctx'.mapLoopStackSeqs∘mapSeq-cancel
                _
                (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq ∘ toFlowSeq))
             (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq  (toFlowSeq rest))
    ⟩
    toWhileResult
      ( toContractedFlowCtx cc ctx
      , inj₁ (toContractedFlow cc rest)
      )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    ( ctx , inj₁ rest)
  ∎
  where
    blockified-seq =
        (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    flow-seq = Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest)
contracted-semanticStep-equals
  ctx cc seq@(stm@(𝕎.ifte annot guard tc fc) ∷ rest)
  with Flow.interesting cc (toFlowStm stm) | Eq.inspect (Flow.interesting cc) (toFlowStm stm) | Ctx'.eval𝔹 guard ctx | Eq.inspect (Ctx'.eval𝔹 guard) ctx
...| yes _ | _ | true | Eq.[ eval𝔹≡true ] =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        flow-seq)
  ≡⟨ cong toWhileResult
          (if-guard-subst eval𝔹-contractd≡true refl)
    ⟩
    toWhileResult
      ( toContractedFlowCtx cc ctx
      , inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
               (toContractedFlow cc tc ++ toContractedFlow cc rest))
      )
   ≡⟨ cong (λ  z → toWhileResult
              (toContractedFlowCtx cc ctx
              , inj₁ z))
              (simplify-ifte-case ctx cc tc rest)
     ⟩
    (toWhileResult ∘ toContractedFlowResult cc)
      ( ctx
      , inj₁ (tc ++ rest)
      )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    ( ctx
    , inj₁ (tc ++ rest))
  ∎
  where
    eval𝔹-contractd≡true : Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx) ≡ true
    eval𝔹-contractd≡true = eval𝔹[ctx]⇒eval𝔹[flowCtx] cc guard ctx eval𝔹≡true
    contracted-branch =
      Flow.branch
        stm guard
        (toContractedFlow cc tc)
        (toContractedFlow cc fc)
    flow-seq = contracted-branch ∷ (toContractedFlow cc rest)

...| yes _  | _ | false | Eq.[ eval𝔹≡false ] =
  begin
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        flow-seq)
  ≡⟨ cong toWhileResult
          (if-guard-subst eval𝔹-contractd≡false refl)
    ⟩
    toWhileResult
      ( toContractedFlowCtx cc ctx
      , inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
               (toContractedFlow cc fc ++ toContractedFlow cc rest))
      )
  ≡⟨ cong (λ  z → toWhileResult
              (toContractedFlowCtx cc ctx
              , inj₁ z))
              (simplify-ifte-case ctx cc fc rest)
    ⟩
    (toWhileResult ∘ toContractedFlowResult cc)
      ( ctx
      , inj₁ (fc ++ rest)
      )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    ( ctx
    , inj₁ (fc ++ rest))
  ∎
  where
    eval𝔹-contractd≡false : Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx) ≡ false
    eval𝔹-contractd≡false = sym (trans (sym eval𝔹≡false) (eval𝔹[ctx]≡eval𝔹[flowCtx] cc guard ctx))
    contracted-branch =
      Flow.branch
        stm guard
        (toContractedFlow cc tc)
        (toContractedFlow cc fc)
    flow-seq = contracted-branch ∷ (toContractedFlow cc rest)
...| no _  | _ | false | Eq.[ eval𝔹≡false ] =
  begin
      toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        contracted-flow-seq)
  ≡⟨ cong toWhileResult
           (Flow'.Properties.sematicStep-on-contractHead
             {cc = cc}
             (toFlowStm⇒¬IsBlock stm)
             blockified-seq
             refl)
    ⟩
    toWhileResult
      (Flow'.Properties.semanicStep-block-case
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (Flow.branch stm guard (toFlowSeq tc) (toFlowSeq fc))
        (toContractedFlow cc rest)
      )
  ≡⟨⟩
    toWhileResult
      (let (ctx' , e-res) =
                 (if Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx)
                 then ( toContractedFlowCtx cc ctx
                      , inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
                             ((toFlowSeq tc) ++ toContractedFlow cc rest))
                      )
                 else ( toContractedFlowCtx cc ctx
                      , inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
                        ((toFlowSeq fc) ++ toContractedFlow cc rest))
                      ))
       in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
          , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
          ))
  ≡⟨ cong₂ (λ z₁ z₂ →
            toWhileResult
              (let (ctx' , e-res) =
                         (if Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx)
                         then ( toContractedFlowCtx cc ctx
                              , inj₁ z₁
                              )
                         else ( toContractedFlowCtx cc ctx
                              , inj₁ z₂
                              ))
               in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
                  , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
                  )))
             (simplify-ifte-blockified-case cc ctx tc rest)
             (simplify-ifte-blockified-case cc ctx fc rest)
    ⟩
    toWhileResult
      (let (ctx' , e-res) =
                 (if Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx)
                 then ( toContractedFlowCtx cc ctx
                      , inj₁ (toContractedFlow cc (tc ++ rest))
                      )
                 else ( toContractedFlowCtx cc ctx
                      , inj₁ (toContractedFlow cc (fc ++ rest))
                      ))
       in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
          , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
          ))
  ≡⟨ cong (λ z →
            toWhileResult
              (let (ctx' , e-res) =
                         (if z
                         then ( toContractedFlowCtx cc ctx
                              , inj₁ (toContractedFlow cc (tc ++ rest))
                              )
                         else ( toContractedFlowCtx cc ctx
                              , inj₁ (toContractedFlow cc (fc ++ rest))
                              ))
               in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
                  , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
                  )))
             (eval𝔹-contractd≡false)
    ⟩
    toWhileResult
    ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) (toContractedFlowCtx cc ctx)
    , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id (inj₁ (toContractedFlow cc (fc ++ rest)))
    )
  ≡⟨ cong₂ (λ z₁ z₂ →
              toWhileResult
                ( z₁ , inj₁ z₂))
            (Ctx'.mapLoopStackSeqs∘mapSeq-cancel
                _
                (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq ∘ toFlowSeq))
             (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq  (toFlowSeq (fc ++ rest)))
    ⟩
    (toWhileResult ∘ toContractedFlowResult cc)
    ( ctx
    , inj₁ (fc ++ rest)
    )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    ( ctx
    , inj₁ (fc ++ rest))
  ∎
  where
    eval𝔹-contractd≡false : Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx) ≡ false
    eval𝔹-contractd≡false = sym (trans (sym eval𝔹≡false) (eval𝔹[ctx]≡eval𝔹[flowCtx] cc guard ctx))
    contracted-branch =
      Flow.branch
        stm guard
        (toContractedFlow cc tc)
        (toContractedFlow cc fc)
    flow-seq = contracted-branch ∷ (toContractedFlow cc rest)
    blockified-seq =
        (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    contracted-flow-seq = (Flow.contractHead (Flow.block NE.[ stm ]) (toContractedFlow cc rest))

...| no _  | _ | true | Eq.[ eval𝔹≡true ] =
  begin
      toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        contracted-flow-seq)
  ≡⟨ cong toWhileResult
           (Flow'.Properties.sematicStep-on-contractHead
             {cc = cc}
             (toFlowStm⇒¬IsBlock stm)
             blockified-seq
             refl)
    ⟩
    toWhileResult
      (Flow'.Properties.semanicStep-block-case
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (Flow.branch stm guard (toFlowSeq tc) (toFlowSeq fc))
        (toContractedFlow cc rest)
      )
  ≡⟨⟩
    toWhileResult
      (let (ctx' , e-res) =
                 (if Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx)
                 then ( toContractedFlowCtx cc ctx
                      , inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
                             ((toFlowSeq tc) ++ toContractedFlow cc rest))
                      )
                 else ( toContractedFlowCtx cc ctx
                      , inj₁ ((Flow.contract ∘ Flow.blockifySeq cc)
                        ((toFlowSeq fc) ++ toContractedFlow cc rest))
                      ))
       in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
          , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
          ))
  ≡⟨ cong₂ (λ z₁ z₂ →
            toWhileResult
              (let (ctx' , e-res) =
                         (if Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx)
                         then ( toContractedFlowCtx cc ctx
                              , inj₁ z₁
                              )
                         else ( toContractedFlowCtx cc ctx
                              , inj₁ z₂
                              ))
               in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
                  , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
                  )))
             (simplify-ifte-blockified-case cc ctx tc rest)
             (simplify-ifte-blockified-case cc ctx fc rest)
    ⟩
    toWhileResult
      (let (ctx' , e-res) =
                 (if Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx)
                 then ( toContractedFlowCtx cc ctx
                      , inj₁ (toContractedFlow cc (tc ++ rest))
                      )
                 else ( toContractedFlowCtx cc ctx
                      , inj₁ (toContractedFlow cc (fc ++ rest))
                      ))
       in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
          , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
          ))
  ≡⟨ cong (λ z →
            toWhileResult
              (let (ctx' , e-res) =
                         (if z
                         then ( toContractedFlowCtx cc ctx
                              , inj₁ (toContractedFlow cc (tc ++ rest))
                              )
                         else ( toContractedFlowCtx cc ctx
                              , inj₁ (toContractedFlow cc (fc ++ rest))
                              ))
               in ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) ctx'
                  , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id e-res
                  )))
             (eval𝔹-contractd≡true)
    ⟩
    toWhileResult
    ( Ctx'.mapLoopStackSeqs (Flow.contract ∘ Flow.blockifySeq cc) (toContractedFlowCtx cc ctx)
    , Data.Sum.map (Flow.contract ∘ Flow.blockifySeq cc) id (inj₁ (toContractedFlow cc (tc ++ rest)))
    )
  ≡⟨ cong₂ (λ z₁ z₂ →
              toWhileResult
                ( z₁ , inj₁ z₂))
            (Ctx'.mapLoopStackSeqs∘mapSeq-cancel
                _
                (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq ∘ toFlowSeq))
             (Flow.[contract∘blockifySeq]²≡contract∘blockifySeq  (toFlowSeq (tc ++ rest)))
    ⟩
    (toWhileResult ∘ toContractedFlowResult cc)
    ( ctx
    , inj₁ (tc ++ rest)
    )
  ≡⟨ toWhileResult∘toContractedFlowResult≡id _ ⟩
    ( ctx
    , inj₁ (tc ++ rest))
  ∎
  where
    eval𝔹-contractd≡true : Ctx'.eval𝔹 guard (toContractedFlowCtx cc ctx) ≡ true
    eval𝔹-contractd≡true = eval𝔹[ctx]⇒eval𝔹[flowCtx] cc guard ctx eval𝔹≡true
    contracted-branch =
      Flow.branch
        stm guard
        (toContractedFlow cc tc)
        (toContractedFlow cc fc)
    flow-seq = contracted-branch ∷ (toContractedFlow cc rest)
    blockified-seq =
        (Flow.block NE.[ stm ]) ∷ (Flow.blockifySeq cc ∘ toFlowSeq) rest
    contracted-flow-seq =
      Flow.contractHead
        (Flow.block NE.[ stm ])
        (toContractedFlow cc rest)


contracted-semantic-equals :
  {A : Set}
  → (ctx : Ctx (𝕎.Stm A))
  → (cc  : Flow.ContractionCtx (𝕎.Stm A))
  → (seq : List (𝕎.Stm A))
  → toWhileResult
      (Flow'.semantic
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        (toContractedFlow cc seq))
  ≡ 𝕎.semantic ctx seq
contracted-semantic-equals {A} ctx cc seq@[] =
  rec ctx
  where
    rec : (ctx : Ctx (𝕎.Stm A)) → toWhileResult
                                      (Flow'.semantic
                                        (contractedSubsemantic cc)
                                        (toContractedFlowCtx cc ctx)
                                        (toContractedFlow cc seq))
                                  ≡ 𝕎.semantic ctx seq
    rec ctx@(record { loopStack = []}) =
      toWhileResult∘toContractedFlowResult≡id _
    rec ctx@(record { loopStack = (node ∷ lsTail)}) =
      contracted-semantic-equals
        (record ctx {loopStack = lsTail})
        cc
        (LoopStack.Node.Node.continue node)
contracted-semantic-equals ctx cc seq@(w ∷ ws)
  with (toContractedFlow cc seq) | Eq.inspect (toContractedFlow cc) seq
...| [] | Eq.[ contractedFlow≡[] ] =
  ⊥-elim (toContractedFlow-not-empty w ws contractedFlow≡[])
...| cSeq@(_ ∷ _) | Eq.[ contractedSeq≡cSeq ] with (Flow'.semanticStep (contractedSubsemantic cc) (toContractedFlowCtx cc ctx) cSeq) | Eq.inspect (Flow'.semanticStep (contractedSubsemantic cc) (toContractedFlowCtx cc ctx)) cSeq | 𝕎.semanticStep ctx seq | Eq.inspect (𝕎.semanticStep ctx) seq
...| fres@(fctx , inj₁ []) | Eq.[ semanticStep≡fres ] | wres@(wctx , inj₂ y) | Eq.[ 𝕎sem≡wres ] =
  ⊥-elim (absurd (trans (cong (proj₂) fres≡𝕎Step) (cong proj₂ 𝕎sem≡wres)))
  where
    fres≡𝕎Step : toWhileResult fres ≡ 𝕎.semanticStep ctx seq
    fres≡𝕎Step =
      trans
        (trans (cong toWhileResult (sym semanticStep≡fres))
              (cong (toWhileResult ∘ (Flow'.semanticStep _ _)) (sym contractedSeq≡cSeq)))
        (contracted-semanticStep-equals ctx cc seq)
    absurd : inj₁ [] ≡ inj₂ y → ⊥
    absurd ()

...| fres@(fctx , inj₁ (x ∷ xs)) | Eq.[ semanticStep≡fres ] | wres@(wctx , inj₁ []) | Eq.[ 𝕎sem≡wres ] =
  ⊥-elim (toWhile-not-empty x xs toWhile≡[])
  where
    fres≡𝕎Step : toWhileResult fres ≡ 𝕎.semanticStep ctx seq
    fres≡𝕎Step =
      trans
        (trans (cong toWhileResult (sym semanticStep≡fres))
              (cong (toWhileResult ∘ (Flow'.semanticStep _ _)) (sym contractedSeq≡cSeq)))
        (contracted-semanticStep-equals ctx cc seq)
    toWhile≡[] : toWhile (x ∷ xs) ≡ []
    toWhile≡[] =
      Data.Sum.Properties.inj₁-injective
        (trans (sym (cong proj₂ (sym fres≡𝕎Step)))
               (cong proj₂ 𝕎sem≡wres))
...| fres@(fctx , inj₁ (x ∷ xs)) | Eq.[ semanticStep≡fres ] | wres@(wctx , inj₂ y) | Eq.[ 𝕎sem≡wres ] =
  ⊥-elim (absurd (trans (cong (proj₂) fres≡𝕎Step) (cong proj₂ 𝕎sem≡wres)))
  where
    fres≡𝕎Step : toWhileResult fres ≡ 𝕎.semanticStep ctx seq
    fres≡𝕎Step =
      trans
        (trans (cong toWhileResult (sym semanticStep≡fres))
              (cong (toWhileResult ∘ (Flow'.semanticStep _ _)) (sym contractedSeq≡cSeq)))
        (contracted-semanticStep-equals ctx cc seq)
    absurd : (inj₁ (toWhile (x ∷ xs))) ≡ inj₂ y → ⊥
    absurd ()
...| fres@(fctx , inj₂ y) | Eq.[ semanticStep≡fres ] | wres@(wctx , inj₁ xs) | Eq.[ 𝕎sem≡wres ] =
  ⊥-elim (absurd (trans (cong (proj₂) fres≡𝕎Step) (cong proj₂ 𝕎sem≡wres)))
  where
    fres≡𝕎Step : toWhileResult fres ≡ 𝕎.semanticStep ctx seq
    fres≡𝕎Step =
      trans
        (trans (cong toWhileResult (sym semanticStep≡fres))
              (cong (toWhileResult ∘ (Flow'.semanticStep _ _)) (sym contractedSeq≡cSeq)))
        (contracted-semanticStep-equals ctx cc seq)
    absurd : (inj₂ y) ≡ inj₁ xs → ⊥
    absurd ()
...| fres@(fctx , inj₁ []) | Eq.[ semanticStep≡fres ] | wres@(wctx , inj₁ (x ∷ xs)) | Eq.[ 𝕎sem≡wres ] =
  ⊥-elim (absurd (trans (cong (proj₂) fres≡𝕎Step) (cong proj₂ 𝕎sem≡wres)))
  where
    fres≡𝕎Step : toWhileResult fres ≡ 𝕎.semanticStep ctx seq
    fres≡𝕎Step =
      trans
        (trans (cong toWhileResult (sym semanticStep≡fres))
              (cong (toWhileResult ∘ (Flow'.semanticStep _ _)) (sym contractedSeq≡cSeq)))
        (contracted-semanticStep-equals ctx cc seq)
    absurd : (inj₁ []) ≡ inj₁ (x ∷ xs) → ⊥
    absurd ()
...| fres@(fctx , inj₂ val) | Eq.[ semanticStep≡fres ] | wres@(wctx , inj₂ _) | Eq.[ 𝕎sem≡wres ] =
  begin
    toWhileResult fres
  ≡⟨ cong toWhileResult (sym semanticStep≡fres) ⟩
    toWhileResult (Flow'.semanticStep
                    (contractedSubsemantic cc)
                    (toContractedFlowCtx cc ctx)
                    cSeq)
  ≡⟨ cong (toWhileResult
          ∘ (Flow'.semanticStep _ _))
          (sym contractedSeq≡cSeq)
   ⟩
    toWhileResult (contractedSemanticStep cc ctx seq)
  ≡⟨ contracted-semanticStep-equals ctx cc seq ⟩
    𝕎.semanticStep ctx seq
  ≡⟨ 𝕎sem≡wres ⟩
    wres
  ∎
...| fres@(fctx , inj₁ []) | Eq.[ semanticStep≡fres ] | wres@(wctx , inj₁ []) | Eq.[ 𝕎sem≡wres ] =
  begin
    (Data.Product.map
      (Ctx'.mapSeq toWhile)
      (Data.Sum.map toWhile id ∘ inj₁)
      (fromMaybe (fctx , []) (Ctx'.popContinue fctx)))
  ≡⟨ productMapDistr
       (Ctx'.mapSeq toWhile)
       (Data.Sum.map toWhile id ∘ inj₁)
       (fctx , [])
       (Ctx'.popContinue fctx)
    ⟩
    fromMaybe
      ( Ctx'.mapSeq toWhile fctx
      , inj₁ [])
      (Data.Maybe.map
         (Data.Product.map
           (Ctx'.mapSeq toWhile)
           (Data.Sum.map toWhile id ∘ inj₁))
           (Data.Maybe.map
             (Data.Product.map₂ LoopStack.Node.Node.continue)
             (Ctx'.pop fctx)))
  ≡⟨ cong (fromMaybe _)
            (sym (Data.Maybe.Properties.map-compose (Ctx'.pop fctx)))
    ⟩
      fromMaybe
        ( Ctx'.mapSeq toWhile fctx
        , inj₁ [])
        (Data.Maybe.map
           ((Data.Product.map₂
             (inj₁ ∘ LoopStack.Node.Node.continue))
            ∘ (Data.Product.map
                (Ctx'.mapSeq toWhile)
                (LoopStack.Node.mapSeq toWhile)))
            (Ctx'.pop fctx))
  ≡⟨ cong (fromMaybe _)
            (Data.Maybe.Properties.map-compose (Ctx'.pop fctx))
    ⟩
      fromMaybe
        ( Ctx'.mapSeq toWhile fctx , inj₁ [])
        (Data.Maybe.map
          (Data.Product.map₂
             (inj₁ ∘ LoopStack.Node.Node.continue))
          (Data.Maybe.map
           (Data.Product.map
              (Ctx'.mapSeq toWhile)
              (LoopStack.Node.mapSeq toWhile))
            (Ctx'.pop fctx)))
  ≡⟨ cong (λ z → fromMaybe (Ctx'.mapSeq toWhile fctx , inj₁ []) (Data.Maybe.map _ z))
          (sym (Ctx'.Properties.pop-mapSeq-commutes-ish fctx toWhile))
    ⟩
    fromMaybe
      ( Ctx'.mapSeq toWhile fctx
      , inj₁ [])
      (Data.Maybe.map
        (Data.Product.map₂
           (inj₁ ∘ LoopStack.Node.Node.continue))
        (Ctx'.pop (Ctx'.mapSeq toWhile fctx)))
  ≡⟨ cong (λ z →
            fromMaybe
             ( z , inj₁ [])
             (Data.Maybe.map
               (Data.Product.map₂
                 (inj₁ ∘ LoopStack.Node.Node.continue))
                 (Ctx'.pop z))
           )
           foo
    ⟩
    fromMaybe
      ( (proj₁ (𝕎.semanticStep ctx seq))
      , inj₁ [])
      (Data.Maybe.map
        (Data.Product.map
          id inj₁
        ∘ (Data.Product.map₂ LoopStack.Node.Node.continue))
       (Ctx'.pop (proj₁ (𝕎.semanticStep ctx seq))))
  ≡⟨ cong (fromMaybe _)
           (Data.Maybe.Properties.map-compose
             (Ctx'.pop (proj₁ (𝕎.semanticStep ctx seq))))
    ⟩
    fromMaybe
      ( (proj₁ (𝕎.semanticStep ctx seq))
      , inj₁ [])
      (Data.Maybe.map
        (Data.Product.map id inj₁)
        (Ctx'.popContinue (proj₁ (𝕎.semanticStep ctx seq))))
  ≡⟨ sym (productMapDistr
            id inj₁
            ((proj₁ (𝕎.semanticStep ctx seq)) , [])
            (Ctx'.popContinue (proj₁ (𝕎.semanticStep ctx seq))))
    ⟩
    Data.Product.map
      id inj₁
      (fromMaybe
        ((proj₁ (𝕎.semanticStep ctx seq)) , [])
        (Ctx'.popContinue (proj₁ (𝕎.semanticStep ctx seq))))
  ≡⟨ cong (λ z → Data.Product.map
                   id inj₁
                   (fromMaybe
                     (proj₁ z , [])
                     (Ctx'.popContinue (proj₁ z))))
           𝕎sem≡wres
    ⟩
    Data.Product.map id inj₁ (fromMaybe (wctx , []) (Ctx'.popContinue wctx))
  ∎
  where

    foo : Ctx'.mapSeq toWhile fctx ≡ proj₁ (𝕎.semanticStep ctx seq)
    foo = begin
            Ctx'.mapSeq toWhile (proj₁ fres)
          ≡⟨ cong (Ctx'.mapSeq toWhile ∘ proj₁) (sym semanticStep≡fres) ⟩
            Ctx'.mapSeq
              toWhile
              (proj₁ (Flow'.semanticStep
                (contractedSubsemantic cc)
                (toContractedFlowCtx cc ctx)
                cSeq))
          ≡⟨ cong (λ z → Ctx'.mapSeq
                            toWhile
                            (proj₁ (Flow'.semanticStep
                              (contractedSubsemantic cc)
                              (toContractedFlowCtx cc ctx)
                              z)))
                   (sym contractedSeq≡cSeq)
             ⟩
            Ctx'.mapSeq
              toWhile
              (proj₁ (Flow'.semanticStep
                (contractedSubsemantic cc)
                (toContractedFlowCtx cc ctx)
                (toContractedFlow cc seq)))
          ≡⟨ cong proj₁ (contracted-semanticStep-equals ctx cc seq) ⟩
            proj₁ (𝕎.semanticStep ctx seq)
          ∎
...| fres@(fctx , inj₁ (_ ∷ _)) | Eq.[ semanticStep≡fres ] | wres@(wctx , inj₁ (_ ∷ _)) | Eq.[ 𝕎sem≡wres ] =
  begin
    toWhileResult fres
  ≡⟨ (cong toWhileResult (sym semanticStep≡fres))
    ⟩
    toWhileResult
      (Flow'.semanticStep
        (contractedSubsemantic cc)
        (toContractedFlowCtx cc ctx)
        cSeq)
  ≡⟨ cong (toWhileResult ∘ (Flow'.semanticStep _ _))
           (sym contractedSeq≡cSeq)
    ⟩
    toWhileResult (contractedSemanticStep cc ctx seq)
  ≡⟨ contracted-semanticStep-equals ctx cc seq ⟩
    𝕎.semanticStep ctx seq
  ≡⟨ 𝕎sem≡wres  ⟩
    wres
  ∎
