{-# OPTIONS --rewriting #-}
open import Agda.Builtin.Equality.Rewrite

import While
import Flow.Core as Flow
import Flow.Semantics
import Flow.Contraction as Flow
import Flow.Contraction.BlockifyProps as Flow

open import Flow.Relation.Unary.Any using (Any ; AnySeq ; here ; inBranch ; inLoop)
import Flow.Relation.Unary.Any as Any

open import Flow.Relation.Unary.NIL-Any using (NILAny ; NILAnySeq ; here ; inBranch)
import Flow.Relation.Unary.NIL-Any as NILAny

open import Flow.Relation.Unary.All using (All ; AllSeq ; leaf ; branch ; loop)
import Flow.Relation.Unary.All as All

import Maps
import Ctx
import LoopStack

open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)

open import Util using ( excluded-middle
                       ; if-guard-subst
                       ; maybe-map-compose
                       ; lift
                       )

open import Function.Base

open import Data.Unit using (⊤) renaming ( tt to unit)

open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary

open import Data.List using (List ; [] ; [_] ; _∷_ ; _++_)
open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)
import Data.List.Properties
open import Data.List.Properties using ( map-++-commute)
import Data.List.NonEmpty as NE

import Data.List.Relation.Unary.All as List using ( All)
import Data.List.Relation.Unary.Any as List using ( Any )
import Data.List.Relation.Unary.Linked as List using ( Linked )

open import Agda.Builtin.Equality using (_≡_ ; refl )
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong₂ ; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)
import Data.Maybe.Properties


open import Data.Integer using (ℤ)
                         renaming (+_ to ℤ+_)

import Extra.Sum

import Expr


module WhileToFlow.NonContracted
       (MapType : Set → Set)
       (MapInstance : Maps.IntMap MapType)
       where

import WhileToFlow
open WhileToFlow MapType MapInstance
import WhileToFlow.CoreProperties
open WhileToFlow.CoreProperties MapType MapInstance


simple-subsemantic : ∀ {A} → Flow'.SubSemantic (𝕎.Stm A)
simple-subsemantic =
  record { stepFunc = whileStep
         ; transl = toFlowStm
         ; compress = id
         ; untransl = Data.List.concatMap toWhileStm
         }

sematincs-equals :
  ∀ {A}
  → (ctx : 𝕎.Ctx A)
  → (seq : 𝕎Seq A)
  → Flow'.semanticStep
      (toFlowCtx ctx)
      simple-subsemantic
      (toFlowSeq seq)
    ≡ mapResult toFlowStm
                 (𝕎.semanticStep ctx seq)
sematincs-equals ctx [] = refl
sematincs-equals
  ctx
  (e@(𝕎.assignExpr _ _ _) ∷ 𝕨rest) = refl
sematincs-equals ctx seq@(e@(𝕎.ifte _ guard tc fc) ∷ rest)
  with Ctx'.eval𝔹 guard ctx
... | true =
    cong (λ z → (_ ,  inj₁ z))
         (sym (toFlowSeq-++-distr tc rest))
... | false =
    cong (λ z → (_ ,  inj₁ z))
         (sym (toFlowSeq-++-distr fc rest))
sematincs-equals ctx
  seq@(e@(𝕎.while _ guard body) ∷ rest)
  with Ctx'.eval𝔹 guard ctx
... | true =
   begin
     (Ctx'.pushStack (toFlowSeq seq) (toFlowSeq rest) (toFlowCtx ctx)
     , inj₁ (toFlowSeq body))
   ≡⟨ refl ⟩
     mapResult toFlowStm (Ctx'.pushStack seq rest  ctx , inj₁ body)
   ∎
... | false = refl
sematincs-equals ctx seq@((𝕎.continue _) ∷ rest)
    with Ctx'.Properties.popElim ctx
... | inj₁ (p , pop≡just) =
  begin
    continueMap (Ctx'.popContinue (toFlowCtx ctx))
  ≡⟨⟩
   continueMap (continueProjMap (Ctx'.pop (toFlowCtx ctx)))
  ≡⟨⟩
   continueMap (continueProjMap (Ctx'.pop (Ctx'.map toFlowStm ctx)))
  ≡⟨ cong (continueMap ∘ continueProjMap) (Ctx'.Properties.pop-map-commutes-ish ctx toFlowStm) ⟩
   continueMap (continueProjMap (Data.Maybe.map (Data.Product.map (Ctx'.map toFlowStm)
                                                          (LoopStack.Node.map toFlowStm))
                                        (Ctx'.pop ctx)))
  ≡⟨ cong continueMap (maybe-map-compose (Ctx'.pop ctx) _ _) ⟩
   continueMap (Data.Maybe.map (((Data.Product.map id LoopStack.Node.Node.continue))
                                 ∘ (Data.Product.map (Ctx'.map toFlowStm)
                                                      (LoopStack.Node.map toFlowStm)))
                               (Ctx'.pop ctx))
  ≡⟨⟩
   continueMap (Data.Maybe.map ((Data.Product.map (Ctx'.map toFlowStm)
                                                      (Data.List.map toFlowStm ∘ LoopStack.Node.Node.continue))
                               )
                               (Ctx'.pop ctx))
  ≡⟨ Data.Maybe.Properties.maybe′-map (Data.Product.map₂ inj₁)
                                       (Flow'.semanticStep (toFlowCtx ctx)
                                         simple-subsemantic (toFlowSeq rest))
                                       (Data.Product.map (Ctx'.map toFlowStm)
                                                      (Data.List.map toFlowStm ∘ LoopStack.Node.Node.continue))
                                       (Ctx'.pop ctx)
    ⟩
    maybe (mapResult toFlowStm ∘ (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.continue)))
            (Flow'.semanticStep (toFlowCtx ctx)
                                simple-subsemantic (toFlowSeq rest))
                               (Ctx'.pop ctx)
  ≡⟨ cong (maybe (mapResult toFlowStm ∘ (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.continue)))
            (Flow'.semanticStep (toFlowCtx ctx)
                                simple-subsemantic (toFlowSeq rest))) pop≡just  ⟩
    mapResult toFlowStm (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.continue) (Ctx'.dropFrame ctx , p))
  ≡⟨⟩
    mapResult toFlowStm (Data.Product.map₂ inj₁ (Ctx'.dropFrame ctx , LoopStack.Node.Node.continue p))
  ≡⟨⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (just (Ctx'.dropFrame ctx , LoopStack.Node.Node.continue p)))
  ≡⟨⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (just ((Data.Product.map₂ LoopStack.Node.Node.continue)
                                            (Ctx'.dropFrame ctx , p))))
  ≡⟨⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)
                                                     (just (Ctx'.dropFrame ctx , p))))
  ≡⟨ cong (λ x → mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)
                                                     x))) (sym pop≡just)
  ⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)
                                                     (Ctx'.pop ctx)))
  ≡⟨⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (Ctx'.popContinue ctx))
  ≡⟨⟩
    mapResult toFlowStm (𝕎.semanticStep ctx seq)
  ∎
  where
    continueProjMap = Data.Maybe.map (Data.Product.map id LoopStack.Node.Node.continue)
    continueMap =
      maybe (Data.Product.map₂ inj₁)
            (Flow'.semanticStep (toFlowCtx ctx)
                                simple-subsemantic (toFlowSeq rest))


... | inj₂ pop≡nothing =
  begin
    continueMap (Ctx'.popContinue (toFlowCtx ctx))
  ≡⟨⟩
    continueMap (continueProjMap (Ctx'.pop (Ctx'.map toFlowStm ctx)))
  ≡⟨ cong (continueMap ∘ continueProjMap) (Ctx'.Properties.pop-map-commutes-ish ctx toFlowStm) ⟩
    continueMap (continueProjMap (Data.Maybe.map (Data.Product.map (Ctx'.map toFlowStm)
                                                 (LoopStack.Node.map toFlowStm))
                                                 (Ctx'.pop ctx)))
  ≡⟨ cong (λ x → continueMap (continueProjMap (Data.Maybe.map (Data.Product.map (Ctx'.map toFlowStm)
                                                 (LoopStack.Node.map toFlowStm))
                                                 x)) )  pop≡nothing
     ⟩
    Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq rest)
  ≡⟨ sematincs-equals ctx rest ⟩
    mapResult toFlowStm (𝕎.semanticStep ctx rest)
  ≡⟨ cong (λ x → mapResult toFlowStm
               (maybe (Data.Product.map₂ inj₁)
                      (𝕎.semanticStep ctx  rest)
                      (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue) x)))
           (sym pop≡nothing)
    ⟩
    mapResult toFlowStm
               (maybe (Data.Product.map₂ inj₁)
                      (𝕎.semanticStep ctx  rest)
                      (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue) (Ctx'.pop ctx)))
  ≡⟨⟩
    mapResult toFlowStm
               (maybe (Data.Product.map₂ inj₁)
                      (𝕎.semanticStep ctx  rest)
                      (Ctx'.popContinue ctx))
  ≡⟨⟩
    mapResult toFlowStm (𝕎.semanticStep ctx seq)
  ∎
  where
    continueProjMap = Data.Maybe.map (Data.Product.map id LoopStack.Node.Node.continue)
    continueMap = maybe (Data.Product.map₂ inj₁)
            (Flow'.semanticStep (toFlowCtx ctx)
                                simple-subsemantic (toFlowSeq rest))
sematincs-equals ctx seq@((𝕎.break _) ∷ rest)
    with Ctx'.Properties.popElim ctx
... | inj₁ (p , pop≡just) =
  begin
    breakMap (Ctx'.popBreak (toFlowCtx ctx))
  ≡⟨⟩
   breakMap (breakProjMap (Ctx'.pop (toFlowCtx ctx)))
  ≡⟨⟩
   breakMap (breakProjMap (Ctx'.pop (Ctx'.map toFlowStm ctx)))
  ≡⟨ cong (breakMap ∘ breakProjMap) (Ctx'.Properties.pop-map-commutes-ish ctx toFlowStm) ⟩
   breakMap (breakProjMap (Data.Maybe.map (Data.Product.map (Ctx'.map toFlowStm)
                                                          (LoopStack.Node.map toFlowStm))
                                        (Ctx'.pop ctx)))
  ≡⟨ cong breakMap
          (maybe-map-compose (Ctx'.pop ctx)
          ((Data.Product.map (Ctx'.map toFlowStm) (LoopStack.Node.map toFlowStm)))
          ((Data.Product.map id LoopStack.Node.Node.break)))

    ⟩
   breakMap (Data.Maybe.map (((Data.Product.map id LoopStack.Node.Node.break))
                                 ∘ (Data.Product.map (Ctx'.map toFlowStm)
                                                      (LoopStack.Node.map toFlowStm)))
                               (Ctx'.pop ctx))
  ≡⟨⟩
   breakMap (Data.Maybe.map ((Data.Product.map (Ctx'.map toFlowStm)
                                                      (Data.List.map toFlowStm ∘ LoopStack.Node.Node.break))
                               )
                               (Ctx'.pop ctx))
  ≡⟨ Data.Maybe.Properties.maybe′-map (Data.Product.map₂ inj₁)
                                       (Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq rest))
                                       (Data.Product.map (Ctx'.map toFlowStm)
                                                      (Data.List.map toFlowStm ∘ LoopStack.Node.Node.break))
                                       (Ctx'.pop ctx)
    ⟩
    maybe (mapResult toFlowStm ∘ (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.break)))
            (Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq rest))
                               (Ctx'.pop ctx)
  ≡⟨ cong (maybe (mapResult toFlowStm ∘ (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.break)))
            (Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq rest))) pop≡just  ⟩
    mapResult toFlowStm (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.break) (Ctx'.dropFrame ctx , p))
  ≡⟨⟩
    mapResult toFlowStm (Data.Product.map₂ inj₁ (Ctx'.dropFrame ctx , LoopStack.Node.Node.break p))
  ≡⟨⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (just (Ctx'.dropFrame ctx , LoopStack.Node.Node.break p)))
  ≡⟨⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (just ((Data.Product.map₂ LoopStack.Node.Node.break)
                                            (Ctx'.dropFrame ctx , p))))
  ≡⟨⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)
                                                     (just (Ctx'.dropFrame ctx , p))))
  ≡⟨ cong (λ x → mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)
                                                     x))) (sym pop≡just)
  ⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)
                                                     (Ctx'.pop ctx)))
  ≡⟨⟩
    mapResult toFlowStm (maybe (Data.Product.map₂ inj₁)
                                     (𝕎.semanticStep ctx  rest)
                                     (Ctx'.popBreak ctx))
  ≡⟨⟩
    mapResult toFlowStm (𝕎.semanticStep ctx seq)
  ∎
  where
    breakProjMap = Data.Maybe.map (Data.Product.map id LoopStack.Node.Node.break)
    breakMap =
      maybe (Data.Product.map₂ inj₁)
            (Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq rest))


... | inj₂ pop≡nothing =
  begin
    breakMap (Ctx'.popBreak (toFlowCtx ctx))
  ≡⟨⟩
    breakMap (breakProjMap (Ctx'.pop (Ctx'.map toFlowStm ctx)))
  ≡⟨ cong (breakMap ∘ breakProjMap) (Ctx'.Properties.pop-map-commutes-ish ctx toFlowStm) ⟩
    breakMap (breakProjMap (Data.Maybe.map (Data.Product.map (Ctx'.map toFlowStm)
                                                 (LoopStack.Node.map toFlowStm))
                                                 (Ctx'.pop ctx)))
  ≡⟨ cong (λ x → breakMap (breakProjMap (Data.Maybe.map (Data.Product.map (Ctx'.map toFlowStm)
                                                 (LoopStack.Node.map toFlowStm))
                                                 x)) )  pop≡nothing
     ⟩
    Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq rest)
  ≡⟨ sematincs-equals ctx rest ⟩
    mapResult toFlowStm (𝕎.semanticStep ctx rest)
  ≡⟨ cong (λ x → mapResult toFlowStm
               (maybe (Data.Product.map₂ inj₁)
                      (𝕎.semanticStep ctx  rest)
                      (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break) x)))
           (sym pop≡nothing)
    ⟩
    mapResult toFlowStm
               (maybe (Data.Product.map₂ inj₁)
                      (𝕎.semanticStep ctx  rest)
                      (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break) (Ctx'.pop ctx)))
  ≡⟨⟩
    mapResult toFlowStm
               (maybe (Data.Product.map₂ inj₁)
                      (𝕎.semanticStep ctx  rest)
                      (Ctx'.popBreak ctx))
  ≡⟨⟩
    mapResult toFlowStm (𝕎.semanticStep ctx seq)
  ∎
  where
    breakProjMap = Data.Maybe.map (Data.Product.map id LoopStack.Node.Node.break)
    breakMap = maybe (Data.Product.map₂ inj₁)
            (Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq rest))
sematincs-equals ctx seq@(e@(𝕎.call annot procIdx args) ∷ rest) =
  begin
    (Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq seq))
  ≡⟨⟩
    ( Flow'.runSubSem (toFlowCtx ctx) simple-subsemantic e
    , inj₁ (toFlowSeq rest)
    )
  ≡⟨ cong (λ z → ( let subCtx-transl = z
                    in Ctx'.setVarCtx (Ctx'.variables (whileStep (Ctx'.clearLoopStack subCtx-transl)
                                      (𝕎.call annot procIdx args)))
                                      (toFlowCtx ctx)
                 , inj₁ (toFlowSeq rest)
                 ))
          (Ctx'.Properties.concatMap-id toWhileStm∘toFlowStm≡id ctx)
    ⟩
    ( Ctx'.setVarCtx (Ctx'.variables (whileStep (Ctx'.clearLoopStack ctx)
                                      (𝕎.call annot procIdx args)))
                                      (toFlowCtx ctx)
    , inj₁ (toFlowSeq rest)
    )
  ≡⟨⟩
    mapResult toFlowStm
    ( (record ctx
      { variables =
                  Ctx'.variables (proj₁ (𝕎.semanticStep (record { procedures = (Ctx'.procedures ctx)
                                              ; variables = Ctx'.variables (toFlowCtx ctx)
                                              ; loopStack = []
                                              }) [ e ]))
                })
    , inj₁ rest
    )
  ≡⟨⟩
    mapResult toFlowStm
    ( (record ctx
      { variables =
                  Ctx'.variables (proj₁  (𝕎.procCall ctx procIdx args)
                  )
                })
    , inj₁ rest
    )
  ≡⟨ cong (mapResult toFlowStm ∘ (_, inj₁ rest)) refl ⟩
    mapResult toFlowStm (
      ((proj₁ (𝕎.procCall ctx procIdx args))  , inj₁ rest)
      )
  ≡⟨⟩
    mapResult toFlowStm (
      ((proj₁ (𝕎.procCall ctx procIdx args))  , inj₁ rest)
      )
  ≡⟨⟩
    mapResult toFlowStm (
      let (ctx' , res) = 𝕎.procCall ctx procIdx args
      in (ctx' , inj₁ rest)
      )
  ≡⟨⟩
    mapResult toFlowStm (𝕎.semanticStep ctx seq)
  ∎
sematincs-equals ctx seq@(e@(𝕎.assignCall annot var procIdx args) ∷ rest) =
  begin
    (Flow'.semanticStep (toFlowCtx ctx) simple-subsemantic (toFlowSeq seq))
  ≡⟨⟩
    ( Flow'.runSubSem (toFlowCtx ctx) simple-subsemantic e
    , inj₁ (toFlowSeq rest)
    )
  ≡⟨ cong (λ z → ( let subCtx-transl = z
                    in Ctx'.setVarCtx (Ctx'.variables (whileStep (Ctx'.clearLoopStack subCtx-transl)
                                      (𝕎.assignCall annot var procIdx args)))
                                      (toFlowCtx ctx)
                 , inj₁ (toFlowSeq rest)
                 ))
          (Ctx'.Properties.concatMap-id toWhileStm∘toFlowStm≡id ctx)
    ⟩
    ( Ctx'.setVarCtx (Ctx'.variables (whileStep (Ctx'.clearLoopStack ctx)
                                      (𝕎.assignCall annot var procIdx args)))
                                      (toFlowCtx ctx)
    , inj₁ (toFlowSeq rest)
    )
  ≡⟨⟩
    mapResult toFlowStm
    ( (record ctx
      { variables =
                  Ctx'.variables (let (ctx' , res) = 𝕎.procCall ctx procIdx args
                                         in ( Ctx'.setVar var res ctx'
                                            ))
                })
    , inj₁ rest
    )
  ≡⟨ cong (mapResult toFlowStm ∘ (_, inj₁ rest)) refl ⟩
    mapResult toFlowStm (𝕎.semanticStep ctx seq)
  ∎
sematincs-equals ctx seq@(e@(𝕎.return annot a) ∷ rest) =
  refl
