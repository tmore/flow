{- Author: Tomas Möre 2020 -}
{- Contains the core defenitions of the 'Flow' language -}

module Flow.Core where

open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)
open import Data.List using (List ; [] ; _∷_ ; foldl ; zipWith ; [_] ; _++_ ; drop)

open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)

open import Data.Unit using (⊤) renaming ( tt to unit)

open import Relation.Nullary.Decidable.Core
open import Relation.Nullary
open import Data.Empty
open import Data.Bool

-- Experiment
import Agda.Builtin.Size
open import Agda.Builtin.Size using (Size ; Size<_ ; ↑_ )

open import Expr
open 𝕍 using (𝕍)
open 𝔸 using (𝔸)
open 𝔹 using (𝔹)

import Data.List.NonEmpty as NE

-- Size is used here in order to simplify later operations It simply makes
-- termination checking using higher order functions recursively


data Stm (A : Set) : Set where
  block : List⁺ A → Stm A
  call  : A → Stm A
  assign : A → Stm A
  break : A → Stm A
  continue : A → Stm A
  return : A → 𝔸 → Stm A
  branch : A → 𝔹 → (List (Stm A)) → (List (Stm A)) → Stm A
  loop : A → 𝔹 → (List (Stm A)) → Stm A

Sequence :  Set → Set
Sequence A = List (Stm A)

annotations : ∀{A} → (Stm A) → List⁺ A
annotations (block annots) = annots
annotations (call a) = NE.[ a ]
annotations (assign a) = NE.[ a ]
annotations (break a) = NE.[ a ]
annotations (continue a) = NE.[ a ]
annotations (return a _) = NE.[ a ]
annotations (branch a _ _ _) = NE.[ a ]
annotations (loop a _ _) = NE.[ a ]

numAnnots : ∀{A} → Stm A → ℕ
numAnnots stm = NE.length (annotations stm)

firstAnnot : ∀{A} → Stm A → A
firstAnnot stm = NE.head (annotations stm)

lastAnnot : ∀{A} → Stm A → A
lastAnnot stm = NE.last (annotations stm)

-- Some utility predicates to check wether a statement has any specific type

IsBlock : ∀ {A} → Stm A → Set
IsBlock (block _) = ⊤
{-# CATCHALL #-}
IsBlock _ = ⊥

IsLoop : ∀ {A} → Stm A → Set
IsLoop (loop _ _ _) = ⊤
{-# CATCHALL #-}
IsLoop _ = ⊥

IsBranch : ∀ {A} → Stm A → Set
IsBranch (branch _ _ _ _) = ⊤
{-# CATCHALL #-}
IsBranch _ = ⊥

IsLeaf : ∀ {A} → Stm A → Set
IsLeaf (branch _ _ _ _) = ⊥
IsLeaf (loop _ _ _) = ⊥
{-# CATCHALL #-}
IsLeaf _ = ⊤

branchTrueCase : ∀ {A} → (S : Stm A) → IsBranch S → List (Stm A)
branchTrueCase (branch _ _ tc _ ) ⊤ = tc
branchTrueCase (block _) ()
branchTrueCase (call _) ()
branchTrueCase (assign _) ()
branchTrueCase (break _) ()
branchTrueCase (continue _) ()
branchTrueCase (return _ _) ()
branchTrueCase (loop _ _ _) ()


branchFalseCase : ∀ {A} → (S : Stm A) → IsBranch S → List (Stm A)
branchFalseCase (branch _ _ _ fc ) ⊤ = fc
branchFalseCase (block _) ()
branchFalseCase (call _) ()
branchFalseCase (assign _) ()
branchFalseCase (break _) ()
branchFalseCase (continue _) ()
branchFalseCase (return _ _) ()
branchFalseCase (loop _ _ _) ()

loopBody : ∀ {A} → (S : Stm A) → IsLoop S → List (Stm A)
loopBody (loop _ _ body) ⊤ = body
loopBody (branch _ _ _ _) ()
loopBody (block _) ()
loopBody (call _) ()
loopBody (assign _) ()
loopBody (break _) ()
loopBody (continue _) ()
loopBody (return _ _) ()


isContinueOrBreak : ∀ {A} → Stm A → Bool
isContinueOrBreak (continue _) = true
isContinueOrBreak (break _) = true
isContinueOrBreak _ = false


-- | Agda can determine termination of this version of map
map' : {A B : Set} → (A → B) → List (Stm A) → List (Stm B)
map' _ [] = []
map' f ((loop a guard body) ∷ rest) =
  loop (f a) guard (map' f body) ∷ map' f rest
map' f ((branch a guard tc fc) ∷ rest) =
  branch (f a) guard (map' f tc) (map' f fc) ∷ map' f rest
map' f (block bs ∷ rest) =
  block (NE.map f bs) ∷ map' f rest
map' f (call a ∷ rest) =
  call (f a) ∷ map' f rest
map' f (assign a ∷ rest) =
  assign (f a) ∷ map' f rest
map' f (continue a ∷ rest) =
  continue (f a) ∷ map' f rest
map' f (break a ∷ rest) =
  break (f a) ∷ map' f rest
map' f (return a expr ∷ rest) =
  return (f a) expr ∷ map' f rest

-- We know that this terminate, don't tell agda she might be upset
{-# TERMINATING #-}
map : {A B : Set} → (A → B) → Stm A → Stm B
map f (block a) = block (NE.map f a)
map f (call a) = call (f a)
map f (assign a) = assign (f a)
map f (continue a) = continue (f a)
map f (break a) = break (f a)
map f (return a expr) = return (f a) expr
map f (loop a guard body) =
  loop (f a) guard (Data.List.map (map f) body)
map f (branch a guard tc fc) =
  branch (f a) guard (Data.List.map (map f) tc)
                     (Data.List.map (map f) fc)
