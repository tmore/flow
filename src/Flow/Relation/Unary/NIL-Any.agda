{- AUTHOR: Tomas Möre 2020 -}

module Flow.Relation.Unary.NIL-Any where

import Data.List.Relation.Unary.Any as List -- using ( All)
import Data.List.Relation.Unary.Any as ListAny
open import Data.Unit using (⊤) renaming ( tt to unit)
open import Data.Empty
open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary
open import Relation.Nullary.Negation hiding (excluded-middle)

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.List using (List ; [] ; _∷_ ;  [_] )

open import Flow.Core hiding (map)

open import Level using (Level ; _⊔_)

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)

data NILAny {j : Level} {A : Set} (P : Stm A → Set j) (stm : Stm A) : Set (j) where
  here : P stm → NILAny P stm
  inBranch : (t : IsBranch stm)
                → (List.Any (NILAny P) (branchTrueCase stm t))
                ⊎ (List.Any (NILAny P) (branchFalseCase stm t))
                → NILAny P stm

NILAnySeq : ∀ {j} {A : Set} (P : Stm A → Set j) → (List (Stm A)) → Set j
NILAnySeq {j} {A} P seq =
  List.Any (NILAny P) seq

anySeq :
  ∀ {i}
  → {A : Set}
  → {P : Pred (Stm A) i}
  → Decidable P
  → Decidable (NILAnySeq P)
anySeq _ [] = no λ ()
anySeq
  decideP
  (stm@(loop _ _ body) ∷ rest)
  with decideP stm | anySeq decideP rest
...| yes p-here | _ =
  yes (ListAny.here (here p-here))
...| _          | yes p-in-rest =
  yes (ListAny.there p-in-rest)
...| no ¬p-here | no ¬p-inRest =
  no λ { (ListAny.there p-inRest) → ¬p-inRest p-inRest
       ; (ListAny.here (here p-here)) → ¬p-here p-here
       }
anySeq
  decideP
  (stm@(branch _ _ tc fc) ∷ rest)
  with decideP stm | anySeq decideP tc | anySeq decideP fc | anySeq decideP rest
...| yes P-here | _            | _          | _  =
  yes (ListAny.here (here P-here))
...| _          | yes P-inTc   | _          | _  =
  yes (ListAny.here (inBranch unit (inj₁ P-inTc)))
...| _          | _            | yes P-inFc | _  =
  yes (ListAny.here (inBranch unit (inj₂ P-inFc)))
...| _          | _            | _ | yes P-in-rest =
  yes (ListAny.there P-in-rest)
...| no ¬P-here | no ¬P-inTc | no ¬P-inFc | no ¬P-inRest =
  no λ { (ListAny.there P-inRest) → ¬P-inRest P-inRest
       ; (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.here (inBranch _ (inj₁ P-inTc))) → ¬P-inTc P-inTc
       ; (ListAny.here (inBranch _ (inj₂ P-inFc))) → ¬P-inFc P-inFc
       }
anySeq decideP (stm@(call _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(assign _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(continue _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(break _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(return _ _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(block _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
any : ∀ {i} {A : Set} → {P : Pred (Stm A) i} → Decidable P → Decidable (NILAny P)
any decideP stm with anySeq decideP [ stm ]
...| yes (ListAny.here P-here) = yes P-here
...| no ¬in-List = no λ inList → ¬in-List (List.here inList)
