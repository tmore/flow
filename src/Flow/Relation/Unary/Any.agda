{- AUTHOR: Tomas Möre 2020 -}
module Flow.Relation.Unary.Any where


open import Function.Base

import Data.List.Relation.Unary.Any as List -- using ( All)
import Data.List.Relation.Unary.Any as ListAny
open import Data.Unit using (⊤) renaming ( tt to unit)
open import Data.Empty
open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary
open import Relation.Nullary.Negation hiding (excluded-middle)

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.List using (List ; [] ; _∷_ ;  [_] )

open import Flow.Core hiding (map)

open import Level using (Level ; _⊔_)
import Agda.Builtin.Size
open import Agda.Builtin.Size using (Size ; Size<_ ; ↑_ ; ∞)

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)


data Any {j : Level} {A : Set} (P : Stm A → Set j) (stm : Stm A) : Set j where
  here : P stm → Any P stm
  inBranch : (t : IsBranch stm)
            → (List.Any (Any P) (branchTrueCase stm t))
              ⊎ (List.Any (Any P) (branchFalseCase stm t))
            → Any P stm
  inLoop : (t : IsLoop stm)
          → List.Any (Any P) (loopBody stm t)
          → Any P stm
AnySeq : ∀ {j} {A : Set} (P : Stm A → Set j) → (List (Stm A)) → Set j
AnySeq {j} {A} P seq =
  List.Any (Any P) seq

-- This terminates, agda just doesn't know about it
anySeq :
  ∀ {i}
  → {A : Set}
  → {P : Pred (Stm A) i}
  → Decidable P
  → Decidable (AnySeq P)
anySeq _ [] = no λ ()
anySeq
  decideP
  (stm@(loop _ _ body) ∷ rest)
  with decideP stm | anySeq decideP body | anySeq decideP rest
...| yes p-here | _            | _ =
  yes (ListAny.here (here p-here))
...| _          | yes p-inLoop | _ =
  yes (ListAny.here (inLoop unit p-inLoop))
...| _          | _            | yes p-in-rest =
  yes (ListAny.there p-in-rest)
...| no ¬p-here | no ¬p-inBody | no ¬p-inRest =
  no λ { (ListAny.there p-inRest) → ¬p-inRest p-inRest
       ; (ListAny.here (here p-here)) → ¬p-here p-here
       ; (ListAny.here (inLoop _ p-inBody)) → ¬p-inBody p-inBody
       }
anySeq
  decideP
  (stm@(branch _ _ tc fc) ∷ rest)
  with decideP stm | anySeq decideP tc | anySeq decideP fc | anySeq decideP rest
...| yes P-here | _            | _          | _  =
  yes (ListAny.here (here P-here))
...| _          | yes P-inTc   | _          | _  =
  yes (ListAny.here (inBranch unit (inj₁ P-inTc)))
...| _          | _            | yes P-inFc | _  =
  yes (ListAny.here (inBranch unit (inj₂ P-inFc)))
...| _          | _            | _ | yes P-in-rest =
  yes (ListAny.there P-in-rest)
...| no ¬P-here | no ¬P-inTc | no ¬P-inFc | no ¬P-inRest =
  no λ { (ListAny.there P-inRest) → ¬P-inRest P-inRest
       ; (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.here (inBranch _ (inj₁ P-inTc))) → ¬P-inTc P-inTc
       ; (ListAny.here (inBranch _ (inj₂ P-inFc))) → ¬P-inFc P-inFc
       }
anySeq decideP (stm@(call _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(assign _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(continue _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(break _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(return _ _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }
anySeq decideP (stm@(block _) ∷ rest)
    with (decideP stm) | anySeq decideP rest
... | yes P-here | _ =
  yes (ListAny.here (here P-here))
... | _          | yes P-in-rest =
  yes (ListAny.there P-in-rest)
... | no ¬P-here | no ¬P-in-rest =
  no λ { (ListAny.here (here P-here)) → ¬P-here P-here
       ; (ListAny.there P-in-rest) → ¬P-in-rest P-in-rest
       }

any : ∀ {i} {A : Set} → {P : Pred (Stm A) i} → Decidable P → Decidable (Any P)
any decideP stm with anySeq decideP [ stm ]
...| yes (ListAny.here P-here) = yes P-here
...| no ¬in-List = no λ inList → ¬in-List (List.here inList)



-- anySeq : ∀ {i} {A : Set} → {P : Pred (Stm A) i} → Decidable P → Decidable (AnySeq P)
-- anySeq p seq = ListAny.any (any p) seq



-- Nice utility function, but not used much because of the termination checker
rec-any-list : ∀ {A}
             → {P : Stm A → Set}
             → {f : Stm A → Stm A}
             → {seq : List (Stm A)}
             → ((z : Stm A)
               → Any P z
               → Any P (f z))
             → AnySeq P seq
             → AnySeq P (Data.List.map f seq)
rec-any-list {_} {_} {f} {(stm ∷ _)} proof (List.Any.here p) =
  List.Any.here (proof stm p)
rec-any-list proof (List.Any.there rest) =
  List.Any.there (rec-any-list proof rest)

any-split-union : ∀ {A}
                → {L : Stm A → Set}
                → {R : Stm A → Set}
                → {stm : Stm A}
                → Any (λ z → L z ⊎ R z) stm
                → Any L stm ⊎ Any R stm
any-split-union (here (inj₁ l)) =
  inj₁ (here l)
any-split-union (here (inj₂ r)) =
  inj₂ (here r)
any-split-union {A} {L} {R} (inLoop t any-inLoop) =
  Data.Sum.map (inLoop t)
               (inLoop t)
               (rec any-inLoop)
  where
    rec : {l : List (Stm A)}
        → AnySeq (λ z → L z ⊎ R z) l
        → AnySeq L l
        ⊎ AnySeq R l
    rec (List.Any.here p) =
      Data.Sum.map List.Any.here
                   List.Any.here
                   (any-split-union p)
    rec (List.Any.there xs) =
      Data.Sum.map List.Any.there
                   List.Any.there
                   (rec xs)
any-split-union {A} {L} {R} (inBranch t (inj₁ in-tc)) =
  Data.Sum.map (inBranch t ∘ inj₁)
               (inBranch t ∘ inj₁)
               (rec in-tc)
  where
    rec : {l : List (Stm A)}
        → AnySeq (λ z → L z ⊎ R z) l
        → AnySeq L l
        ⊎ AnySeq R l
    rec (List.Any.here p) =
      Data.Sum.map List.Any.here
                   List.Any.here
                   (any-split-union p)
    rec (List.Any.there xs) =
      Data.Sum.map List.Any.there
                   List.Any.there
                   (rec xs)
any-split-union {A} {L} {R} (inBranch t (inj₂ in-fc)) =
  Data.Sum.map (inBranch t ∘ inj₂)
               (inBranch t ∘ inj₂)
               (rec in-fc)
  where
    rec : {l : List (Stm A)}
        → AnySeq (λ z → L z ⊎ R z) l
        → AnySeq L l
        ⊎ AnySeq R l
    rec (List.Any.here p) =
      Data.Sum.map List.Any.here
                   List.Any.here
                   (any-split-union p)
    rec (List.Any.there xs) =
      Data.Sum.map List.Any.there
                   List.Any.there
                   (rec xs)


map : {j : Level} {A : Set}
        → {P : Stm A → Set j}
        → {stm : Stm A}
        → {P' : Stm A → Set j}
        → ({stm' : Stm A} → P stm'  → P' stm')
        → Any P stm
        → Any P' stm
map {_} {_} {_} {stm} f (here pred) =
  here (f pred)

map {_} {A} {P} {stm} {P'} f (inLoop t in-body) =
  inLoop t (rec in-body)
  where
    rec : {seq : List (Stm A)} → AnySeq P seq → AnySeq P' seq
    rec (List.Any.here p) =
      List.Any.here (map f p)
    rec (List.Any.there xs) =
      List.Any.there (rec xs)
map {_} {A} {P} {stm} {P'} f (inBranch t (inj₁ in-tc)) =
  inBranch t (inj₁ (rec in-tc))
  where
    rec : {seq : List (Stm A)} → AnySeq P seq → AnySeq P' seq
    rec (List.Any.here p) =
      List.Any.here (map f p)
    rec (List.Any.there xs) =
      List.Any.there (rec xs)
map {_} {A} {P} {stm} {P'} f (inBranch t (inj₂ in-fc)) =
  inBranch t (inj₂ (rec in-fc))
  where
    rec : {seq : List (Stm A)} → AnySeq P seq → AnySeq P' seq
    rec (List.Any.here p) =
      List.Any.here (map f p)
    rec (List.Any.there xs) =
      List.Any.there (rec xs)

mapSeq : {j : Level} {A : Set}
            → {P : Stm A → Set j}
            → {stms : List (Stm A)}
            → {P' : Stm A → Set j}
            → ({stm' : Stm A} → P stm'  → P' stm')
            → AnySeq P stms
            → AnySeq P' stms
mapSeq f (List.Any.here p) =
      List.Any.here (map f p)
mapSeq f (List.Any.there xs) =
  List.Any.there (mapSeq f xs)
