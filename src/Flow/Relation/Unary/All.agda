{- AUTHOR: Tomas Möre 2020 -}
module Flow.Relation.Unary.All where


import Data.List.Relation.Unary.All as List -- using ( All)
import Data.List.Relation.Unary.All as ListAll

open import Data.List using (List ; [] ; _∷_ ;  [_] )

open import Flow.Core hiding (map)

open import Level using (Level ; _⊔_)
-- import Agda.Builtin.Size
--open import Agda.Builtin.Size using (Size ; Size<_ )

-- Datatype correspodning to a proof about all elements in a statement tree

data All {l : Level} {A : Set} (P : Stm A → Set l) (stm : Stm A) : Set l where
  leaf : IsLeaf stm → P stm → All P stm
  branch :  (t : IsBranch stm)
              → P stm
              → List.All (All P) (branchTrueCase stm t)
              → List.All (All P) (branchFalseCase stm t)
              → All P stm
  loop : (t : IsLoop stm)
           → P stm
           → List.All (All P) (loopBody stm t)
           → All P stm

AllSeq : ∀ {j} {A : Set} (P : Stm A → Set j) → (List (Stm A)) → Set j
AllSeq {j} {A} P seq =
  List.All (All P) seq

map : {j : Level} {A : Set}
        → {P : Stm A → Set j}
        → {stm : Stm A}
        → {P' : Stm A → Set j}
        → ({stm' : Stm A} → P stm'  → P' stm')
        → All P stm
        → All P' stm
map {_} {_} {_} {stm} f (leaf t pred) =
  leaf t (f pred)
map {_} {A} {P} {stm} {P'} f (loop t pred body-pred) =
  loop t (f pred) (rec body-pred)
  where
    rec : {stm' : List (Stm A)} → AllSeq P  stm' → AllSeq P' stm'
    rec List.All.[] = List.All.[]
    rec (p List.All.∷ xs) =
      (map f p List.All.∷ rec xs)
map {_} {A} {P} {stm} {P'} f (branch t pred tc-pred fc-pred) =
  branch t (f pred) (rec tc-pred) (rec fc-pred)
  where
    rec : {stm' : List (Stm A)} → AllSeq P  stm' → AllSeq P' stm'
    rec List.All.[] = List.All.[]
    rec (p List.All.∷ xs) =
      (map f p List.All.∷ rec xs)

mapSeq : {j : Level} {A : Set}
        → {P : Stm A → Set j}
        → {stms : List (Stm A)}
        → {P' : Stm A → Set j}
        → ({stm' : Stm A} → P stm'  → P' stm')
        → AllSeq P stms
        → AllSeq P' stms
mapSeq _ List.All.[] =
      List.All.[]
mapSeq f (x List.All.∷ xs) =
      (map f x) List.All.∷ mapSeq f xs
-- all : {A : Set} → (Stm A  → Bool) → Stm A → Bool
-- all p stm = not (any-bool (not ∘ p) stm)
 -- all-seq : {A : Set } → (Stm A → Bool) → List (Stm A) → Bool
-- all-seq p [] = true
-- all-seq p (x ∷ xs) = all p x ∧ all-seq p xs
