{-# OPTIONS --rewriting #-}
module Flow.Contraction.BlockifyProps where

open import Agda.Builtin.Equality.Rewrite

open import Function.Base

open import Data.Nat using (ℕ ; suc )
import Data.Nat.Properties

open import Agda.Builtin.Equality using (_≡_ ; refl )
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; _≢_ ; refl; trans; sym; cong; cong-app; subst ; cong₂)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)
open import Data.Unit using (⊤) renaming ( tt to unit)
open import Data.Empty
open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary
open import Relation.Nullary.Negation hiding (excluded-middle)

open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)


import Data.List.Relation.Unary.All as List -- using ( All)
import Data.List.Relation.Unary.All as ListAll
import Data.List.Relation.Unary.Any as List -- using ( Any )
import Data.List.Relation.Unary.Any as ListAny -- using ( Any )
import Data.List.Relation.Unary.Linked as List using ( Linked )
import Data.List.Relation.Unary.Linked as Linked

open import Data.List.Relation.Unary.All using ( _∷_ ; [])
open import Data.List.Relation.Unary.Any using ( here ; there )
open import Data.List.Relation.Unary.Linked using ( [] ; [-] ; _∷_ )

open import Data.List using (List ; [] ; _∷_ ; [_] ; _++_ ; drop)
open import Data.List.Properties using (++-identityʳ)

import Data.List.NonEmpty as NE
open import Data.List.NonEmpty using (_⁺++⁺_ ; List⁺)

open import Util using (maybeElim ; maybe⊥
                       ; either-of-these
                       ; ∧-injectiveᵣ ; ∧-injectiveₗ
                       ; de-morg₂ ; de-morg₁ ; excluded-middle
                       ; a≡false⇒a≢true
                       ; transport
                       ; lift
                       ; if-guard-subst
                       )

open import Extra.NonEmpty using (⁺++⁺-assoc)

open import Flow.Core
open import Flow.Contraction


open import Flow.Relation.Unary.Any
open import Flow.Relation.Unary.NIL-Any
open import Flow.Relation.Unary.All



NewlyTranslated :
  ∀ {A}
  → (A → Stm A)
  → (Stm A)
  → Set
NewlyTranslated f stm =
 (f (firstAnnot stm) ≡ stm)

-- What should hold for 'freshly blockified properties, may be
-- unecessesary. However I wrote it this way first
NewlyBlockifiedStm : ∀ {A} → ContractionCtx A → (A → Stm A) → Stm A → Set
NewlyBlockifiedStm cc _ stm@(call a) =
  Interesting cc stm
NewlyBlockifiedStm cc _ stm@(assign a) =
  Interesting cc stm
NewlyBlockifiedStm cc _ stm@(branch _ _ tc fc) =
  Interesting cc stm
NewlyBlockifiedStm cc _ stm@(loop _ _ _) =
  Interesting cc stm
NewlyBlockifiedStm cc f stm@(block xs) =
  NewlyTranslated f stm
  ⊎ (¬ (Interesting cc (f (firstAnnot stm))))
{-# CATCHALL #-}
NewlyBlockifiedStm cc _ _ = ⊤

-- Special contraint for blocks. Specifically this says that if a statement is a
-- block then it must be such the translation from the annotation to Flow gives
-- a block or whatever it tralsates to is boring.
BlockConstraint : ∀ {A} → ContractionCtx A → (A → Stm A) → Stm A → Set
BlockConstraint cc transl (block annots) =
  List.All (λ a → (¬ (Interesting cc (transl a)))
                   ⊎ IsBlock (transl a))
           (NE.toList annots)
{-# CATCHALL #-}
BlockConstraint _ _ _ = ⊤

-- In general what should hold any sequence that is blockified
BlockifiedStm : ∀ {A} → ContractionCtx A → Stm A → Set
BlockifiedStm cc stm@(call a) = Interesting cc stm
BlockifiedStm cc stm@(assign a) = Interesting cc stm
BlockifiedStm cc stm@(branch _ _ _ _) = Interesting cc stm
BlockifiedStm cc stm@(loop _ _ _) = Interesting cc stm
{-# CATCHALL #-}
BlockifiedStm cc _ = ⊤


UnblockifiedStm : ∀ {A} → (A → Stm A) → Stm A → Set
UnblockifiedStm f stm =
    numAnnots stm ≡ 1
  × NewlyTranslated f stm

must-be-interesting-stm :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (stm : Stm A)
  → (Interesting cc stm)
  → Σ[ p ∈ Interesting cc stm ]
     (interesting cc stm ≡ yes p)
must-be-interesting-stm cc stm p with lift (interesting cc stm)
...| yes since , interesting≡yes = ( since , interesting≡yes)
...| no since , interesting≡no =
  contradiction p since

isYes-interesting≡true :
  ∀ {A}
  → {cc : ContractionCtx A}
  → (stm : Stm A)
  → Interesting cc stm
  → isYes (interesting cc stm) ≡ true
isYes-interesting≡true
  {cc = cc} stm is-interesting =
  cong isYes (proj₂ (must-be-interesting-stm cc stm is-interesting))

must-be-boring :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (stm : Stm A)
  → ¬ (Interesting cc stm)
  → Σ[ p ∈ ¬ Interesting cc stm ]
    (interesting cc stm ≡ no p)
must-be-boring cc stm p with lift (interesting cc stm)
...| yes since , _ = contradiction since p
...| no since , interesting≡no = ( since , interesting≡no)


any-interesting-node⇒interesting :
  ∀ {A}
  → {cc : ContractionCtx A}
  → (stm : Stm A)
  → (Any (InterestingNode cc) stm)
  → Interesting cc stm
any-interesting-node⇒interesting (call a) (Any.here p) = p
any-interesting-node⇒interesting (assign a) (Any.here p) = p
any-interesting-node⇒interesting (continue _) (Any.here ())
any-interesting-node⇒interesting (break _) (Any.here ())
any-interesting-node⇒interesting (return  _ _) (Any.here p) = p
{-# CATCHALL #-}
any-interesting-node⇒interesting (loop _ _ _) p = p
{-# CATCHALL #-}
any-interesting-node⇒interesting (branch _ _ _ _) p = inj₁ p

-- Utility function defined here since it will be used many tiles
interesting-node⇒isYes-interesting :
  ∀ {A}
  → {cc : ContractionCtx A}
  → {stm : Stm A}
  → Any (InterestingNode cc) stm
  → isYes (interesting cc stm) ≡ true
interesting-node⇒isYes-interesting {_} {cc} {stm} intr =
  (cong isYes (proj₂ (must-be-interesting-stm cc stm
                     (any-interesting-node⇒interesting stm intr))))


blockify-keeps-BlockConstraint :
  ∀ {A}
  → {f : A → Stm A}
  → {cc : ContractionCtx A}
  → (seq : List (Stm A))
  → AllSeq (λ stm → NewlyTranslated f stm ⊎ IsBlock stm) seq
  → AllSeq (BlockConstraint cc f) seq
  → AllSeq (BlockConstraint cc f)
           (blockifySeq cc seq)
blockify-keeps-BlockConstraint
  [] _ _ = ListAll.[]
blockify-keeps-BlockConstraint
  (block xs ∷ rest)
  (leaf _ _ ∷ rest-transl)
  ((leaf _ blockConstrained) ∷ rest-blockConstrained) =
  (leaf _ blockConstrained) ∷ blockify-keeps-BlockConstraint
                        rest rest-transl rest-blockConstrained
blockify-keeps-BlockConstraint
  {cc = cc}
  (stm@(call _) ∷ rest)
  (leaf _ (inj₁ transl≡id) ∷ rest-transl)
  ((leaf _ _) ∷ rest-blockConstrained)
  with interesting cc stm
...| yes since =
  (leaf _ unit) ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
...| no since =
  (leaf _ (inj₁ (transport (cong (λ z → ¬ Interesting cc z) transl≡id) since) ∷ []))
    ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
blockify-keeps-BlockConstraint
  {cc = cc}
  (stm@(assign _) ∷ rest)
  (leaf _ (inj₁ transl≡id) ∷ rest-transl)
  ((leaf _ _) ∷ rest-blockConstrained)
  with interesting cc stm
...| yes since =
  (leaf _ unit) ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
...| no since =
  (leaf _ (inj₁ (transport (cong (λ z → ¬ Interesting cc z) transl≡id) since) ∷ []))
    ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
blockify-keeps-BlockConstraint
  ((continue _) ∷ rest)
  (leaf _ _ ∷ rest-transl)
  ((leaf _ blockConstrained) ∷ rest-blockConstrained) =
  (leaf _ unit) ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
blockify-keeps-BlockConstraint
  (stm@(break _) ∷ rest)
  (leaf _ _ ∷ rest-transl)
  ((leaf _ blockConstrained) ∷ rest-blockConstrained) =
  (leaf _ unit) ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
blockify-keeps-BlockConstraint
  (stm@(return _ _) ∷ rest)
  (leaf _ _ ∷ rest-transl)
  ((leaf _ blockConstrained) ∷ rest-blockConstrained) =
  (leaf _ unit) ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
blockify-keeps-BlockConstraint
  {cc = cc}
  (stm@(loop _ _ body) ∷ rest)
  (loop _ (inj₁ trasl≡id) body-transl ∷ rest-transl)
  ((loop _ _ body-blockConstrained) ∷ rest-blockConstrained)
  with interesting cc stm
...| yes since  =
  (loop _ unit (blockify-keeps-BlockConstraint body body-transl body-blockConstrained))
    ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
...| no since  =
  (leaf _ (inj₁ (transport (cong (λ z → ¬ Interesting cc z) trasl≡id) since) ∷ []))
    ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
blockify-keeps-BlockConstraint
  {cc = cc}
  (stm@(branch _ _ tc fc) ∷ rest)
  (branch _ (inj₁ trasl≡id) tc-transl fc-transl ∷ rest-transl)
  ((branch _ _ tc-blockConstrained fc-blockConstrained) ∷ rest-blockConstrained)
  with interesting cc stm
...| yes since  =
  (branch
    _ unit
    (blockify-keeps-BlockConstraint tc tc-transl tc-blockConstrained)
    (blockify-keeps-BlockConstraint fc fc-transl fc-blockConstrained)
    )
    ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained
...| no since  =
  (leaf _ (inj₁ (transport (cong (λ z → ¬ Interesting cc z) trasl≡id) since) ∷ []))
    ∷ blockify-keeps-BlockConstraint rest rest-transl rest-blockConstrained



unblockify-keeps-PseudoInterestingNode :
  ∀ {A}
  → {f : A → Stm A}
  → (stm : Stm A)
  → NILAny PseudoInterestingNode stm
  → NILAny PseudoInterestingNode (unblockifyStm f stm)
unblockify-keeps-PseudoInterestingNode
  (call a) (here ())
unblockify-keeps-PseudoInterestingNode
  (assign a) (here ())
unblockify-keeps-PseudoInterestingNode
  (return _ _) (here ())
unblockify-keeps-PseudoInterestingNode
  (continue _) (here p) =
  (here p)
unblockify-keeps-PseudoInterestingNode
  (break _) (here p) =
  (here p)
unblockify-keeps-PseudoInterestingNode
  {A} {f} (loop _ _ (x ∷ _)) (here p) =
  (here p)
unblockify-keeps-PseudoInterestingNode
  {A} {f} (branch _ _ _ _) (inBranch t either) =
  inBranch t ((Data.Sum.map rec rec) either)
  where
    rec : {l : List (Stm A)} → NILAnySeq PseudoInterestingNode l → NILAnySeq PseudoInterestingNode (unblockifySeq f l)
    rec {x ∷ _} (here p) = here (unblockify-keeps-PseudoInterestingNode x p)
    rec  (there xs) =
      there (rec xs)

unblockify-keeps-InterestingNode :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (f : A → Stm A)
  → (stm : Stm A)
  → (Any (InterestingNode cc) stm)
  → (Any (InterestingNode cc) (unblockifyStm f stm))
unblockify-keeps-InterestingNode
  _ _ (call a) p = p
unblockify-keeps-InterestingNode
  _ _ (assign a) p = p
unblockify-keeps-InterestingNode
  _ _ (return a expr) p = p
unblockify-keeps-InterestingNode
  _ _ (continue _) (here ())
unblockify-keeps-InterestingNode
  _ _ (break _) (here ())
unblockify-keeps-InterestingNode
  _ _ (block _) (here ())
unblockify-keeps-InterestingNode
  _ _ (loop _ _ _) (here ())
unblockify-keeps-InterestingNode
  _ _ (loop _ _ []) (inLoop _ ())
unblockify-keeps-InterestingNode
  cc f stm@(loop a guard (x ∷ xs))
  t@(inLoop _ (here p)) =
  (inLoop unit (here (unblockify-keeps-InterestingNode cc f x p)))
unblockify-keeps-InterestingNode
  cc f stm@(loop a guard (x ∷ xs))
  t@(inLoop _ (there p))
  with (unblockify-keeps-InterestingNode cc f (loop a guard xs) (inLoop _ p))
...| inLoop _ p' = (inLoop unit (there p'))
unblockify-keeps-InterestingNode
  cc f (branch a guard tc fc) (Any.here ())
unblockify-keeps-InterestingNode
  cc f (branch a guard [] [])
  (inBranch _ (inj₁ ()))
unblockify-keeps-InterestingNode
  cc f (branch a guard [] (y ∷ ys))
  (inBranch _ (inj₁ ()))
unblockify-keeps-InterestingNode
  cc f (branch a guard [] [])
  (inBranch _ (inj₂ ()))
unblockify-keeps-InterestingNode
  cc f (branch a guard (x ∷ xs) [])
  (inBranch _ (inj₂ ()))
{-#  CATCHALL #-}
unblockify-keeps-InterestingNode
  cc f (branch a guard tc (y ∷ ys))
  (inBranch t (inj₂ (here p))) =
  (inBranch
    unit
    (inj₂ (here
      (unblockify-keeps-InterestingNode cc f y p))))
{-#  CATCHALL #-}
unblockify-keeps-InterestingNode
  cc f (branch a guard tc (y ∷ ys))
  (inBranch t (inj₂ (there p)))
  with (unblockify-keeps-InterestingNode cc f (branch a guard tc ys) (inBranch _ (inj₂ p)))
...| inBranch _ (inj₂ p') =
  (inBranch unit (inj₂ (there p')))
...| inBranch _ (inj₁ p') =
  (inBranch unit (inj₁ p'))
{-#  CATCHALL #-}
unblockify-keeps-InterestingNode
  cc f
  (branch a guard (x ∷ xs) fc)
  (inBranch t (inj₁ (here p))) =
  (inBranch
    unit
    (inj₁
      (here
        (unblockify-keeps-InterestingNode cc f x p))))
{-#  CATCHALL #-}
unblockify-keeps-InterestingNode
  cc f
  (branch a guard (x ∷ xs) fc)
  (inBranch t (inj₁ (there p)))
  with (unblockify-keeps-InterestingNode cc f (branch a guard xs fc) (inBranch _ (inj₁ p)))
...| inBranch _ (inj₂ p') =
  (inBranch unit (inj₂ p'))
...| inBranch _ (inj₁ p') =
  (inBranch unit (inj₁ (there p')))

unblockify-keeps-Interesting :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (f : A → Stm A)
  → (stm : Stm A)
  → (Interesting cc stm)
  → Interesting cc (unblockifyStm f stm)
unblockify-keeps-Interesting cc f (call a) p = p
unblockify-keeps-Interesting cc f (assign a) p = p
unblockify-keeps-Interesting cc _ (return a expr) p = p
unblockify-keeps-Interesting _ _ (continue _) ()
unblockify-keeps-Interesting _ _ (break _) ()
unblockify-keeps-Interesting _ _ (block _) ()
unblockify-keeps-Interesting cc f stm@(loop _ _ _) t =
  any-interesting-node⇒interesting
    (unblockifyStm f stm)
    (unblockify-keeps-InterestingNode cc f stm t)
unblockify-keeps-Interesting cc f stm@(branch _ _ _ _) (inj₁ p) =
 any-interesting-node⇒interesting
    (unblockifyStm f stm)
    (unblockify-keeps-InterestingNode cc f stm p)
unblockify-keeps-Interesting cc f stm@(branch _ _ _ _) (inj₂ p) =
  inj₂ (unblockify-keeps-PseudoInterestingNode stm p)


blockify-keeps-PseudoInterestingNode :
  ∀ {A}
  → {cc : ContractionCtx A}
  → (stm : Stm A)
  → NILAny PseudoInterestingNode stm
  → NILAny PseudoInterestingNode
           (blockifyStm cc stm)
blockify-keeps-PseudoInterestingNode
  (call a) (here ())
blockify-keeps-PseudoInterestingNode
  (assign a) (here ())
blockify-keeps-PseudoInterestingNode
  (return _ _) (here ())
blockify-keeps-PseudoInterestingNode
  (continue _) (here p) =
  (here p)
blockify-keeps-PseudoInterestingNode
  (break _) (here p) =
  (here p)
blockify-keeps-PseudoInterestingNode
  {A} {f} (loop _ _ _) (here p) =
  ⊥-elim p
blockify-keeps-PseudoInterestingNode
  {A} {cc} stm@(branch _ _ tc fc) p@(inBranch t either)
  with lift (interesting cc stm)
...| no since , _ = ⊥-elim (since (inj₂ p))
...| yes since , interesting≡yes =
  transport (cong (NILAny PseudoInterestingNode) keeps)
            (inBranch unit (Data.Sum.map rec rec either))
  where
    keeps : blockifyStm cc stm
          ≡ branch _ _ (blockifySeq cc tc)
                       (blockifySeq cc fc)
    keeps = if-guard-subst ((cong isYes) interesting≡yes)
                           refl

    rec : {l : List (Stm A)}
        → NILAnySeq PseudoInterestingNode l
        → NILAnySeq PseudoInterestingNode (blockifySeq cc l)
    rec {x ∷ _} (here p) =
      here (blockify-keeps-PseudoInterestingNode x p)
    rec  (there xs) =
      there (rec xs)

blockify-keeps-InterestingNode :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (stm : Stm A)
  → Any (InterestingNode cc) stm
  → Any (InterestingNode cc) (blockifyStm cc stm)
blockify-keeps-InterestingNode cc (block xs) (here ())
blockify-keeps-InterestingNode cc (continue xs) (here ())
blockify-keeps-InterestingNode cc (break xs) (here ())
blockify-keeps-InterestingNode cc (return a _) p = p
blockify-keeps-InterestingNode cc stm@(call a) intr =
  transport (cong (Any (InterestingNode cc)) keeps)
            intr
  where
    keeps : blockifyStm cc stm ≡ stm
    keeps = if-guard-subst (interesting-node⇒isYes-interesting intr)
                           refl
blockify-keeps-InterestingNode cc stm@(assign a) intr =
  transport (cong (Any (InterestingNode cc)) keeps)
            intr
  where
    keeps : blockifyStm cc stm ≡ stm
    keeps = if-guard-subst (interesting-node⇒isYes-interesting intr)
                           refl
blockify-keeps-InterestingNode {A}
  cc stm@(loop a gurad (x ∷ xs))
  intr@(inLoop t in-body) =
  transport (cong (Any (InterestingNode cc)) keeps)
            (inLoop unit (rec (loopBody stm t) in-body))
  where
    keeps : blockifyStm cc stm ≡ (loop a gurad (blockifySeq cc (x ∷ xs)))
    keeps = if-guard-subst (interesting-node⇒isYes-interesting intr)
                           refl
    rec : (stms : List (Stm A))
        → List.Any (Any (InterestingNode cc))
                   stms →
         List.Any (Any (InterestingNode cc))
                  (blockifySeq cc stms)
    rec (stm ∷ _) (here p) =
      here (blockify-keeps-InterestingNode
                      cc stm p)
    rec (stm ∷ stms) (there rest) =
      there (rec stms rest)
blockify-keeps-InterestingNode {A}
  cc stm@(branch a gurad tc fc)
  intr@(inBranch t (inj₁ in-tc)) =
  transport (cong (Any (InterestingNode cc)) keeps)
            (inBranch unit (inj₁ (rec tc in-tc)))
  where
    keeps : blockifyStm cc stm
          ≡ branch a gurad (blockifySeq cc tc)
                            (blockifySeq cc fc)
    keeps = if-guard-subst (interesting-node⇒isYes-interesting intr)
                           refl
    rec : (stms : List (Stm A))
        → List.Any (Any (InterestingNode cc))
                   stms →
         List.Any (Any (InterestingNode cc))
                  (blockifySeq cc stms)
    rec (stm ∷ _) (here p) =
      here (blockify-keeps-InterestingNode
                      cc stm p)
    rec (stm ∷ stms) (there rest) =
      there (rec stms rest)
blockify-keeps-InterestingNode {A}
  cc stm@(branch a gurad tc fc)
  intr@(inBranch t (inj₂ in-fc)) =
  transport (cong (Any (InterestingNode cc)) keeps)
            (inBranch unit (inj₂ (rec fc in-fc)))
  where
    keeps : blockifyStm cc stm
          ≡ branch a gurad (blockifySeq cc tc)
                            (blockifySeq cc fc)
    keeps = if-guard-subst (interesting-node⇒isYes-interesting intr)
                           refl
    rec : (stms : List (Stm A))
        → List.Any (Any (InterestingNode cc))
                   stms →
         List.Any (Any (InterestingNode cc))
                  (blockifySeq cc stms)
    rec (stm ∷ _) (here p) =
      here (blockify-keeps-InterestingNode
                      cc stm p)
    rec (stm ∷ stms) (there rest) =
      there (rec stms rest)


blockify-keeps-Interesting :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (stm : Stm A)
  → Interesting cc stm
  → Interesting cc (blockifyStm cc stm)
blockify-keeps-Interesting cc (block xs) ()
blockify-keeps-Interesting cc (continue xs) ()
blockify-keeps-Interesting cc (break xs) ()
blockify-keeps-Interesting cc (return a _) p = p
blockify-keeps-Interesting cc (call a) intr =
  transport (cong (Interesting cc) keeps)
            intr
  where
      keeps : blockifyStm cc (call a) ≡ (call a)
      keeps = cong (λ x → if isYes x then call a else  block NE.[ a ])
                   (proj₂ (must-be-interesting-stm cc (call a) intr))
blockify-keeps-Interesting cc (assign a) intr =
  transport (cong (Interesting cc) keeps)
            intr
  where
      keeps : blockifyStm cc (assign a) ≡ (assign a)
      keeps = cong (λ x → if isYes x then assign a else  block NE.[ a ])
                   (proj₂ (must-be-interesting-stm cc (assign a) intr))
blockify-keeps-Interesting
  {A} cc
  stm@(loop a guard body)
  intr@(inLoop t in-body) =
  any-interesting-node⇒interesting
    (blockifyStm cc stm)
    (blockify-keeps-InterestingNode cc stm intr)
blockify-keeps-Interesting  cc stm@(branch _ _ _ _) (inj₁ intr) =
  any-interesting-node⇒interesting
    (blockifyStm cc stm)
    (blockify-keeps-InterestingNode cc stm intr)
blockify-keeps-Interesting
  cc stm@(branch _ _ tc fc) (inj₂ intr)
  with lift (interesting cc stm)
...| no since , _ =
  ⊥-elim (since (inj₂ intr))
...| yes _ , interesting≡yes =
  transport (cong (Interesting cc) keeps)
            (inj₂ still-any)
  where
    keeps : blockifyStm cc stm
         ≡ (branch _ _ (blockifySeq cc tc)
                        (blockifySeq cc fc))
    keeps = if-guard-subst ((cong isYes) interesting≡yes)
                           refl
    still-any : NILAny PseudoInterestingNode
                  (branch _ _ (blockifySeq cc tc)
                              (blockifySeq cc fc))
    still-any = transport (cong (NILAny PseudoInterestingNode) (sym keeps))
                    (blockify-keeps-PseudoInterestingNode stm intr)

-- Monstly unused lemma, keeping it just in case
blockify-holds-NewlyBlockified :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (f : A → Stm A)
  → (seq : List (Stm A))
  → AllSeq (UnblockifiedStm f) seq
  → AllSeq (λ stm → NewlyBlockifiedStm cc f stm
                  × numAnnots stm ≡ 1
                  × (blockifyStm cc (f (firstAnnot stm)) ≡ stm)
           )
           (blockifySeq cc seq)
blockify-holds-NewlyBlockified cc f [] [] = []
blockify-holds-NewlyBlockified
 cc f
 (stm@(block xs) ∷ rest)
 ((leaf t (numAnnots≡1 , self-translated)) ∷ rest-prop) =
 (leaf t ( inj₁ self-translated
             , numAnnots≡1
             , cong (blockifyStm cc) self-translated))
 ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop)
blockify-holds-NewlyBlockified cc f
              (stm@(call a) ∷ rest)
              ((leaf t (numAnnots≡1 , self-translated)) ∷ rest-prop)
              with lift (interesting cc (call a))
... | (yes since , interesting?≡yes) =
 ((leaf (transport (cong IsLeaf blockify≡id) unit)
           ( transport (cong (NewlyBlockifiedStm cc f) blockify≡id)
                       since
           , (trans (cong numAnnots  blockify≡id) numAnnots≡1)
           , trans (cong (λ z → (blockifyStm cc (f (firstAnnot z))))
                        blockify≡id)
                   (cong (blockifyStm cc) self-translated)

           ))
 ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
 where
  blockify≡id : (blockifyStm cc (call a)) ≡ call a
  blockify≡id =
    cong (λ x → if isYes x then (call a) else block NE.[ a ])
         interesting?≡yes

... | (no since , interesting?≡no) =
 ((leaf (transport (cong IsLeaf blockify≡block) unit)
            ( transport (cong (NewlyBlockifiedStm cc f) blockify≡block)
                        (inj₂ (transport same-absurdity ¬interesting))
            , (trans (cong numAnnots  blockify≡block) numAnnots≡1)
            , trans  (trans (cong (λ z → (blockifyStm cc (f z))) refl)
                           (cong (λ z → (blockifyStm cc (f (firstAnnot z))))
                                 blockify≡block))
                    (cong (blockifyStm cc) self-translated)
            ))
 ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
 where
  ¬interesting : ¬ (Interesting cc (call a))
  ¬interesting = since
  same-absurdity : ¬_ (Interesting cc (f (firstAnnot (block  NE.[ a ]))))
      ≡ ¬_ (Interesting cc (call a))
  same-absurdity = cong (¬_ ∘ (Interesting cc))
                        self-translated
  blockify≡block : (blockifyStm cc (call a)) ≡ block NE.[ a ]
  blockify≡block =
    cong (λ x → if isYes x then (call a) else block NE.[ a ])
         interesting?≡no

blockify-holds-NewlyBlockified cc f
              (stm@(assign a) ∷ rest)
              ((leaf t (numAnnots≡1 , self-translated)) ∷ rest-prop)
              with lift (interesting cc (assign a))
... | (yes since , interesting?≡yes) =
 ((leaf (transport (cong IsLeaf blockify≡id) unit)
           ( transport (cong (NewlyBlockifiedStm cc f) blockify≡id)
                       since
           , (trans (cong numAnnots  blockify≡id) numAnnots≡1)
           , trans (cong (λ z → (blockifyStm cc (f (firstAnnot z))))
                      blockify≡id)
                   (cong (blockifyStm cc) self-translated)
           ))
 ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
 where
  blockify≡id : (blockifyStm cc (assign a)) ≡ assign a
  blockify≡id =
    cong (λ x → if isYes x then (assign a) else block NE.[ a ])
         interesting?≡yes
... | (no since , interesting?≡no) =
 ((leaf (transport (cong IsLeaf blockify≡block) unit)
            ( transport (cong (NewlyBlockifiedStm cc f) blockify≡block)
                       (inj₂ (transport same-absurdity ¬interesting))
            , (trans (cong numAnnots  blockify≡block) numAnnots≡1)
            , trans (trans (cong (λ z → (blockifyStm cc (f z))) refl)
                           (cong (λ z → (blockifyStm cc (f (firstAnnot z))))
                                 blockify≡block))
                    (cong (blockifyStm cc) self-translated)
            ))
 ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))

 where
  ¬interesting : ¬ (Interesting cc (assign a))
  ¬interesting = since
  ¬any-interesting : ¬ (Any (InterestingNode cc) (assign a))
  ¬any-interesting = λ { (Any.here p) → since p }
  same-absurdity : ¬_ (Interesting cc (f (firstAnnot (block  NE.[ a ]))))
      ≡ ¬_ (InterestingNode cc (assign a))
  same-absurdity = cong (¬_ ∘ (Interesting cc))
                        self-translated
  blockify≡block : (blockifyStm cc (assign a)) ≡ block NE.[ a ]
  blockify≡block =
    cong (λ x → if isYes x then (assign a) else block NE.[ a ])
         interesting?≡no
blockify-holds-NewlyBlockified cc f
              (break a ∷ rest)
              ((leaf t (numAnnots≡1
                           , self-translated)) ∷ rest-prop) =
 ((leaf t ( unit , numAnnots≡1 , cong (blockifyStm cc) self-translated))
 ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
blockify-holds-NewlyBlockified cc f
              (return a _ ∷ rest)
              ((leaf t (numAnnots≡1 , s-trans)) ∷ rest-prop) =
 ((leaf t (unit , numAnnots≡1 , cong (blockifyStm cc) s-trans))
 ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
blockify-holds-NewlyBlockified cc f
              (continue a ∷ rest)
              ((leaf t (numAnnots≡1
                           , self-translated))
               ∷ rest-prop) =
 ((leaf t( unit , numAnnots≡1 , cong (blockifyStm cc) self-translated))
 ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
blockify-holds-NewlyBlockified {A} cc f
              (stm@(loop a guard body) ∷ rest)
              ((loop t (numAnnots≡1
                           , self-translated)
                           body-props)
               ∷ rest-prop)
               with lift (interesting cc stm)
...| (yes since , interesitng?≡yes) =
      (transport (sym all-is-same)
                  (loop
                    unit
                    ( keeps-any
                    , numAnnots≡1
                    ,  (begin
                        (blockifyStm cc (f (firstAnnot (loop a guard (blockifySeq cc body)))))
                      ≡⟨ cong (blockifyStm cc ∘ f) refl ⟩
                         (blockifyStm cc (f (firstAnnot (loop a guard body))))
                      ≡⟨ cong (blockifyStm cc) self-translated ⟩
                        (blockifyStm cc (loop a guard body))
                      ≡⟨ blockified-is-loop ⟩
                        loop a guard (blockifySeq cc body)
                      ∎)

                      )
                    (blockify-holds-NewlyBlockified cc f body body-props)
                  )
        ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
    where
      blockified-is-loop : (if isYes (interesting cc stm)
                            then loop a guard (blockifySeq cc body)
                            else block NE.[ a ])
                         ≡ (loop a guard (blockifySeq cc body))
      blockified-is-loop =
        cong (λ x →
                if isYes x
                then loop a guard (blockifySeq cc body)
                else block NE.[ a ])
             interesitng?≡yes
      all-is-same : All (λ stm → NewlyBlockifiedStm cc f stm
                               × numAnnots stm ≡ 1
                               × blockifyStm cc (f (firstAnnot stm)) ≡ stm)
                        (loop a guard (blockifySeq cc body))
                 ≡ All (λ stm → NewlyBlockifiedStm cc f stm
                               × numAnnots stm ≡ 1
                               × blockifyStm cc (f (firstAnnot stm)) ≡ stm
                               )
                        (if isYes (interesting cc stm)
                         then loop a guard (blockifySeq cc body)
                         else block NE.[ a ])
      all-is-same =
        cong (All (λ stm → NewlyBlockifiedStm cc f stm
                         × numAnnots stm ≡ 1
                         × blockifyStm cc (f (firstAnnot stm)) ≡ stm))
             (sym blockified-is-loop)

      blockify≡keep : blockifyStm cc stm
                      ≡ loop a guard (blockifySeq cc body)
      blockify≡keep =
        cong (λ x →
                if isYes x
                then loop a guard (blockifySeq cc body)
                else block NE.[ a ])
             interesitng?≡yes
      keeps-any : InterestingLoop cc (loop a guard (blockifySeq cc body))
      keeps-any = transport (sym (cong (Interesting cc) blockify≡keep))
                            (blockify-keeps-Interesting cc stm since)

...| (no since , interesting≡no) =
 ( transport all-is-block
             (leaf unit
                       ( inj₂ (transport same-absurdity since)
                       , refl
                       , (sym (begin
                            block NE.[ a ]
                          ≡⟨ sym blockify≡block ⟩
                            blockifyStm cc (loop a guard body)
                          ≡⟨ cong (blockifyStm cc) (sym self-translated) ⟩
                            blockifyStm cc (f (firstAnnot (loop a guard body)))
                          ≡⟨ cong (blockifyStm cc ∘ f) refl ⟩
                            blockifyStm cc (f (firstAnnot (block NE.[ a ])))
                          ∎
                         ))
                       ))
  ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
 where
   same-absurdity : ¬_ (Interesting cc
                            (f (firstAnnot (block  NE.[ a ]))))
                  ≡ ¬_ (Interesting cc
                             (loop a guard body))
   same-absurdity = cong (¬_ ∘ (Interesting cc))
                         self-translated
   blockified : Stm A
   blockified = block NE.[ a ]
   blockify≡block : blockifyStm cc stm
                   ≡ blockified
   blockify≡block =
        cong (λ x →
                if isYes x
                then loop a guard (blockifySeq cc body)
                else block NE.[ a ])
             interesting≡no
   all-is-block : All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm))) ≡ stm)
                      (blockifyStm cc stm)
                ≡ All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm)) ≡ stm))
                       (block NE.[ a ])
   all-is-block =
      cong (All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm))) ≡ stm))
           blockify≡block
blockify-holds-NewlyBlockified {A} cc f
              (stm@(branch a guard tc fc) ∷ rest)
              ((branch t (numAnnots≡1 , self-translated) tc-props fc-props)
               ∷ rest-prop)
               with lift (interesting cc stm)
...| (yes since , interesitng?≡yes) =
       (transport (sym all-is-same)
                  (branch
                    unit
                    ( (transport (sym (cong (Interesting cc) blockify≡keep))
                                 (blockify-keeps-Interesting cc stm since))
                    , numAnnots≡1
                    , (sym (begin
                        branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
                      ≡⟨ sym blockified-is-branch ⟩
                        blockifyStm cc stm
                      ≡⟨ cong (blockifyStm cc) (sym self-translated) ⟩
                        blockifyStm cc (f (firstAnnot stm))
                      ≡⟨ cong (blockifyStm cc ∘ f) refl ⟩
                        blockifyStm cc
                                     (f (firstAnnot (branch a guard (blockifySeq cc tc)
                                                                     (blockifySeq cc fc))))
                      ∎))
                    )
                    (blockify-holds-NewlyBlockified cc f tc tc-props)
                    (blockify-holds-NewlyBlockified cc f fc fc-props)
                  )
        ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
    where
      blockified-is-branch : (if isYes (interesting cc stm)
                            then branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
                            else block NE.[ a ])
                         ≡ branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
      blockified-is-branch =
        cong (λ x →
                if isYes x
                then branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
                else block NE.[ a ])
             interesitng?≡yes
      all-is-same : All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm)) ≡ stm))
                 (branch a guard (blockifySeq cc tc) (blockifySeq cc fc))
          ≡ All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm)) ≡ stm))
                 (if isYes (interesting cc stm)
                  then branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
                  else block NE.[ a ])
      all-is-same =
        cong (All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm)) ≡ stm)))
             (sym blockified-is-branch)

      blockify≡keep : blockifyStm cc stm
                      ≡ branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
      blockify≡keep =
        cong (λ x →
                if isYes x
                then branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
                else block NE.[ a ])
             interesitng?≡yes
...| (no since , interesting≡no) =
 ( transport all-is-block
             (leaf unit
                       ( inj₂ (transport (sym (cong (¬_ ∘ Interesting cc) (sym self-translated)))
                                         since)
                       , refl
                       , (begin
                           blockifyStm cc (f (firstAnnot (block NE.[ a ])))
                          ≡⟨ cong (blockifyStm cc ∘ f) refl ⟩
                            blockifyStm cc (f (firstAnnot stm))
                          ≡⟨ cong (blockifyStm cc) self-translated ⟩
                            blockifyStm cc stm
                          ≡⟨ blockify≡block ⟩
                            block NE.[ a ]
                          ∎
                         )
                       ))
  ∷ (blockify-holds-NewlyBlockified cc f rest rest-prop))
 where
   blockified : Stm A
   blockified = block NE.[ a ]
   blockify≡block : blockifyStm cc stm
                   ≡ blockified
   blockify≡block =
        cong (λ x →
                if isYes x
                then branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
                else block NE.[ a ])
             interesting≡no
   all-is-block : All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm)) ≡ stm))
                      (blockifyStm cc stm)
                ≡ All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm)) ≡ stm))
                       (block NE.[ a ])
   all-is-block =
      cong (All (λ stm → NewlyBlockifiedStm cc f stm
                             × numAnnots stm ≡ 1
                             × (blockifyStm cc (f (firstAnnot stm)) ≡ stm)))
           blockify≡block

blockify-holds-All-BlockifiedStm :
  ∀ {A}
  → {cc : ContractionCtx A}
  → (seq : List (Stm A))
  → AllSeq (BlockifiedStm cc)
           (blockifySeq cc seq)
blockify-holds-All-BlockifiedStm [] =
  []
blockify-holds-All-BlockifiedStm
  {cc = cc} (stm@(block _) ∷ rest) =
  (leaf unit unit) ∷ blockify-holds-All-BlockifiedStm rest
blockify-holds-All-BlockifiedStm
  {cc = cc} (stm@(call _) ∷ rest)
  with (interesting cc stm)
...| yes p = (leaf unit p) ∷ blockify-holds-All-BlockifiedStm rest
...| no _ = (leaf unit unit) ∷ blockify-holds-All-BlockifiedStm rest
blockify-holds-All-BlockifiedStm
  {cc = cc} (stm@(assign _) ∷ rest)
  with (interesting cc stm)
...| yes p = (leaf unit p) ∷ blockify-holds-All-BlockifiedStm rest
...| no _ = (leaf unit unit) ∷ blockify-holds-All-BlockifiedStm rest
blockify-holds-All-BlockifiedStm
  {cc = cc} (stm@(continue _) ∷ rest) =
  (leaf unit unit) ∷ blockify-holds-All-BlockifiedStm rest
blockify-holds-All-BlockifiedStm
  {cc = cc} (stm@(break _) ∷ rest) =
  (leaf unit unit) ∷ blockify-holds-All-BlockifiedStm rest
blockify-holds-All-BlockifiedStm
  {cc = cc} (stm@(return _ _) ∷ rest) =
  (leaf unit unit) ∷ blockify-holds-All-BlockifiedStm rest
blockify-holds-All-BlockifiedStm
  {cc = cc} (stm@(loop a guard body) ∷ rest)
  with (interesting cc stm)
...| yes p =
  (loop unit
            (transport (sym (cong (Any (InterestingNode cc)) eval≡))
                       keeps-intr)
            (blockify-holds-All-BlockifiedStm body))
  ∷ blockify-holds-All-BlockifiedStm rest
  where
    eval≡ : (blockifyStm cc stm) ≡ loop a guard (blockifySeq cc body)
    eval≡ = if-guard-subst (isYes-interesting≡true stm p)
                            refl
    keeps-intr : Any (InterestingNode cc) (blockifyStm cc stm)
    keeps-intr = (blockify-keeps-InterestingNode cc stm  p)
...| no _  = (leaf unit unit) ∷ blockify-holds-All-BlockifiedStm rest
blockify-holds-All-BlockifiedStm
  {cc = cc} (stm@(branch a guard tc fc) ∷ rest)
  with (interesting cc stm) | Eq.inspect (interesting cc) stm
...| yes p | Eq.[ same ] =
  (branch unit
            (transport (sym (cong (Interesting cc) eval≡)) keeps-intr)
            (blockify-holds-All-BlockifiedStm tc)
            (blockify-holds-All-BlockifiedStm fc))
  ∷ blockify-holds-All-BlockifiedStm rest
  where
    eval≡ : (blockifyStm cc stm)
          ≡ (branch a guard (blockifySeq cc tc) (blockifySeq cc fc))
    eval≡ = if-guard-subst (isYes-interesting≡true stm p)
                            refl
    keeps-intr : Interesting cc (blockifyStm cc stm)
    keeps-intr = (blockify-keeps-Interesting cc stm  p)

...| no _ | _ = (leaf unit unit) ∷ blockify-holds-All-BlockifiedStm rest

blockifySeq-idempotent :
  ∀ {A}
  → {cc : ContractionCtx A}
  → (seq : List (Stm A))
  → AllSeq (BlockifiedStm cc)
           seq
  → blockifySeq cc seq ≡ seq
blockifySeq-idempotent [] _ = refl
blockifySeq-idempotent
  { cc = cc } seq@(stm@(call _) ∷ rest)
  (leaf _ is-interesting
    ∷ rest-is-blockified) =
  begin
    blockifySeq cc seq
  ≡⟨ cong (_∷ _)
          (if-guard-subst
            (cong isYes
                  (proj₂ (must-be-interesting-stm
                           cc stm is-interesting)))
            refl)
    ⟩
   stm ∷ blockifySeq cc rest
  ≡⟨ cong (_ ∷_) (blockifySeq-idempotent rest rest-is-blockified) ⟩
    seq
  ∎
blockifySeq-idempotent
  { cc = cc } seq@(stm@(assign _) ∷ rest)
  (leaf _ is-interesting
    ∷ rest-is-blockified) =
  begin
    blockifySeq cc seq
  ≡⟨ cong (_∷ _)
          (if-guard-subst
            (cong isYes
                  (proj₂ (must-be-interesting-stm
                           cc stm is-interesting)))
            refl)
    ⟩
   stm ∷ blockifySeq cc rest
  ≡⟨ cong (_ ∷_) (blockifySeq-idempotent rest rest-is-blockified) ⟩
    seq
  ∎
blockifySeq-idempotent
  { cc = cc } seq@(stm@(continue _) ∷ rest)
  ((leaf _ is-interesting) ∷ rest-is-blockified)
  = cong (_ ∷_) (blockifySeq-idempotent rest rest-is-blockified)
blockifySeq-idempotent
  { cc = cc} seq@(stm@(break _) ∷ rest)
  ((leaf _ is-interesting) ∷ rest-is-blockified)
  = cong (_ ∷_) (blockifySeq-idempotent rest rest-is-blockified)
blockifySeq-idempotent
  { cc = cc} seq@(stm@(return _ _) ∷ rest)
  ((leaf _ is-interesting) ∷ rest-is-blockified)
  = cong (_ ∷_) (blockifySeq-idempotent rest rest-is-blockified)
blockifySeq-idempotent
  { cc = cc} seq@(stm@(block _) ∷ rest)
  ((leaf _ unit) ∷ rest-is-blockified)
  = cong (_ ∷_) (blockifySeq-idempotent rest rest-is-blockified)
blockifySeq-idempotent
  { cc = cc} seq@(stm@(loop a guard body) ∷ rest)
  ((loop _ is-interesting children-blockified)
    ∷ rest-is-blockified)
  =
  begin
    blockifySeq cc seq
  ≡⟨ cong (_∷ _)
          (if-guard-subst
            (cong isYes
                  (proj₂ (must-be-interesting-stm
                           cc stm is-interesting)))
            refl)
    ⟩
   loop a guard (blockifySeq cc body) ∷ blockifySeq cc rest
  ≡⟨ cong (_ ∷_) (blockifySeq-idempotent rest rest-is-blockified) ⟩
    loop a guard (blockifySeq cc body) ∷ rest
  ≡⟨ cong (λ z → (loop a guard z) ∷ _)
          (blockifySeq-idempotent body children-blockified)
    ⟩
    seq
  ∎
blockifySeq-idempotent
  { cc = cc} seq@(stm@(branch a guard tc fc) ∷ rest)
  ((branch _ is-interesting
                 tc-children-blockified
                 fc-children-blockified)
    ∷ rest-is-blockified)
  =
  begin
    blockifySeq cc seq
  ≡⟨ cong (_∷ _)
          (if-guard-subst
            (cong isYes
                  (proj₂ (must-be-interesting-stm
                           cc stm is-interesting)))
            refl)
    ⟩
   branch a guard (blockifySeq cc tc) (blockifySeq cc fc)
    ∷ blockifySeq cc rest
  ≡⟨ cong (_ ∷_) (blockifySeq-idempotent rest rest-is-blockified) ⟩
    branch a guard (blockifySeq cc tc) (blockifySeq cc fc) ∷ rest
  ≡⟨ cong₂ (λ z₁ z₂ → (branch a guard z₁ z₂) ∷ _)
          (blockifySeq-idempotent tc tc-children-blockified)
          (blockifySeq-idempotent fc fc-children-blockified)
    ⟩
    seq
  ∎

---- Proof that putting the boring things into blocks does not lose any
---- infomration.
unblockify∘blockify≡id :
  ∀ {A}
  → (f : A → Stm A)
  → (cc : ContractionCtx A)
  → (seq : List (Stm A))
  → AllSeq (UnblockifiedStm f) seq
  → (unblockifySeq f ∘ blockifySeq cc) seq ≡ seq
unblockify∘blockify≡id _ _ [] _ = refl
unblockify∘blockify≡id
 f cc
 seq@(stm@(continue _) ∷ rest)
 (_ ∷ rest-props) =
 cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props)
unblockify∘blockify≡id
 f cc
 seq@(stm@(break _) ∷ rest)
 (_ ∷ rest-props) =
 cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props)
unblockify∘blockify≡id
 f cc
 seq@(stm@(return _ _) ∷ rest)
 (_ ∷ rest-props) =
 cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props)
unblockify∘blockify≡id
 f cc
 seq@(stm@(assign a) ∷ rest)
 ((leaf _ stm-props) ∷ rest-props)
 with lift (interesting cc stm)
... | (yes since , interesting≡yes) =
   begin
     unblockifySeq f (blockifySeq cc seq)
   ≡⟨ cong (λ x → unblockifySeq f ((if isYes x
                                          then assign a
                                          else block NE.[ a ])
                                         ∷ blockifySeq cc rest))
            interesting≡yes
     ⟩
     unblockifySeq f ((assign a) ∷ blockifySeq cc rest)
   ≡⟨ cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props) ⟩
     seq
   ∎
... | (no since , interesting≡no)  =
   begin
     unblockifySeq f (blockifySeq cc seq)
   ≡⟨ cong (λ x → unblockifySeq
                     f
                     ((if isYes x
                       then assign a
                       else block NE.[ a ])
                     ∷ blockifySeq cc rest))
            interesting≡no
     ⟩
     unblockifySeq f ((block NE.[ a ])
                              ∷ blockifySeq cc rest)
   ≡⟨⟩
     f a ∷ unblockifySeq f (blockifySeq cc rest)
   ≡⟨ cong (f a ∷_)
            (unblockify∘blockify≡id
              f cc rest rest-props
              )
     ⟩
     f a ∷ rest
   ≡⟨ cong (_∷ rest) (proj₂ stm-props) ⟩
     seq
   ∎
unblockify∘blockify≡id f cc
                       seq@(stm@(call a) ∷ rest)
                       ((leaf _ stm-props) ∷ rest-props)
                       with lift (interesting cc stm)
... | (yes since , interesting≡yes) =
   begin
     unblockifySeq f (blockifySeq cc seq )
   ≡⟨ cong (λ x → unblockifySeq f ((if isYes x
                                             then call a
                                             else block NE.[ a ])
                                           ∷ blockifySeq cc rest))
            interesting≡yes
     ⟩
     unblockifySeq f ((call a) ∷ blockifySeq cc rest)
   ≡⟨ cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props) ⟩
     seq
   ∎
... | (no since , interesting≡no) =
   begin
     unblockifySeq f (blockifySeq cc seq )
   ≡⟨ cong (λ x → unblockifySeq f ((if isYes x
                                          then call a
                                          else block NE.[ a ])
                                         ∷ blockifySeq cc rest))
            interesting≡no
     ⟩
     unblockifySeq f ((block NE.[ a ]) ∷ blockifySeq cc rest)
   ≡⟨⟩
     f a ∷ unblockifySeq f (blockifySeq cc rest)
   ≡⟨ cong (f a ∷_)
            (unblockify∘blockify≡id
              f cc rest rest-props
              )
     ⟩
     f a ∷ rest
   ≡⟨ cong (_∷ rest) (proj₂ stm-props) ⟩
     seq
   ∎
unblockify∘blockify≡id f cc
                       seq@(stm@(block (x NE.∷ [])) ∷ rest)
                       ((leaf _ stm-props) ∷ rest-props) =
 begin
   (unblockifySeq f (blockifySeq cc seq))
 ≡⟨⟩
   (unblockifySeq f (stm ∷ (blockifySeq cc rest)))
 ≡⟨⟩
   (f x ∷ unblockifySeq f (blockifySeq cc rest))
 ≡⟨ cong (f x ∷_)
          (unblockify∘blockify≡id
            f cc rest rest-props)
   ⟩
    (f x ∷ rest)
 ≡⟨ cong (_∷ rest) (proj₂ stm-props) ⟩
   seq
 ∎
unblockify∘blockify≡id
 f cc
 seq@(stm@(block annots@(x₁ NE.∷ x₂ ∷ _)) ∷ rest)
 ((leaf _ stm-props) ∷ rest-props) =
 contradiction (proj₁ stm-props) (λ ())
unblockify∘blockify≡id
 f cc
 seq@(stm@(loop a guard body) ∷ rest)
 ((loop _ stm-props body-props) ∷ rest-props)
 with excluded-middle (isYes (interesting cc stm))
... | inj₁ interesting≡true =
 begin
   unblockifySeq f (blockifyStm cc stm
                           ∷ blockifySeq cc rest)
 ≡⟨ cong (λ x →
            unblockifySeq
              f
              ((if x
                then loop a guard (blockifySeq cc body)
                else block NE.[ a ]
                ) ∷ blockifySeq cc rest
              ))
          interesting≡true
   ⟩
   unblockifySeq f
     ((loop a guard (blockifySeq cc body))
      ∷ blockifySeq cc rest)
 ≡⟨⟩
   (loop a guard
           ((unblockifySeq f ∘ blockifySeq cc)
              body))
   ∷ unblockifySeq f (blockifySeq cc rest)
 ≡⟨⟩
   (loop a guard
         (unblockifySeq f (
           blockifySeq cc body
         ))) ∷ unblockifySeq f (blockifySeq cc rest)
 ≡⟨ cong (λ x →
            (loop a guard x)
            ∷ unblockifySeq f (blockifySeq cc rest))
          (unblockify∘blockify≡id f cc body body-props)
   ⟩
   (loop a guard body) ∷ unblockifySeq f (blockifySeq cc rest)
 ≡⟨ cong ((loop a guard body) ∷_)
          (unblockify∘blockify≡id
            f cc rest rest-props)
     ⟩
   seq
 ∎
... | inj₂ interesting≡false =
 begin
   unblockifySeq f (blockifyStm cc stm ∷ blockifySeq cc rest)
 ≡⟨ cong (λ x →
            unblockifySeq
              f
              ((if x
                then loop a guard (blockifySeq cc body)
                else block NE.[ a ]
                ) ∷ blockifySeq cc rest))
          interesting≡false
   ⟩
   unblockifySeq f ( (block NE.[ a ]) ∷ blockifySeq cc rest)
 ≡⟨ cong (f a ∷_)
          (unblockify∘blockify≡id
            f cc rest rest-props)
     ⟩
    (f a ∷ rest)
 ≡⟨ cong (_∷ rest) (proj₂ stm-props) ⟩
   seq
 ∎
unblockify∘blockify≡id
 f cc
 seq@(stm@(branch a guard tc fc) ∷ rest)
 ((branch _ stm-props tc-props fc-props) ∷ rest-props)
 with excluded-middle (isYes (interesting cc stm))
... | inj₁ interesting≡true =
 begin
   unblockifySeq f (blockifyStm cc stm ∷ blockifySeq cc rest)
 ≡⟨ cong (λ x →
            unblockifySeq
              f
              ((if x
                then branch a guard (blockifySeq cc tc)
                                    (blockifySeq cc fc)
                else block NE.[ a ]
                ) ∷ blockifySeq cc rest
              )
          )
          interesting≡true
   ⟩
   (branch a guard
             (unblockifySeq f (
               blockifySeq cc tc))
             (unblockifySeq f (
               blockifySeq cc fc))
   )
   ∷ unblockifySeq f (blockifySeq cc rest)

 ≡⟨ cong (λ x →
            (branch a guard x (unblockifySeq f (blockifySeq cc fc)))
            ∷ unblockifySeq f (blockifySeq cc rest))
          (unblockify∘blockify≡id f cc tc tc-props)
   ⟩
   (branch a guard tc (unblockifySeq f (blockifySeq cc fc)))
   ∷ unblockifySeq f (blockifySeq cc rest)
 ≡⟨ cong (λ x →
            (branch a guard tc x)
            ∷ unblockifySeq f (blockifySeq cc rest))
          (unblockify∘blockify≡id f cc fc fc-props)
   ⟩
   (branch a guard tc fc)
   ∷ unblockifySeq f (blockifySeq cc rest)
 ≡⟨ cong ((branch a guard tc fc) ∷_)
          (unblockify∘blockify≡id
            f cc rest rest-props)
     ⟩
   seq
 ∎
... | inj₂ interesting≡false =
 begin
   unblockifySeq f (blockifyStm cc stm
                           ∷ blockifySeq cc rest)
 ≡⟨ cong (λ x →
            unblockifySeq
              f
              ((if x
                then branch
                       a guard
                       (blockifySeq cc tc)
                       (blockifySeq cc fc)
                else block NE.[ a ]
                ) ∷ blockifySeq cc rest
              )

          )
          interesting≡false
   ⟩
   unblockifySeq f ( (block NE.[ a ]) ∷ blockifySeq cc rest)
 ≡⟨ cong (f a ∷_)
          (unblockify∘blockify≡id
            f cc rest rest-props)
     ⟩
    (f a ∷ rest)
 ≡⟨ cong (_∷ rest) (proj₂ stm-props) ⟩
   seq
 ∎

blockify∘unblockify≡id :
  ∀ {A}
  → (f : A → Stm A)
  → (cc : ContractionCtx A)
  → (seq : List (Stm A))
  → AllSeq (λ stm → (NewlyBlockifiedStm cc f stm)
                  × (numAnnots stm ≡ 1)
                  × (blockifyStm cc (f (firstAnnot stm)) ≡ stm)
                  )
           seq
  → (blockifySeq cc ∘ unblockifySeq f) seq ≡ seq
blockify∘unblockify≡id _ _ [] _  = refl
blockify∘unblockify≡id f cc
                       seq@(stm@(continue _) ∷ rest)
                       (_ ∷ rest-props) =
 cong (stm ∷_) (blockify∘unblockify≡id f cc  rest rest-props)
blockify∘unblockify≡id f cc
                       seq@(stm@(break _) ∷ rest)
                       (_ ∷ rest-props) =
 cong (stm ∷_) (blockify∘unblockify≡id f cc  rest rest-props)
blockify∘unblockify≡id f cc
                       seq@(stm@(return _ _) ∷ rest)
                       (_ ∷ rest-props) =
 cong (stm ∷_) (blockify∘unblockify≡id f cc  rest rest-props)
blockify∘unblockify≡id f cc
                       seq@(stm@(call a) ∷ rest)
                       (leaf _ (any-interesting
                                   , _ , _) ∷ rest-props)
                      with lift (interesting cc stm)
...| (yes since , interesting≡yes) =
 begin
   blockifySeq cc (unblockifySeq f seq)
 ≡⟨⟩
   blockifySeq cc (call a ∷ (unblockifySeq f rest))
 ≡⟨⟩
   (blockifyStm cc (call a) ∷ blockifySeq cc (unblockifySeq f rest))
 ≡⟨ cong (blockifyStm cc (call a) ∷_)
          (blockify∘unblockify≡id f cc  rest rest-props)
   ⟩
   (blockifyStm cc (call a) ∷ rest)
 ≡⟨ cong (λ x → (if isYes x then call a else block NE.[ a ])  ∷ rest)
          interesting≡yes
   ⟩
   seq
 ∎
...| no since , interesting≡false =
 contradiction any-interesting since
blockify∘unblockify≡id f cc
                       seq@(stm@(assign a) ∷ rest)
                       (leaf _ (any-interesting
                                   , _) ∷ rest-props)
                      with lift (interesting cc stm)
...| (yes since , interesting≡yes) =
 begin
   blockifySeq cc (unblockifySeq f seq)
 ≡⟨⟩
   blockifySeq cc (assign a ∷ (unblockifySeq f rest))
 ≡⟨⟩
   (blockifyStm cc (assign a) ∷ blockifySeq cc (unblockifySeq f rest))
 ≡⟨ cong (blockifyStm cc (assign a) ∷_)
          (blockify∘unblockify≡id f cc  rest rest-props)
   ⟩
   (blockifyStm cc (assign a) ∷ rest)
 ≡⟨ cong (λ x → (if isYes x then assign a else block NE.[ a ])  ∷ rest)
          interesting≡yes
   ⟩
   seq
 ∎
...| no since , interesting≡false =
 contradiction any-interesting since
blockify∘unblockify≡id f cc
                       seq@(stm@(loop a guard body) ∷ rest)
                       ((loop _
                                 (any-interesting
                                 , _)
                                 body-props)
                        ∷ rest-props)
                      with lift (interesting cc stm)
...| (no since , interesting≡false) =
 contradiction any-interesting since
...| (yes since , interesting≡yes) =
 begin
   blockifySeq cc (unblockifySeq f seq)
 ≡⟨⟩
   blockifySeq cc (loop a guard (unblockifySeq f body)
                   ∷ (unblockifySeq f rest))
 ≡⟨⟩
   blockifyStm cc (loop a guard (unblockifySeq f body))
   ∷ blockifySeq cc (unblockifySeq f rest)
 ≡⟨ cong (_∷_ (blockifyStm cc (loop a guard (unblockifySeq f body))))
          (blockify∘unblockify≡id f cc  rest rest-props)
   ⟩
   blockifyStm cc (loop a guard (unblockifySeq f body))
   ∷ rest
 ≡⟨⟩
   ((if isYes (interesting cc (loop a guard (unblockifySeq f body)))
    then loop a guard (blockifySeq cc (unblockifySeq f body))
    else block NE.[ a ]) ∷ rest)
 ≡⟨ cong (λ x → (if isYes x
                  then loop a guard (blockifySeq cc (unblockifySeq f body))
                  else block NE.[ a ])
                 ∷ rest)
          (proj₂ (must-be-interesting-stm cc (unblockifyStm f stm)
                 (unblockify-keeps-Interesting cc f stm any-interesting)))

   ⟩
   (loop a guard (blockifySeq cc (unblockifySeq f body))
   ∷ rest)
 ≡⟨ cong (λ x → (loop a guard x) ∷ rest)
          (blockify∘unblockify≡id f cc  body body-props)
   ⟩
   seq
 ∎
blockify∘unblockify≡id
  f cc
  seq@(stm@(branch a guard tc fc) ∷ rest)
  ((branch
    _ (any-interesting , _)
    tc-props fc-props) ∷ rest-props)
  with lift (interesting cc stm)
...| (no since , interesting≡false) =
 contradiction any-interesting since
...| (yes since , interesting≡yes) =
 begin
   (blockifySeq cc (unblockifySeq f seq))
 ≡⟨⟩
   (blockifySeq cc ((branch a guard (unblockifySeq f tc)
                                     (unblockifySeq f fc))
                    ∷ (unblockifySeq f rest)))
 ≡⟨⟩
    (if isYes (interesting cc (branch a guard (unblockifySeq f tc)
                                     (unblockifySeq f fc)))
    then branch a guard (blockifySeq cc (unblockifySeq f tc))
                        (blockifySeq cc (unblockifySeq f fc))
    else block NE.[ a ])
    ∷ (blockifySeq cc (unblockifySeq f rest))
 ≡⟨ cong (λ x →
           (if isYes x
            then branch a guard (blockifySeq cc (unblockifySeq f tc))
                                (blockifySeq cc (unblockifySeq f fc))
            else block NE.[ a ])
            ∷ (blockifySeq cc (unblockifySeq f rest))
          )
         (proj₂ (must-be-interesting-stm cc (unblockifyStm f stm)
                 (unblockify-keeps-Interesting cc f stm any-interesting)))
   ⟩
    (branch a guard (blockifySeq cc (unblockifySeq f tc))
                    (blockifySeq cc (unblockifySeq f fc)))
    ∷ (blockifySeq cc (unblockifySeq f rest))
 ≡⟨ cong ((branch a guard (blockifySeq cc (unblockifySeq f tc))
                          (blockifySeq cc (unblockifySeq f fc)))
          ∷_)
          (blockify∘unblockify≡id f cc  rest rest-props)
   ⟩
    (branch a guard (blockifySeq cc (unblockifySeq f tc))
                    (blockifySeq cc (unblockifySeq f fc)))
    ∷ rest
 ≡⟨ cong₂ (λ x y → (branch a guard x y) ∷ rest)
           (blockify∘unblockify≡id f cc  tc tc-props)
           (blockify∘unblockify≡id f cc  fc fc-props)
   ⟩
   seq
 ∎
blockify∘unblockify≡id
  f cc
  seq@(stm@(block ( x NE.∷ [])) ∷ rest)
  ((leaf _ (inj₁ direct-transl , _)) ∷ rest-props) =
 begin
   (blockifySeq cc (unblockifySeq f seq))
 ≡⟨⟩
   (blockifySeq cc (f x ∷ (unblockifySeq f rest)))
 ≡⟨ cong (λ g → blockifySeq cc (g ∷ (unblockifySeq f rest)))
         direct-transl
   ⟩
   (block NE.[ x ] ∷ blockifySeq cc (unblockifySeq f rest))
 ≡⟨ cong (block NE.[ x ] ∷_)
          (blockify∘unblockify≡id f cc  rest rest-props)
   ⟩
   seq
 ∎
blockify∘unblockify≡id f
  cc
  seq@(stm@(block ( x NE.∷ [])) ∷ rest)
  ((leaf _ (inj₂ no-interesting
               , _
               , stm-freshly-blockified
               ))
  ∷ rest-props)
  with lift (unblockifyStm f stm)
...| (stm'@(block (x' NE.∷ [])) , unblockify≡block) =
  begin
    (blockifySeq cc (f x ∷ (unblockifySeq f rest)))
  ≡⟨ cong (λ z → blockifySeq cc (z ∷ (unblockifySeq f rest)))
           unblockify≡block
    ⟩
    (stm' ∷ blockifySeq cc (unblockifySeq f rest))
  ≡⟨ cong (stm' ∷_)
          (blockify∘unblockify≡id f cc rest rest-props)
    ⟩
    (stm' ∷ rest)
  ≡⟨ cong (_∷ rest) (sym block[x]≡block[x']) ⟩
    seq
  ∎
 where
   block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
   block[x]≡block[x'] =
     begin
       block (NE.[ x ])
     ≡⟨ (sym stm-freshly-blockified) ⟩
       blockifyStm cc (f x)
     ≡⟨ cong (blockifyStm cc) unblockify≡block ⟩
       blockifyStm cc (block (NE.[ x' ]))
     ≡⟨⟩
       block (NE.[ x' ])
     ∎
...| (stm'@(block (x₁' NE.∷ (x₂ ∷ xs'))) , unblockify≡block) =
 ⊥-elim (absurd weird-stuff)
 where
   absurd : block (NE.[ x ]) ≡ block (x₁' NE.∷ (x₂ ∷ xs')) → ⊥
   absurd ()
   weird-stuff : block (NE.[ x ]) ≡ block (x₁' NE.∷ (x₂ ∷ xs'))
   weird-stuff =
     begin
       block (NE.[ x ])
     ≡⟨ sym stm-freshly-blockified ⟩
       blockifyStm cc (f x)
     ≡⟨ cong (blockifyStm cc) unblockify≡block ⟩
       blockifyStm cc stm'
     ∎
...| (stm'@(call x') , unblockify≡call) =
    begin
      (blockifySeq cc (f x ∷ (unblockifySeq f rest)))
    ≡⟨ cong (λ z → (blockifySeq cc (z ∷ (unblockifySeq f rest))))
             unblockify≡call
      ⟩
      (blockifySeq cc (call x' ∷ (unblockifySeq f rest)))
    ≡⟨ cong (λ z → z ∷ blockifySeq cc (unblockifySeq f rest))
             blockyfy-call≡block
      ⟩
      (block NE.[ x' ] ∷ (blockifySeq cc  (unblockifySeq f rest)))
    ≡⟨ cong₂ (_∷_)
             (sym block[x]≡block[x'])
             (blockify∘unblockify≡id f cc rest rest-props)
      ⟩
      seq
    ∎
  where
    same-absurd : (¬(Interesting cc (f x))) ≡ (¬((Interesting cc (call x'))))
    same-absurd = cong (¬_ ∘ (Interesting cc)) unblockify≡call
    blockyfy-call≡block : blockifyStm cc (call x') ≡ block (NE.[ x' ])
    blockyfy-call≡block =
       cong (λ z → (if isYes z
                     then call x'
                     else block NE.[ x' ]
                     ))
             (proj₂ (must-be-boring cc stm' (transport (sym same-absurd) no-interesting)))
    block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
    block[x]≡block[x'] =
     begin
       block (NE.[ x ])
     ≡⟨ sym stm-freshly-blockified ⟩
       blockifyStm cc (f x)
     ≡⟨ cong (blockifyStm cc) unblockify≡call ⟩
       blockifyStm cc (call x')
     ≡⟨ blockyfy-call≡block ⟩
       block (NE.[ x' ])
     ∎
...| (stm'@(assign x') , unblockify≡assign) =
    begin
      (blockifySeq cc (f x ∷ (unblockifySeq f rest)))
    ≡⟨ cong (λ z → (blockifySeq cc (z ∷ (unblockifySeq f rest))))
             unblockify≡assign
      ⟩
      (blockifySeq cc (assign x' ∷ (unblockifySeq f rest)))
    ≡⟨ cong (λ z → z ∷ blockifySeq cc (unblockifySeq f rest))
             blockyfy-assign≡block
      ⟩
      (block NE.[ x' ] ∷ (blockifySeq cc  (unblockifySeq f rest)))
    ≡⟨ cong₂ (_∷_)
             (sym block[x]≡block[x'])
             (blockify∘unblockify≡id f cc rest rest-props)
      ⟩
      seq
    ∎
  where
    same-absurd : (¬(Interesting cc (f x))) ≡ (¬((Interesting cc (assign x'))))
    same-absurd = cong (¬_ ∘ (Interesting cc)) unblockify≡assign
    blockyfy-assign≡block : blockifyStm cc (assign x') ≡ block (NE.[ x' ])
    blockyfy-assign≡block =
       cong (λ z → (if isYes z
                     then assign x'
                     else block NE.[ x' ]
                     ))
             (proj₂ (must-be-boring cc stm' (transport (sym same-absurd) no-interesting)))
    block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
    block[x]≡block[x'] =
     begin
       block (NE.[ x ])
     ≡⟨ sym stm-freshly-blockified ⟩
       blockifyStm cc (f x)
     ≡⟨ cong (blockifyStm cc) unblockify≡assign ⟩
       blockifyStm cc (assign x')
     ≡⟨ blockyfy-assign≡block ⟩
       block (NE.[ x' ])
     ∎
...| (stm'@(continue x') , unblockify≡continue) =
    ⊥-elim (absurd (trans (sym stm-freshly-blockified) blockify-is-continue))
  where
    blockify-is-continue : blockifyStm cc (f x) ≡ continue x'
    blockify-is-continue =  cong (blockifyStm cc) unblockify≡continue
    absurd : block NE.[ x ] ≡ continue x' → ⊥
    absurd ()
...| (stm'@(break x') , unblockify≡break) =
    ⊥-elim (absurd (trans (sym stm-freshly-blockified) blockify-is-continue))
  where
    blockify-is-continue : blockifyStm cc (f x) ≡ break x'
    blockify-is-continue =  cong (blockifyStm cc) unblockify≡break
    absurd : block NE.[ x ] ≡ break x' → ⊥
    absurd ()
...| (stm'@(return x' expr) , unblockify≡return) =
    ⊥-elim (absurd (trans (sym stm-freshly-blockified) blockify-is-return))
  where
    blockify-is-return : blockifyStm cc (f x) ≡ return x' expr
    blockify-is-return =  cong (blockifyStm cc) unblockify≡return
    absurd : block NE.[ x ] ≡ return x' expr → ⊥
    absurd ()
...| (stm'@(loop x' guard body) , unblockify≡loop) =
  begin
    (blockifySeq cc (f x ∷ (unblockifySeq f rest)))
  ≡⟨ cong (λ z → blockifySeq cc (z ∷ (unblockifySeq f rest)))
           unblockify≡loop
    ⟩
    (blockifyStm cc stm' ∷ blockifySeq cc (unblockifySeq f rest))
  ≡⟨ cong (blockifyStm cc stm'  ∷_)
          (blockify∘unblockify≡id f cc rest rest-props)
    ⟩
    (blockifyStm cc stm'  ∷ rest)
  ≡⟨ cong (λ z →
            (if isYes z
             then loop x' guard (blockifySeq cc  body)
             else block NE.[ x' ]) ∷ rest)
           (proj₂ (must-be-boring cc stm' (transport (sym same-absurd) no-interesting)))
    ⟩
    block NE.[ x' ] ∷ rest
  ≡⟨ cong (_∷ rest) (sym  block[x]≡block[x']) ⟩
    seq
  ∎
  where
    same-absurd : (¬(Interesting cc (f x))) ≡ (¬((Interesting cc (loop x' guard body))))
    same-absurd = cong (¬_ ∘ (Interesting cc)) unblockify≡loop
    blockyfy-loop≡block : blockifyStm cc (loop x' guard body) ≡ block (NE.[ x' ])
    blockyfy-loop≡block =
       cong (λ z → (if isYes z
                     then (loop x' guard (blockifySeq cc body))
                     else block NE.[ x' ]
                     ))
             (proj₂ (must-be-boring cc stm' (transport (sym same-absurd) no-interesting)))
    block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
    block[x]≡block[x'] =
     begin
       block (NE.[ x ])
     ≡⟨ sym stm-freshly-blockified ⟩
       blockifyStm cc (f x)
     ≡⟨ cong (blockifyStm cc) unblockify≡loop ⟩
       blockifyStm cc (loop x' guard body)
     ≡⟨ blockyfy-loop≡block ⟩
       block (NE.[ x' ])
     ∎
...| (stm'@(branch x' guard tc fc) , unblockify≡branch) =
  begin
    (blockifySeq cc (f x ∷ (unblockifySeq f rest)))
  ≡⟨ cong (λ z → blockifySeq cc (z ∷ (unblockifySeq f rest)))
           unblockify≡branch
    ⟩
    (blockifyStm cc stm' ∷ blockifySeq cc (unblockifySeq f rest))
  ≡⟨ cong (blockifyStm cc stm'  ∷_)
          (blockify∘unblockify≡id f cc rest rest-props)
    ⟩
    (blockifyStm cc stm'  ∷ rest)
  ≡⟨ cong (λ z →
            (if isYes z
             then branch x' guard (blockifySeq cc tc) (blockifySeq cc fc)
             else block NE.[ x' ]) ∷ rest)
           (proj₂ (must-be-boring cc stm' still-not-interesting))
    ⟩
    block NE.[ x' ] ∷ rest
  ≡⟨ cong (_∷ rest) (sym  block[x]≡block[x']) ⟩
    seq
  ∎
  where

    have : ¬ Interesting cc (f x)
    have = no-interesting
    still-not-interesting : ¬ Interesting cc (branch x' guard tc fc)
    still-not-interesting = transport (cong (¬_ ∘ Interesting cc) (sym unblockify≡branch)) no-interesting


    same-absurd : (¬(Any (Interesting cc) (f x)))
                ≡ (¬(Any (Interesting cc) (branch x' guard tc fc)))
    same-absurd = cong (λ z → (¬(Any (Interesting cc) z)))
                       unblockify≡branch
    blockyfy-branch≡block : blockifyStm cc (branch x' guard tc fc) ≡ block (NE.[ x' ])
    blockyfy-branch≡block =
       cong (λ z → (if isYes z
                     then (branch x' guard (blockifySeq cc tc) (blockifySeq cc fc))
                     else block NE.[ x' ]
                     ))
             (proj₂ (must-be-boring cc stm' still-not-interesting))
    block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
    block[x]≡block[x'] =
     begin
       block (NE.[ x ])
     ≡⟨ sym stm-freshly-blockified ⟩
       blockifyStm cc (f x)
     ≡⟨ cong (blockifyStm cc) unblockify≡branch ⟩
       blockifyStm cc (branch x' guard tc fc)
     ≡⟨ blockyfy-branch≡block ⟩
       block (NE.[ x' ])
     ∎
blockify∘unblockify≡id
  _ _
  ((block ( x₁ NE.∷ (x₂ ∷ xs))) ∷ rest)
  (leaf _ ( _ , () , _) ∷ rest-props)




blockifySeq≡map[blockifyStm] :
  ∀ {A}
  → {cc : ContractionCtx A}
  → (seq : List (Stm A))
  → blockifySeq cc seq
  ≡ Data.List.map (blockifyStm cc) seq
blockifySeq≡map[blockifyStm] [] = refl
blockifySeq≡map[blockifyStm] (x ∷ xs) =
  cong (_ ∷_) (blockifySeq≡map[blockifyStm] xs)


blockifySeq-not-empty :
  ∀ {A}
  → {cc : ContractionCtx A}
  → (x : Stm A)
  → (xs : List (Stm A))
  → blockifySeq cc (x ∷ xs) ≢ []
blockifySeq-not-empty (call _) _ = λ ()
blockifySeq-not-empty (assign _) _ = λ ()
blockifySeq-not-empty (block _) _ = λ ()
blockifySeq-not-empty (continue _) _ = λ ()
blockifySeq-not-empty (break _) _ = λ ()
blockifySeq-not-empty (return _ _) _ = λ ()
blockifySeq-not-empty (loop _ _ _) _ = λ ()
blockifySeq-not-empty (branch _ _ _ _) _ = λ ()
