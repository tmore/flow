module Flow.Contraction where

open import Flow.Core


open import Data.Unit using (⊤) renaming ( tt to unit)
open import Data.Empty
open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary
open import Relation.Nullary.Negation hiding (excluded-middle)

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
import Data.These
open import Data.These using ( These ; this ; that ; these )


open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ;T
                            )
open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)

open import Data.List using (List ; [] ; _∷_
                            ; [_] ; _++_)

import Data.List.NonEmpty as NE
open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)

open import Util using ( maybeElim ; maybe⊥
                       ; either-of-these
                       ; ∧-injectiveᵣ ; ∧-injectiveₗ
                       ; a≡false⇒a≢true
                       ; transport
                       ; lift
                       ; if-guard-subst
                       )

import Flow.Relation.Unary.Any as Any
open import Flow.Relation.Unary.Any using (Any ; AnySeq)
open import Flow.Relation.Unary.NIL-Any as NILAny
open import Flow.Relation.Unary.NIL-Any using (NILAny ; NILAnySeq)

open import Expr
open 𝕍 using (𝕍)
open 𝔸 using (𝔸)
open 𝔹 using (𝔹)

import Extra.List

-- Used in order to determine wether a flow statement should be contracted or
-- not
record ContractionCtx (A : Set) : Set where
  field
    isLocal : (𝕍 → Bool)
    -- ^ Decides wether a statement is local or not. Should be set according the local variables of a procedure
    symbolFunc : (A → Maybe (These 𝕍 𝕍))
    -- ^ Retrieves the important symbols from a statement.  left hand side is
      -- variables and right hand side is functions
    interestingLocals : List 𝕍
    -- ^ List of all local variables that are deemed interesting
    interestingGlobals : List 𝕍
    -- ^ List of all global variables that are deemed interesting
    interestingProcedures : List 𝕍
    -- ^ List of all procedudues that are deemed interesting

symsInteresting : {A : Set} → ContractionCtx A → A → Bool
symsInteresting cc e with (ContractionCtx.symbolFunc cc) e
... | nothing = false
... | just t =
  either-of-these
    (λ x → if ContractionCtx.isLocal cc x
           then Data.List.any
                  (𝕍.equals x)
                  (ContractionCtx.interestingLocals cc)
           else Data.List.any
                  (𝕍.equals x)
                  (ContractionCtx.interestingGlobals cc))
    (λ x → Data.List.any
             (𝕍.equals x)
             (ContractionCtx.interestingProcedures cc))
    t
annotInteresting : {A : Set} → ContractionCtx A → Stm A → Bool
annotInteresting cc stm =
  symsInteresting cc (firstAnnot stm)

InterestingNode : ∀ {A} → ContractionCtx A → Stm A → Set
InterestingNode cc stm@(call _) =
  T(annotInteresting cc stm)
InterestingNode cc stm@(assign _) =
  T(annotInteresting cc stm)
InterestingNode _ (return _ _) = ⊤
{-# CATCHALL #-}
InterestingNode _ _ = ⊥


interestingNode :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (stm : Stm A)
  → Dec (InterestingNode cc stm)
interestingNode cc stm@(call _)
  with annotInteresting cc stm
...| false = no λ ()
...| true = yes unit
interestingNode cc stm@(assign _)
  with annotInteresting cc stm
...| false = no λ ()
...| true = yes unit
interestingNode _ (return _ _) = yes unit
interestingNode _ (block _) = no λ ()
interestingNode _ (continue _) = no λ ()
interestingNode _ (break _) = no λ ()
interestingNode _ (loop _ _ _) = no λ ()
interestingNode _ (branch _ _ _ _) = no λ ()

PseudoInterestingNode : ∀ {A} → Stm A → Set
PseudoInterestingNode (continue _) = ⊤
PseudoInterestingNode (break _) = ⊤
{-# CATCHALL #-}
PseudoInterestingNode _ = ⊥


pseudoInterestingNode :
  ∀ {A}
  → (stm : Stm A)
  → Dec (PseudoInterestingNode stm)
pseudoInterestingNode (continue _) = yes unit
pseudoInterestingNode (break _) = yes unit
pseudoInterestingNode (block _) = no λ ()
pseudoInterestingNode (call _) = no λ ()
pseudoInterestingNode (assign _) = no λ ()
pseudoInterestingNode (return _ _) = no λ ()
pseudoInterestingNode (loop _ _ _) = no λ ()
pseudoInterestingNode (branch _ _ _ _) = no λ ()



InterestingLoop : ∀ {A} → ContractionCtx A → Stm A → Set
InterestingLoop cc stm =
  Any (InterestingNode cc)
      stm

InterestingBranch : ∀ {A} → ContractionCtx A → Stm A → Set
InterestingBranch cc stm =
  Any (InterestingNode cc) stm
  ⊎ NILAny PseudoInterestingNode stm

Interesting : ∀ {A} → ContractionCtx A → (stm : Stm A) → Set
Interesting cc stm@(continue _) = ⊥
Interesting cc stm@(break _) = ⊥
Interesting cc stm@(return _ _) = ⊤
Interesting cc stm@(block _) = ⊥
Interesting cc stm@(call _) =
  InterestingNode cc stm
Interesting cc stm@(assign _) =
  InterestingNode cc stm
Interesting cc stm@(loop _ _ _) =
  InterestingLoop cc stm
Interesting cc stm@(branch _ _ _ _) =
  InterestingBranch cc stm


interesting :
  ∀ {A}
  → (cc : ContractionCtx A)
  → (stm : Stm A)
  → Dec (Interesting cc stm)
interesting _ (continue _) = no λ ()
interesting _ (break _) = no λ ()
interesting _ (return _ _) = yes unit
interesting _ (block _ ) = no λ ()
interesting cc stm@(call _) =
  interestingNode cc stm
interesting cc stm@(assign _) =
  interestingNode cc stm
interesting cc stm@(loop _ _ _) =
  Any.any (interestingNode cc)  stm
interesting cc stm@(branch _ _ _ _)
  with Any.any (interestingNode cc) stm | NILAny.any pseudoInterestingNode stm
...| yes has-interesting | _ =
  yes (inj₁ has-interesting)
...| _                   | yes has-pseudo-interesting =
  yes (inj₂ has-pseudo-interesting)
...| no ¬has-interesting | no ¬has-pseudo-interesting =
  no λ { (inj₁ has-interesting) →
               ¬has-interesting has-interesting
       ; (inj₂ has-pseudo-interesting) →
               ¬has-pseudo-interesting has-pseudo-interesting
       }



blockifySeq : ∀ {A} → ContractionCtx A → List (Stm A) → List (Stm A)
blockifyStm : ∀ {A} → ContractionCtx A → Stm A → Stm A

blockifyStm cc stm@(continue _) = stm
blockifyStm cc stm@(break _) = stm
blockifyStm cc stm@(return _ _) = stm
blockifyStm cc stm@(block _) = stm
blockifyStm cc stm@(call a) =
  if isYes (interesting cc stm)
   then call a
   else block NE.[ a ]
blockifyStm cc stm@(assign a) =
  if isYes (interesting cc stm)
   then assign a
   else block NE.[ a ]
blockifyStm cc stm@(loop a guard body) =
  if isYes (interesting cc stm)
   then loop a guard (blockifySeq cc body)
   else block NE.[ a ]
blockifyStm cc stm@(branch a guard tc fc) =
  if isYes (interesting cc stm)
   then branch a guard (blockifySeq cc tc)
                       (blockifySeq cc fc)
   else block NE.[ a ]

blockifySeq cc [] = []
blockifySeq cc (stm ∷ rest) =
  blockifyStm cc stm ∷ blockifySeq cc rest

-- blockifySeq : ∀ {A} → ContractionCtx A → List (Stm A) → List (Stm A)
-- blockifySeq cc xs =
--   Data.List.map (λ x → blockifyStm cc x) xs

-- Unblockifies a statement tree

unblockifyStm : {A : Set} → (A → Stm A) → (stm : Stm A) → Stm A
unblockifySeq : {A : Set} → (A → Stm A) → List (Stm A) → List (Stm A)

unblockifyStm f (block (x NE.∷ _)) = f x
unblockifyStm {a} f (loop e guard body) =
  (loop e guard (unblockifySeq f body))
unblockifyStm {A} f (branch e guard tc  fc) =
  (branch e guard (unblockifySeq f tc)
                  (unblockifySeq f fc))
{-# CATCHALL #-}
unblockifyStm _ stm = stm

-- Unblockifies a sequence
unblockifySeq f [] = []
unblockifySeq f (stm ∷ rest) =
  unblockifyStm f stm ∷ unblockifySeq f rest

contractHead : ∀ {A} → Stm A → List (Stm A) → List (Stm A)
contractHead (block bs₁) (block bs₂ ∷ rest) =
  block (bs₁ ⁺++⁺ bs₂) ∷ rest
{-# CATCHALL #-}
contractHead x rest =
  x ∷ rest

contract : ∀ {A} → List (Stm A) → List (Stm A)
contract [] = []
contract (branch a guard tc fc  ∷ rest) =
  (branch a guard (contract tc) (contract fc)) ∷ contract rest
contract (loop a guard body  ∷ rest) =
  (loop a guard (contract body)) ∷ contract rest
contract {A} (block xs ∷  rest) =
  contractHead (block xs) (contract rest)
{-# CATCHALL #-}
contract (x ∷ rest) =
  x ∷ contract rest

uncontract : ∀ {A} → List (Stm A) → List (Stm A)
uncontract [] = []
uncontract (branch a guard tc fc  ∷ rest) =
  (branch a guard (uncontract tc) (uncontract fc)) ∷ uncontract rest
uncontract (loop a guard body  ∷ rest) =
  (loop a guard (uncontract body)) ∷ uncontract rest
uncontract {A} (block xs ∷ rest) =
  NE.toList (NE.map (λ x → block NE.[ x ]) xs) ++ uncontract rest
{-# CATCHALL #-}
uncontract (x ∷ rest) =
  x ∷ uncontract rest
