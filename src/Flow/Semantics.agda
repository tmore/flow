import Maps
import VarCtx

open import Expr
open 𝕍 using (𝕍)
open 𝔸 using (𝔸)
open 𝔹 using (𝔹)

import Ctx

module Flow.Semantics (MapType : Set → Set)
                      (MapInstance : Maps.IntMap MapType)
                      where

module Ctx' = Ctx MapType MapInstance
open import Flow.Core

open import Function.Base

open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )

open import Util using ( lift ; transport )

open import Data.List using (List ; [] ; _∷_ ; [_] ; _++_ ; drop)
import Data.List.Properties
open import Data.List.NonEmpty using (List⁺)
import Data.List.NonEmpty as NE
open import Data.List.NonEmpty using ( _⁺++⁺_ )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)

import Data.Integer as ℤ
open import Data.Integer using (ℤ)

open import Data.Unit using (⊤) renaming (tt to unit)
open import Data.Empty
open import Relation.Nullary
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; _≢_ ; refl; trans; sym; cong; cong-app; subst ; cong₂)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

Ctx : Set → Set
Ctx A = Ctx'.Ctx A

record SubSemantic (A : Set) : Set where
  field
    stepFunc : Ctx A → A → (Ctx A)
    transl : A → Stm A
    -- ^ Should be a direc translaiton function. No uneccesary compression or
    -- uncompression
    compress : List (Stm A) → List (Stm A)
    -- ^ Compression function , run afer evaluating he head of blocks.
    -- May of course be id
    untransl : List (Stm A) → List A
    -- ^ Function that 'untranslates' the annotation.


-- Utility function for semantic
remainingSeq : ∀ {A} → List (Stm A) → List (Stm A)
remainingSeq [] = []
remainingSeq ((block (stmt₁ NE.∷ [])) ∷ rest) = rest
remainingSeq ((block (stmt₁ NE.∷ (stmt₂ ∷ stmts))) ∷ rest) =
  (block ( stmt₂ NE.∷ stmts) ∷ rest)
{-# CATCHALL #-}
remainingSeq ( non-block-head ∷ rest) = rest
-- Statements that run as a sub semantic should not alter the control flow in
-- any way. The only effect they will have that is kept is the variable changes
runSubSem :
  ∀ {A}
  → SubSemantic A
  → Ctx (Stm A)
  → A
  → Ctx (Stm A)
runSubSem subSem ctx stm =
  (Ctx'.setVarCtx ∘ Ctx'.variables)
    (SubSemantic.stepFunc
      subSem
      (Ctx'.clearLoopStack
        (Ctx'.mapSeq
          (SubSemantic.untransl subSem)
          ctx))
    stm)
    ctx


-- No neccesary if we write slightly more ugly code
{-# TERMINATING #-}a
semanticStep :
  ∀ {A}
  → SubSemantic A
  → Ctx (Stm A)
  → List (Stm A)
  → (Ctx (Stm A) × (List (Stm A) ⊎ ℤ))
semanticStep subSem ctx [] =
  (ctx , inj₁ [])
-- The block case may be considered the only weird case, what we do is that
-- given a block we take the first annotation and translate it to *unblockified*
-- flow. Then we take a step on the unblockified version, (todo) and possible
-- conract the rest
semanticStep
  subSem ctx
  seq@(block ( stmt₁ NE.∷ stmts) ∷ rest)
  with (SubSemantic.transl subSem stmt₁)
...| block stmts' = ( runSubSem subSem ctx stmt₁
                     , inj₁ (remainingSeq seq)
                     )
{-# CATCHALL #-}
...| other =
  let (ctx' , e-res) =
            (semanticStep
              subSem ctx
              (other ∷ remainingSeq seq))
  in ( Ctx'.mapLoopStackSeqs (SubSemantic.compress subSem) ctx'
     , Data.Sum.map (SubSemantic.compress subSem) id e-res
     )
semanticStep subSem ctx (call a ∷ rest) =
  (runSubSem subSem ctx a , inj₁ rest)
semanticStep subSem ctx (assign a ∷ rest) =
  (runSubSem subSem ctx a , inj₁ rest)
semanticStep subSem ctx (break _ ∷ rest) =
  maybe (Data.Product.map₂ inj₁)
        (semanticStep subSem ctx rest)
        (Ctx'.popBreak ctx)
  -- ^ ignores breaks if they're not in a context where a pop is possible
semanticStep subSem ctx (continue _ ∷ rest) =
  maybe (Data.Product.map₂ inj₁)
        (semanticStep subSem ctx rest)
        (Ctx'.popContinue ctx)
  -- ^ ignores breaks if they're not in a context where a pop is possible
semanticStep subSem ctx (return _ expr ∷ rest) =
  ( ctx , inj₂ (Ctx'.eval𝔸 expr ctx) )
semanticStep subSem ctx (branch _ guard tc fc ∷ rest) =
  if Ctx'.eval𝔹 guard ctx
  then ( ctx , inj₁ (SubSemantic.compress subSem (tc ++ rest)))
  else ( ctx , inj₁ (SubSemantic.compress subSem (fc ++ rest)))
semanticStep subSem ctx seq@(loop _ guard body ∷ rest) =
  if Ctx'.eval𝔹 guard ctx
  then ( Ctx'.pushStack seq rest ctx , inj₁ body )
  else ( ctx , inj₁ rest )


semantic :
  ∀ {A}
  → SubSemantic A
  → Ctx (Stm A)
  → List (Stm A)
  → Ctx (Stm A) × ((List (Stm A)) ⊎ ℤ)
semantic subSem = Ctx'.step (semanticStep subSem)


module Properties where
  open import Flow.Contraction
  import Extra.NonEmpty as NE

  block-injective : ∀ {A} {a b : List⁺ A} → block a ≡ block b → a ≡ b
  block-injective refl = refl


  remainingSeq-drops-block-head :
    ∀ { A }
    → {stmt₁ : A}
    → {rest : List (Stm A)}
    → (seq : List (Stm A))
    → (seq ≡ ((block NE.[ stmt₁ ]) ∷ rest))
    → remainingSeq (contract seq) ≡ contract rest
  remainingSeq-drops-block-head
    {stmt₁ = stmt₁} {rest = rest}
    seq@((block (stmt₁' NE.∷ [])) ∷ rest') seq-proof
    with contract rest' | Eq.inspect contract rest'
  ...| [] | Eq.[ contract≡[] ] =
    trans (sym contract≡[]) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
  ...| (block stmts) ∷ contracted-rest | Eq.[ contract≡block∷rest ] =
       trans (sym contract≡block∷rest) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
  ...| (call _) ∷ contracted-rest | Eq.[ contract≡block∷rest ] =
       trans (sym contract≡block∷rest) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
  ...| (assign _) ∷ contracted-rest | Eq.[ contract≡block∷rest ] =
       trans (sym contract≡block∷rest) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
  ...| (continue _) ∷ contracted-rest | Eq.[ contract≡block∷rest ] =
       trans (sym contract≡block∷rest) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
  ...| (break _) ∷ contracted-rest | Eq.[ contract≡block∷rest ] =
       trans (sym contract≡block∷rest) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
  ...| (return _ _) ∷ contracted-rest | Eq.[ contract≡block∷rest ] =
       trans (sym contract≡block∷rest) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
  ...| (loop _ _ _) ∷ contracted-rest | Eq.[ contract≡block∷rest ] =
       trans (sym contract≡block∷rest) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
  ...| (branch _ _ _ _) ∷ contracted-rest | Eq.[ contract≡block∷rest ] =
       trans (sym contract≡block∷rest) (cong contract rest'≡rest)
    where
      rest'≡rest : rest' ≡ rest
      rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)

  -- Utility abreviation for the proof below
  semanicStep-block-case :
    ∀ {A}
    → SubSemantic A
    → Ctx (Stm A)
    → (Stm A) -- translated
    → List (Stm A) -- Conracted
    → (Ctx (Stm A) × ((Sequence A) ⊎ ℤ))
  semanicStep-block-case
    subSem ctx transl contracted =
    (let (ctx' , e-res) =
                 (semanticStep
                   subSem ctx
                   (transl ∷ contracted))
     in ( Ctx'.mapLoopStackSeqs (SubSemantic.compress subSem) ctx'
        , Data.Sum.map (SubSemantic.compress subSem) id e-res
     ))

  -- empty case and block case of the inner with match are the only unique
  -- cases, the remaining are duplicates
  sematicStep-on-contractHead :
    ∀ {A}
    → {stmt₁ : A}
    → {cc : ContractionCtx A}
    → {subSem : SubSemantic A}
    → {ctx : Ctx (Stm A)}
    → {rest : List (Stm A)}
    → ¬ (IsBlock (SubSemantic.transl subSem stmt₁))
    → (seq : List (Stm A))
    → (seq ≡ ((block NE.[ stmt₁ ]) ∷ rest))
    → (semanticStep subSem ctx (contract seq))
    ≡ semanicStep-block-case
         subSem ctx
         (SubSemantic.transl subSem stmt₁) (contract rest)
  sematicStep-on-contractHead
    { stmt₁ = stmt₁ } {subSem = subSem} { ctx = ctx }
    not-block [] ()
  sematicStep-on-contractHead
    {A = A}
    { stmt₁ = stmt₁ } {subSem = subSem} { ctx = ctx } {rest = rest }
    ¬is-block seq@((block (stmt₁' NE.∷ [])) ∷ rest') seq-proof
      with  (contract rest') | Eq.inspect contract rest'
  ...| contacted@((block (stmt₂ NE.∷ stmts)) ∷ rest'') | Eq.[ is-contacted ]  =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) contacted)
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           ((block (stmt₂ NE.∷ stmts)) ∷ rest'')
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx )
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ (block (stmt₁' NE.∷ (stmt₂ ∷ stmts))) ∷ rest''
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ ((block (stmt₁' NE.∷ (stmt₂ ∷ stmts))) ∷ rest'')
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted
       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ ((block (stmt₁' NE.∷ (stmt₂ ∷ stmts))) ∷ rest'')
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'
       sub-check : (semanticStep subSem ctx ((block (stmt₁' NE.∷ (stmt₂ ∷ stmts))) ∷ rest''))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          ((block (stmt₂ NE.∷ stmts)) ∷ rest'')
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
  ...|  contacted@(stm₂@(call _) ∷ rest'') | Eq.[ is-contacted ]  =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) contacted)
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           (stm₂ ∷ rest'')
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx )
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest'')
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'

       sub-check : (semanticStep subSem ctx (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest''))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          (stm₂ ∷ rest'')
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
  ...|  contacted@(stm₂@(assign _) ∷ rest'') | Eq.[ is-contacted ]  =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) contacted)
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           (stm₂ ∷ rest'')
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx )
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest'')
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'

       sub-check : (semanticStep subSem ctx (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest''))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          (stm₂ ∷ rest'')
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
  ...|  contacted@(stm₂@(continue _) ∷ rest'') | Eq.[ is-contacted ]  =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) contacted)
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           (stm₂ ∷ rest'')
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx )
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest'')
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'

       sub-check : (semanticStep subSem ctx (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest''))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          (stm₂ ∷ rest'')
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
  ...|  contacted@(stm₂@(break _) ∷ rest'') | Eq.[ is-contacted ]  =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) contacted)
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           (stm₂ ∷ rest'')
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx)
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest'')
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'

       sub-check : (semanticStep subSem ctx (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest''))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          (stm₂ ∷ rest'')
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
  ...|  contacted@(stm₂@(return _ _) ∷ rest'') | Eq.[ is-contacted ]  =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) contacted)
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           (stm₂ ∷ rest'')
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx)
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest'')
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'

       sub-check : (semanticStep subSem ctx (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest''))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          (stm₂ ∷ rest'')
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
  ...|  contacted@(stm₂@(branch _ _ _ _) ∷ rest'') | Eq.[ is-contacted ]  =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) contacted)
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           (stm₂ ∷ rest'')
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx)
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest'')
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'

       sub-check : (semanticStep subSem ctx (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest''))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          (stm₂ ∷ rest'')
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
  ...|  contacted@(stm₂@(loop _ _ _) ∷ rest'') | Eq.[ is-contacted ]  =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) contacted)
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           (stm₂ ∷ rest'')
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx)
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ block NE.[ stmt₁' ] ∷  stm₂ ∷ rest''
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest'')
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'

       sub-check : (semanticStep subSem ctx (block NE.[ stmt₁' ] ∷ stm₂ ∷ rest''))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          (stm₂ ∷ rest'')
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
  sematicStep-on-contractHead {A} {stmt₁} {subSem = subSem} {ctx}
    {rest}
    ¬is-block
    seq@(block (stmt₁' List⁺.∷ []) ∷ rest')
    seq-proof
    | [] | Eq.[ is-contacted ] =
       begin
         semanticStep subSem ctx
         (contractHead  (block NE.[ stmt₁' ]) [])
       ≡⟨ sub-check ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁')
           []
       ≡⟨ cong₂ (semanicStep-block-case subSem ctx)
                 (cong (SubSemantic.transl subSem) stmt₁'≡stmt₁)
                 (trans (sym is-contacted) (cong contract rest'≡rest))
         ⟩
         semanicStep-block-case
           subSem ctx
           (SubSemantic.transl subSem stmt₁)
           (contract rest)
       ∎
     where
       contract-seq≡ : contract seq ≡ block NE.[ stmt₁' ] ∷ []
       contract-seq≡ = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       stmt₁'≡stmt₁ : stmt₁' ≡ stmt₁
       stmt₁'≡stmt₁ =
          (NE.∷-injectiveˡ
          ∘ block-injective
          ∘ Data.List.Properties.∷-injectiveˡ)
          (trans refl seq-proof)
       rest'≡rest : rest' ≡ rest
       rest'≡rest =
         Data.List.Properties.∷-injectiveʳ (trans refl seq-proof)
       is-contracted' : (contract ((block NE.[ stmt₁' ]) ∷ rest'))
                     ≡ block NE.[ stmt₁' ] ∷ []
       is-contracted' = cong (contractHead (block NE.[ stmt₁' ])) is-contacted

       is-contracted : (contract ((block NE.[ stmt₁ ]) ∷ rest))
                     ≡ (block NE.[ stmt₁' ] ∷ [])
       is-contracted = trans (cong contract (sym seq-proof))
                             is-contracted'

       sub-check : (semanticStep subSem ctx (block NE.[ stmt₁' ] ∷ []))
                 ≡ semanicStep-block-case subSem ctx
                                          (SubSemantic.transl subSem stmt₁')
                                          []
       sub-check with (SubSemantic.transl subSem stmt₁') | Eq.inspect (SubSemantic.transl subSem) stmt₁'
       ...| (call _) | _ = refl
       ...| (assign _) | _ = refl
       ...| (continue _) | _ = refl
       ...| (break _) | _ = refl
       ...| (return _ _) | _ = refl
       ...| (loop _ _ _) | _ = refl
       ...| (branch _ _ _ _) | _ = refl
       ...| b@(block _) | Eq.[ trans≡block ] =
            ⊥-elim (¬is-block is-block)
            where
              same-sub-tranls : (SubSemantic.transl subSem stmt₁) ≡ b
              same-sub-tranls =
                trans (cong (SubSemantic.transl subSem) (sym stmt₁'≡stmt₁))
                      trans≡block
              is-block : IsBlock (SubSemantic.transl subSem stmt₁)
              is-block = transport (cong IsBlock same-sub-tranls) unit
