record StackMap (S : Set → Set) : Set₁ where
  field
    _∈_ : ∀ {a} → 𝕍 → S a → Bool
    set : ∀ {a} → 𝕍 → a → S a → S a
    lookup : ∀ {a} → 𝕍 → S a → Maybe a
    define : ∀ {a} → 𝕍 → a → S a → S a
    newFrame : ∀ {a} → S a → S a
    popFrame : ∀ {a} → S a → S a

    empty : ∀ {a} → S a

    fmap : ∀ {a b} → (a → b) → S a → S b

    EmptyIsEmpty : ∀ {v a} → ¬ T(v ∈ empty {a})

    ∈⇔lookup : ∀ {v a} {e : a} → {m : S a} → T(v ∈ m) ⇔ (lookup v m ≡ just e)

    define⇒lookup : ∀ {v a} {e : a} {m : S a} → lookup v (define v e m) ≡ just e
    ∈⇒set⇒lookup : ∀ {v a} {e : a} {m : S a} → T (v ∈ m) → (lookup v (set v e m)) ≡ just e
    ∉⇒set⇒lookup≡nothing : ∀ {v a} {e : a} {m : S a} → ¬ T(v ∈ m) → lookup v (set v e m) ≡ nothing

    v∈m⇒v∈newFrame[m] : ∀ {v a} → {m : S a} {m' : S a} → (m' ≡ newFrame m) → T(v ∈ m) → T(v ∈ m')

    DefineIsLocal : ∀ {v a} {e : a} {e' : a} {m : S a} → lookup v m ≡ just e → lookup v (popFrame (define v e' (newFrame m))) ≡ just e
