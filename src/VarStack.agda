module VarStack where
  VarStack : Set → Set
  VarStack a = List (VarMap a)

  empty : ∀ {a} → VarStack a
  empty = [ [] ]

  lookup : ∀ {a} → 𝕍 → VarStack a → Maybe a
  lookup v [] = nothing
  lookup v (vm ∷ vms) with VarMap.lookup v vm
  ... | just a = just a
  ... | nothing = lookup v vms

  _∈_ : ∀ {a} → 𝕍 → VarStack a → Bool
  _∈_ v [] = false
  _∈_ v (vm ∷ vms) =
    if v VarMap.∈ vm
    then true
    else v ∈ vms

  set : ∀ {a} 𝕍 → a → VarStack a → VarStack a
  set _ _ [] = []
  set v n (vm ∷ vms) =
    if v VarMap.∈ vm
    then VarMap.set v n vm ∷ vms
    else vm ∷ set v n vms

  declare : ∀ {a} → 𝕍 → a → VarStack a → VarStack a
  declare v a [] = [ [ (v , a) ] ]
  declare v a (vm ∷ vms) =
    VarMap.set v a vm ∷ vms

  pop : ∀ {a} → VarStack a → VarStack a
  pop [] = []
  pop (x ∷ xs) = xs


  EitherFirstOrLater : ∀ {A }
                     → (v : 𝕍)
                     → (vm : VarMap A)
                     → (vms : List (VarMap A))
                     → (vs : VarStack A)
                     → (vs ≡ (vm ∷ vms))
                     → (v ∈ vs ≡ true)
                     → ((v VarMap.∈ vm ≡ true) ⊎ (v ∈ vms ≡ true))
  EitherFirstOrLater v vm vms [] ()
  EitherFirstOrLater {A} v vm vms vs IsList InSet with bool-elim (v VarMap.∈ vm )
  ... | (inj₁ v∈vm) = inj₁ v∈vm
  ... | (inj₂ v∉vm) = inj₂ {!!}
    where
      foo : v ∈ vs ≡ v ∈ vms
      foo = {!-t 10!}
      -- v∈vs≡v∈vms : v ∈ vs ≡ v ∈ vms
      -- v∈vs≡v∈vms =
      --   begin
      --     v ∈ vs
      --   ≡⟨ refl ⟩
      --     (if v VarMap.∈ vm then true else v ∈ vms)
      --   ≡⟨  {!!}  ⟩
      --     v ∈ vms
      --   ∎
      -- v∈vms : v ∈ vms ≡ true
      -- v∈vms =
      --   begin
      --     v ∈ vs
      --   ≡⟨ {!!} ⟩
      --     true
      --   ∎

  Set⇒Lookup : ∀ {A}
             → (oldVs : VarStack A)
             → (v : 𝕍)
             → (a : A)
             → (vs : VarStack A)
             → (v ∈ oldVs ≡ true)
             → (set v a oldVs ≡ vs)
             → (lookup v vs ≡ just a)
  Set⇒Lookup [] v a [] ()
  Set⇒Lookup (x₁ ∷ oldVs) v a [] VmFromSet =
    {!!}

  Set⇒Lookup (oldVM ∷ oldVms) v a vs@(vm ∷ vms) SetGivesJust = {!!}
