import Maps
import VarCtx

open import Function.Base
open import Function.Equivalence using ( _⇔_ ; Equivalence )
open import Function.Equality using ( Π ; _⟨$⟩_ )
import Function.Equivalence

open import Data.Unit using (⊤) renaming ( tt to unit)
open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)
open import Data.Nat.Properties using (≡ᵇ⇒≡)
open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )
open import Data.Bool.Properties using (T-≡ ; T? )

open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)

import Data.These
open import Data.These using ( These ; this ; that ; these )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.List using (List ; [] ; _∷_ ; foldl ; zipWith ; [_] ; _++_ ; drop)
import Data.List.Properties
open import Data.List.Properties using (++-identityʳ)
open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)

open import Data.List.Relation.Unary.Any using () renaming ( any to list-any)

import Data.List.Relation.Unary.All as List -- using ( All)
import Data.List.Relation.Unary.All as ListAll
import Data.List.Relation.Unary.Any as List -- using ( Any )
import Data.List.Relation.Unary.Linked as List using ( Linked )
import Data.List.Relation.Unary.Linked as Linked

import Data.List.NonEmpty as NE
open import Data.Integer using (ℤ)
                         renaming (+_ to ℤ+_)

open import Agda.Builtin.Equality using (_≡_ ; refl )

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst ; cong₂)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Relation.Binary.PropositionalEquality using (cong ; _≢_ )

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
open import Util using (maybeElim ; maybe⊥
                       ; either-of-these
                       ; ∧-injectiveᵣ ; ∧-injectiveₗ
                       ; de-morg₂ ; de-morg₁ ; excluded-middle
                       ; a≡false⇒a≢true
                       ; transport
                       ; lift
                       )
open import Data.Empty
open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary
open import Relation.Nullary.Negation hiding (excluded-middle)

open import Expr
open 𝕍 using (𝕍)
open 𝔸 using (𝔸)
open 𝔹 using (𝔹)

import Ctx

open import Level using (Level ; _⊔_)

module Flow (MapType : Set → Set)
            (MapInstance : Maps.IntMap MapType)
            where

  module Ctx' = Ctx MapType MapInstance


  data Stm (A : Set) : Set where
    block : List⁺ A → Stm A
    call  : A → Stm A
    assign : A → Stm A
    break : A → Stm A
    continue : A → Stm A
    return : A → 𝔸 → Stm A
    branch : A → 𝔹 → (List (Stm A)) → (List (Stm A)) → Stm A
    loop : A → 𝔹 → (List (Stm A)) → Stm A

  Is-Block : ∀ {A} → Stm A → Set
  Is-Block (block _) = ⊤
  {-# CATCHALL #-}
  Is-Block _ = ⊥

  Is-Loop : ∀ {A} → Stm A → Set
  Is-Loop (loop _ _ _) = ⊤
  {-# CATCHALL #-}
  Is-Loop _ = ⊥

  Is-Branch : ∀ {A} → Stm A → Set
  Is-Branch (branch _ _ _ _) = ⊤
  {-# CATCHALL #-}
  Is-Branch _ = ⊥

  Is-Leaf : ∀ {A} → Stm A → Set
  Is-Leaf (branch _ _ _ _) = ⊥
  Is-Leaf (loop _ _ _) = ⊥
  {-# CATCHALL #-}
  Is-Leaf _ = ⊤

  -- is-block' : ∀ {A} → (stm : Stm A → Dec (Is-Block stm))
  -- is-block' stm@(block _) = yes (Is-Block stm)
  -- is-block' _ = false
  -- is-block
  -- is-call : ∀ {A} → Stm A → Bool
  -- is-call (call _) = true
  -- is-call _ = false
  -- is-assign : ∀ {A} → Stm A → Bool
  -- is-assign (assign _) = true
  -- is-assign _ = false
  -- is-break : ∀ {A} → Stm A → Bool
  -- is-break (break _) = true
  -- is-break _ = false
  -- is-continue : ∀{A} → Stm A → Bool
  -- is-continue (continue _) = true
  -- is-continue _ = false

  -- is-return : ∀ {A} → Stm A → Bool
  -- is-return (return _ _) = true
  -- is-return _ = false

  -- is-branch : ∀ {A} → Stm A → Bool
  -- is-branch (branch _ _ _ _) = true
  -- is-branch _ = false

  -- is-loop : ∀ {A} → Stm A → Bool
  -- is-loop (loop _ _ _) = true
  -- is-loop _ = false

  -- is-leaf-stm : ∀  {A} → Stm A → Bool
  -- is-leaf-stm stm = is-block stm
  --                 ∨ is-call stm
  --                 ∨ is-assign stm
  --                 ∨ is-break stm
  --                 ∨ is-continue stm
  --                 ∨ is-return stm
  -- is-call-or-assign : ∀ {A}
  --                   → Stm A
  --                   → Bool
  -- is-call-or-assign stm =
  --   is-call stm ∨ is-assign stm

  -- is-control-flow-stm : ∀ {A}
  --                     → Stm A
  --                     → Bool
  -- is-control-flow-stm stm =
  --   is-break stm
  --   ∨ is-continue stm
  -- is-compound : ∀ {A}
  --             → Stm A
  --             → Bool
  -- is-compound stm =
  --   is-branch stm
  --   ∨ is-loop stm

  branch-true-case : ∀ {A} → (stm : Stm A) → Is-Branch stm → List (Stm A)
  branch-true-case (branch _ _ tc _ ) ⊤ = tc
  branch-true-case (block _) ()
  branch-true-case (call _) ()
  branch-true-case (assign _) ()
  branch-true-case (break _) ()
  branch-true-case (continue _) ()
  branch-true-case (return _ _) ()
  branch-true-case (loop _ _ _) ()

  branch-false-case : ∀ {A} → (stm : Stm A) → Is-Branch stm → List (Stm A)
  branch-false-case (branch _ _ _ fc ) ⊤ = fc
  branch-false-case (block _) ()
  branch-false-case (call _) ()
  branch-false-case (assign _) ()
  branch-false-case (break _) ()
  branch-false-case (continue _) ()
  branch-false-case (return _ _) ()
  branch-false-case (loop _ _ _) ()

  loop-body : ∀ {A} → (stm : Stm A) → Is-Loop stm → List (Stm A)
  loop-body (loop _ _ body) ⊤ = body
  loop-body (branch _ _ _ _) ()
  loop-body (block _) ()
  loop-body (call _) ()
  loop-body (assign _) ()
  loop-body (break _) ()
  loop-body (continue _) ()
  loop-body (return _ _) ()


  Ctx : Set → Set
  Ctx stm = Ctx'.Ctx (Stm stm)

  Sequence : Set → Set
  Sequence a = List (Stm a)

  runSubSem : ∀ {stm}
            → Ctx stm
            → (Ctx'.Ctx stm → List stm → Ctx'.Ctx stm)
            → MapType (List 𝕍 × List 𝕍 × List stm)
            → List stm
            → Ctx stm
  runSubSem ctx stmSem stmProcs statements =
    let smtCtx = record { procedures = stmProcs
                        ; variables = Ctx'.variables ctx
                        ; loopStack = []
                        }
    in record ctx
       { variables =
          Ctx'.variables (stmSem smtCtx statements)
       }
  semantic : ∀ {stm}
       → Ctx stm
       → (Ctx'.Ctx stm → List stm → Ctx'.Ctx stm)
       → MapType (List 𝕍 × List 𝕍 × List stm)
       → List (Stm stm)
       → (Ctx stm × ((Sequence stm) ⊎ ℤ))

  semanticStep : ∀ {stm}
       → Ctx stm
       → (Ctx'.Ctx stm → List stm → Ctx'.Ctx stm)
       → MapType (List 𝕍 × List 𝕍 × List stm)
       → List (Stm stm)
       → (Ctx stm × ((Sequence stm) ⊎ ℤ))

  semantic {stm} ctx stmSem stmProcs seq =
    Data.Sum.[ onContinue , const onReturn ]′ res
   where
    step = semanticStep ctx stmSem stmProcs seq
    ctx' = proj₁ step
    res = proj₂ step
    onReturn : Ctx stm × ((List (Stm stm)) ⊎ ℤ)
    onReturn  =
      ( Ctx'.clearLocal ctx'
      , res
      )
    onContinue : List (Stm stm) → Ctx stm × ((List (Stm stm)) ⊎ ℤ)
    onContinue [] =
      Data.Product.map id inj₁  (fromMaybe (ctx , []) (Ctx'.popContinue ctx'))
    onContinue xs =
      (ctx' , inj₁ xs)

  semanticStep ctx stmSem stmProcs [] =
    (ctx , inj₁ [])
  semanticStep ctx stmSem stmProcs
               (block ( stmt₁ NE.∷ stmts) ∷ rest)
               with stmts
  ... | [] = ( runSubSem ctx stmSem stmProcs [ stmt₁ ]
             , inj₁ rest)
  ... | (stmt₂ ∷ stmts') = ( runSubSem ctx stmSem stmProcs [ stmt₁ ]
                            , inj₁ (block (stmt₂ NE.∷ stmts')  ∷ rest)
                            )

  semanticStep ctx stmSem stmProcs (call a ∷ rest) =
    ( runSubSem ctx stmSem stmProcs [ a ]
    , inj₁ rest
    )
  semanticStep ctx stmSem stmProcs (assign a ∷ rest) =
    ( runSubSem ctx stmSem stmProcs [ a ]
    , inj₁ rest
    )
  semanticStep ctx stmSem stmProcs (break _ ∷ rest) =
      maybe (Data.Product.map₂ inj₁)
            (semanticStep ctx stmSem stmProcs rest)
            (Ctx'.popBreak ctx)
  semanticStep ctx stmSem stmProcs (continue _ ∷ rest) =
    maybe (Data.Product.map₂ inj₁)
          (semanticStep ctx stmSem stmProcs rest)
          (Ctx'.popContinue ctx)
  semanticStep ctx stmSem stmProcs (return _ expr ∷ rest) =
    ( ctx , inj₂ (Ctx'.eval𝔸 expr ctx) )
  semanticStep ctx stmSem stmProcs (branch _ guard tc fc ∷ rest) =
    if Ctx'.eval𝔹 guard ctx
    then ( Ctx'.pushStack [] rest ctx , inj₁ tc )
    else ( Ctx'.pushStack [] rest ctx , inj₁ fc )
  semanticStep ctx stmSem stmProcs e@(loop _ guard body ∷ rest) =
    if Ctx'.eval𝔹 guard ctx
    then ( Ctx'.pushStack e rest ctx , inj₁ body )
    else ( ctx , inj₁ rest )

  data Any {j : Level} {A : Set} (P : Stm A → Set j) (stm : Stm A) : Set (j) where
    here : P stm → Any P stm
    in-branch : (t : Is-Branch stm)
              → (List.Any (Any P) (branch-true-case stm t))
                ⊎ (List.Any (Any P) (branch-false-case stm t))
              → Any P stm
    in-loop : (t : Is-Loop stm)
            → List.Any (Any P) (loop-body stm t)
            → Any P stm
  AnySeq : ∀ {j} {A : Set} (P : Stm A → Set j) → (List (Stm A)) → Set j
  AnySeq {j} {A} P seq =
    List.Any (Any P) seq

  {-# TERMINATING #-}
  any : ∀ {i} {A : Set} → {P : Pred (Stm A) i} → Decidable P → Decidable (Any P)
  any {_} {A} {P} p stm@(loop a guard body) with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' = dec-in-body
      where
        absurd : ¬ List.Any (Any P) body → Any P (loop a guard body) → ⊥
        absurd _ (Any.here p) = contradiction p ¬p'
        absurd notInList (Any.in-loop _ in-list) =
          contradiction in-list notInList
        absurd _ (Any.in-branch t _ ) = t
        dec-in-body : Dec (Any P stm)
        dec-in-body with list-any (any p) body
        ...| yes p'' = yes (Any.in-loop unit p'')
        ...| no notInList = no (λ inloop →
                                  ⊥-elim (absurd notInList inloop))
  any {_} {A} {P} p stm@(branch a guard tc fc) with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' = dec-in-branch
      where
        absurd : ( ¬ (List.Any (Any P) tc) × ¬ (List.Any (Any P) fc))
                → Any P (branch a guard tc fc) → ⊥
        absurd _ (Any.here p) = contradiction p ¬p'
        absurd _ (Any.in-loop t in-list) = t
        absurd (¬in-tc , _) (Any.in-branch t (inj₁ in-tc) ) =
          contradiction in-tc ¬in-tc
        absurd (_ , ¬in-fc) (Any.in-branch t (inj₂ in-fc) ) =
          contradiction in-fc ¬in-fc
        dec-in-branch : Dec (Any P stm)
        dec-in-branch with list-any (any p) tc | list-any (any p) fc
        ...| yes in-tc | _ = yes (Any.in-branch unit (inj₁ in-tc))
        ...| no ¬in-tc | yes in-fc = yes (Any.in-branch unit (inj₂ in-fc))
        ...| no ¬in-tc | no ¬in-fc = no (λ in-branch →
                                ⊥-elim (absurd (¬in-tc , ¬in-fc) in-branch))
  any p stm@(call a) with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
                       ; (Any.in-loop t _) → t
                       ; (Any.in-branch t _) → t
                       }
                       )
  any p stm@(assign a) with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
                       ; (Any.in-loop t _) → t
                       ; (Any.in-branch t _) → t
                       })
  any p stm@(continue a) with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
                       ; (Any.in-loop t _) → t
                       ; (Any.in-branch t _) → t
                       })
  any p stm@(break a) with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
                       ; (Any.in-loop t _) → t
                       ; (Any.in-branch t _) → t
                       })
  any p stm@(return a _) with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
                       ; (Any.in-loop t _) → t
                       ; (Any.in-branch t _) → t
                       })
  any p stm@(block a) with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
                       ; (Any.in-loop t _) → t
                       ; (Any.in-branch t _) → t
                       })
  -- any-bool : {A : Set} → (Stm A → Bool) → Stm A → Bool
  -- any-bool p stm = isYes (any (λ stm → T? (p stm)) stm)

  -- any-seq-bool : {A : Set} → (Stm A → Bool) → List (Stm A) → Bool
  -- any-seq-bool p stms = isYes (any-seq (λ stm → T? (p stm)) stms)

  data All {j : Level} {A : Set} (P : Stm A → Set j) (stm : Stm A) : Set (j) where
    all-leaf : Is-Leaf stm → P stm → All P stm
    all-branch : (t : Is-Branch stm)
                →  P stm
                → List.All (All P) (branch-true-case stm t)
                → List.All (All P) (branch-false-case stm t)
                → All P stm
    all-loop : (t : Is-Loop stm)
             → P stm
             → List.All (All P) (loop-body stm t)
             → All P stm
  AllSeq : ∀ {j} {A : Set} (P : Stm A → Set j) → (List (Stm A)) → Set j
  AllSeq {j} {A} P seq =
    List.All (All P) seq

  map-all : {j : Level} {A : Set}
          → {P : Stm A → Set j}
          → {stm : Stm A}
          → {P' : Stm A → Set j}
          → ({stm' : Stm A} → P stm'  → P' stm')
          → All P stm
          → All P' stm
  map-all {_} {_} {_} {stm} f (all-leaf t pred) =
    all-leaf t (f pred)
  map-all {_} {A} {P} {stm} {P'} f (all-loop t pred pred-children) =
    all-loop t (f pred) (rec pred-children)
    where
      rec : {seq : List (Stm A)} → AllSeq P seq → AllSeq P' seq
      rec List.All.[] =
        List.All.[]
      rec (x List.All.∷ xs) =
        (map-all f x) List.All.∷ rec xs
  map-all {_} {A} {P} {stm} {P'} f (all-branch t pred tc-pred fc-pred) =
    all-branch t (f pred) (rec tc-pred) (rec fc-pred)
    where
      rec : {seq : List (Stm A)} → AllSeq P seq → AllSeq P' seq
      rec List.All.[] =
        List.All.[]
      rec (x List.All.∷ xs) =
        (map-all f x) List.All.∷ rec xs


  map-all-seq : {j : Level} {A : Set}
              → {P : Stm A → Set j}
              → {stms : List (Stm A)}
              → {P' : Stm A → Set j}
              → ({stm' : Stm A} → P stm'  → P' stm')
              → AllSeq P stms
              → AllSeq P' stms
  map-all-seq _ List.All.[] =
        List.All.[]
  map-all-seq f (x List.All.∷ xs) =
        (map-all f x) List.All.∷ map-all-seq f xs
  -- all : {A : Set} → (Stm A  → Bool) → Stm A → Bool
  -- all p stm = not (any-bool (not ∘ p) stm)

  -- all-seq : {A : Set } → (Stm A → Bool) → List (Stm A) → Bool
  -- all-seq p [] = true
  -- all-seq p (x ∷ xs) = all p x ∧ all-seq p xs

  annotations : ∀{A} → (Stm A) → List⁺ A
  annotations (block annots) = annots
  annotations (call a) = NE.[ a ]
  annotations (assign a) = NE.[ a ]
  annotations (break a) = NE.[ a ]
  annotations (continue a) = NE.[ a ]
  annotations (return a _) = NE.[ a ]
  annotations (branch a _ _ _) = NE.[ a ]
  annotations (loop a _ _) = NE.[ a ]

  num-annots : ∀{A} → Stm A → ℕ
  num-annots stm = NE.length (annotations stm)

  first-annot : ∀{A} → Stm A → A
  first-annot stm = NE.head (annotations stm)

  last-annot : ∀{A} → Stm A → A
  last-annot stm = NE.last (annotations stm)


  record ContractionCtx  (A : Set) : Set where
    field
      is-local : (𝕍 → Bool)
      symbolFunc : (A → Maybe (These 𝕍 𝕍))
      interesting-locals : List 𝕍
      interesting-globals : List 𝕍
      interesting-procedures : List 𝕍

  syms-interesting? : {A : Set}
                     → ContractionCtx A
                     → A
                     → Bool
  syms-interesting? cc e with (ContractionCtx.symbolFunc cc) e
  ... | nothing = false
  ... | just t =
    either-of-these
      (λ x → if ContractionCtx.is-local cc x
             then Data.List.any
                    (𝕍.equals x)
                    (ContractionCtx.interesting-locals cc)
             else Data.List.any
                    (𝕍.equals x)
                    (ContractionCtx.interesting-globals cc))
      (λ x → Data.List.any
               (𝕍.equals x)
               (ContractionCtx.interesting-procedures cc))
      t
  annot-interesting : {A : Set}
                    → ContractionCtx A
                    → Stm A
                    → Bool
  annot-interesting cc stm =
    syms-interesting? cc (first-annot stm)

  interesting-node : ∀ {A}
                   → ContractionCtx A
                   → Stm A
                   → Bool
  interesting-node cc stm@(call a) = annot-interesting cc stm
  interesting-node cc stm@(assign a) = annot-interesting cc stm
  interesting-node cc (return _ _) = true
  interesting-node _ _ = false

  interesting? : ∀ {A}
              → (cc : ContractionCtx A)
              → (stm : Stm A)
              → Dec (Any (λ z → T (interesting-node cc z)) stm)
  interesting? cc stm =
    (any (λ stm → T? (interesting-node cc stm)) stm)

  interesting : ∀ {A}
              → ContractionCtx A
              → Stm A
              → Bool
  interesting cc stm =
    isYes (interesting? cc stm)

  {-# TERMINATING #-}
  blockify-stm : ∀ {a}
               → ContractionCtx a
               → Stm a
               → Stm a
  blockify-stm cc stm@(call e) =
    if interesting cc stm
    then call e
    else block NE.[ e ]
  blockify-stm cc stm@(assign e) =
    if interesting cc stm
    then assign e
    else block NE.[ e ]
  blockify-stm cc stm@(loop e guard body) =
    if interesting cc stm
    then loop e guard (Data.List.map (λ s → blockify-stm cc s) body)
    else block NE.[ e ]
  blockify-stm cc stm@(branch e guard tc fc) =
    if interesting cc stm
    then branch e guard (Data.List.map (λ s → blockify-stm cc s) tc)
                        (Data.List.map (λ s → blockify-stm cc s) fc)
    else block NE.[ e ]
  blockify-stm cc stm@(block _) = stm
  blockify-stm cc stm@(break _) = stm
  blockify-stm cc stm@(continue _) = stm
  blockify-stm cc stm@(return _ _) = stm

  blockify-seq : ∀ {a}
                      → ContractionCtx a
                      → List (Stm a)
                      → List (Stm a)
  blockify-seq cc xs =
    Data.List.map (λ x → blockify-stm cc x) xs

  {-# TERMINATING #-}
  unblockify-stm : {A : Set}
                 → (A → Stm A)
                 → (stm : Stm A)
                 → Stm A
  unblockify-stm f (block (x NE.∷ _)) =
    f x
  unblockify-stm {a} f (loop e guard body) =
    (loop e guard (Data.List.map (unblockify-stm f) body))

  unblockify-stm {A} f (branch e guard tc  fc) =
    (branch e guard (Data.List.map (unblockify-stm f) tc)
                    (Data.List.map (unblockify-stm f) fc))
  {-# CATCHALL #-}
  unblockify-stm _ stm = stm

  unblockify-seq : {A : Set}
                 → (A → Stm A)
                 → List (Stm A)
                 → List (Stm A)
  unblockify-seq f xs =
    (Data.List.map (unblockify-stm f) xs)

  contract-head : ∀ {A}
            → Stm A
            → List (Stm A)
            → List (Stm A)
  contract-head (block bs₁) (block bs₂ ∷ rest) =
    block (bs₁ ⁺++⁺ bs₂) ∷ rest
  {-# CATCHALL #-}
  contract-head x rest =
    x ∷ rest

  contract : ∀ {A}
                    → List (Stm A)
                    → List (Stm A)
  contract [] = []
  contract (branch a guard tc fc  ∷ rest) =
    (branch a guard (contract tc) (contract fc)) ∷ contract rest
  contract (loop a guard body  ∷ rest) =
    (loop a guard (contract body)) ∷ contract rest
  contract {A} (block xs ∷  rest) =
    contract-head (block xs) (contract rest)

  {-# CATCHALL #-}
  contract (x ∷ rest) =
    x ∷ contract rest

  uncontract : ∀ {A}
             → List (Stm A)
             → List (Stm A)
  uncontract [] = []
  uncontract (branch a guard tc fc  ∷ rest) =
    (branch a guard (uncontract tc) (uncontract fc)) ∷ uncontract rest
  uncontract (loop a guard body  ∷ rest) =
    (loop a guard (uncontract body)) ∷ uncontract rest

  uncontract {A} (block xs ∷ rest) =
    NE.toList (NE.map (λ x → block NE.[ x ]) xs) ++ uncontract rest

  {-# CATCHALL #-}
  uncontract (x ∷ rest) =
    x ∷ uncontract rest

  module Properties where


    -- blockify-block-is-not : ∀ {A}
    --                       → (cc : ContractionCtx A)
    --                       → (stm : Stm A)
    --                       → T(is-block stm)
    --                       → (blockify-stm cc stm ≢ _)
    -- contract-blocks-assoc : ∀ {a}
    --                       → (s1 : Σ[ s ∈ Stm a ] T(is-block s))
    --                       → (s2 : Σ[ s ∈ Stm a ] T(is-block s))
    --                       → (s3 : Σ[ s ∈ Stm a ] T(is-block s))
    --                       → contract-blocks s1 (contract-blocks s2 s3)
    --                         ≡ contract-blocks (contract-blocks s1 s2) s3
    -- contract-blocks-assoc s1@(block c1 , unit) s2@(block c2 , unit) s3@(block c3 , unit) =
    --   cong (λ x → (block x , unit)) (sym (Data.List.Properties.++-assoc c1 c2 c3))
    -- all-seq≡true⇒all-tail≡true : ∀ {A}
    --                                 → {p : Stm A → Bool}
    --                                 → (seq : List (Stm A))
    --                                 → all-seq p seq ≡ true
    --                                 → all-seq p (drop 1 seq) ≡ true
    -- all-seq≡true⇒all-tail≡true {A} {P} [] prop = refl
    -- all-seq≡true⇒all-tail≡true {A} {p} (x ∷ xs) all-seq≡true =
    --   ∧-injectiveᵣ  all-seq≡true

    -- all≡not-any : ∀ {A}
    --             → (p : Stm A → Bool)
    --             → (stm : Stm A)
    --             → all p stm ≡ not (any (not ∘ p) stm)
    -- all≡not-any p stm = refl

    -- all-seq≡not-any-seq : ∀ {A}
    --                      → (p : Stm A → Bool)
    --                      → (stms : List (Stm A))
    --                      → all-seq p stms ≡ not (any-seq (not ∘ p) stms)
    -- all-seq≡not-any-seq _ [] = refl
    -- all-seq≡not-any-seq p (x ∷ xs) =
    --   begin
    --     all-seq p (x ∷ xs)
    --   ≡⟨⟩
    --     all p x ∧ all-seq p xs
    --   ≡⟨ cong (all p x ∧_) (all-seq≡not-any-seq p xs)⟩
    --     all p x ∧ not (any-seq (not ∘ p) xs)
    --   ≡⟨⟩
    --     not (any (not ∘ p) x) ∧ not (any-seq (not ∘ p) xs)
    --   ≡⟨ de-morg₂ (any (not ∘ p) x) (any-seq (not ∘ p) xs) ⟩
    --     not ((any (not ∘ p) x) ∨ (any-seq (not ∘ p) xs))
    --   ≡⟨⟩
    --     not (any-seq (not ∘ p) (x ∷ xs))
    --   ∎
    IsInteresting : ∀ {A}
                  → ContractionCtx A
                  → Stm A
                  → Set
    IsInteresting cc stm =
      T (interesting-node cc stm)

    BlockifyProps : ∀ {A}
                  → ContractionCtx A
                  → (A → Stm A) -- Translation function
                  → (stm : Stm A) -- Blockified statement
                  → Set
    BlockifyProps cc _ stm@(call a) =
      Any (IsInteresting cc) stm
    BlockifyProps cc _ stm@(assign a) =
      Any (IsInteresting cc) stm
    BlockifyProps cc _ stm@(branch _ _ tc fc) =
      Any (IsInteresting cc) stm
    BlockifyProps cc f stm@(loop _ _ _) =
      Any (IsInteresting cc) stm
    BlockifyProps cc f stm@(block xs) =
      (f (first-annot stm)) ≡ stm
      ⊎ (¬ (Any (IsInteresting cc) (f (first-annot stm))))
    BlockifyProps cc _ _ = ⊤

    must-be-interesting-stm : ∀ {A}
                       → (cc : ContractionCtx A)
                       → (stm : Stm A)
                       → (Any (IsInteresting cc) stm)
                       → Σ[ p ∈ (Any (IsInteresting cc) stm) ]
                          (interesting? cc stm ≡ yes p)
    must-be-interesting-stm cc stm p with lift (interesting? cc stm)
    ...| yes since , interesting?≡yes = ( since , interesting?≡yes)
    ...| no since , interesting?≡no =
      contradiction p since

    must-be-boring : ∀ {A}
                   → (cc : ContractionCtx A)
                   → (stm : Stm A)
                   → ¬ (Any (IsInteresting cc) stm)
                   → Σ[ p ∈ ¬ (Any (IsInteresting cc) stm) ]
                       (interesting? cc stm ≡ no p)
    must-be-boring cc stm p with lift (interesting? cc stm)
    ...| yes since , _ = contradiction since p
    ...| no since , interesting?≡no = ( since , interesting?≡no)
    unblockify-keeps-interesting : ∀ {A}
                               → (cc : ContractionCtx A)
                               → (f : A → Stm A)
                               → (stm : Stm A)
                               → (Any (IsInteresting cc) stm)
                               → Σ[ p ∈ Any (IsInteresting cc) (unblockify-stm f stm) ]
                                   (interesting? cc (unblockify-stm f stm) ≡ yes p)
    unblockify-keeps-interesting cc f (call a) (Any.here p) =
      must-be-interesting-stm cc (call a)
                                 (Any.here p)
    unblockify-keeps-interesting cc f (assign a) (Any.here p) =
      must-be-interesting-stm cc (assign a)
                                 (Any.here p)
    unblockify-keeps-interesting cc _ (return a _) (Any.here p) =
      must-be-interesting-stm cc (return a _)
                                 (Any.here p)
    unblockify-keeps-interesting _ _ (continue _) (Any.here ())
    unblockify-keeps-interesting _ _ (break _) (Any.here ())
    unblockify-keeps-interesting _ _ (block _) (Any.here ())
    unblockify-keeps-interesting _ _ (loop _ _ _) (Any.here ())
    unblockify-keeps-interesting _ _ (loop _ _ []) (Any.in-loop _ ())
    unblockify-keeps-interesting {A} cc f stm@(loop a guard (x ∷ xs)) t@(Any.in-loop _ (List.Any.here p)) =
      must-be-interesting-stm cc (loop a guard (unblockify-seq f (x ∷ xs)))
                                 loop-is-interesting
      where
        x-is-interesting : Any (IsInteresting cc) (unblockify-stm f x)
        x-is-interesting = proj₁ (unblockify-keeps-interesting cc f x p)
        loop-is-interesting : Any (IsInteresting cc) (loop a guard (unblockify-seq f (x ∷ xs)))
        loop-is-interesting = Any.in-loop unit (List.Any.here x-is-interesting)
    unblockify-keeps-interesting {A} cc f stm@(loop a guard (x ∷ xs)) t@(Any.in-loop _ s@(List.Any.there p))
      with (proj₁ (unblockify-keeps-interesting cc f (loop a guard xs) (Any.in-loop _ p)))
    ...| Any.in-loop _ p' = must-be-interesting-stm cc (loop a guard (unblockify-seq f (x ∷ xs)))
                                                    (Any.in-loop unit (List.Any.there p'))
    ...| Any.in-branch () _
    ...| Any.here ()
    unblockify-keeps-interesting cc f (branch a guard tc fc) (Any.here interesting) =
      contradiction interesting λ z → z
    unblockify-keeps-interesting cc f (branch a guard [] []) (Any.in-branch _ (inj₁ ()))
    unblockify-keeps-interesting cc f (branch a guard [] (y ∷ ys)) (Any.in-branch _ (inj₁ ()))
    unblockify-keeps-interesting cc f (branch a guard [] []) (Any.in-branch _ (inj₂ ()))
    unblockify-keeps-interesting cc f (branch a guard (x ∷ xs) []) (Any.in-branch _ (inj₂ ()))
    {-#  CATCHALL #-}
    unblockify-keeps-interesting cc f (branch a guard tc (y ∷ ys)) (in-branch t (inj₂ (List.Any.here p))) =
      must-be-interesting-stm cc (branch a guard (unblockify-seq f tc) (unblockify-seq f (y ∷ ys)))
                                 (Any.in-branch unit
                                   (inj₂ (List.Any.here (proj₁ (unblockify-keeps-interesting cc f y p)))))
    {-#  CATCHALL #-}
    unblockify-keeps-interesting cc f (branch a guard tc (y ∷ ys))
                                      (in-branch t (inj₂ (List.Any.there p)))
                                      with (proj₁ (unblockify-keeps-interesting cc f (branch a guard tc ys)
                                                                                     (Any.in-branch _ (inj₂ p))))
    ...| Any.in-branch _ (inj₂ p') =
       must-be-interesting-stm cc (branch a guard
                                          (unblockify-seq f tc)
                                          (unblockify-seq f (y ∷ ys)))
                                          (Any.in-branch unit (inj₂ (List.Any.there p')))
    ...| Any.in-branch _ (inj₁ p') =
       must-be-interesting-stm cc (branch a guard
                                          (unblockify-seq f tc)
                                          (unblockify-seq f (y ∷ ys)))
                                          (Any.in-branch unit (inj₁ p'))
    {-#  CATCHALL #-}
    unblockify-keeps-interesting cc f (branch a guard (x ∷ xs) fc) (in-branch t (inj₁ (List.Any.here p))) =
      must-be-interesting-stm cc (branch a guard (unblockify-seq f (x ∷ xs)) (unblockify-seq f fc))
                                 (Any.in-branch unit
                                   (inj₁ (List.Any.here (proj₁ (unblockify-keeps-interesting cc f x p)))))
    {-#  CATCHALL #-}
    unblockify-keeps-interesting cc f (branch a guard (x ∷ xs) fc)
                                      (in-branch t (inj₁ (List.Any.there p)))
                                      with (proj₁ (unblockify-keeps-interesting cc f (branch a guard xs fc)
                                                                                     (Any.in-branch _ (inj₁ p))))
    ...| Any.in-branch _ (inj₂ p') =
       must-be-interesting-stm cc (branch a guard
                                          (unblockify-seq f (x ∷ xs))
                                          (unblockify-seq f fc))
                                          (Any.in-branch unit (inj₂ p'))
    ...| Any.in-branch _ (inj₁ p') =
       must-be-interesting-stm cc (branch a guard
                                          (unblockify-seq f (x ∷ xs))
                                          (unblockify-seq f fc))
                                          (Any.in-branch unit (inj₁ (List.Any.there p')))

    blockify-keeps-interesting : ∀ {A}
                               → (cc : ContractionCtx A)
                               → (stm : Stm A)
                               → Any (IsInteresting cc) stm
                               → Any (IsInteresting cc)
                                     (blockify-stm cc stm)
    blockify-keeps-interesting cc (block xs) (Any.here intr) =
        ⊥-elim intr
    blockify-keeps-interesting cc (continue xs) (Any.here intr) =
        ⊥-elim intr
    blockify-keeps-interesting cc (break xs) (Any.here intr) =
        ⊥-elim intr
    blockify-keeps-interesting cc (return a _) (Any.here intr) =
        (Any.here intr)
    blockify-keeps-interesting cc (call a) (Any.here intr)
      with lift (interesting? cc (call a))
    ... | (yes since , interesting≡yes) =
        transport (cong (Any (λ x → T (interesting-node cc x))) keeps)
                  (Any.here intr)
        where
          keeps : blockify-stm cc (call a) ≡ (call a)
          keeps = cong (λ x → if isYes x then call a else  block NE.[ a ])
                       interesting≡yes
    ...| (no since , _) =
      contradiction (Any.here intr) since
    blockify-keeps-interesting cc (assign a) (Any.here intr)
      with lift (interesting? cc (assign a))
    ... | (yes since , interesting≡yes) =
      transport (cong (Any (λ x → T (interesting-node cc x))) keeps)
                (Any.here intr)
      where
        keeps : blockify-stm cc (assign a) ≡ (assign a)
        keeps = cong (λ x → if isYes x then assign a else  block NE.[ a ])
                     interesting≡yes
    ...| (no since , _) =
      contradiction (Any.here intr) since
    blockify-keeps-interesting {A} cc stm@(loop a guard body)
                                  p@(Any.in-loop t in-body) =
      transport (cong (Any (λ x → T (interesting-node cc x)))
                      keeps)
                (Any.in-loop unit (rec (loop-body stm t) in-body))
      where
        interesting?≡yes : isYes (interesting? cc stm) ≡ true
        interesting?≡yes with lift (interesting? cc stm)
        ... | (yes p , is-yes) = cong isYes is-yes
        ... | (no ¬p , _) = contradiction p ¬p

        keeps : blockify-stm cc (loop a guard body)
              ≡ (loop a guard (blockify-seq cc body))
        keeps = cong (λ x → if x
                            then loop a guard (blockify-seq cc body)
                            else block NE.[ a ])
                     interesting?≡yes

        rec : (stms : List (Stm A))
            → List.Any (Any (λ x → T (interesting-node cc x)))
                       stms →
             List.Any (Any (λ x → T (interesting-node cc x)))
                      (blockify-seq cc stms)

        rec (stm ∷ _) (List.Any.here p) =
          List.Any.here (blockify-keeps-interesting cc stm p)
        rec (stm ∷ stms) (List.Any.there rest) =
          List.Any.there (rec stms rest)
    blockify-keeps-interesting {A} cc stm@(branch a guard tc fc)
                                  p@(Any.in-branch t (inj₁ in-tc)) =
      transport (cong (Any (λ x → T (interesting-node cc x))) keeps)
                    (Any.in-branch unit (inj₁ (rec tc in-tc)))
      where
        interesting?≡yes : isYes (interesting? cc stm) ≡ true
        interesting?≡yes with lift (interesting? cc stm)
        ... | (yes p , is-yes) = cong isYes is-yes
        ... | (no ¬p , _) = contradiction p ¬p

        keeps : blockify-stm cc (branch a guard tc fc)
              ≡ (branch a guard (blockify-seq cc tc) (blockify-seq cc fc))
        keeps = cong (λ x → if x
                            then branch a guard
                                        (blockify-seq cc tc)
                                        (blockify-seq cc fc)
                            else block NE.[ a ])
                     interesting?≡yes

        rec : (stms : List (Stm A))
            → List.Any (Any (λ x → T (interesting-node cc x)))
                       stms →
             List.Any (Any (λ x → T (interesting-node cc x)))
                      (blockify-seq cc stms)

        rec (stm ∷ _) (List.Any.here p) =
          List.Any.here (blockify-keeps-interesting cc stm p)
        rec (stm ∷ stms) (List.Any.there rest) =
          List.Any.there (rec stms rest)

        fc-case : List.Any (Any (λ x → T (interesting-node cc x))) fc
                → Any (λ x → T (interesting-node cc x)) (blockify-stm cc stm)
        fc-case fc' =
          transport (cong (Any (λ x → T (interesting-node cc x))) keeps)
                    (Any.in-branch unit (inj₂ (rec fc fc')))
    blockify-keeps-interesting {A} cc stm@(branch a guard tc fc)
                                  p@(Any.in-branch t (inj₂ in-fc)) =
      transport (cong (Any (λ x → T (interesting-node cc x))) keeps)
                    (Any.in-branch unit (inj₂ (rec fc in-fc)))
      where
        interesting?≡yes : isYes (interesting? cc stm) ≡ true
        interesting?≡yes with lift (interesting? cc stm)
        ... | (yes p , is-yes) = cong isYes is-yes
        ... | (no ¬p , _) = contradiction p ¬p

        keeps : blockify-stm cc (branch a guard tc fc)
              ≡ (branch a guard (blockify-seq cc tc) (blockify-seq cc fc))
        keeps = cong (λ x → if x
                            then branch a guard
                                        (blockify-seq cc tc)
                                        (blockify-seq cc fc)
                            else block NE.[ a ])
                     interesting?≡yes

        rec : (stms : List (Stm A))
            → List.Any (Any (λ x → T (interesting-node cc x)))
                       stms →
             List.Any (Any (λ x → T (interesting-node cc x)))
                      (blockify-seq cc stms)

        rec (stm ∷ _) (List.Any.here p) =
          List.Any.here (blockify-keeps-interesting cc stm p)
        rec (stm ∷ stms) (List.Any.there rest) =
          List.Any.there (rec stms rest)


    blockify-props : ∀ {A}
                   → (cc : ContractionCtx A)
                   → (f : A → Stm A)
                   → (seq : List (Stm A))
                   → AllSeq (λ stm →
                                   num-annots stm ≡ 1
                                   × stm ≡ f (first-annot stm)
                               )
                             seq
                   → AllSeq (λ stm → BlockifyProps cc f stm
                                   × num-annots stm ≡ 1
                                   × (stm ≡ blockify-stm cc (f (first-annot stm)))
                                   )
                            (blockify-seq cc seq)
    blockify-props cc f [] List.All.[] = List.All.[]
    blockify-props
      cc f
      (stm@(block xs) ∷ rest)
      ((all-leaf t (num-annots≡1 , self-translated)) List.All.∷ rest-prop) =
      (all-leaf t ( inj₁ bar
                  , num-annots≡1
                  , cong (blockify-stm cc) self-translated))
      List.All.∷ (blockify-props cc f rest rest-prop)
      where
        blockify-block : blockify-stm cc (block xs) ≡ block xs
        blockify-block = refl
        bar : f (first-annot (blockify-stm cc (block xs))) ≡ (blockify-stm cc (block xs))
        bar = trans (cong (f ∘ first-annot) blockify-block)  (sym self-translated)

    blockify-props cc f
                   (stm@(call a) ∷ rest)
                   ((all-leaf t (num-annots≡1 , self-translated)) List.All.∷ rest-prop)
                   with lift (interesting? cc (call a))
    ... | (yes since , interesting?≡yes) =
      ((all-leaf (transport (cong Is-Leaf blockify≡id) unit)
                ( transport (cong (BlockifyProps cc f) blockify≡id)
                            since
                , (trans (cong num-annots  blockify≡id) num-annots≡1)
                , trans (cong (blockify-stm cc) self-translated)
                        (cong (λ z → (blockify-stm cc (f (first-annot z))))
                           (sym blockify≡id))
                ))
      List.All.∷ (blockify-props cc f rest rest-prop))
      where
       blockify≡id : (blockify-stm cc (call a)) ≡ call a
       blockify≡id =
         cong (λ x → if isYes x then (call a) else block NE.[ a ])
              interesting?≡yes

    ... | (no since , interesting?≡no) =
      ((all-leaf (transport (cong Is-Leaf blockify≡block) unit)
                 ( transport (cong (BlockifyProps cc f) blockify≡block)
                             (inj₂ (transport same-absurdity since))
                 , (trans (cong num-annots  blockify≡block) num-annots≡1)
                 , trans (cong (blockify-stm cc) self-translated)
                         (trans (cong (λ z → (blockify-stm cc (f z))) refl)
                                (cong (λ z → (blockify-stm cc (f (first-annot z))))
                                      (sym blockify≡block)))
                 ))
      List.All.∷ (blockify-props cc f rest rest-prop))

      where
       same-absurdity : ¬_ (Any (λ x → T (interesting-node cc x)) (f (first-annot (block  NE.[ a ]))))
           ≡ ¬_ (Any (λ x → T (interesting-node cc x)) (call a))
       same-absurdity = cong (¬_ ∘ Any (λ x → T (interesting-node cc x)))
                  (sym self-translated)
       blockify≡block : (blockify-stm cc (call a)) ≡ block NE.[ a ]
       blockify≡block =
         cong (λ x → if isYes x then (call a) else block NE.[ a ])
              interesting?≡no

    blockify-props cc f
                   (stm@(assign a) ∷ rest)
                   ((all-leaf t (num-annots≡1 , self-translated)) List.All.∷ rest-prop)
                   with lift (interesting? cc (assign a))
    ... | (yes since , interesting?≡yes) =
      ((all-leaf (transport (cong Is-Leaf blockify≡id) unit)
                ( transport (cong (BlockifyProps cc f) blockify≡id)
                            since
                , (trans (cong num-annots  blockify≡id) num-annots≡1)
                , trans (cong (blockify-stm cc) self-translated)
                        (cong (λ z → (blockify-stm cc (f (first-annot z))))
                           (sym blockify≡id))
                ))
      List.All.∷ (blockify-props cc f rest rest-prop))
      where
       blockify≡id : (blockify-stm cc (assign a)) ≡ assign a
       blockify≡id =
         cong (λ x → if isYes x then (assign a) else block NE.[ a ])
              interesting?≡yes
    ... | (no since , interesting?≡no) =
      ((all-leaf (transport (cong Is-Leaf blockify≡block) unit)
                 ( transport (cong (BlockifyProps cc f) blockify≡block)
                            (inj₂ (transport same-absurdity since))
                 , (trans (cong num-annots  blockify≡block) num-annots≡1)
                 , trans (cong (blockify-stm cc) self-translated)
                         (trans (cong (λ z → (blockify-stm cc (f z))) refl)
                                (cong (λ z → (blockify-stm cc (f (first-annot z))))
                                      (sym blockify≡block)))
                 ))
      List.All.∷ (blockify-props cc f rest rest-prop))

      where
       same-absurdity : ¬_ (Any (λ x → T (interesting-node cc x)) (f (first-annot (block  NE.[ a ]))))
           ≡ ¬_ (Any (λ x → T (interesting-node cc x)) (assign a))
       same-absurdity = cong (¬_ ∘ Any (λ x → T (interesting-node cc x)))
                  (sym self-translated)
       blockify≡block : (blockify-stm cc (assign a)) ≡ block NE.[ a ]
       blockify≡block =
         cong (λ x → if isYes x then (assign a) else block NE.[ a ])
              interesting?≡no
    blockify-props cc f
                   (break a ∷ rest)
                   ((all-leaf t (num-annots≡1
                                , self-translated)) List.All.∷ rest-prop) =
      ((all-leaf t ( unit , num-annots≡1 , cong (blockify-stm cc) self-translated))
      List.All.∷ (blockify-props cc f rest rest-prop))
    blockify-props cc f
                   (return a _ ∷ rest)
                   ((all-leaf t (num-annots≡1 , s-trans)) List.All.∷ rest-prop) =
      ((all-leaf t (unit , num-annots≡1 , cong (blockify-stm cc) s-trans))
      List.All.∷ (blockify-props cc f rest rest-prop))
    blockify-props cc f
                   (continue a ∷ rest)
                   ((all-leaf t (num-annots≡1
                                , self-translated))
                    List.All.∷ rest-prop) =
      ((all-leaf t( unit , num-annots≡1 , cong (blockify-stm cc) self-translated))
      List.All.∷ (blockify-props cc f rest rest-prop))
    blockify-props {A} cc f
                   (stm@(loop a guard body) ∷ rest)
                   ((all-loop t (num-annots≡1
                                , self-translated)
                                body-props)
                    List.All.∷ rest-prop)
                    with lift (interesting? cc stm)
    ...| (yes since , interesitng?≡yes) =
           (transport (sym all-is-same)
                       (all-loop
                         unit
                         ( keeps-any
                         , num-annots≡1
                         , (begin
                             loop a guard (blockify-seq cc body)
                           ≡⟨ sym blockified-is-loop ⟩
                             (blockify-stm cc (loop a guard body))
                           ≡⟨ cong (blockify-stm cc) self-translated ⟩
                              (blockify-stm cc (f (first-annot (loop a guard body))))
                           ≡⟨ cong (blockify-stm cc ∘ f) refl ⟩
                              (blockify-stm cc (f (first-annot (loop a guard (blockify-seq cc body)))))
                           ∎))
                         (blockify-props cc f body body-props)
                       )
             List.All.∷ (blockify-props cc f rest rest-prop))
         where
           blockified-is-loop : (if interesting cc stm
                                 then loop a guard (blockify-seq cc body)
                                 else block NE.[ a ])
                              ≡ (loop a guard (blockify-seq cc body))
           blockified-is-loop =
             cong (λ x →
                     if isYes x
                     then loop a guard (blockify-seq cc body)
                     else block NE.[ a ])
                  interesitng?≡yes
           all-is-same : All (λ stm → BlockifyProps cc f stm
                                    × num-annots stm ≡ 1
                                    × stm ≡ blockify-stm cc (f (first-annot stm)))
                             (loop a guard (blockify-seq cc body))
                      ≡ All (λ stm → BlockifyProps cc f stm
                                    × num-annots stm ≡ 1
                                    × stm ≡ blockify-stm cc (f (first-annot stm))
                                    )
                             (if interesting cc stm
                              then loop a guard (blockify-seq cc body)
                              else block NE.[ a ])
           all-is-same =
             cong (All (λ stm → BlockifyProps cc f stm
                              × num-annots stm ≡ 1
                              × stm ≡ blockify-stm cc (f (first-annot stm))))
                  (sym blockified-is-loop)

           blockify≡keep : blockify-stm cc stm
                           ≡ loop a guard (blockify-seq cc body)
           blockify≡keep =
             cong (λ x →
                     if isYes x
                     then loop a guard (blockify-seq cc body)
                     else block NE.[ a ])
                  interesitng?≡yes

           any-is-same : Any (λ x → T (interesting-node cc x))
                          (blockify-stm cc stm)
                       ≡ Any (λ x → T (interesting-node cc x))
                          (loop a guard (blockify-seq cc body))
           any-is-same =
             cong (Any (λ x → T (interesting-node cc x)))
                  blockify≡keep
           keeps-any :  Any (λ x → T (interesting-node cc x))
                        (loop a guard (blockify-seq cc body))
           keeps-any = transport (sym any-is-same)
                             (blockify-keeps-interesting cc stm since)

    ...| (no since , interesting?≡no) =
      ( transport all-is-block
                  (all-leaf unit
                            ( inj₂ (transport same-absurdity since)
                            , refl
                            , (begin
                                 block NE.[ a ]
                               ≡⟨ sym blockify≡block ⟩
                                 blockify-stm cc (loop a guard body)
                               ≡⟨ cong (blockify-stm cc) self-translated ⟩
                                 blockify-stm cc (f (first-annot (loop a guard body)))
                               ≡⟨ cong (blockify-stm cc ∘ f) refl ⟩
                                 blockify-stm cc (f (first-annot (block NE.[ a ])))
                               ∎
                              )
                            ))
       List.All.∷ (blockify-props cc f rest rest-prop))
      where
--        blockify-proos : cong (blockify-stm cc) self-translated
        same-absurdity : ¬_ (Any (λ x → T (interesting-node cc x))
                                 (f (first-annot (block  NE.[ a ]))))
           ≡ ¬_ (Any (λ x → T (interesting-node cc x))
                      (loop a guard body))
        same-absurdity = cong (¬_ ∘ Any (λ x → T (interesting-node cc x)))
                  (sym self-translated)
        blockified : Stm A
        blockified = block NE.[ a ]
        blockify≡block : blockify-stm cc stm
                        ≡ blockified
        blockify≡block =
             cong (λ x →
                     if isYes x
                     then loop a guard (blockify-seq cc body)
                     else block NE.[ a ])
                  interesting?≡no
        all-is-block : All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm))))
                           (blockify-stm cc stm)
                     ≡ All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm))))
                            (block NE.[ a ])
        all-is-block =
           cong (All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm)))))
                blockify≡block
    blockify-props {A} cc f
                   (stm@(branch a guard tc fc) ∷ rest)
                   ((all-branch t (num-annots≡1 , self-translated) tc-props fc-props)
                    List.All.∷ rest-prop)
                    with lift (interesting? cc stm)
    ...| (yes since , interesitng?≡yes) =
            (transport (sym all-is-same)
                       (all-branch
                         unit
                         ( keeps-any
                         , num-annots≡1
                         , (begin
                             branch a guard (blockify-seq cc tc) (blockify-seq cc fc)
                           ≡⟨ sym blockified-is-branch ⟩
                             blockify-stm cc stm
                           ≡⟨ cong (blockify-stm cc) self-translated ⟩
                             blockify-stm cc (f (first-annot stm))
                           ≡⟨ cong (blockify-stm cc ∘ f) refl ⟩
                             blockify-stm cc
                                          (f (first-annot (branch a guard (blockify-seq cc tc)
                                                                          (blockify-seq cc fc))))
                           ∎)
                         )
                         (blockify-props cc f tc tc-props)
                         (blockify-props cc f fc fc-props)
                       )
             List.All.∷ (blockify-props cc f rest rest-prop))
         where
           blockified-is-branch : (if interesting cc stm
                                 then branch a guard (blockify-seq cc tc) (blockify-seq cc fc)
                                 else block NE.[ a ])
                              ≡ branch a guard (blockify-seq cc tc) (blockify-seq cc fc)
           blockified-is-branch =
             cong (λ x →
                     if isYes x
                     then branch a guard (blockify-seq cc tc) (blockify-seq cc fc)
                     else block NE.[ a ])
                  interesitng?≡yes
           all-is-same : All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm))))
                      (branch a guard (blockify-seq cc tc) (blockify-seq cc fc))
               ≡ All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm))))
                      (if interesting cc stm
                       then branch a guard (blockify-seq cc tc) (blockify-seq cc fc)
                       else block NE.[ a ])
           all-is-same =
             cong (All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm)))))
                  (sym blockified-is-branch)

           blockify≡keep : blockify-stm cc stm
                           ≡ branch a guard (blockify-seq cc tc) (blockify-seq cc fc)
           blockify≡keep =
             cong (λ x →
                     if isYes x
                     then branch a guard (blockify-seq cc tc) (blockify-seq cc fc)
                     else block NE.[ a ])
                  interesitng?≡yes

           any-is-same : Any (λ x → T (interesting-node cc x))
                          (blockify-stm cc stm)
                       ≡ Any (λ x → T (interesting-node cc x))
                          (branch a guard (blockify-seq cc tc) (blockify-seq cc fc))
           any-is-same =
             cong (Any (λ x → T (interesting-node cc x)))
                  blockify≡keep
           keeps-any :  Any (λ x → T (interesting-node cc x))
                        (branch a guard (blockify-seq cc tc) (blockify-seq cc fc))
           keeps-any = transport (sym any-is-same)
                             (blockify-keeps-interesting cc stm since)
    ...| (no since , interesting?≡no) =
      ( transport all-is-block
                  (all-leaf unit
                            ( inj₂ (transport same-absurdity since)
                            , refl
                            , (begin
                                 block NE.[ a ]
                               ≡⟨ sym blockify≡block ⟩
                                 blockify-stm cc stm
                               ≡⟨ cong (blockify-stm cc) self-translated ⟩
                                 blockify-stm cc (f (first-annot stm))
                               ≡⟨ cong (blockify-stm cc ∘ f) refl ⟩
                                 blockify-stm cc (f (first-annot (block NE.[ a ])))
                               ∎
                              )
                            ))
       List.All.∷ (blockify-props cc f rest rest-prop))
      where
        same-absurdity : ¬_ (Any (λ x → T (interesting-node cc x)) (f (first-annot (block  NE.[ a ]))))
           ≡ ¬_ (Any (λ x → T (interesting-node cc x)) (branch a guard tc fc))
        same-absurdity = cong (¬_ ∘ Any (λ x → T (interesting-node cc x)))
                  (sym self-translated)
        blockified : Stm A
        blockified = block NE.[ a ]
        blockify≡block : blockify-stm cc stm
                        ≡ blockified
        blockify≡block =
             cong (λ x →
                     if isYes x
                     then branch a guard (blockify-seq cc tc) (blockify-seq cc fc)
                     else block NE.[ a ])
                  interesting?≡no
        all-is-block : All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm))))
                           (blockify-stm cc stm)
                     ≡ All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm))))
                            (block NE.[ a ])
        all-is-block =
           cong (All (λ stm → BlockifyProps cc f stm
                                  × num-annots stm ≡ 1
                                  × (stm ≡ blockify-stm cc (f (first-annot stm)))))
                blockify≡block


    NewlyTranslated : ∀ {A}
                    → (transl-function : A → Stm A)
                    → (stm : Stm A)
                    → Set
    NewlyTranslated f stm =
      (stm ≡ f (first-annot stm))


    -- Proof that putting the boring things into blocks does not lose any
    -- infomration.
    unblockify∘blockify≡id : ∀ {A}
                            → (f : A → Stm A)
                            → (cc : ContractionCtx A)
                            → (seq : List (Stm A))
                            → AllSeq (λ stm → (num-annots stm ≡ 1)
                                            × NewlyTranslated f stm
                                         )
                                     seq
                            → (unblockify-seq f (blockify-seq cc seq)) ≡ seq
    unblockify∘blockify≡id _ _ [] _ = refl
    unblockify∘blockify≡id
      f cc
      seq@(stm@(continue _) ∷ rest)
      (_ List.All.∷ rest-props) =
      cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props)
    unblockify∘blockify≡id
      f cc
      seq@(stm@(break _) ∷ rest)
      (_ List.All.∷ rest-props) =
      cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props)
    unblockify∘blockify≡id
      f cc
      seq@(stm@(return _ _) ∷ rest)
      (_ List.All.∷ rest-props) =
      cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props)
    unblockify∘blockify≡id
      f cc
      seq@(stm@(assign a) ∷ rest)
      ((all-leaf _ stm-props) List.All.∷ rest-props)
      with lift (interesting? cc stm)
    ... | (yes since , interesting?≡yes) =
        begin
          unblockify-seq f (blockify-seq cc seq)
        ≡⟨ cong (λ x → unblockify-seq f ((if isYes x
                                               then assign a
                                               else block NE.[ a ])
                                              ∷ blockify-seq cc rest))
                 interesting?≡yes
          ⟩
          unblockify-seq f ((assign a) ∷ blockify-seq cc rest)
        ≡⟨ cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props) ⟩
          seq
        ∎
    ... | (no since , interesting≡no)  =
        begin
          unblockify-seq f (blockify-seq cc seq)
        ≡⟨ cong (λ x → unblockify-seq
                          f
                          ((if isYes x
                            then assign a
                            else block NE.[ a ])
                          ∷ blockify-seq cc rest))
                 interesting≡no
          ⟩
          unblockify-seq f ((block NE.[ a ])
                                   ∷ blockify-seq cc rest)
        ≡⟨⟩
          f a ∷ unblockify-seq f (blockify-seq cc rest)
        ≡⟨ cong (f a ∷_)
                 (unblockify∘blockify≡id
                   f cc rest rest-props
                   )
          ⟩
          f a ∷ rest
        ≡⟨ cong (_∷ rest) (sym (proj₂ stm-props)) ⟩
          seq
        ∎
    unblockify∘blockify≡id f cc
                            seq@(stm@(call a) ∷ rest)
                            ((all-leaf _ stm-props) List.All.∷ rest-props)
                            with lift (interesting? cc stm)
    ... | (yes since , interesting?≡yes) =
        begin
          unblockify-seq f (blockify-seq cc seq )
        ≡⟨ cong (λ x → unblockify-seq f ((if isYes x
                                                  then call a
                                                  else block NE.[ a ])
                                                ∷ blockify-seq cc rest))
                 interesting?≡yes
          ⟩
          unblockify-seq f ((call a) ∷ blockify-seq cc rest)
        ≡⟨ cong (stm ∷_) (unblockify∘blockify≡id f cc rest rest-props) ⟩
          seq
        ∎
    ... | (no since , interesting?≡no) =
        begin
          unblockify-seq f (blockify-seq cc seq )
        ≡⟨ cong (λ x → unblockify-seq f ((if isYes x
                                               then call a
                                               else block NE.[ a ])
                                              ∷ blockify-seq cc rest))
                 interesting?≡no
          ⟩
          unblockify-seq f ((block NE.[ a ]) ∷ blockify-seq cc rest)
        ≡⟨⟩
          f a ∷ unblockify-seq f (blockify-seq cc rest)
        ≡⟨ cong (f a ∷_)
                 (unblockify∘blockify≡id
                   f cc rest rest-props
                   )
          ⟩
          f a ∷ rest
        ≡⟨ cong (_∷ rest) (sym (proj₂ stm-props)) ⟩
          seq
        ∎
    unblockify∘blockify≡id f cc
                            seq@(stm@(block (x NE.∷ [])) ∷ rest)
                            ((all-leaf _ stm-props) List.All.∷ rest-props) =
      begin
        (unblockify-seq f (blockify-seq cc seq))
      ≡⟨⟩
        (unblockify-seq f (stm ∷ (blockify-seq cc rest)))
      ≡⟨⟩
        (f x ∷ unblockify-seq f (blockify-seq cc rest))
      ≡⟨ cong (f x ∷_)
               (unblockify∘blockify≡id
                 f cc rest rest-props)
        ⟩
         (f x ∷ rest)
      ≡⟨ cong (_∷ rest) (sym (proj₂ stm-props)) ⟩
        seq
      ∎
    unblockify∘blockify≡id
      f cc
      seq@(stm@(block annots@(x₁ NE.∷ x₂ ∷ _)) ∷ rest)
      ((all-leaf _ stm-props) List.All.∷ rest-props) =
      contradiction (proj₁ stm-props) (λ ())
    unblockify∘blockify≡id
      f cc
      seq@(stm@(loop a guard body) ∷ rest)
      ((all-loop _ stm-props body-props) List.All.∷ rest-props)
      with excluded-middle (interesting cc stm)
    ... | inj₁ interesting≡true =
      begin
        unblockify-seq f (blockify-stm cc stm
                                ∷ blockify-seq cc rest)
      ≡⟨ cong (λ x →
                 unblockify-seq
                   f
                   ((if x
                     then loop a guard (Data.List.map (λ s → blockify-stm cc s) body)
                     else block NE.[ a ]
                     ) ∷ blockify-seq cc rest
                   ))
               interesting≡true
        ⟩
        unblockify-seq f
          ((loop a guard (Data.List.map (λ s → blockify-stm cc s) body))
           ∷ blockify-seq cc rest)
      ≡⟨⟩
        [ (loop a guard
                  (Data.List.map
                    (unblockify-stm f)
                    (Data.List.map
                      (λ s → blockify-stm cc s)
                        body
                        )
                    )
          ) ]
        ++ unblockify-seq f (blockify-seq cc rest)
      ≡⟨⟩
        (loop a guard
              (unblockify-seq f (
                blockify-seq cc body
              )))
        ∷ unblockify-seq f (blockify-seq cc rest)
      ≡⟨ cong (λ x →
                 (loop a guard x)
                 ∷ unblockify-seq f (blockify-seq cc rest))
               (unblockify∘blockify≡id f cc body body-props)
        ⟩
        (loop a guard body)
        ∷ unblockify-seq f (blockify-seq cc rest)
      ≡⟨ cong ((loop a guard body) ∷_)
               (unblockify∘blockify≡id
                 f cc rest rest-props)
          ⟩
        seq
      ∎
    ... | inj₂ interesting≡false =
      begin
        unblockify-seq f (blockify-stm cc stm ∷ blockify-seq cc rest)
      ≡⟨ cong (λ x →
                 unblockify-seq
                   f
                   ((if x
                     then loop a guard (Data.List.map (λ s → blockify-stm cc s) body)
                     else block NE.[ a ]
                     ) ∷ blockify-seq cc rest))
               interesting≡false
        ⟩
        unblockify-seq f ( (block NE.[ a ]) ∷ blockify-seq cc rest)
      ≡⟨ cong (f a ∷_)
               (unblockify∘blockify≡id
                 f cc rest rest-props)
          ⟩
         (f a ∷ rest)
      ≡⟨ cong (_∷ rest) (sym (proj₂ stm-props)) ⟩
        seq
      ∎

    unblockify∘blockify≡id
      f cc
      seq@(stm@(branch a guard tc fc) ∷ rest)
      ((all-branch _ stm-props tc-props fc-props) List.All.∷ rest-props)
      with excluded-middle (interesting cc stm)
    ... | inj₁ interesting≡true =
      begin
        unblockify-seq f (blockify-stm cc stm ∷ blockify-seq cc rest)
      ≡⟨ cong (λ x →
                 unblockify-seq
                   f
                   ((if x
                     then branch a guard (Data.List.map (λ s → blockify-stm cc s) tc)
                                         (Data.List.map (λ s → blockify-stm cc s) fc)
                     else block NE.[ a ]
                     ) ∷ blockify-seq cc rest
                   )

               )
               interesting≡true

        ⟩
        (branch a guard
                  (unblockify-seq f (
                    blockify-seq cc tc))
                  (unblockify-seq f (
                    blockify-seq cc fc))
        )
        ∷ unblockify-seq f (blockify-seq cc rest)

      ≡⟨ cong (λ x →
                 (branch a guard x (unblockify-seq f (blockify-seq cc fc)))
                 ∷ unblockify-seq f (blockify-seq cc rest))
               (unblockify∘blockify≡id f cc tc tc-props)
        ⟩
        (branch a guard tc (unblockify-seq f (blockify-seq cc fc)))
        ∷ unblockify-seq f (blockify-seq cc rest)
      ≡⟨ cong (λ x →
                 (branch a guard tc x)
                 ∷ unblockify-seq f (blockify-seq cc rest))
               (unblockify∘blockify≡id f cc fc fc-props)
        ⟩
        (branch a guard tc fc)
        ∷ unblockify-seq f (blockify-seq cc rest)
      ≡⟨ cong ((branch a guard tc fc) ∷_)
               (unblockify∘blockify≡id
                 f cc rest rest-props)
          ⟩
        seq
      ∎
    ... | inj₂ interesting≡false =
      begin
        unblockify-seq f (blockify-stm cc stm
                                ∷ blockify-seq cc rest)
      ≡⟨ cong (λ x →
                 unblockify-seq
                   f
                   ((if x
                     then branch
                            a guard
                            (Data.List.map
                              (λ s → blockify-stm cc s)
                              tc)
                            (Data.List.map
                              (λ s → blockify-stm cc s)
                              fc)
                     else block NE.[ a ]
                     ) ∷ blockify-seq cc rest
                   )

               )
               interesting≡false
        ⟩
        unblockify-seq f ( (block NE.[ a ]) ∷ blockify-seq cc rest)
      ≡⟨ cong (f a ∷_)
               (unblockify∘blockify≡id
                 f cc rest rest-props)
          ⟩
         (f a ∷ rest)
      ≡⟨ cong (_∷ rest) (sym (proj₂ stm-props)) ⟩
        seq
      ∎

    blockify∘unblockify≡id : ∀ {A}
                            → (f : A → Stm A)
                            → (cc : ContractionCtx A)
                            → (seq : List (Stm A))
                            → AllSeq (λ stm →
                                        (BlockifyProps cc f stm)
                                        × (num-annots stm ≡ 1)
                                        × (stm ≡ blockify-stm cc (f (first-annot stm)))
                                        )

                                     seq
                            → (blockify-seq cc (unblockify-seq f seq))
                              ≡ seq
    blockify∘unblockify≡id _ _ [] _  = refl
    blockify∘unblockify≡id f cc
                            seq@(stm@(continue _) ∷ rest)
                            (_ List.All.∷ rest-props) =
      cong (stm ∷_) (blockify∘unblockify≡id f cc  rest rest-props)
    blockify∘unblockify≡id f cc
                            seq@(stm@(break _) ∷ rest)
                            (_ List.All.∷ rest-props) =
      cong (stm ∷_) (blockify∘unblockify≡id f cc  rest rest-props)
    blockify∘unblockify≡id f cc
                            seq@(stm@(return _ _) ∷ rest)
                            (_ List.All.∷ rest-props) =
      cong (stm ∷_) (blockify∘unblockify≡id f cc  rest rest-props)
    blockify∘unblockify≡id f cc
                            seq@(stm@(call a) ∷ rest)
                            (all-leaf _ (any-interesting
                                        , _ , _) List.All.∷ rest-props)
                           with lift (interesting? cc stm)
    ...| (yes since , interesting?≡yes) =
      begin
        blockify-seq cc (unblockify-seq f seq)
      ≡⟨⟩
        blockify-seq cc (call a ∷ (unblockify-seq f rest))
      ≡⟨⟩
        (blockify-stm cc (call a) ∷ blockify-seq cc (unblockify-seq f rest))
      ≡⟨ cong (blockify-stm cc (call a) ∷_)
               (blockify∘unblockify≡id f cc  rest rest-props)
        ⟩
        (blockify-stm cc (call a) ∷ rest)
      ≡⟨ cong (λ x → (if isYes x then call a else block NE.[ a ])  ∷ rest)
               interesting?≡yes
        ⟩
        seq
      ∎
    ...| no since , interesting≡false =
      contradiction any-interesting since
    blockify∘unblockify≡id f cc
                            seq@(stm@(assign a) ∷ rest)
                            (all-leaf _ (any-interesting
                                        , _) List.All.∷ rest-props)
                           with lift (interesting? cc stm)
    ...| (yes since , interesting?≡yes) =
      begin
        blockify-seq cc (unblockify-seq f seq)
      ≡⟨⟩
        blockify-seq cc (assign a ∷ (unblockify-seq f rest))
      ≡⟨⟩
        (blockify-stm cc (assign a) ∷ blockify-seq cc (unblockify-seq f rest))
      ≡⟨ cong (blockify-stm cc (assign a) ∷_)
               (blockify∘unblockify≡id f cc  rest rest-props)
        ⟩
        (blockify-stm cc (assign a) ∷ rest)
      ≡⟨ cong (λ x → (if isYes x then assign a else block NE.[ a ])  ∷ rest)
               interesting?≡yes
        ⟩
        seq
      ∎
    ...| no since , interesting≡false =
      contradiction any-interesting since
    blockify∘unblockify≡id f cc
                            seq@(stm@(loop a guard body) ∷ rest)
                            ((all-loop _
                                      (any-interesting
                                      , _)
                                      body-props)
                             List.All.∷ rest-props)
                           with lift (interesting? cc stm)
    ...| (no since , interesting≡false) =
      contradiction any-interesting since
    ...| (yes since , interesting?≡yes) =
      begin
        blockify-seq cc (unblockify-seq f seq)
      ≡⟨⟩
        blockify-seq cc (loop a guard (unblockify-seq f body)
                        ∷ (unblockify-seq f rest))
      ≡⟨⟩
        blockify-stm cc (loop a guard (unblockify-seq f body))
        ∷ blockify-seq cc (unblockify-seq f rest)
      ≡⟨ cong (_∷_ (blockify-stm cc (loop a guard (unblockify-seq f body))))
               (blockify∘unblockify≡id f cc  rest rest-props)
        ⟩
        blockify-stm cc (loop a guard (unblockify-seq f body))
        ∷ rest
      ≡⟨⟩
        ((if interesting cc (loop a guard (unblockify-seq f body))
         then loop a guard (blockify-seq cc (unblockify-seq f body))
         else block NE.[ a ]) ∷ rest)
      ≡⟨ cong (λ x → (if isYes x
                       then loop a guard (blockify-seq cc (unblockify-seq f body))
                       else block NE.[ a ])
                      ∷ rest)
               (proj₂ (unblockify-keeps-interesting cc f stm any-interesting))
        ⟩
        (loop a guard (blockify-seq cc (unblockify-seq f body))
        ∷ rest)
      ≡⟨ cong (λ x → (loop a guard x) ∷ rest)
               (blockify∘unblockify≡id f cc  body body-props)
        ⟩
        seq
      ∎

    blockify∘unblockify≡id f cc
                            seq@(stm@(branch a guard tc fc) ∷ rest)
                            ((all-branch _
                                      (any-interesting
                                      , _)
                                      tc-props
                                      fc-props
                                      )
                             List.All.∷ rest-props)
                           with lift (interesting? cc stm)
    ...| (no since , interesting≡false) =
      contradiction any-interesting since
    ...| (yes since , interesting?≡yes) =
      begin
        (blockify-seq cc (unblockify-seq f seq))
      ≡⟨⟩
        (blockify-seq cc ((branch a guard (unblockify-seq f tc)
                                          (unblockify-seq f fc))
                         ∷ (unblockify-seq f rest)))
      ≡⟨⟩
         (if interesting cc (branch a guard (unblockify-seq f tc)
                                          (unblockify-seq f fc))
         then branch a guard (blockify-seq cc (unblockify-seq f tc))
                             (blockify-seq cc (unblockify-seq f fc))
         else block NE.[ a ])
         ∷ (blockify-seq cc (unblockify-seq f rest))
      ≡⟨ cong (λ x →
                (if isYes x
                 then branch a guard (blockify-seq cc (unblockify-seq f tc))
                                     (blockify-seq cc (unblockify-seq f fc))
                 else block NE.[ a ])
                 ∷ (blockify-seq cc (unblockify-seq f rest))
               )
               (proj₂ (unblockify-keeps-interesting cc f stm any-interesting))
        ⟩
         (branch a guard (blockify-seq cc (unblockify-seq f tc))
                         (blockify-seq cc (unblockify-seq f fc)))
         ∷ (blockify-seq cc (unblockify-seq f rest))
      ≡⟨ cong ((branch a guard (blockify-seq cc (unblockify-seq f tc))
                               (blockify-seq cc (unblockify-seq f fc)))
               ∷_)
               (blockify∘unblockify≡id f cc  rest rest-props)
        ⟩
         (branch a guard (blockify-seq cc (unblockify-seq f tc))
                         (blockify-seq cc (unblockify-seq f fc)))
         ∷ rest
      ≡⟨ cong₂ (λ x y → (branch a guard x y) ∷ rest)
                (blockify∘unblockify≡id f cc  tc tc-props)
                (blockify∘unblockify≡id f cc  fc fc-props)
        ⟩
        seq
      ∎
    blockify∘unblockify≡id f cc
                            seq@(stm@(block ( x NE.∷ [])) ∷ rest)
                            ((All.all-leaf _ (inj₁ direct-transl
                                             , _))
                            List.All.∷ rest-props)
                            =
      begin
        (blockify-seq cc (unblockify-seq f seq))
      ≡⟨⟩
        (blockify-seq cc (f x ∷ (unblockify-seq f rest)))
      ≡⟨ cong (λ g → blockify-seq cc (g ∷ (unblockify-seq f rest)))
              direct-transl
        ⟩
        (block NE.[ x ] ∷ blockify-seq cc (unblockify-seq f rest))
      ≡⟨ cong (block NE.[ x ] ∷_)
               (blockify∘unblockify≡id f cc  rest rest-props)
        ⟩
        seq
      ∎
    blockify∘unblockify≡id f cc
                            seq@(stm@(block ( x NE.∷ [])) ∷ rest)
                            ((All.all-leaf _ (inj₂ no-interesting
                                             , _
                                             , stm-freshly-blockified
                                             ))
                            List.All.∷ rest-props)
                            with lift (unblockify-stm f stm)
    ...| (stm'@(block (x' NE.∷ [])) , unblockify≡block) =
       begin
         (blockify-seq cc (f x ∷ (unblockify-seq f rest)))
       ≡⟨ cong (λ z → blockify-seq cc (z ∷ (unblockify-seq f rest)))
                unblockify≡block
         ⟩
         (stm' ∷ blockify-seq cc (unblockify-seq f rest))
       ≡⟨ cong (stm' ∷_)
               (blockify∘unblockify≡id f cc rest rest-props)
         ⟩
         (stm' ∷ rest)
       ≡⟨ cong (_∷ rest) (sym block[x]≡block[x']) ⟩
         seq
       ∎
      where
        block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
        block[x]≡block[x'] =
          begin
            block (NE.[ x ])
          ≡⟨ stm-freshly-blockified ⟩
            blockify-stm cc (f x)
          ≡⟨ cong (blockify-stm cc) unblockify≡block ⟩
            blockify-stm cc (block (NE.[ x' ]))
          ≡⟨⟩
            block (NE.[ x' ])
          ∎
    ...| (stm'@(block (x₁' NE.∷ (x₂ ∷ xs'))) , unblockify≡block) =
      ⊥-elim (absurd weird-stuff)
      where
        absurd : block (NE.[ x ]) ≡ block (x₁' NE.∷ (x₂ ∷ xs')) → ⊥
        absurd ()
        weird-stuff : block (NE.[ x ]) ≡ block (x₁' NE.∷ (x₂ ∷ xs'))
        weird-stuff =
          begin
            block (NE.[ x ])
          ≡⟨ stm-freshly-blockified ⟩
            blockify-stm cc (f x)
          ≡⟨ cong (blockify-stm cc) unblockify≡block ⟩
            blockify-stm cc stm'
          ∎

    ...| (stm'@(call x') , unblockify≡call) =
         begin
           (blockify-seq cc (f x ∷ (unblockify-seq f rest)))
         ≡⟨ cong (λ z → (blockify-seq cc (z ∷ (unblockify-seq f rest))))
                  unblockify≡call
           ⟩
           (blockify-seq cc (call x' ∷ (unblockify-seq f rest)))
         ≡⟨ cong (λ z → z ∷ blockify-seq cc (unblockify-seq f rest))
                  blockyfy-call≡block
           ⟩
           (block NE.[ x' ] ∷ (blockify-seq cc  (unblockify-seq f rest)))
         ≡⟨ cong₂ (_∷_)
                  (sym block[x]≡block[x'])
                  (blockify∘unblockify≡id f cc rest rest-props)
           ⟩
           seq
         ∎
       where
         same-absurd : (¬(Any (IsInteresting cc) (f x))) ≡ (¬(Any (IsInteresting cc) (call x')))
         same-absurd = cong (λ z → (¬(Any (IsInteresting cc) z)))
                            unblockify≡call
         still-not-interesting : ¬(Any (IsInteresting cc) (call x'))
         still-not-interesting = transport (sym same-absurd) no-interesting
         blockyfy-call≡block : blockify-stm cc (call x') ≡ block (NE.[ x' ])
         blockyfy-call≡block =
            cong (λ z → (if isYes z
                          then call x'
                          else block NE.[ x' ]
                          ))
                  (proj₂ (must-be-boring cc stm' still-not-interesting))
         block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
         block[x]≡block[x'] =
          begin
            block (NE.[ x ])
          ≡⟨ stm-freshly-blockified ⟩
            blockify-stm cc (f x)
          ≡⟨ cong (blockify-stm cc) unblockify≡call ⟩
            blockify-stm cc (call x')
          ≡⟨ blockyfy-call≡block ⟩
            block (NE.[ x' ])
          ∎
    ...| (stm'@(assign x') , unblockify≡assign) =
         begin
           (blockify-seq cc (f x ∷ (unblockify-seq f rest)))
         ≡⟨ cong (λ z → (blockify-seq cc (z ∷ (unblockify-seq f rest))))
                  unblockify≡assign
           ⟩
           (blockify-seq cc (assign x' ∷ (unblockify-seq f rest)))
         ≡⟨ cong (λ z → z ∷ blockify-seq cc (unblockify-seq f rest))
                  blockyfy-assign≡block
           ⟩
           (block NE.[ x' ] ∷ (blockify-seq cc  (unblockify-seq f rest)))
         ≡⟨ cong₂ (_∷_)
                  (sym block[x]≡block[x'])
                  (blockify∘unblockify≡id f cc rest rest-props)
           ⟩
           seq
         ∎
       where
         same-absurd : (¬(Any (IsInteresting cc) (f x))) ≡ (¬(Any (IsInteresting cc) (assign x')))
         same-absurd = cong (λ z → (¬(Any (IsInteresting cc) z)))
                            unblockify≡assign
         still-not-interesting : ¬(Any (IsInteresting cc) (assign x'))
         still-not-interesting = transport (sym same-absurd) no-interesting
         blockyfy-assign≡block : blockify-stm cc (assign x') ≡ block (NE.[ x' ])
         blockyfy-assign≡block =
            cong (λ z → (if isYes z
                          then assign x'
                          else block NE.[ x' ]
                          ))
                  (proj₂ (must-be-boring cc stm' still-not-interesting))
         block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
         block[x]≡block[x'] =
          begin
            block (NE.[ x ])
          ≡⟨ stm-freshly-blockified ⟩
            blockify-stm cc (f x)
          ≡⟨ cong (blockify-stm cc) unblockify≡assign ⟩
            blockify-stm cc (assign x')
          ≡⟨ blockyfy-assign≡block ⟩
            block (NE.[ x' ])
          ∎
    ...| (stm'@(continue x') , unblockify≡continue) =
         ⊥-elim (absurd (trans stm-freshly-blockified blockify-is-continue))
       where
         blockify-is-continue : blockify-stm cc (f x) ≡ continue x'
         blockify-is-continue =  cong (blockify-stm cc) unblockify≡continue
         absurd : block NE.[ x ] ≡ continue x' → ⊥
         absurd ()
    ...| (stm'@(break x') , unblockify≡break) =
         ⊥-elim (absurd (trans stm-freshly-blockified blockify-is-continue))
       where
         blockify-is-continue : blockify-stm cc (f x) ≡ break x'
         blockify-is-continue =  cong (blockify-stm cc) unblockify≡break
         absurd : block NE.[ x ] ≡ break x' → ⊥
         absurd ()
    ...| (stm'@(return x' expr) , unblockify≡return) =
         ⊥-elim (absurd (trans stm-freshly-blockified blockify-is-return))
       where
         blockify-is-return : blockify-stm cc (f x) ≡ return x' expr
         blockify-is-return =  cong (blockify-stm cc) unblockify≡return
         absurd : block NE.[ x ] ≡ return x' expr → ⊥
         absurd ()
    ...| (stm'@(loop x' guard body) , unblockify≡loop) =
       begin
         (blockify-seq cc (f x ∷ (unblockify-seq f rest)))
       ≡⟨ cong (λ z → blockify-seq cc (z ∷ (unblockify-seq f rest)))
                unblockify≡loop
         ⟩
         (blockify-stm cc stm' ∷ blockify-seq cc (unblockify-seq f rest))
       ≡⟨ cong (blockify-stm cc stm'  ∷_)
               (blockify∘unblockify≡id f cc rest rest-props)
         ⟩
         (blockify-stm cc stm'  ∷ rest)
       ≡⟨ cong (λ z →
                 (if isYes z
                  then loop x' guard (blockify-seq cc  body)
                  else block NE.[ x' ]) ∷ rest)
                (proj₂ (must-be-boring cc stm' loop-absurdity))
         ⟩
         block NE.[ x' ] ∷ rest
       ≡⟨ cong (_∷ rest) (sym  block[x]≡block[x']) ⟩
         seq
       ∎
       where
         same-absurd : (¬(Any (IsInteresting cc) (f x)))
                     ≡ (¬(Any (IsInteresting cc) (loop x' guard body)))
         same-absurd = cong (λ z → (¬(Any (IsInteresting cc) z)))
                            unblockify≡loop
         still-not-interesting : ¬(Any (IsInteresting cc) (loop x' guard body))
         still-not-interesting = transport (sym same-absurd) no-interesting
         blockyfy-loop≡block : blockify-stm cc (loop x' guard body) ≡ block (NE.[ x' ])
         blockyfy-loop≡block =
            cong (λ z → (if isYes z
                          then (loop x' guard (blockify-seq cc body))
                          else block NE.[ x' ]
                          ))
                  (proj₂ (must-be-boring cc stm' still-not-interesting))
         block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
         block[x]≡block[x'] =
          begin
            block (NE.[ x ])
          ≡⟨ stm-freshly-blockified ⟩
            blockify-stm cc (f x)
          ≡⟨ cong (blockify-stm cc) unblockify≡loop ⟩
            blockify-stm cc (loop x' guard body)
          ≡⟨ blockyfy-loop≡block ⟩
            block (NE.[ x' ])
          ∎
         loop-absurdity : ¬ (Any (IsInteresting cc) (loop x' guard body))
         loop-absurdity = transport (sym same-absurd) no-interesting
    ...| (stm'@(branch x' guard tc fc) , unblockify≡branch) =
       begin
         (blockify-seq cc (f x ∷ (unblockify-seq f rest)))
       ≡⟨ cong (λ z → blockify-seq cc (z ∷ (unblockify-seq f rest)))
                unblockify≡branch
         ⟩
         (blockify-stm cc stm' ∷ blockify-seq cc (unblockify-seq f rest))
       ≡⟨ cong (blockify-stm cc stm'  ∷_)
               (blockify∘unblockify≡id f cc rest rest-props)
         ⟩
         (blockify-stm cc stm'  ∷ rest)
       ≡⟨ cong (λ z →
                 (if isYes z
                  then branch x' guard (blockify-seq cc tc) (blockify-seq cc fc)
                  else block NE.[ x' ]) ∷ rest)
                (proj₂ (must-be-boring cc stm' branch-absurdity))
         ⟩
         block NE.[ x' ] ∷ rest
       ≡⟨ cong (_∷ rest) (sym  block[x]≡block[x']) ⟩
         seq
       ∎
       where
         same-absurd : (¬(Any (IsInteresting cc) (f x)))
                     ≡ (¬(Any (IsInteresting cc) (branch x' guard tc fc)))
         same-absurd = cong (λ z → (¬(Any (IsInteresting cc) z)))
                            unblockify≡branch
         still-not-interesting : ¬(Any (IsInteresting cc) (branch x' guard tc fc))
         still-not-interesting = transport (sym same-absurd) no-interesting
         blockyfy-branch≡block : blockify-stm cc (branch x' guard tc fc) ≡ block (NE.[ x' ])
         blockyfy-branch≡block =
            cong (λ z → (if isYes z
                          then (branch x' guard (blockify-seq cc tc) (blockify-seq cc fc))
                          else block NE.[ x' ]
                          ))
                  (proj₂ (must-be-boring cc stm' still-not-interesting))
         block[x]≡block[x'] : block (NE.[ x ]) ≡ block (NE.[ x' ])
         block[x]≡block[x'] =
          begin
            block (NE.[ x ])
          ≡⟨ stm-freshly-blockified ⟩
            blockify-stm cc (f x)
          ≡⟨ cong (blockify-stm cc) unblockify≡branch ⟩
            blockify-stm cc (branch x' guard tc fc)
          ≡⟨ blockyfy-branch≡block ⟩
            block (NE.[ x' ])
          ∎

         branch-absurdity : ¬ (Any (IsInteresting cc) (branch x' guard tc fc))
         branch-absurdity = transport (sym same-absurd) no-interesting
    blockify∘unblockify≡id f cc
                            ((block ( x₁ NE.∷ (x₂ ∷ xs))) ∷ rest)
                            (All.all-leaf _ ( _ , () , _)
                            List.All.∷ rest-props)
    NotBothBlocks : ∀ {A}
                  → Stm A
                  → Stm A
                  → Set
    NotBothBlocks (block _) (block _) = ⊥
    {-# CATCHALL #-}
    NotBothBlocks _ _ = ⊤

    not-blockʳ : ∀ {A}
               → (a b : Stm A)
               → (Is-Block a)
               → NotBothBlocks a b
               → ¬ (Is-Block b)
    not-blockʳ (block a) (block b) _ ()
    {-# CATCHALL #-}
    not-blockʳ (block a) (call _) _ _ = λ ()
    not-blockʳ (block a) (assign _) _ _ = λ ()
    not-blockʳ (block a) (continue _) _ _ = λ ()
    not-blockʳ (block a) (break _) _ _ = λ ()
    not-blockʳ (block a) (return _ _) _ _ = λ ()
    not-blockʳ (block a) (branch _ _ _ _) _ _ = λ ()
    not-blockʳ (block a) (loop _ _ _) _ _ = λ ()

    ContractedSeq : ∀ {A}
                    → List (Stm A)
                    → Set
    ContractedSeq seq = List.Linked NotBothBlocks seq

    ContractedChildren : ∀ {A}
                           → Stm A
                           → Set
    ContractedChildren (loop _ _ body) =
      ContractedSeq body
    ContractedChildren (branch _ _ tc fc) =
      (ContractedSeq tc)
      × (ContractedSeq fc)
    {-# CATCHALL #-}
    ContractedChildren _ = ⊤

    Contracted : ∀ {A}
                    → List (Stm A)
                    → Set
    Contracted seq =
      ContractedSeq seq
      × AllSeq ContractedChildren seq

    contract-not-null : ∀ {A}
                      → (seq : List (Stm A))
                      → seq ≢ []
                      → Σ[ x ∈ Stm A ] Σ[ xs ∈ List (Stm A)]
                          (contract seq ≡ (x ∷ xs))
    contract-not-null [] notEmpty =
      contradiction refl notEmpty
    contract-not-null seq@(call _ ∷ xs) _
      with lift (contract seq)
    ...| (x' ∷ xs' , is-self) = (x' , xs' , is-self)
    contract-not-null seq@(assign _ ∷ xs) _
      with lift (contract seq)
    ...| (x' ∷ xs' , is-self) = (x' , xs' , is-self)
    contract-not-null seq@(continue _ ∷ xs) _
      with lift (contract seq)
    ...| (x' ∷ xs' , is-self) = (x' , xs' , is-self)
    contract-not-null seq@(break a ∷ xs) _
      with lift (contract seq)
    ...| (x' ∷ xs' , is-self) = (x' , xs' , is-self)
    contract-not-null seq@(return _ _ ∷ xs) _
      with lift (contract seq)
    ...| (x' ∷ xs' , is-self) = (x' , xs' , is-self)
    contract-not-null seq@(branch _ _ _ _ ∷ xs) _
      with lift (contract seq)
    ...| (x' ∷ xs' , is-self) = (x' , xs' , is-self)
    contract-not-null seq@(loop _ _ _ ∷ xs) _
      with lift (contract seq)
    ...| (x' ∷ xs' , is-self) = (x' , xs' , is-self)
    contract-not-null seq@(block bs₁ ∷ xs) _
      with (contract xs)
    ...| []  =
      ( block (bs₁) , [] , refl)
    ...| block bs₂ ∷ rest =
      ( block (bs₁ ⁺++⁺ bs₂) , rest , refl)
    ...| (x@(call _) ∷ rest)  =
      ( block bs₁ , (x ∷ rest) , refl)
    ...| (x@(assign _) ∷ rest)  =
      ( block bs₁ , (x ∷ rest) , refl)
    ...| (x@(continue _) ∷ rest)  =
      ( block bs₁ , (x ∷ rest) , refl)
    ...| (x@(break _) ∷ rest)  =
      ( block bs₁ , (x ∷ rest) , refl)
    ...| (x@(return _ _) ∷ rest)  =
      ( block bs₁ , (x ∷ rest) , refl)
    ...| (x@(loop _ _ _) ∷ rest)  =
      ( block bs₁ , (x ∷ rest) , refl)
    ...| (x@(branch _ _ _ _) ∷ rest)  =
      ( block bs₁ , (x ∷ rest) , refl)


    contract-contracts : ∀ {A}
                       → (seq : List (Stm A))
                       → Contracted (contract seq)
    contract-contracts [] =
       List.Linked.[] , List.All.[]
    contract-contracts (call a ∷ []) =
       List.Linked.[-] , all-leaf unit unit List.All.∷ List.All.[]
    contract-contracts (assign a ∷ []) =
       List.Linked.[-] , all-leaf unit unit List.All.∷ List.All.[]
    contract-contracts (continue a ∷ []) =
      List.Linked.[-] , all-leaf unit unit List.∷ List.[]
    contract-contracts (break a ∷ []) =
      List.Linked.[-] , all-leaf unit unit List.∷ List.[]
    contract-contracts (return a expr ∷ []) =
      List.Linked.[-] , all-leaf unit unit List.∷ List.[]
    contract-contracts (block xs ∷ []) =
      List.Linked.[-] , all-leaf unit unit List.∷ List.[]
    contract-contracts seq@(stm@(loop _ _ body) ∷ []) =
      let ( body-contracted , children-contracted)
             = contract-contracts body
      in
      ( List.Linked.[-]
      , (all-loop unit body-contracted
                       children-contracted)
          List.All.∷ List.All.[]
      )
    contract-contracts seq@(stm@(branch _ _ tc fc) ∷ []) =
      let (tc-contracted , tc-children-contracted)
             = contract-contracts tc
          (fc-contracted , fc-children-contracted)
             = contract-contracts fc
      in
      ( List.Linked.[-]
      , (all-branch unit ( tc-contracted
                         , fc-contracted
                         )
                         tc-children-contracted
                         fc-children-contracted)
          List.All.∷ List.All.[]
      )
    contract-contracts seq@(stm@(call _) ∷ (x ∷ xs)) =
      let (rest-proof , child-proof ) =
              contract-contracts (x ∷ xs)
          (x' , xs' , contractredx∷xs≡x'∷xs') =
              contract-not-null (x ∷ xs) (λ ())
          same-rest-proof : ContractedSeq (x' ∷ xs') ≡ ContractedSeq (contract (x ∷ xs))
          same-rest-proof = cong ContractedSeq (sym contractredx∷xs≡x'∷xs')
          goal1 : ContractedSeq (stm ∷ contract (x ∷ xs))
          goal1 = transport (cong (ContractedSeq ∘ (stm ∷_)) contractredx∷xs≡x'∷xs')
                            (unit List.Linked.∷ (transport same-rest-proof rest-proof))

          goal2 = (All.all-leaf unit unit) List.All.∷ child-proof
      in ( goal1 , goal2 )
    contract-contracts seq@(stm@(assign _) ∷ (x ∷ xs)) =
      let (rest-proof , child-proof ) =
              contract-contracts (x ∷ xs)
          (x' , xs' , contractredx∷xs≡x'∷xs') =
              contract-not-null (x ∷ xs) (λ ())
          same-rest-proof : ContractedSeq (x' ∷ xs') ≡ ContractedSeq (contract (x ∷ xs))
          same-rest-proof = cong ContractedSeq (sym contractredx∷xs≡x'∷xs')
          goal1 : ContractedSeq (stm ∷ contract (x ∷ xs))
          goal1 = transport (cong (ContractedSeq ∘ (stm ∷_)) contractredx∷xs≡x'∷xs')
                            (unit List.Linked.∷ (transport same-rest-proof rest-proof))

          goal2 = (All.all-leaf unit unit) List.All.∷ child-proof
      in ( goal1 , goal2 )
    contract-contracts seq@(stm@(continue _) ∷ (x ∷ xs)) =
      let (rest-proof , child-proof ) =
              contract-contracts (x ∷ xs)
          (x' , xs' , contractredx∷xs≡x'∷xs') =
              contract-not-null (x ∷ xs) (λ ())
          same-rest-proof : ContractedSeq (x' ∷ xs') ≡ ContractedSeq (contract (x ∷ xs))
          same-rest-proof = cong ContractedSeq (sym contractredx∷xs≡x'∷xs')
          goal1 : ContractedSeq (stm ∷ contract (x ∷ xs))
          goal1 = transport (cong (ContractedSeq ∘ (stm ∷_)) contractredx∷xs≡x'∷xs')
                            (unit List.Linked.∷ (transport same-rest-proof rest-proof))

          goal2 = (All.all-leaf unit unit) List.All.∷ child-proof
      in ( goal1 , goal2 )
    contract-contracts seq@(stm@(break _) ∷ (x ∷ xs)) =
      let (rest-proof , child-proof ) =
              contract-contracts (x ∷ xs)
          (x' , xs' , contractredx∷xs≡x'∷xs') =
              contract-not-null (x ∷ xs) (λ ())
          same-rest-proof : ContractedSeq (x' ∷ xs') ≡ ContractedSeq (contract (x ∷ xs))
          same-rest-proof = cong ContractedSeq (sym contractredx∷xs≡x'∷xs')
          goal1 : ContractedSeq (stm ∷ contract (x ∷ xs))
          goal1 = transport (cong (ContractedSeq ∘ (stm ∷_)) contractredx∷xs≡x'∷xs')
                            (unit List.Linked.∷ (transport same-rest-proof rest-proof))

          goal2 = (All.all-leaf unit unit) List.All.∷ child-proof
      in ( goal1 , goal2 )
    contract-contracts seq@(stm@(return _ _) ∷ (x ∷ xs)) =
      let (rest-proof , child-proof ) =
              contract-contracts (x ∷ xs)
          (x' , xs' , contract-x∷xs≡x'∷xs') =
              contract-not-null (x ∷ xs) (λ ())
          same-rest-proof : ContractedSeq (x' ∷ xs') ≡ ContractedSeq (contract (x ∷ xs))
          same-rest-proof = cong ContractedSeq (sym contract-x∷xs≡x'∷xs')
          goal1 : ContractedSeq (stm ∷ contract (x ∷ xs))
          goal1 = transport (cong (ContractedSeq ∘ (stm ∷_)) contract-x∷xs≡x'∷xs')
                            (unit List.Linked.∷ (transport same-rest-proof rest-proof))
          goal2 = (All.all-leaf unit unit) List.All.∷ child-proof
      in ( goal1 , goal2 )
    contract-contracts seq@(stm@(loop a guard body) ∷ (x ∷ xs)) =
      let ( rest-proof , rest-child-proof ) =
              contract-contracts (x ∷ xs)
          (x' , xs' , contract-x∷xs≡x'∷xs'  ) =
              contract-not-null (x ∷ xs)  λ ()
          (body-contracted , body-children-contracted)
             = contract-contracts body
          same-rest-proof : ContractedSeq (x' ∷ xs') ≡ ContractedSeq (contract (x ∷ xs))
          same-rest-proof = cong ContractedSeq (sym contract-x∷xs≡x'∷xs')
          goal1 = transport (cong (ContractedSeq ∘ ((loop a guard (contract body)) ∷_)) contract-x∷xs≡x'∷xs')
                            (unit List.Linked.∷ (transport same-rest-proof rest-proof))
          goal2 = (All.all-loop unit
                                body-contracted
                                body-children-contracted)
                  List.All.∷ rest-child-proof
      in ( goal1 , goal2 )
    contract-contracts seq@(stm@(branch a guard tc fc) ∷ (x ∷ xs)) =
      let ( rest-proof , rest-child-proof ) =
              contract-contracts (x ∷ xs)
          (x' , xs' , contract-x∷xs≡x'∷xs'  ) =
              contract-not-null (x ∷ xs)  λ ()
          (tc-contracted , tc-children-contracted)
             = contract-contracts tc
          (fc-contracted , fc-children-contracted)
             = contract-contracts fc
          same-rest-proof : ContractedSeq (x' ∷ xs') ≡ ContractedSeq (contract (x ∷ xs))
          same-rest-proof = cong ContractedSeq (sym contract-x∷xs≡x'∷xs')
          goal1 = transport (cong (ContractedSeq ∘ ((branch a guard (contract tc) (contract fc)) ∷_)) contract-x∷xs≡x'∷xs')
                            (unit List.Linked.∷ (transport same-rest-proof rest-proof))
          goal2 = (all-branch unit
                              (tc-contracted , fc-contracted)
                              tc-children-contracted
                              fc-children-contracted)
                  List.All.∷ rest-child-proof
      in ( goal1 , goal2 )

    contract-contracts {A} seq@(stm@(block bs₁) ∷ rest@(x ∷ xs))
      with lift (contract rest)
    ...| ( ([] , contract≡[]))  =
             let (x' , xs' , conraction≡x'∷xs') =
                     contract-not-null rest (λ ())
                 weird : [] ≡ x' ∷ xs'
                 weird = trans (sym contract≡[]) conraction≡x'∷xs'
             in ⊥-elim (absurd x' xs' weird)
         where
           absurd : (x : Stm A) → (xs : List (Stm A)) → [] ≡ x ∷ xs → ⊥
           absurd _ _ ()
    ...| (contracted@(block bs₂ ∷ block bs₃ ∷ _) , contract-is) =
      contradiction (proj₁ (contract-contracts rest))
                    (transport ((cong (¬_ ∘ ContractedSeq)) (contract-is))
                              not-contracted-seq)
      where
        not-contracted-seq : ¬ ContractedSeq (block bs₂ ∷ block bs₃ ∷ _)
        not-contracted-seq (() List.Linked.∷ _)

    ...| (block bs₂ ∷ [] , is-contract ) =
      let is-contracted : contract seq ≡ [ block (bs₁ ⁺++⁺ bs₂) ]
          is-contracted = cong (contract-head (block bs₁)) is-contract

      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     List.Linked.[-]
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     (all-leaf unit unit List.All.∷ List.All.[])
         )
    ...| (block bs₂ ∷ rest'@(call _ ∷ _)
         , is-contract
         ) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          is-contracted-seq : ContractedSeq (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (Linked.tail (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted))
          is-contracted-children : AllSeq ContractedChildren (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (ListAll.tail (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted))
          is-contracted : contract seq ≡ (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract

      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| (block bs₂ ∷ rest'@(assign _ ∷ _)
         , is-contract
         ) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          is-contracted-seq : ContractedSeq (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (Linked.tail (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted))
          is-contracted-children : AllSeq ContractedChildren (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (ListAll.tail (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted))
          is-contracted : contract seq ≡ (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract

      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| (block bs₂ ∷ rest'@(continue _ ∷ _)
         , is-contract
         ) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          is-contracted-seq : ContractedSeq (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (Linked.tail (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted))
          is-contracted-children : AllSeq ContractedChildren (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (ListAll.tail (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted))
          is-contracted : contract seq ≡ (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract

      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| (block bs₂ ∷ rest'@(break _ ∷ _)
         , is-contract
         ) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          is-contracted-seq : ContractedSeq (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (Linked.tail (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted))
          is-contracted-children : AllSeq ContractedChildren (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (ListAll.tail (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted))
          is-contracted : contract seq ≡ (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract

      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| (block bs₂ ∷ rest'@(return _ _ ∷ _)
         , is-contract
         ) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          is-contracted-seq : ContractedSeq (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (Linked.tail (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted))
          is-contracted-children : AllSeq ContractedChildren (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (ListAll.tail (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted))
          is-contracted : contract seq ≡ (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract

      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| (block bs₂ ∷ rest'@(loop _ _ _ ∷ _)
         , is-contract
         ) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          is-contracted-seq : ContractedSeq (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (Linked.tail (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted))
          is-contracted-children : AllSeq ContractedChildren (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (ListAll.tail (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted))
          is-contracted : contract seq ≡ (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract

      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| (block bs₂ ∷ rest'@(branch _ _ _ _ ∷ _)
         , is-contract
         ) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          is-contracted-seq : ContractedSeq (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (Linked.tail (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted))
          is-contracted-children : AllSeq ContractedChildren (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (ListAll.tail (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted))
          is-contracted : contract seq ≡ (block (bs₁ ⁺++⁺ bs₂) ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract

      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )

    ...| ((call a) ∷ rest' , is-contract) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          stm = call a
          is-contracted-seq : ContractedSeq ((block bs₁) ∷ stm ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted)
          is-contracted-children : AllSeq ContractedChildren (block bs₁ ∷ stm ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted)
          is-contracted : contract seq ≡ (block bs₁ ∷ stm ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract
      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| ((assign a) ∷ rest' , is-contract) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          stm = assign a
          is-contracted-seq : ContractedSeq ((block bs₁) ∷ stm ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted)
          is-contracted-children : AllSeq ContractedChildren (block bs₁ ∷ stm ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted)
          is-contracted : contract seq ≡ (block bs₁ ∷ stm ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract
      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| ((continue a) ∷ rest' , is-contract) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          stm = continue a
          is-contracted-seq : ContractedSeq ((block bs₁) ∷ stm ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted)
          is-contracted-children : AllSeq ContractedChildren (block bs₁ ∷ stm ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted)
          is-contracted : contract seq ≡ (block bs₁ ∷ stm ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract
      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| ((break a) ∷ rest' , is-contract) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          stm = break a
          is-contracted-seq : ContractedSeq ((block bs₁) ∷ stm ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted)
          is-contracted-children : AllSeq ContractedChildren (block bs₁ ∷ stm ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted)
          is-contracted : contract seq ≡ (block bs₁ ∷ stm ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract
      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| ((return a expr) ∷ rest' , is-contract) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          stm = return a expr
          is-contracted-seq : ContractedSeq ((block bs₁) ∷ stm ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted)
          is-contracted-children : AllSeq ContractedChildren (block bs₁ ∷ stm ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted)
          is-contracted : contract seq ≡ (block bs₁ ∷ stm ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract
      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )
    ...| ((loop a gurad body) ∷ rest' , is-contract) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          stm = loop a gurad body
          is-contracted-seq : ContractedSeq ((block bs₁) ∷ stm ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted)
          is-contracted-children : AllSeq ContractedChildren (block bs₁ ∷ stm ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted)
          is-contracted : contract seq ≡ (block bs₁ ∷ stm ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract
      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )

    ...| ((branch a gurad tc fc) ∷ rest' , is-contract) =
      let (rest-contracted , rest-children-contracted) =
                     contract-contracts rest
          stm = branch a gurad tc fc
          is-contracted-seq : ContractedSeq ((block bs₁) ∷ stm ∷ rest')
          is-contracted-seq = unit List.Linked.∷ (transport (sym (cong (List.Linked NotBothBlocks) is-contract))
                                                                          rest-contracted)
          is-contracted-children : AllSeq ContractedChildren (block bs₁ ∷ stm ∷ rest')
          is-contracted-children = all-leaf unit unit List.All.∷ (transport (sym (cong (AllSeq ContractedChildren) is-contract))
                                                                               rest-children-contracted)
          is-contracted : contract seq ≡ (block bs₁ ∷ stm ∷ rest')
          is-contracted = cong (contract-head (block bs₁)) is-contract
      in ( transport (sym (cong ContractedSeq (sym is-contracted)))
                     is-contracted-seq
         , transport (sym (cong (AllSeq ContractedChildren) (sym is-contracted)))
                     is-contracted-children
         )

    -- Lemma stating that contracting and uncontracting a single block
    -- is the same as doing nothing at all
    contract∘uncontract[block]≡id : ∀ {A}
                                   → (b : Stm A)
                                   → Is-Block b
                                   → contract (uncontract [ b ] ) ≡ [ b ]
    contract∘uncontract[block]≡id {A} b b-is-block =
      contract∘uncontract[block]≡id' (num-annots b) b b-is-block refl
      where
        contract∘uncontract[block]≡id' : (annots : ℕ)
                                        → (b' : Stm A)
                                        → Is-Block b'
                                        → (annots ≡ num-annots b')
                                        → contract (uncontract [ b' ] ) ≡ [ b' ]
        contract∘uncontract[block]≡id' 1 (block (x₁ NE.∷ [])) _ _ = refl
        contract∘uncontract[block]≡id' (suc n) (block (x₁ NE.∷ x₂ ∷ xs)) _ refl =
          begin
            contract (uncontract [ (block (x₁ NE.∷ x₂ ∷ xs)) ] )
          ≡⟨⟩
            contract (block NE.[ x₁ ] ∷ uncontract [ block (x₂ NE.∷ xs) ])
          ≡⟨⟩
            contract-head (block NE.[ x₁ ]) (contract (uncontract [ block (x₂ NE.∷ xs) ]))
          ≡⟨ cong (contract-head (block NE.[ x₁ ]))
                   (contract∘uncontract[block]≡id' n (block (x₂ NE.∷ xs)) unit refl)
            ⟩
            contract-head (block NE.[ x₁ ]) [ block (x₂ NE.∷ xs) ]
          ≡⟨⟩
            [ (block (x₁ NE.∷ x₂ ∷ xs)) ]
          ∎

    -- Helper function for 'concat-unconcat-++-disr' below
    concat-unconcat-++-disr' : ∀ {A}
                             → (bs : List⁺ A)
                             → (rest-head : Stm A)
                             → (rest : List (Stm A))
                             → (¬ (Is-Block rest-head))
                             → Contracted rest
                             → (len : ℕ)
                             → (len ≡ NE.length bs)
                             → contract (uncontract [ block bs ] ++ uncontract (rest-head ∷ rest))
                         ≡ contract (uncontract [ block bs ]) ++ (contract (uncontract (rest-head ∷ rest)))
    concat-unconcat-++-disr' _ (block _) _ not-block _ _ _ =
      contradiction unit not-block
    concat-unconcat-++-disr' (x₁ NE.∷ []) (call _) _ _ _ 1 _ = refl
    concat-unconcat-++-disr' (x₁ NE.∷ []) (assign _) _ _ _ 1 _ = refl
    concat-unconcat-++-disr' (x₁ NE.∷ []) (continue _) _ _ _ 1 _ = refl
    concat-unconcat-++-disr' (x₁ NE.∷ []) (break _) _ _ _ 1 _ = refl
    concat-unconcat-++-disr' (x₁ NE.∷ []) (return _ _) _ _ _ 1 _ = refl
    concat-unconcat-++-disr' (x₁ NE.∷ []) (loop _ _ _) _ _ _ 1 _ = refl
    concat-unconcat-++-disr' (x₁ NE.∷ []) (branch _ _ _ _) _ _ _ 1 _ = refl
    {-# CATCHALL #-}
    concat-unconcat-++-disr' bs@(x₁ NE.∷ x₂ ∷ xs) rest-head rest
                             rest-head≠block rest-is-contracted
                             (suc n) n-is-length =
        begin
          (contract-head (block NE.[ x₁ ])
                        (contract (uncontract [ block (x₂ NE.∷ xs) ]
                        ++ uncontract (rest-head ∷ rest))))
        ≡⟨ cong (contract-head (block NE.[ x₁ ]))
                 (concat-unconcat-++-disr' (x₂ NE.∷ xs) rest-head rest
                                           rest-head≠block rest-is-contracted
                                           n n-is-sub-length)
          ⟩
          (contract-head (block NE.[ x₁ ])
                        (contract (uncontract [ block (x₂ NE.∷ xs) ])
                        ++ (contract (uncontract (rest-head ∷ rest)))))
        ≡⟨ cong (λ z →
                   (contract-head (block NE.[ x₁ ])
                        (z ++ (contract (uncontract (rest-head ∷ rest)))))
                 )
                 (contract∘uncontract[block]≡id (block (x₂ NE.∷ xs)) unit)
          ⟩
          (contract-head (block NE.[ x₁ ])
                        ([ block (x₂ NE.∷ xs) ]
                        ++ (contract (uncontract (rest-head ∷ rest)))))
        ≡⟨⟩
           ([ block (x₁ NE.∷ x₂ ∷ xs) ]
           ++ (contract (uncontract (rest-head ∷ rest))))
        ≡⟨ cong (_++ _) (sym (contract∘uncontract[block]≡id (block bs) unit ))
          ⟩
          contract (uncontract [ block bs ]) ++ (contract (uncontract (rest-head ∷ rest)))
        ∎
        where
           n-is-sub-length : n ≡ NE.length (x₂ NE.∷ xs)
           n-is-sub-length = Data.Nat.Properties.suc-injective n-is-length

    -- If the first element in a sequence is a block and the sequence is
    -- contracted then concat unconcat distirbutes as shown
    concat-unconcat-++-disr : ∀ {A}
                            → (x : Stm A)
                            → (xs : List (Stm A))
                            → Contracted (x ∷ xs)
                            → Is-Block x
                            → contract (uncontract [ x ] ++ uncontract xs)
                             ≡ contract (uncontract [ x ]) ++ (contract (uncontract xs))
    concat-unconcat-++-disr x@(block _) [] _ _ =
      begin
        contract (uncontract [ x ] ++ uncontract [])
      ≡⟨ cong contract (++-identityʳ (uncontract [ x ])) ⟩
        contract (uncontract [ x ])
      ≡⟨ sym (++-identityʳ _) ⟩
        contract (uncontract [ x ]) ++ []
      ≡⟨⟩
        contract (uncontract [ x ]) ++ contract (uncontract [])
      ∎
    concat-unconcat-++-disr x₁@(block bs) (x₂ ∷ .[])
                            (not-both-blocks List.Linked.∷ List.Linked.[-]
                            , _ List.All.∷ _ List.All.∷ contacted-children-rest)
                            x-is-block =
      concat-unconcat-++-disr' bs x₂ [] (not-blockʳ (block bs) x₂ unit not-both-blocks)
                               (  List.Linked.[]
                               , contacted-children-rest)
                               (NE.length bs)
                               refl
    concat-unconcat-++-disr {A} (block bs) (x₂ ∷ xs)
                            (not-both-blocks List.Linked.∷ _ List.Linked.∷ contracted-rest
                            , _ List.All.∷ _ List.All.∷ contacted-children-rest)
                            x-is-block =
      concat-unconcat-++-disr' bs x₂ xs (not-blockʳ (block bs) x₂ unit not-both-blocks)
                               ( contracted-rest
                               , contacted-children-rest)
                               (NE.length bs)
                               refl

    contract∘uncontract≡id : ∀ {A}
                            → (seq : List (Stm A))
                            → (Contracted seq)
                            → ((contract (uncontract seq)) ≡ seq)
    contract∘uncontract≡id [] _ = refl
    contract∘uncontract≡id
      (stm@(call _) ∷ rest)
      ( _ List.Linked.∷ alpha
      , _ List.All.∷ beta) =
      cong (stm ∷_) (contract∘uncontract≡id rest (alpha , beta))
    contract∘uncontract≡id
      (stm@(assign _) ∷ rest)
      ( _ List.Linked.∷ alpha
      , _ List.All.∷ beta) =
      cong (stm ∷_) (contract∘uncontract≡id rest (alpha , beta))
    contract∘uncontract≡id
      (stm@(continue _) ∷ rest)
      ( _ List.Linked.∷ alpha
      , _ List.All.∷ beta) =
      cong (stm ∷_) (contract∘uncontract≡id rest (alpha , beta))
    contract∘uncontract≡id
      (stm@(break _) ∷ rest)
      ( _ List.Linked.∷ alpha
      , _ List.All.∷ beta) =
      cong (stm ∷_) (contract∘uncontract≡id rest (alpha , beta))
    contract∘uncontract≡id
      (stm@(return _ _) ∷ rest)
      ( _ List.Linked.∷ alpha
      , _ List.All.∷ beta) =
      cong (stm ∷_) (contract∘uncontract≡id rest (alpha , beta))
    contract∘uncontract≡id
      seq@(stm@(loop _ _ body) ∷ rest)
      ( _ List.Linked.∷ alpha
      , all-loop _ body-contracted body-child-props
        List.All.∷ beta) =
      begin
        (loop _ _ (contract (uncontract body))) ∷ contract (uncontract rest)
      ≡⟨ cong (λ x → (loop _ _ x) ∷ contract (uncontract rest))
              (contract∘uncontract≡id body (body-contracted , body-child-props))
        ⟩
        stm ∷  contract (uncontract rest)
      ≡⟨ cong (stm ∷_) (contract∘uncontract≡id rest (alpha , beta)) ⟩
       seq
      ∎
    contract∘uncontract≡id
      seq@(stm@(branch _ _ tc fc) ∷ rest)
      ( _ List.Linked.∷ alpha
      , (all-branch _ (tc-contracted , fc-contracted)
                          tc-children-contracted
                          fc-children-contracted
      ) List.All.∷ beta) =
      begin
        (branch _ _ (contract (uncontract tc))
                    (contract (uncontract fc)))
        ∷ contract (uncontract rest)
      ≡⟨ cong₂ (λ x y → (branch _ _ x y) ∷ contract (uncontract rest))
                (contract∘uncontract≡id tc (tc-contracted , tc-children-contracted))
                (contract∘uncontract≡id fc (fc-contracted , fc-children-contracted))
        ⟩
        stm ∷  contract (uncontract rest)
      ≡⟨ cong (stm ∷_) (contract∘uncontract≡id rest (alpha , beta)) ⟩
       seq
      ∎
    contract∘uncontract≡id
      ((block _ ) ∷ block _ ∷ _)
      ( () List.Linked.∷ _
      , (_ List.All.∷ _))
    contract∘uncontract≡id {A}
      seq@(stm@(block bs₁ ) ∷ rest )
      contracted@( _ List.Linked.∷ contracted-rest
                 , t List.All.∷ rest-children-contracted
                 ) =
        begin
          contract (uncontract seq)
        ≡⟨ cong (λ z → contract (z ++ uncontract rest))
                 (sym inner-is-uncontract)
          ⟩
          contract (uncontract [ block bs₁ ] ++ uncontract rest)
        ≡⟨ concat-unconcat-++-disr (block bs₁) rest contracted unit  ⟩
          contract (uncontract [ block bs₁ ]) ++ (contract (uncontract rest))
        ≡⟨ cong₂ _++_
                 (contract∘uncontract[block]≡id (block bs₁) unit)
                 -- ^ Could recur on self but termination checking fails
                 (contract∘uncontract≡id rest (contracted-rest
                                               , rest-children-contracted
                                               ))
          ⟩
          seq
        ∎
        where
          inner-is-uncontract : uncontract ([ block bs₁ ])
                              ≡ NE.toList (NE.map (λ x → block NE.[ x ]) bs₁)
          inner-is-uncontract = ++-identityʳ _
    contract∘uncontract≡id (block x ∷ [])
                            (List.Linked.[-]
                            , px List.All.∷ snd
                            ) =
        contract∘uncontract[block]≡id (block x) unit
    contract∘uncontract≡id (call x ∷ .[])
      (List.Linked.[-] , px List.All.∷ snd) = refl
    contract∘uncontract≡id (assign x ∷ .[])
      (List.Linked.[-] , px List.All.∷ snd) = refl
    contract∘uncontract≡id (break x ∷ .[])
      (List.Linked.[-] , px List.All.∷ snd) = refl
    contract∘uncontract≡id (continue x ∷ .[])
      (List.Linked.[-] , px List.All.∷ snd) = refl
    contract∘uncontract≡id (return x x₁ ∷ .[])
      (List.Linked.[-] , px List.All.∷ snd) = refl
    contract∘uncontract≡id stm@(branch a guard tc fc ∷ .[])
      (List.Linked.[-]
      , All.all-branch _ (tc-contracted , fc-contracted)
                         tc-children-contracted
                         fc-children-contracted
        List.All.∷ _) =
      begin
        (branch _ _ (contract (uncontract tc))
                    (contract (uncontract fc)))
        ∷ []
      ≡⟨ cong₂ (λ x y → (branch _ _ x y) ∷ [])
                (contract∘uncontract≡id tc (tc-contracted , tc-children-contracted))
                (contract∘uncontract≡id fc (fc-contracted , fc-children-contracted))
        ⟩
       [ branch a guard tc fc ]
      ∎

    contract∘uncontract≡id (loop _ _ body ∷ .[])
      (List.Linked.[-]
      , all-loop _ body-contracted body-child-props
        List.All.∷ snd) =
      begin
        (loop _ _ (contract (uncontract body))) ∷ []
      ≡⟨ cong (λ x → (loop _ _ x) ∷ [])
              (contract∘uncontract≡id body (body-contracted , body-child-props))
        ⟩
        [ loop _ _ body ]
      ∎

    uncontract-cancles-contract-head :
      ∀ {A}
      → (b : A)
      → (xs : List (Stm A))
      → uncontract (contract-head (block NE.[ b ]) xs)
      ≡ block NE.[ b ] ∷ uncontract xs
    uncontract-cancles-contract-head x [] = refl
    uncontract-cancles-contract-head _ (block _ ∷ _) = refl
    uncontract-cancles-contract-head _ (call _ ∷ _) = refl
    uncontract-cancles-contract-head _ (assign _ ∷ _) = refl
    uncontract-cancles-contract-head _ (continue _ ∷ _) = refl
    uncontract-cancles-contract-head _ (break _ ∷ _) = refl
    uncontract-cancles-contract-head _ (return _ _ ∷ _) = refl
    uncontract-cancles-contract-head _ (branch _ _ _ _ ∷ _) = refl
    uncontract-cancles-contract-head _ (loop _ _ _ ∷ _) = refl
      -- begin
      --   uncontract (contract-head (block NE.[ b₁ ] ) (block b₂ ∷ xs))
      -- ≡⟨⟩
      --   uncontract (block (NE.[ b₁ ] ⁺++⁺ b₂) ∷ xs)
      -- ≡⟨⟩
      --   uncontract (block (NE.[ b₁ ] ⁺++⁺ b₂) ∷ xs)
      -- ≡⟨ {!!} ⟩
      --   block NE.[ b₁ ] ∷ uncontract (block b₂ ∷ xs)
      -- ∎

    uncontract∘contract≡id : ∀ {A}
                            → (seq : List (Stm A))
                            → AllSeq (λ stm → num-annots stm ≡ 1) seq
                            → (uncontract (contract seq)) ≡ seq
    uncontract∘contract≡id [] _ = refl
    uncontract∘contract≡id (call a ∷ rest) (_ List.All.∷ rest-props) =
      cong (_ ∷_) (uncontract∘contract≡id rest rest-props)
    uncontract∘contract≡id (assign a ∷ rest) (_ List.All.∷ rest-props) =
      cong (_ ∷_) (uncontract∘contract≡id rest rest-props)
    uncontract∘contract≡id (continue a ∷ rest) (_ List.All.∷ rest-props) =
      cong (_ ∷_) (uncontract∘contract≡id rest rest-props)
    uncontract∘contract≡id (break a ∷ rest) (_ List.All.∷ rest-props) =
      cong (_ ∷_) (uncontract∘contract≡id rest rest-props)
    uncontract∘contract≡id (return a expr ∷ rest) (_ List.All.∷ rest-props) =
      cong (_ ∷_) (uncontract∘contract≡id rest rest-props)
    uncontract∘contract≡id (loop a guard body ∷ rest)
                            ((all-loop _ _ body-props) List.All.∷ rest-props) =
       cong₂ (_∷_)
             (cong (loop _ _) (uncontract∘contract≡id body body-props))
             (uncontract∘contract≡id rest rest-props)

    uncontract∘contract≡id (branch a guard tc fc ∷ rest)
                            ((all-branch _ _ tc-props fc-props)
                              List.All.∷ rest-props) =
       cong₂ (_∷_)
             (cong₂ (branch _ _)
                    (uncontract∘contract≡id tc tc-props)
                    (uncontract∘contract≡id fc fc-props))
             (uncontract∘contract≡id rest rest-props)

    uncontract∘contract≡id (block (_ NE.∷ (_ ∷ _)) ∷ rest) (all-leaf _ () List.All.∷ _)
    uncontract∘contract≡id (block (x₁ NE.∷ []) ∷ []) _ = refl
    {-# CATCHALL #-}
    uncontract∘contract≡id (block (x₁ NE.∷ [])
                             ∷ rest) (_ List.All.∷ rest-props) =
        begin
          uncontract (contract (block NE.[ x₁ ] ∷ rest))
        ≡⟨⟩
          uncontract (contract-head (block NE.[ x₁ ]) (contract  rest))
        ≡⟨ uncontract-cancles-contract-head  x₁ (contract (rest)) ⟩
          block NE.[ x₁ ] ∷ uncontract (contract ( rest))
        ≡⟨ cong (_ ∷_) (uncontract∘contract≡id rest rest-props) ⟩
          (block NE.[ x₁ ] ∷ rest)
        ∎
