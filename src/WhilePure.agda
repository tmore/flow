module WhilePure where
open import Function.Base
open import Function.Equivalence using ( _⇔_ ; Equivalence )
open import Function.Equality using ( Π )
import Function.Equivalence
open import Agda.Builtin.Equality using (_≡_ ; refl )

open import Data.Unit using (⊤) renaming ( tt to unit)

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.Char using (toℕ ; Char)
open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)
open import Data.Nat.Properties using (≡ᵇ⇒≡)
import Data.Nat.Properties
open import Data.Maybe using (Maybe ; just ; nothing  ; fromMaybe ; is-nothing ; is-just ) renaming ( maybe′ to maybe)
import Data.Maybe
import Data.Maybe.Properties

import Data.These
open import Data.These using ( These ; this ; that ; these )

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
import Data.Sum

open import Data.String using (String  ) renaming ( _==_ to _≡ˢ_ ; _≟_ to _≟S_)

open import Data.List hiding (break ; fromMaybe ; lookup ; any ; all)
import Data.List.Properties
import Data.List.Relation.Unary.All as List using ( All)
-- import Data.List.Relation.Unary.All as ListAll using ( All ; [] ; _∷_ )

open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)
import Data.List.NonEmpty as NE

open import Data.Bool using (if_then_else_ ; Bool ; _∧_ ; _∨_ ; true ; false ; not ; T  ) renaming ( )
open import Data.Bool.Properties using (T-≡)

open import Data.Integer using (ℤ) renaming (_+_ to _+ℤ_ ; _*_ to _*ℤ_ ;
                                             _-_ to _-ℤ_ ; +_ to ℤ+_ ;
                                             _≤?_ to _≤?ℤ_ ; _≟_ to _≟ℤ_ ; _<?_ to _<?ℤ_)
open import Relation.Nullary.Decidable.Core using (isYes ; isNo)


open import Relation.Binary.PropositionalEquality using (cong ; _≢_ )
open import Relation.Nullary.Negation hiding (excluded-middle)
open import Relation.Nullary using ( ¬_ )
open import Data.Empty

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Relation.Nullary


open import Level hiding (suc)




open LoopStack using (LoopStack)





module 𝕎toFlow (MapType : Set → Set)
                (MapInstance : Map MapType)
                where

  module 𝕎' = 𝕎 MapType MapInstance
  module Flow' = Flow MapType MapInstance
  module Ctx' = Ctx MapType MapInstance

  -- Todo, this termination proof is easy, do it later
  {-# TERMINATING #-}
  𝕎StmToFlowStm : ∀ {a} → (stm : 𝕎'.Stm a) → Flow'.Stm (𝕎'.Stm a)
  𝕎StmToFlowStm t@(𝕎.assignExpr _ _ _) = Flow.assign t
  𝕎StmToFlowStm t@(𝕎.assignCall _ _ _ _) = Flow.call t
  𝕎StmToFlowStm t@(𝕎.call _ _ _) = Flow.call t
  𝕎StmToFlowStm t@(𝕎.ifte _ guard tc fc) =
    Flow.branch
      t
      guard
      (Data.List.map 𝕎StmToFlowStm tc)
      (Data.List.map 𝕎StmToFlowStm fc)
  𝕎StmToFlowStm t@(𝕎.while _ guard body) =
    Flow.loop
      t
      guard
      (Data.List.map 𝕎StmToFlowStm body)
  𝕎StmToFlowStm t@(𝕎.continue _) =
    Flow.continue t
  𝕎StmToFlowStm t@(𝕎.break _) =
    Flow.break t
  𝕎StmToFlowStm t@(𝕎.return _ expr) =
    Flow.return t expr

  𝕎toFlow : ∀ {a} → List (𝕎'.Stm a) → List (Flow'.Stm (𝕎'.Stm a))
  𝕎toFlow = Data.List.map 𝕎StmToFlowStm

  FlowStmTo𝕎Stm : ∀ {a} → Flow'.Stm (𝕎'.Stm a) → List (𝕎'.Stm a)
  FlowStmTo𝕎Stm (Flow.block a) = NE.toList a
  FlowStmTo𝕎Stm (Flow.assign a) = [ a ]
  FlowStmTo𝕎Stm (Flow.call a) = [ a ]
  FlowStmTo𝕎Stm (Flow.branch a _ _ _)  = [ a ]
  FlowStmTo𝕎Stm (Flow.loop a _ _)  = [ a ]
  FlowStmTo𝕎Stm (Flow.continue a)  = [ a ]
  FlowStmTo𝕎Stm (Flow.break a)  = [ a ]
  FlowStmTo𝕎Stm (Flow.return a _)  = [ a ]


  FlowTo𝕎 : ∀ {a} → List (Flow'.Stm (𝕎'.Stm a)) → List (𝕎'.Stm a)
  FlowTo𝕎 [] = []
  FlowTo𝕎 (stm ∷ rest) = FlowStmTo𝕎Stm stm ++ FlowTo𝕎 rest

  𝕨StmtoFlow∘flowTo𝕎≡ : ∀ {a} → (stmt : 𝕎'.Stm a) → FlowStmTo𝕎Stm (𝕎StmToFlowStm stmt) ≡ [ stmt ]
  𝕨StmtoFlow∘flowTo𝕎≡ (𝕎'.assignExpr _ _ _) = refl
  𝕨StmtoFlow∘flowTo𝕎≡ (𝕎'.assignCall _ _ _ _) = refl
  𝕨StmtoFlow∘flowTo𝕎≡ (𝕎'.call _ _ _) = refl
  𝕨StmtoFlow∘flowTo𝕎≡ (𝕎'.ifte _ _ _ _) = refl
  𝕨StmtoFlow∘flowTo𝕎≡ (𝕎'.while _ _ _) = refl
  𝕨StmtoFlow∘flowTo𝕎≡ (𝕎'.continue _) = refl
  𝕨StmtoFlow∘flowTo𝕎≡ (𝕎'.break _) = refl
  𝕨StmtoFlow∘flowTo𝕎≡ (𝕎'.return _ _) = refl


  𝕎toFlow∘FlowTo𝕎≡id : ∀ {a} → (stmts : List (𝕎'.Stm a)) → FlowTo𝕎 (𝕎toFlow stmts) ≡ stmts
  𝕎toFlow∘FlowTo𝕎≡id [] = refl
  𝕎toFlow∘FlowTo𝕎≡id (x ∷ xs) =
    begin
      FlowStmTo𝕎Stm (𝕎StmToFlowStm x) ++ FlowTo𝕎 (𝕎toFlow xs)
    ≡⟨ cong (_++ FlowTo𝕎 (𝕎toFlow xs)) (𝕨StmtoFlow∘flowTo𝕎≡ x) ⟩
      [ x ] ++ FlowTo𝕎 (𝕎toFlow xs)
    ≡⟨ cong ([ x ] ++_) (𝕎toFlow∘FlowTo𝕎≡id xs) ⟩
      [ x ] ++ xs
    ∎

  flow𝕎Semantic : ∀ {a} → 𝕎'.Ctx a → List (𝕎'.Stm a) → 𝕎'.Ctx a
  flow𝕎Semantic ctx seq =
    proj₁ (𝕎'.bigStep ctx seq)

  FlowSeq : Set → Set
  FlowSeq a = List (Flow'.Stm (𝕎'.Stm a))

  𝕎Seq : Set → Set
  𝕎Seq a = List (𝕎'.Stm a)

  Result : Set → Set
  Result a = Ctx'.Ctx a × (List a ⊎ ℤ)

  fmapResult : ∀ {a b} → (a → b) → Result a → Result b
  fmapResult f (ctx , res) = (Ctx'.fmap f ctx , Data.Sum.map (Data.List.map f) id res)

  ++-perserves-𝕎toFlow : ∀ {a}
                 → (𝕨Init : 𝕎Seq a)
                 → (𝕨Tail : 𝕎Seq a)
                 → (flowInit : FlowSeq a)
                 → (flowTail : FlowSeq a)
                 → (flowInit ≡ 𝕎toFlow 𝕨Init)
                 → (flowTail ≡ 𝕎toFlow 𝕨Tail)
                 → (flowInit ++ flowTail ≡ 𝕎toFlow (𝕨Init ++ 𝕨Tail))
  ++-perserves-𝕎toFlow 𝕨Init 𝕨Tail flowInit flowTail x x₁ =
    begin
      flowInit ++ flowTail
    ≡⟨ Eq.cong₂ _++_ x x₁ ⟩
      𝕎toFlow 𝕨Init ++ 𝕎toFlow 𝕨Tail
    ≡⟨ sym (Data.List.Properties.map-++-commute 𝕎StmToFlowStm 𝕨Init 𝕨Tail) ⟩
      𝕎toFlow (𝕨Init ++ 𝕨Tail)
    ∎

  SemanticsEqual : ∀ {a}
                 → (𝕨Ctx : 𝕎'.Ctx a)
                 → (fCtx : Flow'.Ctx (𝕎'.Stm a))
                 → (fCtx ≡ (Ctx'.fmap 𝕎StmToFlowStm 𝕨Ctx))
                 → (𝕨Seq : 𝕎Seq a)
                 → (flowSeq : FlowSeq a)
                 → (flowSeq ≡ 𝕎toFlow 𝕨Seq)
                 → ((Flow'.semanticStep fCtx
                                        (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                        (Ctx'.procedures 𝕨Ctx)
                                        flowSeq)
                   ≡ fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq))
  SemanticsEqual 𝕨Ctx fCtx refl [] [] refl = refl
  SemanticsEqual 𝕨Ctx fCtx refl
                 (e@(𝕎'.assignExpr _ _ _) ∷ 𝕨rest)
                 ((Flow.assign e) ∷ frest)
                 refl = refl
  SemanticsEqual 𝕨Ctx fCtx refl
                 𝕨Seq@(e@(𝕎'.ifte _ guard tc fc) ∷ 𝕨rest)
                 fSeq@((Flow.branch _ guard tc' fc') ∷ frest)
                 refl
                 with excluded-middle (Ctx'.eval𝔹 guard 𝕨Ctx)
  ... | (inj₁ guard≡true) =
        begin
           (if Ctx'.eval𝔹 guard fCtx
           then ( Ctx'.pushStack [] frest fCtx , inj₁ tc')
           else ( Ctx'.pushStack [] frest fCtx , inj₁ fc'))
        ≡⟨ if-guard-subst guard≡true refl ⟩
          ( Ctx'.pushStack [] frest fCtx , inj₁ tc' )
        ≡⟨ refl ⟩
          fmapResult 𝕎StmToFlowStm (Ctx'.pushStack [] 𝕨rest  𝕨Ctx , inj₁ tc )
        ≡⟨ cong (fmapResult 𝕎StmToFlowStm) (if-guard-subst (sym guard≡true) refl) ⟩
           fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
        ∎
  ... | (inj₂ guard≡false) =
         begin
           (if Ctx'.eval𝔹 guard fCtx
           then ( Ctx'.pushStack [] frest fCtx , inj₁ tc')
           else ( Ctx'.pushStack [] frest fCtx , inj₁ fc'))
        ≡⟨ if-guard-subst guard≡false refl ⟩
          ( Ctx'.pushStack [] frest fCtx , inj₁ fc' )
        ≡⟨ refl ⟩
          fmapResult 𝕎StmToFlowStm (Ctx'.pushStack [] 𝕨rest  𝕨Ctx , inj₁ fc )
        ≡⟨ cong (fmapResult 𝕎StmToFlowStm) (if-guard-subst (sym guard≡false) refl) ⟩
           fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
        ∎
  SemanticsEqual 𝕨Ctx fCtx refl
                 𝕨Seq@(e@(𝕎'.while _ guard 𝕨body) ∷ 𝕨rest)
                 fSeq@((Flow.loop _ guard fbody) ∷ frest)
                 refl
                 with excluded-middle (Ctx'.eval𝔹 guard 𝕨Ctx)
  ... | inj₁ guard≡true =
             begin
               (if Ctx'.eval𝔹 guard fCtx
               then ( Ctx'.pushStack fSeq frest fCtx , inj₁ fbody )
               else ( fCtx , inj₁ frest ))
             ≡⟨ if-guard-subst guard≡true refl ⟩
               (Ctx'.pushStack fSeq frest fCtx , inj₁ fbody)
             ≡⟨ refl ⟩
               fmapResult 𝕎StmToFlowStm (Ctx'.pushStack 𝕨Seq 𝕨rest  𝕨Ctx , inj₁ 𝕨body)
             ≡⟨ cong (fmapResult 𝕎StmToFlowStm) (if-guard-subst (sym guard≡true) refl) ⟩
               fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
             ∎
  ... | inj₂ guard≡false =
             begin
               (if Ctx'.eval𝔹 guard fCtx
               then ( Ctx'.pushStack fSeq frest fCtx , inj₁ fbody )
               else ( fCtx , inj₁ frest ))
             ≡⟨ if-guard-subst guard≡false refl ⟩
               (fCtx , inj₁ frest)
             ≡⟨ cong (fmapResult 𝕎StmToFlowStm) (if-guard-subst (sym guard≡false) refl) ⟩
               fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
             ∎
  SemanticsEqual 𝕨Ctx fCtx refl
                 𝕨Seq@((𝕎'.continue _) ∷ 𝕨rest)
                 fSeq@((Flow.continue _) ∷ frest) refl
                 with Ctx'.Properties.popElim 𝕨Ctx
  ... | inj₁ (p , pop≡just) =
    begin
      continueMap (Ctx'.popContinue fCtx)
    ≡⟨⟩
     continueMap (continueProjMap (Ctx'.pop fCtx))
    ≡⟨⟩
     continueMap (continueProjMap (Ctx'.pop (Ctx'.fmap 𝕎StmToFlowStm 𝕨Ctx)))
    ≡⟨ cong (continueMap ∘ continueProjMap) (Ctx'.Properties.pop-fmap-commutes-ish 𝕨Ctx 𝕎StmToFlowStm) ⟩
     continueMap (continueProjMap (Data.Maybe.map (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                            (LoopStack.Node.fmap 𝕎StmToFlowStm))
                                          (Ctx'.pop 𝕨Ctx)))
    ≡⟨ cong continueMap
            (maybe-map-compose (Ctx'.pop 𝕨Ctx)
            ((Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm) (LoopStack.Node.fmap 𝕎StmToFlowStm)))
            ((Data.Product.map id LoopStack.Node.Node.continue)))

      ⟩
     continueMap (Data.Maybe.map (((Data.Product.map id LoopStack.Node.Node.continue))
                                   ∘ (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                        (LoopStack.Node.fmap 𝕎StmToFlowStm)))
                                 (Ctx'.pop 𝕨Ctx))
    ≡⟨⟩
     continueMap (Data.Maybe.map ((Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                        (Data.List.map 𝕎StmToFlowStm ∘ LoopStack.Node.Node.continue))
                                 )
                                 (Ctx'.pop 𝕨Ctx))
    ≡⟨ Data.Maybe.Properties.maybe′-map (Data.Product.map₂ inj₁)
                                         (Flow'.semanticStep fCtx
                                           (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                           (Ctx'.procedures 𝕨Ctx) frest)
                                         (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                        (Data.List.map 𝕎StmToFlowStm ∘ LoopStack.Node.Node.continue))
                                         (Ctx'.pop 𝕨Ctx)
      ⟩
      maybe (fmapResult 𝕎StmToFlowStm ∘ (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.continue)))
              (Flow'.semanticStep fCtx
                                  (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                  (Ctx'.procedures 𝕨Ctx) frest)
                                 (Ctx'.pop 𝕨Ctx)
    ≡⟨ cong (maybe (fmapResult 𝕎StmToFlowStm ∘ (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.continue)))
              (Flow'.semanticStep fCtx
                                  (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                  (Ctx'.procedures 𝕨Ctx) frest)) pop≡just  ⟩
      fmapResult 𝕎StmToFlowStm (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.continue) (Ctx'.dropFrame 𝕨Ctx , p))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (Data.Product.map₂ inj₁ (Ctx'.dropFrame 𝕨Ctx , LoopStack.Node.Node.continue p))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (just (Ctx'.dropFrame 𝕨Ctx , LoopStack.Node.Node.continue p)))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (just ((Data.Product.map₂ LoopStack.Node.Node.continue)
                                              (Ctx'.dropFrame 𝕨Ctx , p))))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)
                                                       (just (Ctx'.dropFrame 𝕨Ctx , p))))
    ≡⟨ cong (λ x → fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)
                                                       x))) (sym pop≡just)
    ⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue)
                                                       (Ctx'.pop 𝕨Ctx)))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (Ctx'.popContinue 𝕨Ctx))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
    ∎
    where
      continueProjMap = Data.Maybe.map (Data.Product.map id LoopStack.Node.Node.continue)
      continueMap =
        maybe (Data.Product.map₂ inj₁)
              (Flow'.semanticStep fCtx
                                  (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                  (Ctx'.procedures 𝕨Ctx) frest)


  ... | inj₂ pop≡nothing =
    begin
      continueMap (Ctx'.popContinue fCtx)
    ≡⟨⟩
      continueMap (continueProjMap (Ctx'.pop (Ctx'.fmap 𝕎StmToFlowStm 𝕨Ctx)))
    ≡⟨ cong (continueMap ∘ continueProjMap) (Ctx'.Properties.pop-fmap-commutes-ish 𝕨Ctx 𝕎StmToFlowStm) ⟩
      continueMap (continueProjMap (Data.Maybe.map (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                   (LoopStack.Node.fmap 𝕎StmToFlowStm))
                                                   (Ctx'.pop 𝕨Ctx)))
    ≡⟨ cong (λ x → continueMap (continueProjMap (Data.Maybe.map (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                   (LoopStack.Node.fmap 𝕎StmToFlowStm))
                                                   x)) )  pop≡nothing
       ⟩
      Flow'.semanticStep fCtx (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                               (Ctx'.procedures 𝕨Ctx) frest
    ≡⟨ SemanticsEqual 𝕨Ctx fCtx refl 𝕨rest frest refl ⟩
      fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨rest)
    ≡⟨ cong (λ x → fmapResult 𝕎StmToFlowStm
                 (maybe (Data.Product.map₂ inj₁)
                        (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                        (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue) x)))
             (sym pop≡nothing)
      ⟩
      fmapResult 𝕎StmToFlowStm
                 (maybe (Data.Product.map₂ inj₁)
                        (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                        (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.continue) (Ctx'.pop 𝕨Ctx)))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm
                 (maybe (Data.Product.map₂ inj₁)
                        (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                        (Ctx'.popContinue 𝕨Ctx))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
    ∎
    where
      continueProjMap = Data.Maybe.map (Data.Product.map id LoopStack.Node.Node.continue)
      continueMap = maybe (Data.Product.map₂ inj₁)
              (Flow'.semanticStep fCtx
                                  (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                  (Ctx'.procedures 𝕨Ctx) frest)
  SemanticsEqual 𝕨Ctx fCtx refl
                 𝕨Seq@((𝕎'.break _) ∷ 𝕨rest)
                 fSeq@((Flow.break _) ∷ frest) refl
                 with Ctx'.Properties.popElim 𝕨Ctx
  ... | inj₁ (p , pop≡just) =
    begin
      breakMap (Ctx'.popBreak fCtx)
    ≡⟨⟩
     breakMap (breakProjMap (Ctx'.pop fCtx))
    ≡⟨⟩
     breakMap (breakProjMap (Ctx'.pop (Ctx'.fmap 𝕎StmToFlowStm 𝕨Ctx)))
    ≡⟨ cong (breakMap ∘ breakProjMap) (Ctx'.Properties.pop-fmap-commutes-ish 𝕨Ctx 𝕎StmToFlowStm) ⟩
     breakMap (breakProjMap (Data.Maybe.map (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                            (LoopStack.Node.fmap 𝕎StmToFlowStm))
                                          (Ctx'.pop 𝕨Ctx)))
    ≡⟨ cong breakMap
            (maybe-map-compose (Ctx'.pop 𝕨Ctx)
            ((Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm) (LoopStack.Node.fmap 𝕎StmToFlowStm)))
            ((Data.Product.map id LoopStack.Node.Node.break)))

      ⟩
     breakMap (Data.Maybe.map (((Data.Product.map id LoopStack.Node.Node.break))
                                   ∘ (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                        (LoopStack.Node.fmap 𝕎StmToFlowStm)))
                                 (Ctx'.pop 𝕨Ctx))
    ≡⟨⟩
     breakMap (Data.Maybe.map ((Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                        (Data.List.map 𝕎StmToFlowStm ∘ LoopStack.Node.Node.break))
                                 )
                                 (Ctx'.pop 𝕨Ctx))
    ≡⟨ Data.Maybe.Properties.maybe′-map (Data.Product.map₂ inj₁)
                                         (Flow'.semanticStep fCtx
                                           (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                           (Ctx'.procedures 𝕨Ctx) frest)
                                         (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                        (Data.List.map 𝕎StmToFlowStm ∘ LoopStack.Node.Node.break))
                                         (Ctx'.pop 𝕨Ctx)
      ⟩
      maybe (fmapResult 𝕎StmToFlowStm ∘ (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.break)))
              (Flow'.semanticStep fCtx
                                  (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                  (Ctx'.procedures 𝕨Ctx) frest)
                                 (Ctx'.pop 𝕨Ctx)
    ≡⟨ cong (maybe (fmapResult 𝕎StmToFlowStm ∘ (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.break)))
              (Flow'.semanticStep fCtx
                                  (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                  (Ctx'.procedures 𝕨Ctx) frest)) pop≡just  ⟩
      fmapResult 𝕎StmToFlowStm (Data.Product.map id (inj₁ ∘ LoopStack.Node.Node.break) (Ctx'.dropFrame 𝕨Ctx , p))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (Data.Product.map₂ inj₁ (Ctx'.dropFrame 𝕨Ctx , LoopStack.Node.Node.break p))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (just (Ctx'.dropFrame 𝕨Ctx , LoopStack.Node.Node.break p)))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (just ((Data.Product.map₂ LoopStack.Node.Node.break)
                                              (Ctx'.dropFrame 𝕨Ctx , p))))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)
                                                       (just (Ctx'.dropFrame 𝕨Ctx , p))))
    ≡⟨ cong (λ x → fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)
                                                       x))) (sym pop≡just)
    ⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break)
                                                       (Ctx'.pop 𝕨Ctx)))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (maybe (Data.Product.map₂ inj₁)
                                       (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                                       (Ctx'.popBreak 𝕨Ctx))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
    ∎
    where
      breakProjMap = Data.Maybe.map (Data.Product.map id LoopStack.Node.Node.break)
      breakMap =
        maybe (Data.Product.map₂ inj₁)
              (Flow'.semanticStep fCtx
                                  (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                  (Ctx'.procedures 𝕨Ctx) frest)


  ... | inj₂ pop≡nothing =
    begin
      breakMap (Ctx'.popBreak fCtx)
    ≡⟨⟩
      breakMap (breakProjMap (Ctx'.pop (Ctx'.fmap 𝕎StmToFlowStm 𝕨Ctx)))
    ≡⟨ cong (breakMap ∘ breakProjMap) (Ctx'.Properties.pop-fmap-commutes-ish 𝕨Ctx 𝕎StmToFlowStm) ⟩
      breakMap (breakProjMap (Data.Maybe.map (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                   (LoopStack.Node.fmap 𝕎StmToFlowStm))
                                                   (Ctx'.pop 𝕨Ctx)))
    ≡⟨ cong (λ x → breakMap (breakProjMap (Data.Maybe.map (Data.Product.map (Ctx'.fmap 𝕎StmToFlowStm)
                                                   (LoopStack.Node.fmap 𝕎StmToFlowStm))
                                                   x)) )  pop≡nothing
       ⟩
      Flow'.semanticStep fCtx (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                               (Ctx'.procedures 𝕨Ctx) frest
    ≡⟨ SemanticsEqual 𝕨Ctx fCtx refl 𝕨rest frest refl ⟩
      fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨rest)
    ≡⟨ cong (λ x → fmapResult 𝕎StmToFlowStm
                 (maybe (Data.Product.map₂ inj₁)
                        (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                        (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break) x)))
             (sym pop≡nothing)
      ⟩
      fmapResult 𝕎StmToFlowStm
                 (maybe (Data.Product.map₂ inj₁)
                        (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                        (Data.Maybe.map (Data.Product.map₂ LoopStack.Node.Node.break) (Ctx'.pop 𝕨Ctx)))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm
                 (maybe (Data.Product.map₂ inj₁)
                        (𝕎'.semanticStep 𝕨Ctx  𝕨rest)
                        (Ctx'.popBreak 𝕨Ctx))
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
    ∎
    where
      breakProjMap = Data.Maybe.map (Data.Product.map id LoopStack.Node.Node.break)
      breakMap = maybe (Data.Product.map₂ inj₁)
              (Flow'.semanticStep fCtx
                                  (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq))
                                  (Ctx'.procedures 𝕨Ctx) frest)
  SemanticsEqual 𝕨Ctx fCtx refl
                 𝕨Seq@(e@(𝕎'.call annot procIdx args)  ∷ 𝕨rest)
                 fSeq@((Flow.call (𝕎'.call _ _ _)) ∷ frest)
                 refl =
    begin
      (Flow'.semanticStep fCtx (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq)) (Ctx'.procedures 𝕨Ctx) fSeq)
    ≡⟨⟩
      ( Flow'.runSubSem fCtx (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq)) (Ctx'.procedures 𝕨Ctx) [ e ]
      , inj₁ frest
      )
    ≡⟨⟩
      ( record fCtx { variables =
                      Ctx'.variables (proj₁ (𝕎'.semanticStep (record { procedures = (Ctx'.procedures 𝕨Ctx)
                                                ; variables = Ctx'.variables fCtx
                                                ; loopStack = []
                                                }) [ e ]))
                    }

      , inj₁ frest
      )
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm
      ( (record 𝕨Ctx
        { variables =
                    Ctx'.variables (proj₁ (𝕎'.semanticStep (record { procedures = (Ctx'.procedures 𝕨Ctx)
                                                ; variables = Ctx'.variables fCtx
                                                ; loopStack = []
                                                }) [ e ]))
                  })
      , inj₁ 𝕨rest
      )
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm
      ( (record 𝕨Ctx
        { variables =
                    Ctx'.variables (proj₁  (𝕎'.procCall 𝕨Ctx procIdx args)
                    )
                  })
      , inj₁ 𝕨rest
      )
    ≡⟨ cong (fmapResult 𝕎StmToFlowStm ∘ (_, inj₁ 𝕨rest)) refl ⟩
      fmapResult 𝕎StmToFlowStm (
        ((proj₁ (𝕎'.procCall 𝕨Ctx procIdx args))  , inj₁ 𝕨rest)
        )
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (
        ((proj₁ (𝕎'.procCall 𝕨Ctx procIdx args))  , inj₁ 𝕨rest)
        )
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (
        let (ctx' , res) = 𝕎'.procCall 𝕨Ctx procIdx args
        in (ctx' , inj₁ 𝕨rest)
        )
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
    ∎
  SemanticsEqual 𝕨Ctx fCtx refl
                 𝕨Seq@(e@(𝕎'.assignCall annot var procIdx args)  ∷ 𝕨rest)
                 fSeq@((Flow.call (𝕎'.assignCall _ _ _ _)) ∷ frest)
                 refl =
    begin
      (Flow'.semanticStep fCtx (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq)) (Ctx'.procedures 𝕨Ctx) fSeq)
    ≡⟨⟩
      ( Flow'.runSubSem fCtx (λ ctx seq → proj₁ (𝕎'.semanticStep ctx seq)) (Ctx'.procedures 𝕨Ctx) [ e ]
      , inj₁ frest
      )
    ≡⟨⟩
      fmapResult 𝕎StmToFlowStm
      ( (record 𝕨Ctx
        { variables =
                    Ctx'.variables (let (ctx' , res) = 𝕎'.procCall 𝕨Ctx procIdx args
                                           in ( Ctx'.setVar var res ctx'
                                              ))
                  })
      , inj₁ 𝕨rest
      )
    ≡⟨ cong (fmapResult 𝕎StmToFlowStm ∘ (_, inj₁ 𝕨rest)) refl ⟩
      fmapResult 𝕎StmToFlowStm (𝕎'.semanticStep 𝕨Ctx 𝕨Seq)
    ∎

  SemanticsEqual 𝕨Ctx fCtx refl
                 𝕨Seq@(e@(𝕎'.return annot a)  ∷ 𝕨rest)
                 fSeq@((Flow.return _ _) ∷ frest)
                 refl = refl


-- -- AssignChangesVariabe : ∀ {a}
-- --                      → (v : 𝕍)
-- --                      → (expr : 𝔸)
-- --                      → (annot : a)
-- --                      → (𝕨ctx : 𝕎.Ctx a)
-- --                      → ( vm : VarCtx)
-- --                      → (Ctx.variables 𝕨ctx ≡ vm)
-- --                      → (VarCtx.get v (Ctx.variables (proj₁ (𝕎.semantic 𝕨ctx [ 𝕎.assignExpr annot v expr ]))) ≡ (Ctx.eval𝔸 expr 𝕨ctx))
-- -- AssignChangesVariabe v expr annot ctx vm refl = {! !}

-- -- FlowStepEQ𝕎Step : ∀ {a}
-- --                  → (𝕨ctx : 𝕎.Ctx a)
-- --                  → (flowCtx : Flow.Ctx (𝕎.Stm a))
-- --                  → (pm : VarMap (List 𝕍 × List (𝕎.Stm a)))
-- --                  → (Ctx.variables 𝕨ctx ≡ Ctx.variables flowCtx)
-- --                  → (stm : 𝕎.Stm a)
-- --                  → (fStm : Flow.Stm (𝕎.Stm a))
-- --                  → (fStm ≡ 𝕎StmToFlowStm stm)
-- --                  → (Ctx.variables (proj₁ (𝕎.semantic 𝕨ctx [ stm ]))) ≡
-- --                    (Ctx.variables (proj₁ (Flow.semantic flowCtx flow𝕎Semantic pm [ fStm ])))
-- -- FlowStepEQ𝕎Step 𝕨ctx flowCtx pm refl (𝕎.assignExpr a v expr) .(Flow.block [ (𝕎.assignExpr a v expr) ]) refl =
-- --   {!!}
-- -- FlowStepEQ𝕎Step 𝕨ctx flowCtx pm refl stm fStm refl =
-- --   {!!}

-- CtxEq𝔸 : ∀ {a b}
--         → (expr : 𝔸)
--         → (ctx1 : Ctx.State a)
--         → (ctx2 : Ctx.State b)
--         → (Ctx.State.variables ctx1) ≡ (Ctx.State.variables ctx2)
--         → Ctx.eval𝔸 expr ctx1 ≡ Ctx.eval𝔸 expr ctx2
-- CtxEq𝔸 expr ctx1 ctx2 refl = refl


-- CtxEq𝔹 : ∀ {a b}
--         → (expr : 𝔹)
--         → (ctx1 : Ctx.State a)
--         → (ctx2 : Ctx.State b)
--         → (Ctx.State.variables ctx1) ≡ (Ctx.State.variables ctx2)
--         → Ctx.eval𝔹 expr ctx1 ≡ Ctx.eval𝔹 expr ctx2
-- CtxEq𝔹 expr ctx1 ctx2 refl = refl
