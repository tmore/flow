module Graphs where

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_ ; refl)

open import Data.Nat using (ℕ ; _≡ᵇ_)
open import Data.List using (List ; _∷_ ; [])
import Data.List


import Data.List.Relation.Unary.All as ListAll
import Data.List.Relation.Unary.Any as ListAny
open import Data.List.Relation.Unary.All using (All)
open import Data.List.Relation.Unary.Any using (Any)

open import Data.List.Relation.Unary.All using ( _∷_ ; [])
open import Data.List.Relation.Unary.Any using ( here ; there )

open import Data.Bool

open import Data.Maybe using (Maybe ; just ; nothing ; is-just ; is-nothing)

open import Data.Product
open import Level

private
  variable
    a b c p : Level
    Elbl : Set
    Vlbl : Set

record Graph (G : Set → Set → Set) : Set₁ where
  field
    empty : G Elbl Vlbl
    insertEdge : ℕ → Elbl →  ℕ → G Elbl Vlbl → G Elbl Vlbl
    insertUnique : Vlbl → G Elbl Vlbl → (ℕ × G Elbl Vlbl)
    insertVertex : ℕ → Vlbl → G Elbl Vlbl → G Elbl Vlbl

    _∪_ : G Elbl Vlbl → G Elbl Vlbl → G Elbl Vlbl
    _∈_ : ℕ → G Elbl Vlbl → Bool
    _∉_ : ℕ → G Elbl Vlbl → Bool


    nodeLabel :  ℕ → G Elbl Vlbl → Maybe Vlbl
    edgesFrom : ℕ → G Elbl Vlbl → List (ℕ × Elbl × ℕ)
    edges : G Elbl Vlbl → List (ℕ × Elbl × ℕ)
    edgesBetween : ℕ → ℕ → G Elbl Vlbl → List (ℕ × Elbl × ℕ)

    empty-is-empty : (v : ℕ) → v ∈ empty {Elbl} {Vlbl} ≡ false
    ∉≡¬∈ : (v : ℕ) → (g : G Elbl Vlbl) → v ∉ g ≡ not (v ∈ g)

    nodeLabel-just-when-in-otherwise-nothing :
      ∀ {Elbl Vlbl}
      → (v : ℕ)
      → (g : G Elbl Vlbl)
      → if v ∈ g
        then is-just (nodeLabel v g) ≡ true
        else is-nothing (nodeLabel v g) ≡ false
    insertVertex-inserts-vertex :
      ∀ {Elbl Vlbl}
      → (v : ℕ)
      → (l : Vlbl)
      → (g : G Elbl Vlbl)
      → v ∈ insertVertex v l g ≡ true

    insertVertex-inserts-label :
      ∀ {Elbl Vlbl}
      → (v : ℕ)
      → (l : Vlbl)
      → (g : G Elbl Vlbl)
      → nodeLabel v (insertVertex v l g) ≡ just l


    edgesBetween-correct-vertices :
      ∀ {Elbl Vlbl}
      → (from : ℕ)
      → (to : ℕ)
      → (g : G Elbl Vlbl)
      → All (λ { (from' , _ , to') →
                   from ≡ from' × to ≡ to'})
            (edgesBetween from to g)


    insertEdge-inserts-edge :
      ∀ {Elbl Vlbl}
      → (from : ℕ)
      → (l : Elbl)
      → (to : ℕ)
      → (g : G Elbl Vlbl)
      → if from ∈ g ∧ to ∈ g
        then Any (λ { (_ , l' , _) → l' ≡ l })
                 (edgesBetween from to (insertEdge from l to g))
        else edgesBetween from to (insertEdge from l to g) ≡ []

    insertUnique-unique :
      ∀ {Elbl Vlbl}
      → (l : Vlbl)
      → (g : G Elbl Vlbl)
      → proj₁ (insertUnique l g) ∈ g ≡ false
    insertUnique-inserts :
      ∀ {Elbl Vlbl}
      → (l : Vlbl)
      → (g : G Elbl Vlbl)
      → nodeLabel (proj₁ (insertUnique l g)) (proj₂ (insertUnique l g)) ≡ just l

    ∪-keeps-nodes :
      ∀ {Elbl Vlbl}
      → (v : ℕ)
      → (g₁ g₂ : G Elbl Vlbl)
      → if v ∈ g₁ ∨ v ∈ g₂
        then v ∈ (g₁ ∪ g₂) ≡ true
        else v ∈ (g₁ ∪ g₂) ≡ false
