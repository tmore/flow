import Maps

open import Data.Integer using (ℤ)
import Data.Integer as ℤ
open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )

open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )

import Expr
module 𝕍 = Expr.𝕍
open 𝕍 using (𝕍)

open import Level using (Level ; _⊔_ ; suc)

module VarCtx (MapType : Set → Set)
              (MapInstance : Maps.IntMap MapType) where


  module Map = Maps.IntMap MapInstance

  record VarCtx : Set where
    field
      locals : MapType ℤ
      globals : MapType ℤ

  setLocals : MapType ℤ → VarCtx → VarCtx
  setLocals m ctx =
    record ctx { locals = m }

  setGlobals : MapType ℤ → VarCtx → VarCtx
  setGlobals m ctx =
    record ctx { globals = m }

  isLocal : 𝕍 → VarCtx → Bool
  isLocal v ctx = v Map.∈ (VarCtx.locals ctx)

  set : 𝕍 → ℤ → VarCtx → VarCtx
  set v n ctx =
    if isLocal v ctx
    then setLocals (Map.set v n (VarCtx.locals ctx)) ctx
    else setGlobals (Map.set v n (VarCtx.globals ctx)) ctx

  getGlobal : 𝕍 → VarCtx → ℤ
  getGlobal v ctx =
    fromMaybe (ℤ.+ 0) (Map.lookup v (VarCtx.globals ctx))

  get : 𝕍 → VarCtx → ℤ
  get v ctx =
    fromMaybe (getGlobal v ctx) (Map.lookup v (VarCtx.locals ctx))

  define : 𝕍 → ℤ → VarCtx → VarCtx
  define v n ctx =
    setLocals (Map.set v n (VarCtx.locals ctx)) ctx

  clearLocals : VarCtx -> VarCtx
  clearLocals = setLocals Map.empty

  getGlobals : VarCtx → MapType ℤ
  getGlobals = VarCtx.globals



  empty : VarCtx
  empty = record { locals = Map.empty
                 ; globals = Map.empty
                 }
