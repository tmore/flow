import Maps
import VarCtx

open import Function
open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)
open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )
open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)
import Data.Maybe.Properties

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.List using (List ; [] ; _∷_ ; foldl ; zipWith ; _++_ )
import Data.Integer as ℤ
open import Data.Integer using (ℤ)

open import Agda.Builtin.Equality using (_≡_ ; refl )

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)


open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
open import Util using (maybeElim ; maybe⊥)
open import Data.Empty

open import Expr
open 𝕍 using (𝕍)
open 𝔸 using (𝔸)
open 𝔹 using (𝔹)

import Ctx

module While (MapType : Set → Set)
             (MapInstance : Maps.IntMap MapType)
             where
  module Ctx' = Ctx MapType MapInstance

  data Stm (A : Set) : Set where
    assignExpr : A → 𝕍 → 𝔸 → Stm A
    assignCall : A → 𝕍 → 𝕍 → List 𝕍 → Stm A
    call : A → 𝕍 → List 𝕍 → Stm A
    -- ^ Assignment
    ifte : A → 𝔹 → List (Stm A) → List (Stm A) → Stm A
    -- ^ if statement
    while : A → 𝔹 → List (Stm A) → Stm A
    -- ^ While statement: <variant> <guard> <body>.
    continue : A → Stm A
    break : A → Stm A
    return : A → 𝔸 → Stm A


  Ctx : Set → Set
  Ctx A = Ctx'.Ctx A

  size : ∀ {a} → Stm a → ℕ
  size (assignExpr _ _ _) = 1
  size (assignCall _ _ _ _) = 1
  size (call _ _ _) = 1
  size (ifte _ _ [] []) = 1
  size (ifte a g [] (x ∷ xs)) =  size x +ℕ size (ifte a g [] xs)
  size (ifte a g (x ∷ xs) fc) = size x +ℕ size (ifte a g xs fc)
  size (while _ _ []) = 1
  size (while a g (x ∷ xs)) = size x +ℕ size (while a g (xs))
  size (continue _) = 1
  size (break _) = 1
  size (return _ _) = 1

  seq-size : ∀ {a} → List (Stm a) → ℕ
  seq-size [] = 0
  seq-size (x ∷ xs) = size x +ℕ seq-size xs

  height : ∀ {a} → Stm a → ℕ
  height (assignExpr _ _ _) = 1
  height (assignCall _ _ _ _) = 1
  height (call _ _ _) = 1
  height (ifte _ _ [] []) = 1
  height (ifte a g [] (x ∷ xs)) =  height x +ℕ height (ifte a g [] xs)
  height (ifte a g (x ∷ xs) fc) = height x +ℕ height (ifte a g xs fc)
  height (while _ _ []) = 1
  height (while a g (x ∷ xs)) = height x +ℕ height (while a g (xs))
  height (continue _) = 1
  height (break _) = 1
  height (return _ _) = 1

--   module Stmt where
--     {-# TERMINATING #-}
--     equals : ∀ {a}  → Stm a → Stm a → Bool
--     equals (varDecl _ v1) (varDecl _ v2) =
--       𝕍.equals v1 v2
--     equals (assignExpr _ var1 expr1) (assignExpr _ var2 expr2) =
--       𝕍.equals var1 var2 ∧ 𝔸.equals expr1 expr2
--     equals (assignCall _ var1 proc1 args1) (assignCall _ var2 proc2 args2) =
--       𝕍.equals var1 var2 ∧ 𝕍.equals proc1 proc2 ∧ Data.List.and (zipWith 𝕍.equals args1 args2)
--     equals (call _ proc1 args1) (call _ proc2 args2) =
--       𝕍.equals proc1 proc2 ∧ Data.List.and (zipWith 𝕍.equals args1 args2)
--     equals (ifte _ guard1 tc1 fc1) (ifte _ guard2 tc2 fc2) =
--       𝔹.equals guard1 guard2 ∧ Data.List.and (zipWith equals tc1 tc2) ∧ Data.List.and (zipWith equals fc1 fc2)
--     equals (while _ guard1 body1) (while _ guard2 body2) =
--       𝔹.equals guard1 guard2 ∧ Data.List.and (zipWith equals body1 body2)
--     equals (continue _) (continue _) =
--       true
--     equals (break _) (break _) =
--       true
--     equals (return _ expr1) (return _ expr2) =
--       𝔸.equals expr1 expr2
--     equals _ _ = false

  Proc : Set → Set
  Proc A = (List 𝕍 × List (Stm A))

  getGlobals : ∀ {A} → Ctx (Stm A) → MapType ℤ
  getGlobals ctx =
    Ctx'.VarCtx'.getGlobals (Ctx'.Ctx.variables ctx)

  {-# TERMINATING #-}
  bigStep : ∀ {A} → Ctx (Stm A) → List (Stm A) → Ctx (Stm A) × Maybe ℤ
  procCall : ∀ {A} → Ctx (Stm A) → 𝕍 → List 𝕍 → Ctx (Stm A) × ℤ
  semanticStep : ∀ {A} → Ctx (Stm A) → List (Stm A) → Ctx (Stm A) × ((List (Stm A)) ⊎ ℤ)


  bigStep ctx []
     with Ctx'.popContinue ctx
  ...| just (ctx' , rest) = bigStep ctx' rest
  ...| nothing = (ctx , nothing)
  bigStep ctx seq =
    let (ctx' , restOrRes) = semanticStep ctx seq
    in Data.Sum.[ bigStep ctx' , (_,_ ctx' ∘ just) ]′ restOrRes


  procCall {A} ctx procIdx args =
    let procInfo@(params , locals , body) = Ctx'.getProc procIdx ctx
        procCtxWParams = foldl (λ { newCtx' (param , val) →
                                 Ctx'.defineLocal
                                   param
                                   (Ctx'.getVar val ctx)
                                   newCtx'})
                        (Ctx'.clearLocal ctx)
                        (zipWith _,_ params args)
        procCtxWLocals = foldl (λ newCtx' local →
                                  Ctx'.defineLocal local (ℤ.+ 0) newCtx')
                              procCtxWParams
                              locals
    in Data.Product.map resetLocals
                        (fromMaybe (ℤ.+ 0))
                        (bigStep procCtxWLocals body)
    where
      resetLocals : Ctx (Stm A) → Ctx (Stm A)
      resetLocals c =
        Ctx'.setGlobalsFrom (Ctx'.variables c) ctx


  semanticStep ctx [] = (ctx , inj₁ [])
  semanticStep ctx (assignExpr _ v expr ∷ rest) =
    ( Ctx'.setVar v (Ctx'.eval𝔸 expr ctx) ctx
    , inj₁ rest
    )
  semanticStep ctx (assignCall annot v procIdx args ∷ rest) =
    let (ctx' , res) = procCall ctx procIdx args
    in ( Ctx'.setVar v res ctx'
       , inj₁ rest
       )
  semanticStep ctx  (call annot procIdx args ∷ rest) =
    let (ctx' , res) = procCall ctx procIdx args
    in (ctx' , inj₁ rest)
  semanticStep ctx  (ifte _ guard trueCase falseCase ∷ rest) =
    if (Ctx'.eval𝔹 guard ctx)
    then (ctx , inj₁ (trueCase ++ rest))
    else (ctx , inj₁ (falseCase ++ rest))
  semanticStep ctx e@(while _ guard body ∷ rest) =
    if (Ctx'.eval𝔹 guard ctx)
    then ( Ctx'.pushStack e rest ctx , inj₁ body )
    else ( ctx , inj₁ rest )

  semanticStep ctx (continue _ ∷ rest) =
      maybe (Data.Product.map₂ inj₁)
            (semanticStep ctx  rest)
            (Ctx'.popContinue ctx)
  semanticStep ctx (break _ ∷ rest) =
      maybe (Data.Product.map₂ inj₁)
            (semanticStep ctx rest)
            (Ctx'.popBreak ctx)
  semanticStep ctx (return _ expr ∷ rest) =
      ( ctx
      , inj₂ (Ctx'.eval𝔸 expr ctx)
      )


  semantic : ∀ {A} → Ctx (Stm A) → List (Stm A) → Ctx (Stm A) × (List (Stm A) ⊎ ℤ)
  semantic = Ctx'.step semanticStep

  module Properties where
