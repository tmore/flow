import While
import Flow.Core as Flow
import Flow.Semantics
import Flow.Contraction as Flow
import Flow.Contraction.BlockifyProps as Flow

import Flow.Relation.Unary.All as Flow using (All)
import Flow.Relation.Unary.All as FlowAll

import Flow.Relation.Unary.Any as Flow using (Any)
import Flow.Relation.Unary.Any  as FlowAny

import Flow.Relation.Unary.NIL-Any as Flow using (NILAny)
import Flow.Relation.Unary.NIL-Any as FlowNIL-Any

import Maps
import Ctx
import LoopStack

open import Util using ( if-guard-subst
                       ; maybe-map-compose
                       ; lift
                       )

open import Function.Base

open import Data.Unit using (⊤) renaming ( tt to unit)

open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary

open import Data.List using (List ; [] ; [_] ; _∷_ ; _++_)
open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)
import Data.List.Properties
open import Data.List.Properties using ( map-++-commute)
import Data.List.NonEmpty as NE

import Data.List.Relation.Unary.All as List using ( All)
import Data.List.Relation.Unary.Any as List using ( Any )
import Data.List.Relation.Unary.Linked as List using ( Linked )

open import Agda.Builtin.Equality using (_≡_ ; refl )
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong₂ ; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)

open import Data.Integer using (ℤ)

import Extra.Sum
import Extra.List

import Expr


module WhileToFlow (MapType : Set → Set)
                   (MapInstance : Maps.IntMap MapType)
                   where

module 𝕎 = While MapType MapInstance
module Flow' = Flow.Semantics MapType MapInstance

module Ctx' = Ctx MapType MapInstance

Ctx : Set → Set
Ctx A = Ctx'.Ctx A

-- The semantic step taken during executing statements inside of the flow
-- context
whileStep : ∀ {A} → (Ctx (𝕎.Stm A)) → (𝕎.Stm A) → Ctx (𝕎.Stm A)
whileStep ctx stm = proj₁ (𝕎.semanticStep ctx  [ stm ])

toFlowSeq : ∀ {A} → List (𝕎.Stm A) → List (Flow.Stm (𝕎.Stm A))
toFlowStm : ∀ {A} → 𝕎.Stm A → Flow.Stm (𝕎.Stm A)

toFlowStm t@(𝕎.assignExpr _ _ _) =
  Flow.assign t
toFlowStm t@(𝕎.assignCall _ _ _ _) =
  Flow.call t
toFlowStm t@(𝕎.call _ _ _)  =
  Flow.call t
toFlowStm t@(𝕎.continue _) =
  Flow.continue t
toFlowStm t@(𝕎.break _) =
  Flow.break t
toFlowStm t@(𝕎.return _ expr)=
  Flow.return t expr
toFlowStm t@(𝕎.ifte _ guard tc fc) =
  Flow.branch t guard (toFlowSeq tc) (toFlowSeq fc)
toFlowStm t@(𝕎.while _ guard body) =
  Flow.loop t guard (toFlowSeq body)

toFlowSeq [] = []
toFlowSeq (stm ∷ rest) =
  toFlowStm stm ∷ toFlowSeq rest


toContractedFlow :
  ∀ {A}
  → (Flow.ContractionCtx (𝕎.Stm A))
  → List (𝕎.Stm A)
  → List (Flow.Stm (𝕎.Stm A))
toContractedFlow cc =
 (Flow.contract ∘ Flow.blockifySeq cc ∘ toFlowSeq)


toFlowCtx : ∀ {A} → (ctx : Ctx (𝕎.Stm A)) → Ctx (Flow.Stm (𝕎.Stm A))
toFlowCtx ctx = Ctx'.map toFlowStm ctx

toContractedFlowCtx :
  {A : Set}
  → Flow.ContractionCtx (𝕎.Stm A)
  → Ctx (𝕎.Stm A)
  → Ctx (Flow.Stm (𝕎.Stm A))
toContractedFlowCtx cc =
  Ctx'.mapSeq (toContractedFlow cc)

toWhileStm : {A : Set} → Flow.Stm (𝕎.Stm A) → List (𝕎.Stm A)
toWhileStm stm = NE.toList (Flow.annotations stm)

-- A version of toWhile that only picks out the first annotation of blocks
toWhileStmFst : ∀ {A} → Flow.Stm (𝕎.Stm A) → 𝕎.Stm A
toWhileStmFst (Flow.block (a NE.∷ _)) = a
toWhileStmFst (Flow.assign a) = a
toWhileStmFst (Flow.call a) = a
toWhileStmFst (Flow.branch a _ _ _)  = a
toWhileStmFst (Flow.loop a _ _)  = a
toWhileStmFst (Flow.continue a)  = a
toWhileStmFst (Flow.break a)  = a
toWhileStmFst (Flow.return a _)  = a


toWhileSeq : ∀ {A} → List (Flow.Stm (𝕎.Stm A)) → List (𝕎.Stm A)
toWhileSeq = Data.List.concatMap toWhileStm

-- Bascially same as toWhile. Only uses extra steps to simplify some proofs
toWhile : ∀ {A} → List (Flow.Stm (𝕎.Stm A)) → List (𝕎.Stm A)
toWhile =
  (toWhileSeq ∘ Flow.unblockifySeq toFlowStm ∘ Flow.uncontract)


FlowSeq : Set → Set
FlowSeq A = List (Flow.Stm (𝕎.Stm A))

𝕎Seq : Set → Set
𝕎Seq A = List (𝕎.Stm A)

Result : Set → Set
Result A = Ctx A × (List A ⊎ ℤ)

mapResult : ∀ {A B} → (A → B) → Result A → Result B
mapResult f (ctx , res) =
  ( Ctx'.map f ctx
  , Data.Sum.map (Data.List.map f)
                 id
                 res
  )

mapResultSeq :
  ∀ {A B}
  → (List A → List B)
  → Result A
  → Result B
mapResultSeq f (ctx , res) =
  ( Ctx'.mapSeq f ctx
  , Data.Sum.map f
                 id
                 res
  )

toWhileResult : ∀ {A} → Result (Flow.Stm (𝕎.Stm A)) → Result (𝕎.Stm A)
toWhileResult res =
  mapResultSeq toWhile res

toContractedFlowResult :
 ∀ {A}
 → (cc : Flow.ContractionCtx (𝕎.Stm A))
 → Result (𝕎.Stm A)
 → Result (Flow.Stm (𝕎.Stm A))
toContractedFlowResult cc res =
  mapResultSeq (toContractedFlow cc) res


contractedSubsemantic :
  ∀ {A}
  → Flow.ContractionCtx (𝕎.Stm A)
  → Flow'.SubSemantic (𝕎.Stm A)
contractedSubsemantic  cc =
  record { stepFunc = whileStep
         ; transl = toFlowStm
         ; compress = (Flow.contract ∘ Flow.blockifySeq cc)
         ; untransl = toWhile
         }

contractedSemanticStep :
  ∀ {A}
  → (Flow.ContractionCtx (𝕎.Stm A))
  → (Ctx (𝕎.Stm A))
  → List (𝕎.Stm A)
  → Result (Flow.Stm (𝕎.Stm A))
contractedSemanticStep cc ctx seq =
  (Flow'.semanticStep
    (contractedSubsemantic cc)
    (toContractedFlowCtx cc ctx)
    (toContractedFlow cc seq))
