module Util where

open import Data.Bool using (if_then_else_ ; Bool ; _∧_ ; _∨_ ; true ; false ; not ; T  )
open import Data.These using ( These ; this ; that ; these )
open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
import Data.Sum

open import Relation.Binary.PropositionalEquality using (cong ; _≢_ )
open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)

open import Agda.Builtin.Equality using (_≡_ ; refl )

open import Relation.Nullary.Negation hiding (excluded-middle)

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)

open import Data.Maybe using (Maybe ; just ; nothing  ; fromMaybe ; is-nothing ; is-just ) renaming ( maybe′ to maybe)
import Data.Maybe
import Data.Maybe.Properties

open import Relation.Nullary using ( ¬_ )

open import Data.Empty

open import Function.Base

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Data.List hiding (break ; fromMaybe ; lookup ; any ; all)

open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)

de-morg₁ : (a : Bool) → (b : Bool) → not (a ∧ b) ≡ (not a ∨ not b)
de-morg₁ true _ = refl
de-morg₁ false true = refl
de-morg₁ false false  = refl

de-morg₂ : (a : Bool) → (b : Bool) → (not a ∧ not b) ≡ not (a ∨ b)
de-morg₂ true _ = refl
de-morg₂ false true = refl
de-morg₂ false false  = refl

if-guard-subst : ∀ {b1 b2}
               → {A : Set}
               → {a1 a2 a3 : A}
               → (b1 ≡ b2)
               → ((if b2 then a1 else a2) ≡ a3)
               → ((if b1 then a1 else a2) ≡ a3)
if-guard-subst refl refl = refl

if-true-elim : ∀ {b}
             → {A : Set}
             → {a1 a2 a3 : A}
             → (b ≡ true)
             → (if b then a1 else a2) ≡ a3
             → a1 ≡ a3
if-true-elim refl refl = refl

excluded-middle : (b : Bool) → (b ≡ true) ⊎ (b ≡ false)
excluded-middle true = inj₁ refl
excluded-middle false = inj₂ refl

a≡false⇒a≢true : {a : Bool} → a ≡ false → a ≢ true
a≡false⇒a≢true refl = λ ()

-- Property name may be wrong, but whatevs
∧-injective : {a b : Bool} → a ∧ b ≡ true → (a ≡ true × b ≡ true)
∧-injective {a} {b} a∧b≡true with excluded-middle a | excluded-middle b
... | inj₁ a≡true | inj₁ b≡true = (a≡true , b≡true)
... | inj₂ a≡false | _ =
    contradiction a∧b≡true (a≡false⇒a≢true (cong (_∧ b) a≡false))
... | inj₁ a≡true | inj₂ b≡false  =
   contradiction a∧b≡true (a≡false⇒a≢true (trans  (cong (_∧ b) a≡true) b≡false))

∧-injectiveₗ : {a b : Bool} → a ∧ b ≡ true → a ≡ true
∧-injectiveₗ a∧b≡true = proj₁ (∧-injective a∧b≡true)
∧-injectiveᵣ : {a b : Bool} → a ∧ b ≡ true → b ≡ true
∧-injectiveᵣ a∧b≡true = proj₂ (∧-injective a∧b≡true)

either-of-these : {a b : Set}
                → (a → Bool)
                → (b → Bool)
                → (These a b)
                → Bool
either-of-these f g (this a) = f a
either-of-these f g (that b) = g b
either-of-these f g (these a b) = f a ∨ g b


maybeElim : {a : Set}
          → (m : Maybe a)
          → (Σ[ e ∈ a ] m ≡ just e
            ⊎ m ≡ nothing)
maybeElim nothing = inj₂ refl
maybeElim (just p) = inj₁ (p , refl)

maybeNeg : {a : Set} {e : a}
         → (m : Maybe a)
         → (m ≡ just e)
         → ¬ (m ≡ nothing)
maybeNeg nothing ()
maybeNeg (just e)  = λ _ ()

maybe⊥ : {a : Set} {e : a}
        → (nothing ≡ just e)
        → ⊥
maybe⊥ ()

maybeContr : {a : Set}
           → (m : Maybe a)
           → Σ[ e ∈ a ] m ≡ just e
           → (m ≡ nothing)
           → ⊥
maybeContr m (e , refl) ()

maybe-map-compose : {a : Set} {b : Set} {c : Set}
           → (m : Maybe a)
           → (f : a → b)
           → (g : b → c)
           → Data.Maybe.map g (Data.Maybe.map f m) ≡ Data.Maybe.map (g ∘ f) m
maybe-map-compose m f g with maybeElim m
... | inj₂ m≡nothing =
           begin
             Data.Maybe.map g (Data.Maybe.map f m)
           ≡⟨ cong ((Data.Maybe.map g) ∘ (Data.Maybe.map f)) m≡nothing ⟩
             nothing
           ≡⟨⟩
             Data.Maybe.map (g ∘ f) nothing
           ≡⟨ cong (Data.Maybe.map (g ∘ f)) (sym m≡nothing) ⟩
             Data.Maybe.map (g ∘ f) m
           ∎
... | inj₁ (e , m≡just) =
           begin
             Data.Maybe.map g (Data.Maybe.map f m)
           ≡⟨ cong ((Data.Maybe.map g) ∘ (Data.Maybe.map f)) m≡just ⟩
             Data.Maybe.map g (Data.Maybe.map f (just e))
           ≡⟨⟩
             (just (g (f e)))
           ≡⟨⟩
             Data.Maybe.map (g ∘ f) (just e)
           ≡⟨ cong (Data.Maybe.map (g ∘ f)) (sym m≡just) ⟩
             Data.Maybe.map (g ∘ f) m
           ∎



map-compose : {A B C : Set}
            → (f : B → C)
            → (g : A → B)
            → (xs : List A)
            → map f (map g xs)
              ≡ map (f ∘ g) xs
map-compose {A} { B} {C} f g [] = refl
map-compose {A} { B} {C} f g (x ∷ xs) =
  begin
    map f (map g (x ∷ xs))
  ≡⟨⟩
    (f ∘ g) x ∷ map f (map g xs)
  ≡⟨ cong ( (f ∘ g) x ∷_ ) (map-compose f g xs) ⟩
    (f ∘ g) x ∷ map (f ∘ g) xs
  ≡⟨⟩
    map (f ∘ g) (x ∷ xs)
  ∎
repeat : ∀ {i} {x : Set i} → (x → x) → x → ℕ → x
repeat _ x 0 = x
repeat f x (suc n) = repeat f (f x) n

list-refl : {A : Set} → (xs : List A) → Σ[ xs' ∈ List A ] xs ≡ xs'
list-refl xs = ( xs , refl)


transport : {A B : Set} → A ≡ B → B → A
transport refl x = x

lift : {A : Set} → (a : A) → Σ[ reduced ∈ A ] a ≡ reduced
lift a = (a , refl)

--guard : ∀ {i} → {Res : Set i} → (Bool → Set i) →

-- case_↦_ : ∀ {i} {Res : Set i} → Bool → Res → Res → Res
-- case_↦_ true r _ = r
-- case_↦_ false _ otherwise = otherwise

-- otherwise↦_ : ∀ {i} {Res : Set i} → Res → Res
-- otherwise↦_ res = res


-- test : Bool
-- test = case true ↦ true
--        case fasle ↦ false
--        otherwise↦ false

productMapDistr :
  ∀ {l}
  → {A B C D : Set l}
  → (lf : A → B)
  → (rf : C → D)
  → (def : (A × C))
  → (ma : Maybe (A × C))
  → Data.Product.map lf rf (fromMaybe def ma)
    ≡ fromMaybe
         (Data.Product.map
           lf rf def)
         (Data.Maybe.map
           (Data.Product.map lf rf)
           ma)
productMapDistr lf rf def nothing = refl
productMapDistr lf rf def (just x) = refl
