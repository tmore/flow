module Maps where

open import Function.Base

open import Data.Nat using (ℕ)
open import Data.Bool using (Bool ; T)
open import Data.Maybe using (Maybe ; just ; nothing)
open import Relation.Nullary using (¬_)
open import Function.Equivalence using ( _⇔_ ; Equivalence )

open import Agda.Builtin.Equality

open import Level using  (Level ; _⊔_ ; suc)

record IntMap (M : Set → Set) : Set₁ where
  field
    _∈_ : ∀ {A} → ℕ → M A → Bool
    lookup : ∀ {A} → ℕ → M A → Maybe A
    set : ∀ {A} → ℕ → A → M A → M A
    empty : ∀ {A} → M A

    map : ∀ {A B} → (A → B) → M A → M B

    EmptyIsEmpty : ∀ {v A} → ¬ T(v ∈ empty {A})
    ∈⇔lookup : ∀ {v A} { e : A} {m : M A} → T(v ∈ m) ⇔ (lookup v m ≡ just e)
    ∉⇔lookup≡nothing : ∀ {v A} {m : M A} → (¬ T(v ∈ m)) ⇔ (lookup v m ≡ nothing)
    set⇒lookup : ∀ {v A} {e : A} {m : M A} → lookup v (set v e m) ≡ just e


    map∘map : {A B C : Set}
              → {f : A → B}
              → {g : B → C}
              → {m : M A}
              → map g (map f m)
              ≡ map (g ∘ f) m

    map-id : ∀ {A}
              → {m : M A}
              → {f : A → A}
              → ((a : A) → f a ≡ a)
              → map f m ≡ m
