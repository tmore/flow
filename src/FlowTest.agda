open import Function.Base
open import Function.Equivalence using ( _⇔_ ; Equivalence )
open import Function.Equality using ( Π ; _⟨$⟩_ )
import Function.Equivalence

open import Data.Unit using (⊤) renaming ( tt to unit)
open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)
open import Data.Nat.Properties using (≡ᵇ⇒≡)
open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )
open import Data.Bool.Properties using (T-≡ ; T? )

open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)

import Data.These
open import Data.These using ( These ; this ; that ; these )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.List using (List ; [] ; _∷_ ; foldl ; zipWith ; [_] ; _++_ ; drop)
import Data.List.Properties
open import Data.List.Properties using (++-identityʳ)
open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)

open import Data.List.Relation.Unary.Any using () renaming ( any to list-any)

import Data.List.Relation.Unary.All as List -- using ( All)
import Data.List.Relation.Unary.All as ListAll
import Data.List.Relation.Unary.Any as List -- using ( Any )
import Data.List.Relation.Unary.Any as ListAny -- using ( Any )
import Data.List.Relation.Unary.Linked as List using ( Linked )
import Data.List.Relation.Unary.Linked as Linked

import Data.List.NonEmpty as NE
open import Data.Integer using (ℤ)
                         renaming (+_ to ℤ+_)

open import Agda.Builtin.Equality using (_≡_ ; refl )

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst ; cong₂)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Relation.Binary.PropositionalEquality using (cong ; _≢_ )

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)

open import Data.Empty
open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary
open import Relation.Nullary.Negation hiding (excluded-middle)

open import Level using (Level ; _⊔_)

import Agda.Builtin.Size
open import Agda.Builtin.Size using (Size ; Size<_ ; ∞ ; ↑_ ; _⊔ˢ_)

module FlowTest where

  -- Placeholders to decrease file size
  𝔹 : Set
  𝔹 = ⊤
  𝕍 : Set
  𝕍 = ⊤
  𝔸 : Set
  𝔸 = ⊤

  data Seq (i : Size) (A : Size → Set) : Set where
    []ₛ : Seq i A
    _∷ₛ_ : {j : Size< i} → (A j) → Seq j A → Seq i A

  map-seq : ∀ {i A B} → ({k : Size} → A k → B k) → Seq i A → Seq i B
  map-seq _ []ₛ = []ₛ
  map-seq f (x ∷ₛ xs) =
    f x ∷ₛ map-seq f xs

  data Stm (A : Set) (i : Size) : Set where
    block : List⁺ A → Stm A i
    call  : A → Stm A i
    assign : A → Stm A i
    break : A → Stm A i
    continue : A → Stm A i
    return : A → 𝔸 → Stm A i

    branch : ∀{ j : Size< i}
           → A
           → 𝔹
           → List (Stm A j)
           → List (Stm A j)
           → Stm A i
    loop : ∀{ j : Size< i}
         → A → 𝔹
         → (List (Stm A j))
         → Stm A i

  map-stm : ∀ {i} {A B : Set} → (A → B) → Stm A i → Stm B i
  map-stm f (block a) = block (NE.map f a)
  map-stm f (call a) = call (f a)
  map-stm f (assign a) = assign (f a)
  map-stm f (continue a) = continue (f a)
  map-stm f (break a) = break (f a)
  map-stm f (return a expr) = return (f a) expr
  map-stm f (loop a guard body) =
    loop (f a) guard (Data.List.map (map-stm f) body)
  map-stm f (branch a guard tc fc) =
    branch (f a) guard (Data.List.map (map-stm f) tc)
                       (Data.List.map (map-stm f) fc)


  Is-Loop : ∀ {i A} → Stm A i → Set
  Is-Loop (loop _ _ _) = ⊤
  {-# CATCHALL #-}
  Is-Loop _ = ⊥

  Is-Branch : ∀ {i A} → Stm A i → Set
  Is-Branch (branch _ _ _ _) = ⊤
  {-# CATCHALL #-}
  Is-Branch _ = ⊥

  -- Non accepted function signature
  -- branch-true-case : ∀ {i A} {k : Size< i}
  --                  → (stm : Stm A i)
  --                  → Is-Branch {i} stm
  --                  → List (Stm A k)
  branch-true-case : ∀ {i A}
                   → (stm : Stm A (↑ i))
                   → Is-Branch  stm
                   → List (Stm A i)
  branch-true-case (branch _ _ tc _ ) ⊤ = tc
  branch-true-case (block _) ()
  branch-true-case (call _) ()
  branch-true-case (assign _) ()
  branch-true-case (break _) ()
  branch-true-case (continue _) ()
  branch-true-case (return _ _) ()
  branch-true-case (loop _ _ _) ()

  branch-false-case : ∀ {i A}
                    → (stm : Stm A (↑ i))
                    → Is-Branch stm
                    → List (Stm A i)
  branch-false-case (branch _ _ _ fc ) ⊤ = fc
  branch-false-case (block _) ()
  branch-false-case (call _) ()
  branch-false-case (assign _) ()
  branch-false-case (break _) ()
  branch-false-case (continue _) ()
  branch-false-case (return _ _) ()
  branch-false-case (loop _ _ _) ()

  loop-body : ∀ {A i}
            → (stm : Stm A (↑ i))
            → Is-Loop stm
            → List (Stm A i)
  loop-body (loop _ _ body) ⊤ = body
  loop-body (branch _ _ _ _) ()
  loop-body (block _) ()
  loop-body (call _) ()
  loop-body (assign _) ()
  loop-body (break _) ()
  loop-body (continue _) ()
  loop-body (return _ _) ()

  -- The Sizes here are probably all wrong.
  -- data Any {j : Level} {A : Set} (i : Size) (P : {k : Size} → Stm A k → Set j) : (Stm A i) → Set j where
  data Any {j : Level} {A : Set} {k' : Size} ( i : Size) (P : {k : Size} → Stm A k → Set j)  (stm : Stm A k') : Set j where
    here : P stm → Any i P stm
    in-branch : {k : Size< i}
              → (t : Is-Branch stm)
              → (List.Any (Any k P) (branch-true-case stm t))
                ⊎ (List.Any (Any k P) (branch-false-case stm t))
              → Any i P stm
    in-loop : {k : Size< i}
            → (t : Is-Loop stm)
            → List.Any (Any k P) (loop-body stm t)
            → Any i P stm

  map-any : ∀ {j} {k : Size} {i : Size< k}  {A : Set}
          → {P P' : {g : Size} → Stm A g → Set j}
          → {stm : Stm A k}
          → ({g : Size} → {stm' : Stm A g} → P stm' → P' stm')
          → Any i P stm
          → Any i P' stm
  map-any f (here p) = here (f p)
  map-any f (in-branch t (inj₁ p)) = (in-branch t (inj₁ (ListAny.map (map-any f) p)))
  map-any f (in-branch t (inj₂ p)) = (in-branch t (inj₂ (ListAny.map (map-any f) p)))
  map-any f (in-loop t p) = (in-loop t (ListAny.map (map-any f) p))


  dec-elim : ∀ {i} → {B : Set} {P : B → Set i} {P' : Set i} → {b : B} → (P b → P') → (¬ (P b) → P') → Dec (P b) → P'
  dec-elim yes-case _ (yes since) = yes-case since
  dec-elim _ no-case (no since) = no-case since


  any : ∀ {i} {k' : Size} {k : Size}  {A : Set}
      → {P : {g : Size} → Stm A g → Set i}
      → ({g : Size} → (stm' : Stm A g) → Dec (P stm'))
      → (stm : Stm A k')
      → Dec (Any k P stm)
  any {_} { (k')} {k} {A} {P} p stm@(loop a guard body)
   with (p stm)
  ... | yes p' = yes (Any.here p')
  ... | no ¬p' =
    let
     absurd : ¬ List.Any (Any k P) (loop-body stm unit) → Any (↑ k) P stm → ⊥
     absurd l r  = {!!}
     dec-in-body : Dec (Any k P stm)
     dec-in-body with ListAny.any (any p) (loop-body stm unit)
     ...| yes p'' = yes {! (Any.in-loop unit p'') !}
     ...| no notInList = no (λ inloop →
                                  ⊥-elim (absurd notInList inloop))
    in {!!}

    -- where
    --   absurd : ¬ List.Any (Any k P) (loop-body stm unit) → Any (↑ k) P stm → ⊥
    --   absurd _ (Any.here p) = contradiction p ¬p'
    --   absurd notInList (Any.in-loop _ in-list) =
    --     contradiction in-list notInList
    --   absurd _ (Any.in-branch t _ ) = t

    --   foo = (loop-body stm unit)
    --   rec : Dec (List.Any (Any k P) (loop-body stm unit))
    --   rec = ListAny.any (any p) (loop-body stm unit)






-- dec-elim {P = List.Any (Any k P) } {P' = Dec (Any (↑ k) P stm)}
--                              (λ z → yes (Any.in-loop unit {!!}))
--                              (λ not-in-list → no (λ in-loop → ⊥-elim (absurd {!!} {!!})))
--                              (ListAny.any (any {k = {!!} }  p) (loop-body stm unit))


-- -- any {_} {A} {P} p stm@(branch a guard tc fc) with (p stm)
-- -- ... | yes p' = yes (Any.here p')
-- -- ... | no ¬p' = dec-in-branch
-- --     where
-- --       absurd : ( ¬ (List.Any (Any P) tc) × ¬ (List.Any (Any P) fc))
-- --               → Any P (branch a guard tc fc) → ⊥
-- --       absurd _ (Any.here p) = contradiction p ¬p'
-- --       absurd _ (Any.in-loop t in-list) = t
-- --       absurd (¬in-tc , _) (Any.in-branch t (inj₁ in-tc) ) =
-- --         contradiction in-tc ¬in-tc
-- --       absurd (_ , ¬in-fc) (Any.in-branch t (inj₂ in-fc) ) =
-- --         contradiction in-fc ¬in-fc
-- --       dec-in-branch : Dec (Any P stm)
-- --       dec-in-branch with ListAny.any (any p) tc | ListAny.any (any p) fc
-- --       ...| yes in-tc | _ = yes (Any.in-branch unit (inj₁ in-tc))
-- --       ...| no ¬in-tc | yes in-fc = yes (Any.in-branch unit (inj₂ in-fc))
-- --       ...| no ¬in-tc | no ¬in-fc = no (λ in-branch →
-- --                               ⊥-elim (absurd (¬in-tc , ¬in-fc) in-branch))
-- -- any p stm@(call a) with (p stm)
-- -- ... | yes p' = yes (Any.here p')
-- -- ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
-- --                      ; (Any.in-loop t _) → t
-- --                      ; (Any.in-branch t _) → t
-- --                      }
-- --                      )
-- -- any p stm@(assign a) with (p stm)
-- -- ... | yes p' = yes (Any.here p')
-- -- ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
-- --                      ; (Any.in-loop t _) → t
-- --                      ; (Any.in-branch t _) → t
-- --                      })
-- -- any p stm@(continue a) with (p stm)
-- -- ... | yes p' = yes (Any.here p')
-- -- ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
-- --                      ; (Any.in-loop t _) → t
-- --                      ; (Any.in-branch t _) → t
-- --                      })
-- -- any p stm@(break a) with (p stm)
-- -- ... | yes p' = yes (Any.here p')
-- -- ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
-- --                      ; (Any.in-loop t _) → t
-- --                      ; (Any.in-branch t _) → t
-- --                      })
-- -- any p stm@(return a _) with (p stm)
-- -- ... | yes p' = yes (Any.here p')
-- -- ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
-- --                      ; (Any.in-loop t _) → t
-- --                      ; (Any.in-branch t _) → t
-- --                      })
-- -- any p stm@(block a) with (p stm)
-- -- ... | yes p' = yes (Any.here p')
-- -- ... | no ¬p' = no (λ { (Any.here p') → contradiction p' ¬p'
-- --                      ; (Any.in-loop t _) → t
-- --                      ; (Any.in-branch t _) → t
-- --                      })
