open import Function.Base
open import Function.Equivalence using ( _⇔_ ; Equivalence )
open import Function.Equality using ( Π ; _⟨$⟩_ )
import Function.Equivalence

open import Data.Unit using (⊤) renaming ( tt to unit)
open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)
open import Data.Nat.Properties using (≡ᵇ⇒≡)
open import Data.Bool using ( if_then_else_
                            ; Bool
                            ; _∧_ ; _∨_ ; not
                            ; true ; false
                            ; T
                            )
open import Data.Bool.Properties using (T-≡ ; T? )

open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)

import Data.These
open import Data.These using ( These ; this ; that ; these )

open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.List using (List ; [] ; _∷_ ; foldl ; zipWith ; [_] ; _++_ ; drop)
import Data.List.Properties
open import Data.List.Properties using (++-identityʳ)
open import Data.List.NonEmpty using (List⁺ ; _⁺++⁺_)

open import Data.List.Relation.Unary.Any using () renaming ( any to list-any)

import Data.List.Relation.Unary.All as List -- using ( All)
import Data.List.Relation.Unary.All as ListAll
import Data.List.Relation.Unary.Any as List -- using ( Any )
import Data.List.Relation.Unary.Any as ListAny -- using ( Any )
import Data.List.Relation.Unary.Linked as List using ( Linked )
import Data.List.Relation.Unary.Linked as Linked

import Data.List.NonEmpty as NE
open import Data.Integer using (ℤ)
                         renaming (+_ to ℤ+_)

open import Agda.Builtin.Equality using (_≡_ ; refl )

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst ; cong₂)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Relation.Binary.PropositionalEquality using (cong ; _≢_ )

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)

open import Data.Empty
open import Relation.Unary using (Pred ; Decidable)
open import Relation.Nullary.Decidable.Core
open import Relation.Nullary
open import Relation.Nullary.Negation hiding (excluded-middle)

open import Level using (Level ; _⊔_)

import Agda.Builtin.Size
open import Agda.Builtin.Size using (Size ; Size<_ ; ∞ ; ↑_ ; _⊔ˢ_)

module FlowTest2 where

  -- Placeholders to decrease file size
  𝔹 : Set
  𝔹 = ⊤
  𝕍 : Set
  𝕍 = ⊤
  𝔸 : Set
  𝔸 = ⊤

  data Seq (i : Size) (A : Size → Set) : Set where
    []ₛ : Seq i A
    _∷ₛ_ : {j : Size< i} → (A j) → Seq j A → Seq i A

  map-seq : ∀ {i A B} → ({k : Size} → A k → B k) → Seq i A → Seq i B
  map-seq _ []ₛ = []ₛ
  map-seq f (x ∷ₛ xs) =
    f x ∷ₛ map-seq f xs

  data StmType : Set where
    *ₛₜₘ : StmType
    ¬⊕ : StmType

  -- st-lub : StmType → StmType → StmType
  -- st-lub AnyStm _ = AnyStm
  -- st-lub _ AnyStm = AnyStm
  -- st-lub NonComposeStm NonComposeStm

  data Stm (A : Set) : StmType → Set where
    block : List⁺ A → Stm A ¬⊕
    call  : A → Stm A ¬⊕
    assign : A → Stm A ¬⊕
    break : A → Stm A ¬⊕
    continue : A → Stm A ¬⊕
    compose : ∀ {st} → Stm A ¬⊕ → Stm A st → Stm A *ₛₜₘ
    return : A → 𝔸 → Stm A ¬⊕
    branch : ∀ {st}
           → A
           → 𝔹
           → Stm A st
           → Stm A st
           → Stm A ¬⊕
    loop : ∀ {st}
         → A
         → 𝔹
         → Stm A st
         → Stm A ¬⊕

  t1 : Stm ⊤ _
  t1 = compose ((call unit)) (call unit)

  map-stm : ∀{st} {A B : Set} → (A → B) → Stm A st → Stm B st
  map-stm f (block a) = block (NE.map f a)
  map-stm f (call a) = call (f a)
  map-stm f (assign a) = assign (f a)
  map-stm f (continue a) = continue (f a)
  map-stm f (break a) = break (f a)
  map-stm f (return a expr) = return (f a) expr
  map-stm f (compose lhs rhs) = compose (map-stm f lhs) (map-stm f rhs)
  map-stm f (loop a guard body) =
    loop (f a) guard (map-stm f body)
  map-stm f (branch a guard tc fc) =
    branch (f a) guard (map-stm f tc)
                       (map-stm f fc)

  Is-Loop : ∀ {i A} → Stm A i → Set
  Is-Loop (loop _ _ _) = ⊤
  {-# CATCHALL #-}
  Is-Loop _ = ⊥

  Is-Branch : ∀ {i A} → Stm A i → Set
  Is-Branch (branch _ _ _ _) = ⊤
  {-# CATCHALL #-}
  Is-Branch _ = ⊥

  -- Non accepted function signature
  -- branch-true-case : ∀ {i A} {k : Size< i}
  --                  → (stm : Stm A i)
  --                  → Is-Branch {i} stm
  --                  → List (Stm A k)
  branch-true-case : ∀ {i A}
                   → (stm : Stm A (↑ i))
                   → Is-Branch  stm
                   → List (Stm A i)
  branch-true-case (branch _ _ tc _ ) ⊤ = tc
  branch-true-case (block _) ()
  branch-true-case (call _) ()
  branch-true-case (assign _) ()
  branch-true-case (break _) ()
  branch-true-case (continue _) ()
  branch-true-case (return _ _) ()
  branch-true-case (loop _ _ _) ()

  branch-false-case : ∀ {i A}
                    → (stm : Stm A (↑ i))
                    → Is-Branch stm
                    → (Stm A i)
  branch-false-case (branch _ _ _ fc ) ⊤ = fc
  branch-false-case (block _) ()
  branch-false-case (call _) ()
  branch-false-case (assign _) ()
  branch-false-case (break _) ()
  branch-false-case (continue _) ()
  branch-false-case (return _ _) ()
  branch-false-case (loop _ _ _) ()

  loop-body : ∀ {A st st'}
            → (stm : Stm A st)
            → Is-Loop stm
            → Stm A st'
  loop-body (loop _ _ body) ⊤ = body
  loop-body (branch _ _ _ _) ()
  loop-body (block _) ()
  loop-body (call _) ()
  loop-body (assign _) ()
  loop-body (break _) ()
  loop-body (continue _) ()
  loop-body (return _ _) ()

  data Any {j : Level} {A : Set (P : Stm A k → Set j) (stm : Stm A k') : Set j where
    here : P stm → Any i P stm
    in-branch : {k : Size< i}
              → (t : Is-Branch stm)
              → (List.Any (Any k P) (branch-true-case stm t))
                ⊎ (List.Any (Any k P) (branch-false-case stm t))
              → Any i P stm
    in-loop : {k : Size< i}
            → (t : Is-Loop stm)
            → List.Any (Any k P) (loop-body stm t)
            → Any i P stm
