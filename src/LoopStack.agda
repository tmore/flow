module LoopStack where

  open import Function.Base
  open import Data.List using (List ; [] ; _∷_ ; [_])
  open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                         renaming ( maybe′ to maybe)
  open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
  open import Agda.Builtin.Equality using (_≡_ ; refl )

  open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)

  import Relation.Binary.PropositionalEquality as Eq
  open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)
  open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

  import Extra.List

  module Node where
    record Node (A : Set) : Set where
      field
        continue : List A
        break : List A

    empty : ∀ {A} → Node A
    empty = record
            { continue = []
            ; break = []
            }
    mapSeq : ∀ {A B}
            → (List A → List B)
            → Node A
            → Node B
    mapSeq f node =
      record { continue = f (Node.continue node)
             ; break = f (Node.break node)
             }

    map : ∀ {A B} → (A → B) → Node A → Node B
    map f node = mapSeq (Data.List.map f) node


    concatMap : {A B : Set}
               → (A → List B)
               → Node A
               → Node B
    concatMap f node =
      record
      { continue =
        Data.List.concat (Data.List.map f (Node.continue node))
      ; break =
        Data.List.concat (Data.List.map f (Node.break node))
      }

    module Properties where
      -- continue-mapSeq-commutes-ish :
      --   ∀ {A B}
      --   → {f : List A → List B}
      --   → (node : Node A)
      --   → (Node.continue ∘ mapSeq f) node
      --   ≡ f (Node.continue node)
      -- continue-mapSeq-commutes-ish {f = f} node =
      --   begin
      --     (Node.continue ∘ mapSeq f) node
      --   ≡⟨⟩
      --     f (Node.continue node)
      --   ∎
      mapSeq-cancel :
        ∀ {A B}
        → {f : List B → List B}
        → {g : List A → List B}
        → (node : Node A)
        → ((seq : List A) → ((f ∘ g) seq ≡ g seq))
        → mapSeq (f ∘ g) node
        ≡ mapSeq g node
      mapSeq-cancel {f = f} {g = g} node g-cancels-f =
        begin
          mapSeq (f ∘ g) node
        ≡⟨⟩
          record
          { continue = (f ∘ g) (Node.continue node)
          ; break = (f ∘ g) (Node.break node)
          }
        ≡⟨ Eq.cong₂ (λ z₁ z₂ →
                       record
                       { continue = z₁
                       ; break = z₂
                       }
                     )
                    (g-cancels-f (Node.continue node))
                    (g-cancels-f (Node.break node))
          ⟩
          mapSeq g node
        ∎

      mapSeq-map :
        ∀ {A B C}
        → {f : A → B}
        → {g : List B → List C}
        → (node : Node A)
        → (mapSeq g ∘ (map f)) node
        ≡ mapSeq (g ∘ Data.List.map f) node
      mapSeq-map {f = f} {g = g} node =
        refl

      mapSeq-composes :
        ∀ {A B C}
        → {g : List A → List B}
        → {f : List B → List C}
        → (node : Node A )
        → (mapSeq f ∘ mapSeq g) node
        ≡ mapSeq (f ∘ g) node
      mapSeq-composes node = refl

      concatMap-id
        : ∀ {A B}
        → {from : A → B}
        → {to : B -> List A}
        → ((a : A) → ((to (from a)) ≡ [ a ]))
        → (node : Node A)
        → concatMap to (map from node) ≡ node
      concatMap-id {_} {_} {from} {to} id-ish-proof node =
        Eq.cong₂ (λ z₁ z₂ → record { continue = z₁ ; break = z₂})
                    (Extra.List.concatMap-id id-ish-proof (Node.continue node))
                    (Extra.List.concatMap-id id-ish-proof (Node.break node))

    mapSeq[id]≡id :
      {A : Set}
      → {f : List A → List A}
      → ((as : List A) → (f as ≡ as))
      → (node : Node A)
      → mapSeq f node ≡  node
    mapSeq[id]≡id {_} {f} id-proof node =
      begin
        mapSeq f node
      ≡⟨ Eq.cong₂ (λ z₁ z₂ →
                  record
                  { continue = z₁
                  ; break = z₂
                  })
                (id-proof (Node.continue node))
                (id-proof (Node.break node))
        ⟩
        node
      ∎
  open Node using (Node)

  LoopStack : Set → Set
  LoopStack A = List (Node A)

  empty : ∀ {A} → LoopStack A
  empty = []

  popNode : ∀ {A} → LoopStack A → Maybe (LoopStack A × Node A)
  popNode [] = nothing
  popNode (x ∷ xs) = just (xs , x)

  pop : ∀ {A} → LoopStack A → LoopStack A
  pop ls = maybe proj₁ empty (popNode ls)

  popContinue : ∀ {A} → LoopStack A → Maybe (LoopStack A × List A)
  popContinue ls =
    Data.Maybe.map (Data.Product.map₂ Node.continue) (popNode ls)

  popBreak : ∀ {A} → LoopStack A → Maybe (LoopStack A × List A)
  popBreak ls =
    Data.Maybe.map (Data.Product.map₂ Node.break) (popNode ls)

  pushNode : ∀ {A} → Node A → LoopStack A → LoopStack A
  pushNode node ls = node ∷ ls

  push : ∀ {A} → List A → List A → LoopStack A → LoopStack A
  push ct bt ls =
    pushNode (record { continue = ct ; break = bt }) ls
a
  map : ∀ {A B} → (A → B) → LoopStack A → LoopStack B
  map f xs = Data.List.map (Node.map f) xs

  mapSeq : ∀ {A B} → (List A → List B) → LoopStack A → LoopStack B
  mapSeq f node = Data.List.map (Node.mapSeq f) node

  concatMap : {A B : Set}
             → (A → List B)
             → LoopStack A
             → LoopStack B
  concatMap f g = Data.List.map (Node.concatMap f) g

  module Properties where
    loopStackElim : ∀ {a}
                  → (ls : LoopStack a)
                  → (Σ[ x ∈ Node a ] Σ[ xs ∈ LoopStack a ] ls ≡ pushNode x xs
                    ⊎ ls ≡ empty
                    )
    loopStackElim  [] = inj₂ refl
    loopStackElim  (x ∷ xs) = inj₁ (x , xs , refl)

    popNodeElim : ∀ {a}
                → (ls : LoopStack a)
                → ( (Σ[ x ∈ Node a ] (Σ[ xs ∈ LoopStack a ]  ((popNode ls ≡ just (xs , x)) × ((ls ≡ pushNode x xs)))))
                  ⊎ popNode ls ≡ nothing
                  )
    popNodeElim [] = inj₂ refl
    popNodeElim ls@(x ∷ xs) =
      inj₁ (x , ( xs , (refl , refl)))


    popNode-injective : ∀ {a}
               → (x : LoopStack a)
               → (y : LoopStack a)
               → popNode x ≡ popNode y
               → x ≡ y
    popNode-injective [] [] popNode≡popNode = refl
    popNode-injective (x ∷ xs) (y ∷ ys) popNode≡popNode =
      cong (Data.Product.uncurry _∷_) x,xs≡y,ys
      where
        popNodex≡y' : popNode (x ∷ xs) ≡ just (ys , y)
        popNodex≡y' = (trans popNode≡popNode refl)
        open import Data.Maybe.Properties
        xs,x≡ys,y :  (xs , x) ≡ (ys , y)
        xs,x≡ys,y = just-injective (trans (sym refl) popNodex≡y')
        x,xs≡y,ys : (x , xs) ≡ (y , ys)
        x,xs≡y,ys = cong Data.Product.swap xs,x≡ys,y


    popNode≡nothing⇒empty : ∀ {a}
                            → (ls : LoopStack a)
                            → popNode ls ≡ nothing
                            → ls ≡ empty
    popNode≡nothing⇒empty [] popNode≡nothing = refl
    popNode≡nothing⇒empty ls@(x ∷ xs) ()


    PopContinueElim : ∀ {a}
                    → (ls : LoopStack a)
                    → ( Σ[ p ∈ (LoopStack a × List a) ] popContinue ls ≡ just p
                      ⊎ popContinue ls ≡ nothing
                      )
    PopContinueElim ls with popContinue ls
    ... | just p = inj₁ (p , refl)
    ... | nothing = inj₂ refl

    popNode≡just⇒popContinue≡just : ∀ {a p}
                                    → (ls : LoopStack a)
                                    → (popNode ls ≡ just p)
                                    → (popContinue ls ≡ just (Data.Product.map₂ Node.continue p  ))
    popNode≡just⇒popContinue≡just {a} {p} ls popNode≡just =
      begin
        popContinue ls
      ≡⟨ cong (Data.Maybe.map (Data.Product.map₂ Node.continue)) popNode≡just ⟩
        just (Data.Product.map₂ Node.continue p )
      ∎


    popNode-map-commutes-ish : {A : Set} {B : Set}
                      → (ls : LoopStack A)
                      → (f : A → B)
                      → popNode (map f ls)
                        ≡ Data.Maybe.map (Data.Product.map (map f)
                                                            (Node.map f))
                                          (popNode ls)
    popNode-map-commutes-ish ls f with (popNodeElim ls)
    ... | inj₂ popNode≡nothing =
      begin
        popNode (map f ls)
      ≡⟨ refl ⟩
        popNode (Data.List.map (Node.map f) ls)
      ≡⟨ cong (popNode ∘ (Data.List.map (Node.map f)))
               ls≡empty ⟩
        popNode (Data.List.map (Node.map f) [])
      ≡⟨ refl ⟩
        nothing
      ≡⟨ refl ⟩
        rightMap nothing
      ≡⟨ cong rightMap (sym popNode≡nothing) ⟩
        Data.Maybe.map (Data.Product.map (map f) (Node.map f)) (popNode ls)
      ∎
      where
        rightMap = Data.Maybe.map (Data.Product.map (map f) (Node.map f))
        ls≡empty : ls ≡ empty
        ls≡empty = (popNode≡nothing⇒empty ls popNode≡nothing)
    ...| inj₁ p@( node , ls' , (popNode≡just , ls≡push[node,ls'])) =
      begin
        popNode (map f ls)
      ≡⟨ refl ⟩
        popNode (Data.List.map (Node.map f) ls)
      ≡⟨ cong (popNode ∘ (Data.List.map (Node.map f)))
               ( ls≡push[node,ls']    ) ⟩
         popNode (Data.List.map (Node.map f) (node ∷ ls' ))
      ≡⟨⟩
        popNode (Node.map f node ∷ (Data.List.map (Node.map f) ls'))
      ≡⟨⟩
        just ((Data.List.map (Node.map f) ls') , Node.map f node)
      ≡⟨⟩
        maybeMap (just (ls' ,  node))
      ≡⟨⟩
        maybeMap (popNode (pushNode node ls'))
      ≡⟨ cong (maybeMap ∘ popNode) (sym ls≡push[node,ls'] )⟩
        Data.Maybe.map (Data.Product.map (map f) (Node.map f)) (popNode ls)
      ∎
        where
          maybeMap =
            Data.Maybe.map (Data.Product.map (map f) (Node.map f))

    -- Almost the same as popNode-map-commutes-ish
    popNode-mapSeq-commutes-ish :
      {A : Set} {B : Set}
      → (ls : LoopStack A)
      → (f : List A → List B)
      → popNode (mapSeq f ls)
      ≡ Data.Maybe.map (Data.Product.map (mapSeq f) (Node.mapSeq f))
                        (popNode ls)
    popNode-mapSeq-commutes-ish ls f with (popNodeElim ls)
    ... | inj₂ popNode≡nothing =
      begin
        popNode (mapSeq f ls)
      ≡⟨ refl ⟩
        popNode (Data.List.map (Node.mapSeq f) ls)
      ≡⟨ cong (popNode ∘ (Data.List.map (Node.mapSeq f)))
               ls≡empty ⟩
        popNode (Data.List.map (Node.mapSeq f) [])
      ≡⟨ cong rightMap (sym popNode≡nothing) ⟩
        Data.Maybe.map (Data.Product.map (mapSeq f) (Node.mapSeq f)) (popNode ls)
      ∎
      where
        rightMap = Data.Maybe.map (Data.Product.map (mapSeq f) (Node.mapSeq f))
        ls≡empty : ls ≡ empty
        ls≡empty = (popNode≡nothing⇒empty ls popNode≡nothing)
    ...| inj₁ p@( node , ls' , (popNode≡just , ls≡push[node,ls'])) =
      begin
        popNode (mapSeq f ls)
      ≡⟨⟩
        popNode (Data.List.map (Node.mapSeq f) ls)
      ≡⟨ cong (popNode ∘ (Data.List.map (Node.mapSeq f)))
               ( ls≡push[node,ls']    ) ⟩
         popNode (Data.List.map (Node.mapSeq f) (node ∷ ls' ))
      ≡⟨⟩
        popNode (Node.mapSeq f node ∷ (Data.List.map (Node.mapSeq f) ls'))
      ≡⟨⟩
        just ((Data.List.map (Node.mapSeq f) ls') , Node.mapSeq f node)
      ≡⟨⟩
        maybeMap (just (ls' ,  node))
      ≡⟨⟩
        maybeMap (popNode (pushNode node ls'))
      ≡⟨ cong (maybeMap ∘ popNode) (sym ls≡push[node,ls'] )⟩
        Data.Maybe.map (Data.Product.map (mapSeq f) (Node.mapSeq f)) (popNode ls)
      ∎
        where
          maybeMap =
            Data.Maybe.map (Data.Product.map (mapSeq f) (Node.mapSeq f))

    concatMap-id : {A B : Set}
                  → {from : A → B}
                  → {to : B -> List A}
                  → ((a : A) → ((to (from a)) ≡ [ a ]))
                  → (ls : LoopStack A)
                  → concatMap to (map from ls) ≡ ls
    concatMap-id id-ish-proof [] = refl
    concatMap-id {_} {_} {from} {to} id-ish-proof (x ∷ xs) =
      Eq.cong₂ (_∷_)
               (Node.Properties.concatMap-id id-ish-proof x)
               (concatMap-id id-ish-proof xs)


    mapSeq-composes :
      ∀ {A B C}
      → {g : List A → List B}
      → {f : List B → List C}
      → (ls : LoopStack A)
      → (mapSeq f ∘ mapSeq g) ls
      ≡ mapSeq (f ∘ g) ls
    mapSeq-composes [] = refl
    mapSeq-composes {_} {_} {_} {g} {f}  (x ∷ xs) =
      cong (_ ∷_) (mapSeq-composes xs)

    mapSeq[id]≡id :
      {A : Set}
      → {f : List A → List A}
      → ((as : List A) → (f as ≡ as))
      → (ls : LoopStack A)
      → mapSeq f ls ≡ ls
    mapSeq[id]≡id {_} {f} id-proof [] = refl
    mapSeq[id]≡id {_} {f} id-proof (x ∷ xs) =
      begin
        mapSeq f (x ∷ xs)
      ≡⟨⟩
        Node.mapSeq f x ∷ mapSeq f xs
      ≡⟨ cong (_ ∷_) (mapSeq[id]≡id {f = f} id-proof xs) ⟩
        Node.mapSeq f x ∷ xs
      ≡⟨ cong (_∷ _) (Node.mapSeq[id]≡id {f = f} id-proof x) ⟩
        (x ∷ xs)
      ∎

    mapSeq-cancel :
      {A B : Set}
      → {f : List B → List B}
      → {g : List A → List B}
      → (ls : LoopStack A)
      → ((seq : List A) → ((f ∘ g) seq ≡ g seq))
      → mapSeq (f ∘ g) ls
      ≡ mapSeq g ls
    mapSeq-cancel [] _ = refl
    mapSeq-cancel {f = f} {g = g} (x ∷ xs) g-cancels-f =
      Eq.cong₂ _∷_
               (Node.Properties.mapSeq-cancel {f = f} {g = g} x g-cancels-f)
               (mapSeq-cancel {f = f} {g = g} xs g-cancels-f)

    mapSeq-map :
      ∀ {A B C}
      → {f : A → B}
      → {g : List B → List C}
      → (ls : LoopStack A)
      → (mapSeq g ∘ (map f)) ls
      ≡ mapSeq (g ∘ Data.List.map f) ls
    mapSeq-map {f = f} {g = g} [] = refl
    mapSeq-map {f = f} {g = g} (x ∷ xs) =
      cong (_ ∷_) (mapSeq-map xs)
