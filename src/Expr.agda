module Expr where

open import Data.Nat using ( suc ; ℕ ; _≡ᵇ_ ) renaming (_<_ to _<ℕ_ ; _+_ to _+ℕ_)
open import Data.Bool using (if_then_else_ ; Bool ; _∧_ ; _∨_ ; true ; false ; not ; T  ) renaming ( )

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)
open import Relation.Binary.PropositionalEquality using (_≢_ )
open import Relation.Nullary
open import Relation.Nullary.Negation hiding (excluded-middle)
open import Data.Empty

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
import Data.Sum
import Data.Integer as ℤ
open import Data.Integer using (ℤ)

open import Relation.Nullary.Decidable.Core using (isYes ; isNo)

module 𝕍 where
  𝕍 : Set
  𝕍 = ℕ
  equals : 𝕍 → 𝕍 → Bool
  equals 0 0 = true
  equals (suc n) 0 = false
  equals 0 (suc m) = false
  equals (suc n) (suc m) = equals n m

  equalsxy≡true : (x y : 𝕍) → (x ≡ y) → (equals x y ≡ true)
  equalsxy≡true 0 0 refl = refl
  equalsxy≡true (suc x) (suc y) refl = equalsxy≡true x y refl

  equalsxx≡true : (v : 𝕍) → (equals v v ≡ true)
  equalsxx≡true 0 = refl
  equalsxx≡true (suc v) = equalsxx≡true v


  equalsxy≡false : (x y : 𝕍) → x ≢ y → (equals x y ≡ false)
  equalsxy≡false 0 0 nEq = contradiction refl nEq
  equalsxy≡false 0 (suc n) nEq = refl
  equalsxy≡false (suc n) 0 nEq = refl
  equalsxy≡false (suc x) (suc y) nEq = equalsxy≡false x y (λ z → nEq (cong suc z))

  equalsxy≡false∧equalsxy≡true⇒⊥ : ∀ {x y}
                                     → (equals x y ≡ true)
                                     → (equals x y ≡ false)
                                     → ⊥
  equalsxy≡false∧equalsxy≡true⇒⊥ t f with (trans (sym t) f)
  ... | ()

  equalsxy≡false⇒x≢y : (x y : 𝕍) → (equals x y ≡ false) → x ≢ y
  equalsxy≡false⇒x≢y x y IsFalse with (Data.Nat._≟_ x y)
  ... | (true because ofʸ x≡y) =
              let
                IsTrue : equals x y ≡ true
                IsTrue = (equalsxy≡true x y x≡y)
                Absurd : ⊥
                Absurd = equalsxy≡false∧equalsxy≡true⇒⊥ {x} {y} IsTrue IsFalse
              in ⊥-elim Absurd
  ... | (false because ofⁿ np) = np

  equals⇒≡ : (v1 : 𝕍) → (v2 : 𝕍) → true ≡ equals v1 v2 → v1 ≡ v2
  equals⇒≡ 0 0 refl = refl
  equals⇒≡ (suc n) (suc m) r = cong suc (equals⇒≡ n m r)

  ≡⇒equals : (v v' : 𝕍) → v ≡ v' → (equals v v' ≡ true)
  ≡⇒equals v v' refl =
    begin
      equals v v
    ≡⟨ equalsxx≡true v ⟩
      true
    ∎


  equalsxy≡true∨false : (x y : 𝕍) → ((equals x y ≡ true) ⊎ (equals x y ≡ false))
  equalsxy≡true∨false x y with (Data.Nat._≟_ x y)
  ... | (true because ofʸ p) = inj₁ (equalsxy≡true x y p)
  ... | (false because ofⁿ np) = inj₂ (equalsxy≡false x y np)

  equalsxx,x≡x'⇒equalsxx' : ∀ {x x'} → x ≡ x' → equals x x ≡ equals x x'
  equalsxx,x≡x'⇒equalsxx' refl = refl

  returnVar : 𝕍
  returnVar = 0
open 𝕍 using (𝕍)



module 𝔸 where
  data 𝔸  : Set where
    𝕫 : ℤ → 𝔸
    -- ^ constants
    𝕧 : 𝕍 → 𝔸
    -- ^ Variables
    add : 𝔸 → 𝔸 → 𝔸
    mul : 𝔸 → 𝔸 → 𝔸
    sub : 𝔸 → 𝔸 → 𝔸

  equals : 𝔸 → 𝔸 → Bool
  equals (𝕫 n1) (𝕫 n2) = isYes (n1 ℤ.≟ n2)
  equals (𝕧 v1) (𝕧 v2) = 𝕍.equals v1 v2
  equals (add lhs1 rhs1) (add lhs2 rhs2) = equals lhs1 lhs2 ∧ equals rhs1 rhs2
  equals (mul lhs1 rhs1) (mul lhs2 rhs2) = equals lhs1 lhs2 ∧ equals rhs1 rhs2
  equals (sub lhs1 rhs1) (sub lhs2 rhs2) = equals lhs1 lhs2 ∧ equals rhs1 rhs2
  equals _ _ = false

  sem : 𝔸 → (Γ : 𝕍 → ℤ) → ℤ
  sem (𝕫 z) _ = z
  sem (𝕧 v) γ = γ v
  sem (add lhs rhs) γ = sem lhs γ ℤ.+ sem rhs γ
  sem (mul lhs rhs) γ = sem lhs γ ℤ.* sem rhs γ
  sem (sub lhs rhs) γ = sem lhs γ ℤ.- sem rhs γ
open 𝔸 using (𝔸)

module 𝔹 where
  data 𝔹 : Set where
    tt : 𝔹
    ff : 𝔹
    ! : 𝔹 → 𝔹
    eq : 𝔸 → 𝔸 → 𝔹
    lt : 𝔸 → 𝔸 → 𝔹
    && : 𝔹 → 𝔹 → 𝔹

  equals  : 𝔹 → 𝔹 → Bool
  equals tt tt = true
  equals ff ff = true
  equals (! a1) (! a2) = equals a1 a2
  equals (eq a1 b1) (eq a2 b2) = 𝔸.equals a1 a2 ∧ 𝔸.equals b1 b2
  equals (lt a1 b1) (lt a2 b2) = 𝔸.equals a1 a2 ∧ 𝔸.equals b1 b2
  equals (&& a1 b1) (&& a2 b2) = equals a1 a2 ∧ equals b1 b2
  equals _ _ = false

  sem : 𝔹 → (Γ : 𝕍 → ℤ) → Bool
  sem tt _ = true
  sem ff _ = false
  sem (eq lhs rhs) γ = isYes ((𝔸.sem lhs) γ ℤ.≟ (𝔸.sem rhs γ))
  sem (lt lhs rhs) γ = isYes (𝔸.sem lhs γ ℤ.<?  𝔸.sem rhs γ)
  sem (! b) γ = not (sem b γ)
  sem (&& lhs rhs) γ = (sem lhs γ) ∧ (sem rhs γ)
