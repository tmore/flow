import Maps
import VarCtx

open import Function
open import Data.Bool using (Bool ; true ; false)
open import Data.Maybe using (Maybe ; just ; nothing ; fromMaybe )
                       renaming ( maybe′ to maybe)
open import Data.Product using ( _×_ ; _,_ ; proj₁ ; proj₂ ; Σ-syntax)
open import Data.List using (List ; [] ; _∷_ ; [_] )

import Data.Integer as ℤ
open import Data.Integer using (ℤ)

open import Agda.Builtin.Equality using (_≡_ ; refl )

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)


open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
open import Util using (maybeElim ; maybe⊥)
open import Data.Empty

open import Expr
open 𝕍 using (𝕍)
open 𝔸 using (𝔸)
open 𝔹 using (𝔹)

import LoopStack
open LoopStack using (LoopStack)
open import Level using (Level ; _⊔_)

import Extra.List


module Ctx (MapType : Set → Set)
           (MapInstance : Maps.IntMap MapType)
           where

  module VarCtx' = VarCtx MapType MapInstance
  open VarCtx' using (VarCtx)
  module Map' = Maps.IntMap MapInstance

  -- First list is argument seconed is the local variables
  Proc : Set → Set
  Proc A = List 𝕍 × List 𝕍 × List A

  record Ctx (A : Set) : Set where
    field
      procedures : MapType (Proc A)
      variables : VarCtx
      loopStack : LoopStack A

  empty : ∀ {A} → Ctx A
  empty = record
          { procedures = Map'.empty
          ; variables = VarCtx'.empty
          ; loopStack = LoopStack.empty
          }

  mapSeq : ∀ {A B} → (List A → List B) → Ctx A → Ctx B
  mapSeq f ctx =
    record
    { procedures =
      Map'.map (λ { (args , locals , body) →
                     (args , locals , f body)
                   })
                (Ctx.procedures ctx)
    ; variables = Ctx.variables ctx
    ; loopStack =
       LoopStack.mapSeq f (Ctx.loopStack ctx)
    }

  map : {A B : Set} → (A → B) → Ctx A → Ctx B
  map f ctx = mapSeq (Data.List.map f) ctx

  concatMap : {A B : Set} → (A → List B) → Ctx A → Ctx B
  concatMap f ctx =
    record
    { procedures =
      Map'.map
        (λ { (args , locals , body) →
          ( args , locals , Data.List.concat (Data.List.map f body))})
        (Ctx.procedures ctx)
    ; variables = Ctx.variables ctx
    ; loopStack = LoopStack.concatMap f (Ctx.loopStack ctx)
    }

  mapLoopStackSeqs : ∀ {A} → (List A → List A) → Ctx A → Ctx A
  mapLoopStackSeqs f ctx =
    record ctx
    { loopStack =
      LoopStack.mapSeq f (Ctx.loopStack ctx)
    }

  getProc : ∀ {A} → 𝕍 → Ctx A → Proc A
  getProc name ctx =
    fromMaybe ([] , [] , []) (Map'.lookup name (Ctx.procedures ctx))


  variables : ∀ {A} → Ctx A → VarCtx
  variables = Ctx.variables

  procedures : ∀ {A} → Ctx A → MapType (Proc A)
  procedures = Ctx.procedures

  isLocal : ∀ {A} → 𝕍 → Ctx A → Bool
  isLocal v ctx =
    VarCtx'.isLocal v (Ctx.variables ctx)

  getVar : ∀ {A} → 𝕍 → Ctx A → ℤ
  getVar v s =
    VarCtx'.get v (Ctx.variables s)

  setVarCtx : ∀ {A} → VarCtx → Ctx A → Ctx A
  setVarCtx newVarCtx ctx =
    record ctx { variables = newVarCtx }

  setVar : ∀ {A} → 𝕍 → ℤ → Ctx A → Ctx A
  setVar v n ctx =
    setVarCtx (VarCtx'.set v n (Ctx.variables ctx)) ctx

  defineLocal : ∀ {A} → 𝕍 → ℤ → Ctx A → Ctx A
  defineLocal var value ctx =
    setVarCtx (VarCtx'.define var value (Ctx.variables ctx)) ctx

  -- Replaces all global values in the Ctx with the global variables from the
  -- provided VarCtx
  setGlobalsFrom : ∀ {A} → VarCtx → Ctx A → Ctx A
  setGlobalsFrom varCtx ctx =
    setVarCtx (VarCtx'.setGlobals (VarCtx'.getGlobals varCtx)
                                  (Ctx.variables ctx))
              ctx


  clearLocal : ∀ {A} → Ctx A → Ctx A
  clearLocal ctx =
    record ctx { loopStack = []
               ; variables =
                 VarCtx'.clearLocals (Ctx.variables ctx)
               }



  open LoopStack.Node using (Node)

  pop : ∀ {A} → Ctx A → Maybe (Ctx A × Node A)
  pop ctx =
    Data.Maybe.map
      (λ { ( ls' , node) →
             ( record ctx { loopStack = ls' }
             , node)})
      (LoopStack.popNode (Ctx.loopStack ctx))

  popContinue : ∀ {A} → Ctx A → Maybe (Ctx A × List A)
  popContinue ctx =
    Data.Maybe.map (Data.Product.map₂ Node.continue) (pop ctx)

  popBreak : ∀ {A} → Ctx A → Maybe (Ctx A × List A)
  popBreak ctx =
    Data.Maybe.map (Data.Product.map₂ Node.break) (pop ctx)

  dropFrame : ∀ {A} → Ctx A → Ctx A
  dropFrame ctx =
    maybe proj₁ ctx (pop ctx)

  pushStack : ∀ {A} → List A → List A → Ctx A → Ctx A
  pushStack continueTo breakTo ctx =
    record ctx
    { loopStack = LoopStack.push continueTo breakTo (Ctx.loopStack ctx)
    }

  clearLoopStack : ∀ {A} → Ctx A → Ctx A
  clearLoopStack ctx =
    record ctx { loopStack = LoopStack.empty }

  toFuncMap : ∀ {A} → Ctx A → (𝕍 → ℤ)
  toFuncMap ctx = (λ v → getVar v ctx)

  eval𝔸 : ∀ {A} → 𝔸 → Ctx A → ℤ
  eval𝔸 expr ctx =
    𝔸.sem expr (toFuncMap ctx)

  eval𝔹 : ∀ {A} → 𝔹 → Ctx A → Bool
  eval𝔹 expr ctx =
    𝔹.sem expr (toFuncMap ctx)

  step :
    ∀ {A}
    → (Ctx A → List A → Ctx A × (List A ⊎ ℤ))
    → Ctx A
    → List A
    → Ctx A × (List A ⊎ ℤ)
  step {A} sem ctx [] =
       rec ctx
    where
      rec : Ctx A → Ctx A × (List A ⊎ ℤ)
      rec ctx@(record { loopStack = []}) = (ctx , inj₂ (ℤ.+ 0))
      rec ctx@(record { loopStack = (node ∷ lsTail)}) =
        step sem
             (record ctx {loopStack = lsTail})
             (LoopStack.Node.Node.continue node)
  step {A} sem ctx seq@(_ ∷ _)
    with sem ctx seq
  ...| (ctx' , inj₂ val) = (ctx' , inj₂ val)
  ...| (ctx' , inj₁ []) =
    Data.Product.map id inj₁ (fromMaybe (ctx' , []) (popContinue ctx'))
  ...| (ctx' , inj₁ (x ∷ xs)) = (ctx' , inj₁ (x ∷ xs))
    -- let (ctx' , res) = sem ctx seq
    -- in Data.Sum.[ (λ { [] → Data.Product.map
    --                           id inj₁
    --                           (fromMaybe (ctx' , []) (popContinue ctx'))
    --               ; xs@(_ ∷ _) → (ctx' , inj₁ xs)
    --               })
    --             , const (clearLocal ctx' , res)
    --             ]′ res

  module Properties where

    popNode≡just⇒pop≡just :
      ∀ {A}
      → (ctx : Ctx A)
      → (x : Node A)
      → (xs : LoopStack A)
      → LoopStack.popNode (Ctx.loopStack ctx) ≡ just (xs , x)
      → pop ctx ≡ just ( dropFrame ctx , x)
      × dropFrame ctx ≡ record ctx { loopStack = xs}
    popNode≡just⇒pop≡just {a} ctx node  ls  ≡justp =
      foo , dropFrame≡
      where
        pop≡ : pop ctx ≡ just ( record ctx { loopStack = ls } , node)
        pop≡ = cong (Data.Maybe.map (λ p → ( record ctx { loopStack = proj₁ p} , proj₂ p))) ≡justp

        dropFrame≡ : dropFrame ctx ≡ record ctx { loopStack = ls}
        dropFrame≡ = cong (maybe proj₁ ctx) pop≡

        foo : pop ctx ≡ just ( dropFrame ctx , node)
        foo =
          begin
            pop ctx
          ≡⟨ pop≡  ⟩
            Data.Maybe.map (λ p → ( record ctx
                            { loopStack = proj₁ p
                            }
                          , proj₂ p
                          ))
                          (just (ls , node))
          ≡⟨ cong (just ∘ ( _, node)) (sym dropFrame≡)  ⟩
            just ( dropFrame ctx , node)
          ∎
    pop≡just⇒popNode≡just : ∀ {a }
                         → (ctx : Ctx a)
                         → Σ[ node ∈ Node a ] pop ctx ≡ just ( dropFrame ctx , node)
                         → Σ[ x ∈ Node a ] Σ[ xs ∈ LoopStack a ] (LoopStack.popNode (Ctx.loopStack ctx) ≡ just (xs , x))
    pop≡just⇒popNode≡just ctx ( node , pop≡just) with maybeElim (LoopStack.popNode (Ctx.loopStack ctx))
    ... | inj₂ popNode≡nothing =
          ⊥-elim (maybe⊥ (trans (sym pop≡nothing)  pop≡just))
        where
          pop≡nothing : pop ctx ≡ nothing
          pop≡nothing =
            cong (Data.Maybe.map (λ p → ( record ctx { loopStack = proj₁ p }
                                 , proj₂ p
                                 ))) popNode≡nothing
    ... | inj₁ ((ls' , node') , popNode≡just) = ( node' , ls' , popNode≡just)

    popNode≡just⇒looStack≡pusxxs : ∀ {a}
                                   → (ctx : Ctx a)
                                   → (node : Node a)
                                   → (tail : LoopStack a)
                                   → LoopStack.popNode (Ctx.loopStack ctx)
                                     ≡ just (tail , node)
                                   → Ctx.loopStack ctx ≡ LoopStack.pushNode node tail
    popNode≡just⇒looStack≡pusxxs ctx node tail popNode≡just =
      LoopStack.Properties.popNode-injective (Ctx.loopStack ctx) (LoopStack.pushNode node tail) popNode≡just


    PopUniqueness : ∀ {a}
            → (ctx : Ctx a)
            → (node : Node a)
            → (node' : Node a)
            → (tail : LoopStack a)
            → pop ctx ≡ just ( dropFrame ctx , node)
            → LoopStack.popNode (Ctx.loopStack ctx) ≡ just (tail , node')
            → (node' ≡ node × dropFrame ctx ≡ record ctx { loopStack = tail})
    PopUniqueness ctx node node' tail pop≡just popNode≡just =
      ( node'≡node , proj₂ foo)
      where

        foo = popNode≡just⇒pop≡just ctx node' tail popNode≡just

        bar : pop ctx ≡ just (( record ctx { loopStack = tail } , node'))
        bar =
          begin
            pop ctx
          ≡⟨ cong (Data.Maybe.map (λ p → ( record ctx { loopStack = proj₁ p} , proj₂ p ))) popNode≡just ⟩
             just (( record ctx { loopStack = tail }
                          , node'
                          ))
          ∎
        proj₂pox≡node' : Data.Maybe.map proj₂ (pop ctx) ≡ just node'
        proj₂pox≡node' = cong (Data.Maybe.map proj₂) bar
        proj₂pox≡node : Data.Maybe.map proj₂ (pop ctx) ≡ just node
        proj₂pox≡node = cong (Data.Maybe.map proj₂) pop≡just
        just'≡just : just node' ≡ just node
        just'≡just = trans (sym proj₂pox≡node') proj₂pox≡node
        open import Data.Maybe.Properties
        node'≡node : node' ≡ node
        node'≡node = just-injective just'≡just

    popElim : ∀ {a}
            → (ctx : Ctx a)
            → (Σ[ node ∈ Node a ] pop ctx ≡ just ( dropFrame ctx , node)
              ⊎ pop ctx ≡ nothing)
    popElim ctx with maybeElim (LoopStack.popNode (Ctx.loopStack ctx))
    ... | inj₂ popNode≡nothing =
               inj₂ (cong (Data.Maybe.map (λ p → ( record ctx
                            { loopStack = proj₁ p
                            }
                          , proj₂ p
                          ))) popNode≡nothing)
    ... | inj₁ ( ( ls , node) , popNode≡just) =
               inj₁ ( node , proj₁ (popNode≡just⇒pop≡just ctx node  ls  popNode≡just))

    popContinueElim : {a : Set}
                    → (ctx : Ctx a)
                    → ( (Σ[ bt ∈  List a ] ((popContinue ctx) ≡ just (dropFrame ctx , bt)))
                      ⊎ (popContinue ctx ≡ nothing)
                      )
    popContinueElim {a} ctx with popElim ctx
    ... | inj₂ P = inj₂ (cong (Data.Maybe.map (Data.Product.map₂ Node.continue)) P)
    ... | inj₁ (node , P) =
               inj₁ ( Node.continue node
                    , (cong (Data.Maybe.map (Data.Product.map₂ Node.continue)) P)
                    )

    popBreakElim : {a : Set}
                    → (ctx : Ctx a)
                    → ( (Σ[ bt ∈  List a ] ((popBreak ctx) ≡ just (dropFrame ctx , bt)))
                      ⊎ (popBreak ctx ≡ nothing)
                      )
    popBreakElim {a} ctx with popElim ctx
    ... | inj₂ P = inj₂ (cong (Data.Maybe.map (Data.Product.map₂ Node.break)) P)
    ... | inj₁ (node , P) =
               inj₁ ( Node.break node
                    , (cong (Data.Maybe.map (Data.Product.map₂ Node.break)) P)
                    )
    pop≡nothing⇒popNode≡nothing : ∀ {a}
                                  → (ctx : Ctx a)
                                  → (pop ctx ≡ nothing)
                                  → LoopStack.popNode (Ctx.loopStack ctx) ≡ nothing
    pop≡nothing⇒popNode≡nothing {a} ctx pop≡nothing with maybeElim (LoopStack.popNode (Ctx.loopStack ctx))
    ... | inj₂ ≡nothing = ≡nothing
    ... | inj₁ (p@(ls , node) , ≡just) =
                   let pop≡just = proj₂ foo
                   in  ⊥-elim (maybe⊥ (sym (trans (sym pop≡just) pop≡nothing)))
          where
            foo : Σ[ e ∈ (Ctx a × Node a) ] (pop ctx ≡ just e)
            foo =
              (( record ctx { loopStack = ls } , node)
              , (begin
                  pop ctx
                ≡⟨ cong (Data.Maybe.map (λ p → ( record ctx
                            { loopStack = proj₁ p
                            }
                          , proj₂ p
                          ))) ≡just ⟩
                     just ( record ctx { loopStack = ls }
                          , node
                          )
                ∎
                )
              )

    pop≡nothing⇒loopStack≡empty : ∀ {a}
                                  → (ctx : Ctx a)
                                  → (pop ctx ≡ nothing)
                                  → (Ctx.loopStack ctx ≡ LoopStack.empty )
    pop≡nothing⇒loopStack≡empty {a} ctx pop≡nothing with LoopStack.Properties.loopStackElim (Ctx.loopStack ctx)
    ... | inj₂ ≡empty  = ≡empty
    ... | inj₁ ( x , xs , ≡pushxxs ) =
      ⊥-elim (maybe⊥ (trans (sym popNode≡nothing) popNode≡just))
      where
        popNode≡just : LoopStack.popNode (Ctx.loopStack ctx) ≡ just (xs , x)
        popNode≡just =
          begin
            LoopStack.popNode (Ctx.loopStack ctx)
          ≡⟨ cong LoopStack.popNode ≡pushxxs ⟩
            just (xs , x)
          ∎
        popNode≡nothing : LoopStack.popNode (Ctx.loopStack ctx) ≡ nothing
        popNode≡nothing = pop≡nothing⇒popNode≡nothing ctx pop≡nothing


    -- Almost a copy of pop-map
    pop-mapSeq-commutes-ish :
      ∀ {A B}
      → (ctx : Ctx A)
      → (f : List A → List B)
      → pop (mapSeq f ctx)
      ≡ Data.Maybe.map
          (Data.Product.map (mapSeq f) (LoopStack.Node.mapSeq f))
          (pop ctx)
    pop-mapSeq-commutes-ish
      {A} {B} ctx f with popElim ctx
    ...| inj₂ pop≡nothing =
      begin
        pop (mapSeq f ctx)
      ≡⟨⟩
        pop (ctx' (LoopStack.mapSeq f (Ctx.loopStack ctx)))
      ≡⟨ cong (pop ∘ ctx' ∘ (LoopStack.mapSeq f))
               (pop≡nothing⇒loopStack≡empty ctx pop≡nothing)
        ⟩
        nothing
      ≡⟨⟩
        maybe-map-abriv nothing
      ≡⟨ cong maybe-map-abriv (sym pop≡nothing) ⟩
        maybe-map-abriv (pop ctx)
      ∎
     where
        maybe-map-abriv =
          Data.Maybe.map
            (Data.Product.map (mapSeq f) (LoopStack.Node.mapSeq f))
        ctx' : LoopStack B → Ctx B
        ctx' ls = (record { procedures = Map'.map
                                       (λ { (args , locals , body) →
                                         ( args
                                         , locals
                                         , f body
                                         )
                                         })
                                       (Ctx.procedures ctx)
                      ; variables = Ctx.variables ctx
                      ; loopStack = ls
                      })
    ... | inj₁ ( node , pop≡just) =
      begin
        pop (mapSeq f ctx)
      ≡⟨⟩
        pop (ctx' (LoopStack.mapSeq f (Ctx.loopStack ctx)))
      ≡⟨⟩
        popMap (LoopStack.popNode (LoopStack.mapSeq f (Ctx.loopStack ctx)))
      ≡⟨ cong  popMap (LoopStack.Properties.popNode-mapSeq-commutes-ish (Ctx.loopStack ctx) (f) ) ⟩
        popMap (popNodeOuterMap (LoopStack.popNode (Ctx.loopStack ctx)))
      ≡⟨ cong (popMap ∘ popNodeOuterMap) loopStack≡just ⟩
        popMap (popNodeOuterMap (just (popNodeTail , popNodeHead)))
      ≡⟨⟩
        just (record (mapSeq f ctx) { loopStack =  (LoopStack.mapSeq f popNodeTail) }
             , ((LoopStack.Node.mapSeq f) popNodeHead))
      ≡⟨⟩
        maybeMap (just ( record ctx { loopStack = popNodeTail }
                       , popNodeHead
                       ))
      ≡⟨ cong (λ x → maybeMap (just (x , popNodeHead))) (sym (proj₂ uniqueness)) ⟩
        maybeMap (just ( dropFrame ctx , popNodeHead))
      ≡⟨ cong (λ x → maybeMap (just (dropFrame ctx , x))) (proj₁ uniqueness) ⟩
        maybeMap (just ( dropFrame ctx , node))
      ≡⟨ cong maybeMap (sym pop≡just)  ⟩
        maybeMap (pop ctx)
      ∎
      where

        popNodeOuterMap = Data.Maybe.map (Data.Product.map (LoopStack.mapSeq f)
                                                 (LoopStack.Node.mapSeq f))
        popMap =  Data.Maybe.map (λ p → ( record (mapSeq f ctx) { loopStack = proj₁ p }, proj₂ p))
        popNodeContents : Σ[ x ∈ Node A ] Σ[ xs ∈ LoopStack A ] (LoopStack.popNode (Ctx.loopStack ctx) ≡ just (xs , x))
        popNodeContents = pop≡just⇒popNode≡just ctx ( node , pop≡just)
        popNodeHead = proj₁  popNodeContents
        popNodeTail = (proj₁  ∘ proj₂) popNodeContents
        loopStack≡just = (proj₂  ∘ proj₂) popNodeContents

        uniqueness : popNodeHead ≡ node × dropFrame ctx ≡ record ctx { loopStack = popNodeTail}
        uniqueness = PopUniqueness ctx node popNodeHead popNodeTail pop≡just loopStack≡just

        maybeMap = Data.Maybe.map (Data.Product.map (mapSeq f)
                                         (LoopStack.Node.mapSeq f))
        ctx' : LoopStack B → Ctx B
        ctx' ls = (record { procedures = Map'.map
                                       (λ { (args , locals , body) →
                                         ( args
                                         , locals
                                         , f body
                                         )}
                                       ) (Ctx.procedures ctx)
                      ; variables = Ctx.variables ctx
                      ; loopStack = ls
                      })

    pop-map-commutes-ish : ∀ {A B}
                      → (ctx : Ctx A)
                      → (f : A → B)
                      → pop (map f ctx)
                        ≡ Data.Maybe.map (Data.Product.map (map f)
                                                            (LoopStack.Node.map f))
                                          (pop ctx)
    pop-map-commutes-ish {A} {B} ctx f with popElim ctx
    ... | inj₂ pop≡nothing =
      begin
        pop (map f ctx)
      ≡⟨⟩
        pop (ctx' (LoopStack.map f (Ctx.loopStack ctx)))
      ≡⟨ cong (pop ∘ ctx' ∘ (LoopStack.map f)) (pop≡nothing⇒loopStack≡empty ctx pop≡nothing) ⟩
        pop (ctx' (LoopStack.map f LoopStack.empty))
      ≡⟨⟩
        pop (ctx' LoopStack.empty)
      ≡⟨⟩
        nothing
      ≡⟨⟩
        maybeMap nothing
      ≡⟨ cong maybeMap (sym pop≡nothing) ⟩
        maybeMap (pop ctx)
      ∎
      where
        maybeMap = Data.Maybe.map (Data.Product.map (map f)
                                         (LoopStack.Node.map f))
        ctx' : LoopStack B → Ctx B
        ctx' ls = (record { procedures = Map'.map
                                       (λ { (args , locals , body) →
                                         ( args
                                         , locals
                                         , Data.List.map f body
                                         )})
                                       (Ctx.procedures ctx)
                      ; variables = Ctx.variables ctx
                      ; loopStack = ls

                      })
    ... | inj₁ ( node , pop≡just) =
      begin
        pop (map f ctx)
      ≡⟨⟩
        pop (ctx' (LoopStack.map f (Ctx.loopStack ctx)))
      ≡⟨⟩
        popMap (LoopStack.popNode (LoopStack.map f (Ctx.loopStack ctx)))
      ≡⟨ cong  popMap (LoopStack.Properties.popNode-map-commutes-ish (Ctx.loopStack ctx) (f) ) ⟩
        popMap (popNodeOuterMap (LoopStack.popNode (Ctx.loopStack ctx)))
      ≡⟨ cong (popMap ∘ popNodeOuterMap) loopStack≡just ⟩
        popMap (popNodeOuterMap (just (popNodeTail , popNodeHead)))
      ≡⟨⟩
        just (record (map f ctx) { loopStack =  (LoopStack.map f popNodeTail) }
             , ((LoopStack.Node.map f) popNodeHead))
      ≡⟨⟩
        just (record  { loopStack =  (LoopStack.map f popNodeTail)
                      ; variables = variables ctx
                      ; procedures = Map'.map
                                       (λ { (args , locals , body) →
                                         ( args
                                         , locals
                                         , Data.List.map f body
                                         )}
                                       ) (Ctx.procedures ctx)
                      }
             , ((LoopStack.Node.map f) popNodeHead))
      ≡⟨⟩
        just ( map f (record ctx { loopStack = popNodeTail })
             , ((LoopStack.Node.map f) popNodeHead)
             )
      ≡⟨⟩
        maybeMap (just ( record ctx { loopStack = popNodeTail }
                       , popNodeHead
                       ))
      ≡⟨ cong (λ x → maybeMap (just (x , popNodeHead))) (sym (proj₂ uniqueness)) ⟩
        maybeMap (just ( dropFrame ctx , popNodeHead))
      ≡⟨ cong (λ x → maybeMap (just (dropFrame ctx , x))) (proj₁ uniqueness) ⟩
        maybeMap (just ( dropFrame ctx , node))
      ≡⟨ cong maybeMap (sym pop≡just)  ⟩
        maybeMap (pop ctx)
      ∎
      where

        popNodeOuterMap = Data.Maybe.map (Data.Product.map (LoopStack.map f)
                                                 (LoopStack.Node.map f))
        popMap =  Data.Maybe.map (λ p → ( record (map f ctx) { loopStack = proj₁ p }, proj₂ p))
        popNodeContents : Σ[ x ∈ Node A ] Σ[ xs ∈ LoopStack A ] (LoopStack.popNode (Ctx.loopStack ctx) ≡ just (xs , x))
        popNodeContents = pop≡just⇒popNode≡just ctx ( node , pop≡just)
        popNodeHead = proj₁  popNodeContents
        popNodeTail = (proj₁  ∘ proj₂) popNodeContents
        loopStack≡just = (proj₂  ∘ proj₂) popNodeContents

        uniqueness : popNodeHead ≡ node × dropFrame ctx ≡ record ctx { loopStack = popNodeTail}
        uniqueness = PopUniqueness ctx node popNodeHead popNodeTail pop≡just loopStack≡just

        maybeMap = Data.Maybe.map (Data.Product.map (map f)
                                         (LoopStack.Node.map f))
        ctx' : LoopStack B → Ctx B
        ctx' ls = (record { procedures = Map'.map
                                       (λ { (args , locals , body) →
                                         ( args
                                         , locals
                                         , Data.List.map f body
                                         )}
                                       ) (Ctx.procedures ctx)
                      ; variables = Ctx.variables ctx
                      ; loopStack = ls
                      })

    dropFrame-map-commutes : ∀ {a b}
                            →  (ctx : Ctx a)
                            →  (f : a → b)
                            →  map f (dropFrame ctx) ≡ dropFrame (map f ctx)
    dropFrame-map-commutes {a} ctx f with popElim ctx
    ... | inj₂ pop≡nothing =
      begin
        map f (dropFrame ctx)
      ≡⟨ refl ⟩
        map f (maybe proj₁ ctx (pop ctx))
      ≡⟨ cong (map f ∘ (maybe proj₁ ctx)) pop≡nothing ⟩
        map f ctx
      ≡⟨ cong (maybe proj₁ (map f ctx) ∘ resMap) (sym pop≡nothing) ⟩
        maybe proj₁ (map f ctx) (resMap (pop ctx))
      ≡⟨ cong (maybe proj₁ (map f ctx))  (sym (pop-map-commutes-ish ctx f)) ⟩
        maybe proj₁ (map f ctx) (pop (map f ctx))
      ≡⟨ refl ⟩
        dropFrame (map f ctx)
      ∎
      where
       resMap =
         Data.Maybe.map (Data.Product.map (map f)
                        (LoopStack.Node.map f))
    -- cong (maybe proj₁ ctx) stackEmpty
    ... | inj₁ (node , pop≡just) =
      let ( node' , tail , popNode≡just ) = pop≡just⇒popNode≡just ctx (node , pop≡just)
          ( node'≡node , _) =  PopUniqueness ctx node node' tail pop≡just popNode≡just
          loopStackctx≡pushNode : Ctx.loopStack ctx ≡ LoopStack.pushNode node' tail
          loopStackctx≡pushNode = (popNode≡just⇒looStack≡pusxxs ctx node' tail popNode≡just)
      in
      begin
        map f (dropFrame ctx)
      ≡⟨ refl ⟩
        map f (maybe proj₁ ctx (popNodeMap (LoopStack.popNode (Ctx.loopStack ctx))))
      ≡⟨ cong (λ x → map f (maybe proj₁ ctx (popNodeMap x))) popNode≡just ⟩
        dropFrame (map f
                        ( record { loopStack = LoopStack.pushNode node' tail
                                 ; variables = Ctx.variables ctx
                                 ; procedures = Ctx.procedures ctx})
                        )
      ≡⟨ cong (λ x → dropFrame (map f (rec x))) (sym loopStackctx≡pushNode) ⟩
        dropFrame (map f
                        ( record { loopStack = Ctx.loopStack ctx
                                 ; variables = Ctx.variables ctx
                                 ; procedures = Ctx.procedures ctx})
                        )
      ≡⟨ refl ⟩
        dropFrame (map f ctx)
      ∎
      where
        rec : LoopStack a → Ctx a
        rec ls = ( record { loopStack = ls
                       ; variables = Ctx.variables ctx
                       ; procedures = (Ctx.procedures ctx)})
        popNodeMap = Data.Maybe.map (λ p → ( record ctx
                            { loopStack = proj₁ p
                            }
                          , proj₂ p
                          ))
    mapSeq-composes :
      ∀ {A B C }
      → {g : List A → List B}
      → {f : List B → List C}
      → (ctx : Ctx A)
      → (mapSeq f ∘ mapSeq g) ctx ≡ mapSeq (f ∘ g) ctx
    mapSeq-composes {_} {_} {_} {g} {f} ctx =
       Eq.cong₂ (λ z₁ z₂ →
                  record
                  { procedures = z₁
                  ; variables = Ctx.variables ctx
                  ; loopStack = z₂
                  })
                 proc-map-compose
                 (LoopStack.Properties.mapSeq-composes _)
      where
        proc-map-compose =
          begin
            Map'.map
            (λ { (args , locals , body) →
            ( args
            , locals
            , f body
            )})
          (Map'.map
            (λ { (args , locals , body) →
            ( args
            , locals
            , g body
            )})
            (Ctx.procedures ctx))
          ≡⟨ Map'.map∘map ⟩
           (Map'.map
            (λ { (args , locals , body) →
            ( args
            , locals
            , (f ∘ g) body
            )})
            (Ctx.procedures ctx))
          ∎

    mapSeq[id]≡id :
      {A : Set}
      → {f : List A → List A}
      → ((as : List A) → (f as ≡ as))
      → (ctx : Ctx A)
      → mapSeq f ctx ≡ ctx
    mapSeq[id]≡id  {A} {f} id-proof ctx =
      begin
        mapSeq f ctx
      ≡⟨⟩
        record
        { loopStack =
          LoopStack.mapSeq f (Ctx.loopStack ctx)
        ; variables = Ctx.variables ctx
        ; procedures =
          Map'.map (λ { (args , locals , body) →
                     (args , locals , f body)
                   })  (Ctx.procedures ctx)
        }
      ≡⟨ Eq.cong₂ (λ z₁ z₂ →
                     record
                     { loopStack = z₁
                     ; variables = Ctx.variables ctx
                     ; procedures = z₂
                     })
                     (LoopStack.Properties.mapSeq[id]≡id
                        {f = f} id-proof (Ctx.loopStack ctx))
                     (Map'.map-id proc-inner-is-id)

        ⟩
        ctx
      ∎
      where
        map-f : {T₁ T₂ : Set} → (T₁ × T₂ × List A) → (T₁ × T₂ × List A)
        map-f = (λ { (args , locals , body) →
                ( args
                , locals
                ,  f body
                )})
        proc-inner-is-id :
          ∀ {T₁ T₂}
          → (p : (T₁ × T₂ × List A))
          → map-f p
           ≡ p
        proc-inner-is-id p@( args , locals , body) =
          begin
            map-f
            p
          ≡⟨⟩
            (args , locals , f body)
          ≡⟨ cong (λ z → (args , locals , z)) (id-proof body) ⟩
            p
          ∎


    mapSeq-map :
      ∀ {A B C}
      → {f : A → B}
      → {g : List B → List C}
      → (ctx : Ctx A)
      → (mapSeq g ∘ map f) ctx
      ≡ mapSeq (g ∘ Data.List.map f) ctx
    mapSeq-map {f = f} {g = g} ctx =
      Eq.cong₂ (λ z₁ z₂ →
                     record
                     { procedures = z₁
                     ; variables = Ctx.variables ctx
                     ; loopStack = z₂
                     })
                   Map'.map∘map
                   (LoopStack.Properties.mapSeq-map (Ctx.loopStack ctx))


    concatMap-id : {A B : Set}
                  → {from : A → B}
                  → {to : B -> List A}
                  → ((a : A) → ((to (from a)) ≡ [ a ]))
                  → (ctx : Ctx A)
                  → concatMap to (map from ctx) ≡ ctx
    concatMap-id {A} {B} {from} {to} id-ish ctx =
      begin
        concatMap to (map from ctx)
      ≡⟨⟩
        record
        { procedures = _
        ; variables = Ctx.variables ctx
        ; loopStack = LoopStack.concatMap to (Ctx.loopStack (map from ctx))
        }
      ≡⟨ cong (λ z → record
                     { procedures = _
                     ; variables = Ctx.variables ctx
                     ; loopStack = z
                     })
               (LoopStack.Properties.concatMap-id id-ish (Ctx.loopStack ctx))
        ⟩
         record
         { procedures = Map'.map
            (λ proc-info →
              ( proj₁ proc-info
              , proj₁ (proj₂ proc-info)
              , Data.List.concat (Data.List.map to (proj₂ (proj₂ proc-info)))
              ))
              (Map'.map
                 (λ procInfo →
                    proj₁ procInfo ,
                    proj₁ (proj₂ procInfo) ,
                    Data.List.map from (proj₂ (proj₂ procInfo)))
                 (Ctx.procedures ctx))
         ; variables = Ctx.variables ctx
         ; loopStack = (Ctx.loopStack ctx)
         }
      ≡⟨ cong (λ z → record
                     { procedures = z
                     ; variables = _
                     ; loopStack = _
                     })
               Map'.map∘map
        ⟩
        record
         { procedures = Map'.map
            (λ { (params , locals , body) →
              ( params
              , locals
              , Data.List.concat (Data.List.map to (Data.List.map from body))
              )})
              (Ctx.procedures ctx)
         ; variables = Ctx.variables ctx
         ; loopStack = (Ctx.loopStack ctx)
         }
      ≡⟨ cong (λ z →
                 record
                 { procedures = z
                 ; variables = Ctx.variables ctx
                 ; loopStack = (Ctx.loopStack ctx)
                 })
                 (Map'.map-id (proc-map≡id))
        ⟩
        ctx
      ∎
      where
        proc-map≡id : {T₁ T₂ : Set}
                     → (p : T₁ × T₂ × List A)
                     → ((λ p →
                            ( (proj₁ p)
                            , (proj₁ (proj₂ p)
                            , Data.List.concat (Data.List.map to (Data.List.map from (proj₂ (proj₂ p))))
                            ))) p ≡ p)
        proc-map≡id p@(a , b , body) =
          begin
            ( a
            , b
            , Data.List.concat (Data.List.map to (Data.List.map from body))
            )
          ≡⟨ cong (λ z → (_ , _ , z))
                   (Extra.List.concatMap-id id-ish body)
            ⟩
            p
          ∎


  mapLoopStackSeqs∘mapSeq-cancel :
    ∀ {A B}
    → {f : List B → List B}
    → {g : List A → List B}
    → (ctx : Ctx A)
    → ((seq : List A) → ((f ∘ g) seq ≡ g seq))
    → mapLoopStackSeqs f (mapSeq g ctx)
    ≡ mapSeq g ctx
  mapLoopStackSeqs∘mapSeq-cancel
    {f = f} {g = g} ctx g-cancels-f =
    begin
       mapLoopStackSeqs f (mapSeq g ctx)
    ≡⟨⟩
      record
      { loopStack =
        LoopStack.mapSeq f (LoopStack.mapSeq g (Ctx.loopStack ctx))
      ; variables = Ctx.variables ctx
      ; procedures = proc-map
      }
    ≡⟨ cong (λ z →
               record
               { loopStack =
                           z
               ; variables = Ctx.variables ctx
               ; procedures = proc-map
               }
             )
             (LoopStack.Properties.mapSeq-composes (Ctx.loopStack ctx))
      ⟩
      record
      { loopStack =
        LoopStack.mapSeq (f ∘ g) (Ctx.loopStack ctx)
      ; variables = Ctx.variables ctx
      ; procedures = proc-map

      }
    ≡⟨ cong (λ z →
               record
               { loopStack =
                           z
               ; variables = Ctx.variables ctx
               ; procedures = proc-map
               }
             )
             (LoopStack.Properties.mapSeq-cancel
               {f = f } {g = g}
               (Ctx.loopStack ctx)
               g-cancels-f)
      ⟩
      mapSeq g ctx
    ∎
   where
     proc-map =  Map'.map (λ { (args , locals , body) →
                     (args , locals , g body)
                   })
                (Ctx.procedures ctx)
