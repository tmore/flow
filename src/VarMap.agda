module VarMap where

  𝕍 : Set
  𝕍 = ℕ

  VarMap : Set → Set
  VarMap a = List (𝕍 × a)

  empty : ∀ {a} → VarMap a
  empty = []

  lookup : ∀ {a} → 𝕍 → VarMap a → Maybe a
  lookup v [] = nothing
  lookup v ((v' , val) ∷ xs) =
    if 𝕍.equals v v' then just val else lookup v xs

  _∈_ : ∀ {a} → 𝕍 → VarMap a → Bool
  _∈_ k [] = false
  _∈_ k ((v , _) ∷ rest) =
    if 𝕍.equals k v
    then true
    else k ∈ rest

  set : ∀ {a} → 𝕍 → a → VarMap a → VarMap a
  set v x [] = [ (v , x) ]
  set v x ((v' , x') ∷ xs) =
    if 𝕍.equals v v'
    then (v , x) ∷ xs
    else (v' , x') ∷ set v x xs

  equals : ∀ {a} → (a → a → Bool) → VarMap a → VarMap a → Bool
  equals _ [] [] = true
  equals _ [] (_ ∷ _) = false
  equals eqF ((var , val) ∷ xs) mapB =
    maybe (λ val2 → equals eqF xs mapB) false (lookup var mapB)

  v∈[]≡false : ∀ {a} → (v : 𝕍) → ( vm : VarMap a) → vm ≡ [] → v ∈ vm ≡ false
  v∈[]≡false v [] refl = refl

  v∈[]≡false⇒v∈[]≢true : ∀ {a} → (v : 𝕍) → ( vm : VarMap a) → vm ≡ [] → ¬ (v ∈ vm ≡ true)
  v∈[]≡false⇒v∈[]≢true v []  = λ _ ()

  v∈[]≡true⇒⊥ : ∀ {a} → (v : 𝕍) → (vm : VarMap a) → vm ≡ [] → (v ∈ vm ≡ true) → ⊥
  v∈[]≡true⇒⊥ v [] refl ()

  v∈vm-subst : ∀ {a b v}
            → (vm : VarMap a)
            → (vm' : VarMap a)
            → vm ≡ vm'
            → (v ∈ vm ≡ b)
            → (v ∈ vm' ≡ b)
  v∈vm-subst vm vm' refl refl = refl

  -- notHeadthenTail : ∀ {A v' xs } → {a : A} → (v : 𝕍) → (vm : VarMap A) → vm ≡ ( (v' , a) ∷ xs) → (v ∈ vm ≡ true) → (𝕍.equals v v' ∨ v ∈ xs ≡ true)
  -- notHeadthenTail _ [] ()
  -- notHeadthenTail v (e@(v' , _) ∷ []) isList isInList =
  --   if 𝕍.equals v v'
  --   then refl
  --   else cong (_∷_ e)

  NotFirstThenLater : ∀ {A v' k rest}
                    → (v : 𝕍)
                    → (vm : VarMap A)
                    → (vm ≡ ((v' , k) ∷ rest))
                    → (v ≢ v')
                    → (v ∈ vm ≡ true)
                    → (v ∈ rest ≡ true)
  NotFirstThenLater v [] ()
  NotFirstThenLater v vm@((v' , _) ∷ rest) refl nEq InMap =
    true≡v∈rest
   where
    v∈vm≡v∈rest : v ∈ vm ≡ v ∈ rest
    v∈vm≡v∈rest =
      begin
        (v ∈ vm)
      ≡⟨ refl ⟩
        (if 𝕍.equals v v' then true else v ∈ rest)
      ≡⟨ if-guard-subst (𝕍.equalsxy≡false v v' nEq) refl ⟩
        (v ∈ rest)
      ∎
    true≡v∈rest : v ∈ rest ≡ true
    true≡v∈rest = trans (sym v∈vm≡v∈rest) InMap

  EitherFirstOrLater : ∀ {A k rest }
                      → (v : 𝕍)
                      → (v' : 𝕍)
                      → (vm : VarMap A)
                      → (vm ≡ ((v' , k) ∷ rest))
                      → (v ∈ vm ≡ true)
                      → ((𝕍.equals v v') ≡ true) ⊎ (v ∈ rest ≡ true)
  EitherFirstOrLater v v' [] () --
  EitherFirstOrLater v v' vm@((vo , _) ∷ rest) NonEmpty InSet with 𝕍.equalsxy≡true∨false v v'
  ... | (inj₁ eq)  = inj₁ eq
  ... | (inj₂ nEq) = inj₂ (NotFirstThenLater v vm NonEmpty (𝕍.equalsxy≡false⇒x≢y v v' nEq) InSet)



  Set1⇒Get1 : (v : 𝕍)
            → (vm : VarMap ℕ)
            → (vm ≡ [])
            → ((lookup v (set v 1 vm)) ≡ just 1)
  Set1⇒Get1 v [] refl =
    begin
      (lookup v (set v 1 []))
    ≡⟨ refl  ⟩
      (if (𝕍.equals v v) then (just 1) else nothing)
    ≡⟨ if-guard-subst (𝕍.equalsxx≡true v) refl  ⟩
      (if true then (just 1) else nothing)
    ≡⟨ refl ⟩
      (just 1)
    ∎

  -- subst-snd : ∀ {A B : Set} {a : A} {b1 b2 : B} {P : A → B → Set}
  --   → b1 ≡ b2
  --   → P a b1
  --   → P a b2
  -- subst-snd P refl

  Set⇒Get : ∀ {A}
          → (v : 𝕍)
          → (a : A)
          → (vm : VarMap A)
          → ((lookup v (set v a vm)) ≡ just a)
  Set⇒Get v a vm@[] =
    begin
      (lookup v (set v a vm))
    ≡⟨ refl ⟩
      (lookup v [(v , a)])
    ≡⟨ refl ⟩
      (if (𝕍.equals v v) then (just a) else nothing)
    ≡⟨ if-guard-subst (𝕍.equalsxx≡true v) refl ⟩
      just a
    ∎

  Set⇒Get v a vm@((v' , a') ∷ rest)  with (Data.Nat._≟_ v v')
  ... | (true because ofʸ v≡v') =
              begin
                (lookup v (set v a vm))
              ≡⟨ lookup-subst SetEval refl ⟩
                (lookup v ((v , a) ∷ rest))
              ≡⟨ refl ⟩
                (if 𝕍.equals v v then just a else lookup v rest)
              ≡⟨ if-guard-subst (𝕍.equalsxx≡true v) refl ⟩
                just a
              ∎
    where
      SetEval : set v a vm ≡ ((v , a) ∷ rest)
      SetEval = begin
                  set v a vm
                ≡⟨ refl ⟩
                  (set v a ((v' , a') ∷ rest))
                ≡⟨ refl ⟩
                  (if 𝕍.equals v v' then (v , a) ∷ rest else ((v' , a') ∷ set v a rest))
                ≡⟨ (if-guard-subst (𝕍.≡⇒equals v v' v≡v') refl) ⟩
                  (v , a) ∷ rest
                ∎
      lookup-subst : ∀ {v a b}
                   → a ≡ b
                   → (lookup v a) ≡ (lookup v a)
                   → (lookup v a) ≡ (lookup v b)
      lookup-subst refl p = refl
  ... | (false because ofⁿ v≢v') =
               begin
                 (lookup v (set v a vm))
               ≡⟨ lookup-subst SetEval refl ⟩
                 (lookup v ((v' , a') ∷ (set v a rest)))
               ≡⟨ if-guard-subst (𝕍.equalsxy≡false v v' v≢v') refl ⟩
                 (lookup v (set v a rest))
               ≡⟨ Set⇒Get v a rest ⟩
                 just a
               ∎
      where
        SetEval : set v a vm ≡ ((v' , a') ∷ (set v a rest))
        SetEval = begin
                  set v a vm
                ≡⟨ refl ⟩
                  (set v a ((v' , a') ∷ rest))
                ≡⟨ refl ⟩
                  (if 𝕍.equals v v' then (v , a) ∷ rest else ((v' , a') ∷ set v a rest))
                ≡⟨ (if-guard-subst (𝕍.equalsxy≡false v v' v≢v') refl) ⟩
                  (v' , a') ∷ (set v a rest)
                ∎
        lookup-subst : ∀ {v a b}
                   → a ≡ b
                   → (lookup v a) ≡ (lookup v a)
                   → (lookup v a) ≡ (lookup v b)
        lookup-subst refl p = refl
